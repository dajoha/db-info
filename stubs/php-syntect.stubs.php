<?php

// Stubs for php-syntect

namespace Syntect {
    class Syntect {
        /**
         * Highlights a string for the given syntax.
         */
        public static function highlightHtml(string $text, \Syntect\Syntax $syntax, ?\Syntect\Theme $theme = null): string {}

        /**
         * Checks if syntax definition exists, from file extension or syntax name.
         *
         * @param string $name File extension or syntax name
         */
        public static function hasSyntax(string $name): bool {}

        /**
         * Finds a syntax definition, from file extension or syntax name.
         *
         * @param string $name File extension or syntax name
         */
        public static function getSyntax(string $name): ?\Syntect\Syntax {}

        /**
         * List all syntax definitions.
         *
         * @return Syntax[]
         */
        public static function listSyntaxes(): array {}

        /**
         * Gets a theme by name.
         *
         * Currently, available themes are the following:
         *
         *  - "base16-eighties.dark"
         *  - "base16-mocha.dark"
         *  - "base16-ocean.dark"
         *  - "base16-ocean.light"
         *  - "InspiredGitHub"
         *  - "Solarized (dark)"
         *  - "Solarized (light)"
         */
        public static function getTheme(string $name): ?\Syntect\Theme {}

        /**
         * List all available themes.
         *
         * @return Theme[]
         */
        public static function listThemes(): array {}
    }

    class Syntax {
        public string $name;

        /** @var string[] */
        public array $extensions;
    }

    class Theme {
        public string $name;

        public ?string $author;
    }
}
