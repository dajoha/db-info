<?php

use App\ErrorHandler\AppErrorHandler;
use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {
    AppErrorHandler::register();

    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};
