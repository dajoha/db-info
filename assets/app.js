/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';
import './domain/markdown/styles/index.scss';

import '@fortawesome/fontawesome-free/css/fontawesome.min.css';
import '@fortawesome/fontawesome-free/css/regular.min.css';
import '@fortawesome/fontawesome-free/css/solid.min.css';

import './js/actions/copy-value';
import './js/actions/copy-column';
import './js/behaviours/is-sticky-class';
import './js/behaviours/highlight-table-rows';
import './js/components/data-fetcher';
import './js/components/dropdown';
import './js/components/filter-editor';
import './js/components/foldable-zone';
import './js/components/modal';
import './js/components/invalidate-cache-command';
import './js/components/live-filters/list-filter';
import './js/components/live-filters/table-filter';
import './js/components/quick-find-zone';
import './js/general/text-input';
import './js/global-shortcuts';
