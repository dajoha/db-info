function initTextInputs() {
    /** @type {NodeListOf<HTMLElement>} */
    const textInputs = document.querySelectorAll('input[type=text]');
    for (const textInput of textInputs) {
        textInput.addEventListener('keydown', event => {
            if (event.ctrlKey || event.altKey) {
                return;
            }
            if (event.key === 'Escape') {
                event.target.blur();
            }
        });
    }
}

initTextInputs();
