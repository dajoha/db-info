/**
 * @param {HTMLElement} button
 * @param {Promise} promise
 */
export function setButtonLoadingUntilFinished(button, promise) {
    if (!button.classList.contains('command')) {
        button = button.querySelector('.command');
    }
    if (button === null) {
        return;
    }

    button.classList.add('loading');

    promise.finally(() => {
        button.classList.remove('loading');
    });
}
