/**
 * @typedef {{
 *     success: bool,
 *     error_message: string,
 *     class: string,
 *     stack_trace: string[],
 * }} ErrorMessage
 */
