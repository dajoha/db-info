import {getOffset, getViewPortSize, onClickOutside} from "../util";

const CLASS_TARGET_IS_OPEN = 'dropdown--target-is-open';
const ATTR_TARGET = 'data-dropdown--target';
const ATTR_ANCHOR_POS = 'data-dropdown--anchor-position';

class Dropdown {
    /**
     * @param {HTMLElement} triggerElement
     */
    constructor(triggerElement) {
        this.triggerElement = triggerElement;
        this.dropdownId = triggerElement.getAttribute(ATTR_TARGET);
        this.dropdown = document.querySelector(`#${this.dropdownId}`);
        this.isOpen = false;
        /** @type {"left"|"right"} */
        this.preferredAnchorPos = this.triggerElement.getAttribute(ATTR_ANCHOR_POS) ?? 'left';
        this.onResize = this.setDropdownPosition.bind(this);

        this.triggerElement.addEventListener('click', () => {
            this.open();
        });
    }

    open() {
        if (this.isOpen) {
            return;
        }

        window.addEventListener('resize', this.onResize);

        this.triggerElement.classList.add(CLASS_TARGET_IS_OPEN);

        this.dropdown.style.display = 'block';
        this.setDropdownPosition();
        this.isOpen = true;

        setTimeout(() => {
            onClickOutside(this.dropdown, () => this.close());
        }, 0);
    }

    setDropdownPosition() {
        this.dropdown.style.top = null;
        this.dropdown.style.right = null;
        this.dropdown.style.bottom = null;
        this.dropdown.style.left = null;

        const trigger = getOffset(this.triggerElement);
        const dropdown = this.dropdown.getBoundingClientRect();
        const viewport = getViewPortSize();

        this.dropdown.style.top = `${trigger.bottom + 10}px`;

        const dropdownAnchorLeft = trigger.left;
        const dropdownAnchorRight = trigger.right - dropdown.width;

        let dropdownLeft;
        if (this.preferredAnchorPos === 'left') {
            dropdownLeft = dropdownAnchorLeft;
        } else {
            dropdownLeft = dropdownAnchorRight;
        }

        // Correct horizontal pos on overflow:
        if (dropdownLeft < 0) {
            dropdownLeft = dropdownAnchorLeft;
        } else if (dropdownLeft + dropdown.width >= viewport.width) {
            dropdownLeft = dropdownAnchorRight;
        }

        this.dropdown.style.left = `${dropdownLeft}px`;
    }

    close() {
        window.removeEventListener('resize', this.onResize);

        this.triggerElement.classList.remove(CLASS_TARGET_IS_OPEN);

        this.dropdown.style.display = 'none';
        setTimeout(() => {
            this.isOpen = false;
        }, 200);
    }
}

function init() {
    const triggerElements = document.querySelectorAll(`[${ATTR_TARGET}]`);
    for (const triggerElement of triggerElements) {
        new Dropdown(triggerElement);
    }
}

init();
