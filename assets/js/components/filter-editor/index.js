import { EditorView } from 'codemirror';
import { drawSelection, dropCursor, crosshairCursor, keymap } from '@codemirror/view';
import { EditorState } from '@codemirror/state';
import { bracketMatching } from '@codemirror/language';
import { history, defaultKeymap, historyKeymap } from '@codemirror/commands';
import { closeBrackets, autocompletion, closeBracketsKeymap, completionKeymap, CompletionContext } from '@codemirror/autocomplete';
import {KeyBinding} from '@codemirror/view';

import { getDefaultTheme } from '../../vendor-specific/codemirror/theme';
import specificTheme from './theme';
import {EVENT_OPEN, toggleFoldableZone} from '../foldable-zone';
import appApiClient from '../../service/app-api-client';
import {registerSearchShortcutAction} from "../../global-shortcuts";
import {appKeymap} from "../../vendor-specific/codemirror";

/**
 * @typedef {CustomEvent<{ searchExpression: string }>} FilterEditorSubmitEvent
 */

export const EVENT_SUBMIT = 'filter-editor:submit';

export const ACTION_SUBMIT = 'submit';
export const ACTION_CLEAR = 'clear';

class FilterEditor {
    /**
     * @param {HTMLElement} container
     */
    constructor(container) {
        this.container = container;
        this.editorId = container.getAttribute('data-filter-editor');
        this.tableName = container.getAttribute('data-filter-editor--table-name');
        this.outerFoldableZoneId = container.getAttribute('data-filter-editor--outer-foldable-zone');
        this.allowKeyShorcut = container.getAttribute('data-filter-editor--search-key-shortcut') === '1';
        /** @type {HTMLElement|null} */
        this.outerFoldableZone = null;

        this.setupEditor();
        this.setupActionButtons();
        this.setupAutoFocus();
        this.setupSearchShortcut();

        this.container.classList.remove('filter-editor--height-before-js');
    }

    setupEditor() {
        /** @type {KeyBinding[]} appKeymap */
        const customKeymap = [
            {
                key: 'Enter',
                run: _ => {
                    this.submit();
                    return true;
                },
            }, {
                key: 'Escape',
                run: editorView => {
                    editorView.contentDOM.blur();
                    if (this.editorView.contentDOM.innerText.trim() === '') {
                        this.toggleOuterFoldableZone(false);
                    }
                    return true;
                },
            },
        ];

        const initialContent = this.container.getAttribute('data-filter-editor--initial-content');

        this.editorView = new EditorView({
            doc: initialContent,
            extensions: [
                history(),
                drawSelection(),
                dropCursor(),
                EditorState.allowMultipleSelections.of(true),
                bracketMatching(),
                closeBrackets(),
                autocompletion({
                    override: [this.autoComplete.bind(this)],
                    defaultKeymap: false,
                }),
                crosshairCursor(),
                keymap.of([
                    ...completionKeymap,
                    ...customKeymap,
                    ...appKeymap(),
                    ...closeBracketsKeymap,
                    ...defaultKeymap,
                    ...historyKeymap,
                ]),
                getDefaultTheme(),
                specificTheme,
            ],
            parent: this.container,
        });

        // Dev: focus editor, prefill some text, and open completion tooltip:
        // const state = this.editorView.state;
        // const update = state.update({changes: {from: 0, to: state.doc.length, insert: 'st'}})
        // this.editorView.update([update]);
        // this.editorView.focus();
        // this.editorView.dispatch({selection: {anchor: 2}});
        // startCompletion(this.editorView);
    }

    setupActionButtons() {
        const buttons = document.querySelectorAll(
            `[data-filter-editor--target-id="${this.editorId}"]`
        );

        for (const button of buttons) {
            const action = button.getAttribute('data-filter-editor--action');
            button.addEventListener('click', _ => {
                switch (action) {
                    case ACTION_SUBMIT:
                        this.submit();
                        break;
                    case ACTION_CLEAR:
                        this.clear();
                        break;
                }
            });
        }
    }

    /**
     * Activate focus when the outer foldable zone opens.
     */
    setupAutoFocus() {
        if (!this.outerFoldableZoneId) {
            return;
        }
        this.outerFoldableZone = document.getElementById(this.outerFoldableZoneId);
        this.outerFoldableZone.addEventListener(EVENT_OPEN, _ => {
            this.editorView.focus();
        });
    }

    setupSearchShortcut() {
        if (!this.allowKeyShorcut) {
            return;
        }

        registerSearchShortcutAction(() => {
            this.toggleOuterFoldableZone(true);
            this.editorView.focus();
        });
    }

    /**
     * @param {boolean=} value
     */
    toggleOuterFoldableZone(value) {
        if (this.outerFoldableZone !== null) {
            toggleFoldableZone(this.outerFoldableZone, value);
        }
    }

    /**
     * Sends an event which indicates that the search has to be submitted.
     */
    submit() {
        const searchExpression = this.editorView.contentDOM.innerText;
        /** @type {FilterEditorSubmitEvent} */
        const event = new CustomEvent(EVENT_SUBMIT, {
            detail: { searchExpression },
        });
        this.container.dispatchEvent(event);
    }

    /**
     * Clears the search expression, then submit the search in order to update the view.
     */
    clear() {
        const state = this.editorView.state;
        const update = state.update({changes: {from: 0, to: state.doc.length, insert: ''}})
        this.editorView.update([update]);
        this.submit();
    }

    /**
     * @param {CompletionContext} context
     * @return {Promise}
     */
    autoComplete(context) {
        return appApiClient.getTableFilterCompletions({
            input: context.state.doc.toString(),
            cursor: context.pos,
            tableName: this.tableName,
        })
            .then(data => {
                if (data === null) {
                    return {
                        from: context.pos,
                        options: [],
                    }
                }

                const options = data.completions.map(
                    /** @param {Object} completionInfo */
                    completionInfo => {
                        let info = null;
                        let htmlInfo = completionInfo.info;
                        if (htmlInfo) {
                            info = _ => {
                                const element = document.createElement('span');
                                element.innerHTML = htmlInfo;
                                return element;
                            }
                        }
                        return {
                            ...completionInfo,
                            ...{info},
                        };
                    }
                )

                return {
                    from: data.completeFrom,
                    options,
                };
            });
    }
}

init();

function init() {
    const containers = document.querySelectorAll('[data-filter-editor]');

    for (const container of containers) {
        new FilterEditor(container);
    }
}
