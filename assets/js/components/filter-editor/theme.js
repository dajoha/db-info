import { EditorView } from 'codemirror';

export default EditorView.theme({
    '.cm-completionIcon': {
        width: '2.5em',
    },
});
