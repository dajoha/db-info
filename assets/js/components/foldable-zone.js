import { getCssVariable } from '../util';

export const EVENT_OPEN = 'foldable-zone:open';

init();

function init() {
    const foldableZones = document.querySelectorAll('.foldable-zone');

    for (const foldableZone of foldableZones) {
        setupFoldableZone(foldableZone);
    }
}

/**
 * @param {HTMLElement} foldableZone
 */
function setupFoldableZone(foldableZone) {
    const checkbox = getFoldableZoneCheckBox(foldableZone);
    const main = foldableZone.querySelector(':scope > .foldable-zone__main');
    let transitionTime = getCssVariable('--transition-time-ms', foldableZone);
    transitionTime = parseInt(transitionTime);

    checkbox.checked = foldableZone.classList.contains('opened');

    setTimeout(_ => {
        foldableZone.classList.remove('foldable-zone--initial-transition');
    }, 10);

    checkbox.addEventListener('change', _ => {
        const checked = checkbox.checked;

        foldableZone.style.setProperty('--height', `${main.scrollHeight}px`);
        main.classList.remove('no-max-height');

        setTimeout(_ => {
            foldableZone.classList.toggle('opened', checked);
            setTimeout(_ => {
                if (checked) {
                    main.classList.add('no-max-height');
                    /** @var {Event} event */
                    const event = new Event(EVENT_OPEN);
                    foldableZone.dispatchEvent(event);
                }
            }, transitionTime);
        }, 30);
    })
}

/**
 * @param {HTMLElement} foldableZone
 * @param {boolean=} value
 */
export function toggleFoldableZone(foldableZone, value) {
    const checkbox = getFoldableZoneCheckBox(foldableZone);

    if (value === undefined) {
        checkbox.checked = !checkbox.checked;
    } else if (value === true) {
        checkbox.checked = true;
    } else if (value === false) {
        checkbox.checked = false;
    }

    checkbox.dispatchEvent(/** @type Event */ new Event('change'));
}

/**
 * @param {HTMLElement} foldableZone
 *
 * @return HTMLInputElement
 */
function getFoldableZoneCheckBox(foldableZone) {
    return foldableZone.querySelector(':scope > .foldable-zone__toggle');
}
