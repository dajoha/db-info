/**
 * @typedef {{
 *     headerMessage: string=,
 *     errorMessage: string=,
 * }} ErrorModalOptions
 */

import {getModal} from "./index";

const defaultHeaderMessage = 'Oops, an error has occurred:';

/**
 * @param {ErrorModalOptions} options
 */
export function showErrorModal(options) {
    const modal = getModal('error-modal');

    const headerMessageContainer = modal.root.querySelector('.error-modal__header-message');
    headerMessageContainer.innerText = options.headerMessage ?? defaultHeaderMessage;

    const messageContainer = modal.root.querySelector('.error-modal__error-message');
    messageContainer.innerText = options.errorMessage ?? '';

    modal.show();
}
