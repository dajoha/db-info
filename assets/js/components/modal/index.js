import {lockGlobalShortcuts, unlockGlobalShortcuts} from "../../global-shortcuts";

class Modal {
    /**
     * @param {HTMLElement} root
     */
    constructor(root) {
        this.root = root;
        this.overlay = root.querySelector('.modal__overlay');

        this.overlay.addEventListener('click', () => {
            this.hide();
        })

        const closeButtons = this.root.querySelectorAll('.modal-command--close');
        for (const closeButton of closeButtons) {
            closeButton.addEventListener('click', () => {
                this.hide();
            })
        }

        this.keyListener = (ev) => {
            if (ev.key === 'Escape') {
                this.hide();
            }
        };
    }

    id() {
        return this.root.id;
    }

    show() {
        this.root.style.display = 'block';
        document.addEventListener('keydown', this.keyListener);
        lockGlobalShortcuts();
    }

    hide() {
        this.root.style.display = 'none';
        document.removeEventListener('keydown', this.keyListener);
        unlockGlobalShortcuts();
    }
}

/** @type {Object<string, Modal>} */
const modals = {};

init();

function init() {
    const modalRoots = document.querySelectorAll('.modal');

    for (const modalRoot of modalRoots) {
        const modal = new Modal(modalRoot);
        modals[modal.id()] = modal;
    }

    const openButtons = document.querySelectorAll('[data-modal--open]');
    for (const openButton of openButtons) {
        openButton.addEventListener('click', () => {
            const modalId = openButton.getAttribute('data-modal--open');
            const modal = modals[modalId];
            if (modal) {
                modal.show();
            }
        });
    }
}

/**
 * @param {string} id
 * @return {Modal|undefined}
 */
export function getModal(id) {
    return modals[id];
}
