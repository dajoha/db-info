import { debounce } from '../../util';
import { buildRegexp } from './util/live-filter-util';

const DEBOUNCE_TIME = 200;

class ListFilter {
    /**
     * @param {HTMLInputElement} baseInput
     */
    constructor(baseInput) {
        this.baseInput = baseInput;

        const targetListId = baseInput.getAttribute('data-list-filter--list');
        /** @type {HTMLTableElement} */
        this.targetList = document.getElementById(targetListId);

        this.baseInput.addEventListener(
            'input',
            debounce(this.updateFilter.bind(this), DEBOUNCE_TIME),
        );
    }

    updateFilter() {
        const elements = this.targetList.children;

        const regex = buildRegexp(this.baseInput.value);

        for (const element of elements) {
            const text = element.getAttribute('data-list-filter--text');
            if (text === null) {
                continue;
            }
            const visibleRow = text.match(regex);
            element.classList.toggle('list-filter--hidden', !visibleRow);
        }
    }
}

init();

function init() {
    const inputs = document.querySelectorAll('[data-list-filter]');

    for (const input of inputs) {
        new ListFilter(input);
    }
}
