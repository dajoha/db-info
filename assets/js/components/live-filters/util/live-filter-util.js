import {escapeRegExp} from '../../../util';

/**
 * @param {string} inputValue
 *
 * @return {RegExp}
 */
export function buildRegexp(inputValue) {
    const regexp = inputValue
        .trim()
        .split(/\s+/)
        .map(escapeRegExp)
        .join('.*')
    ;

    return new RegExp(regexp, 'i');
}
