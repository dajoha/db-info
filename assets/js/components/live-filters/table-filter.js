import { debounce } from '../../util';
import { highlightTablesRows } from '../../behaviours/highlight-table-rows';
import { buildRegexp } from './util/live-filter-util';

const DEBOUNCE_TIME = 200;

class TableFilter {
    /**
     * @param {HTMLInputElement} baseInput
     */
    constructor(baseInput) {
        this.baseInput = baseInput;

        const targetTableId = baseInput.getAttribute('data-table-filter--table');
        /** @type {HTMLTableElement} */
        this.targetTable = document.getElementById(targetTableId);

        // When the user goes back in the web history, the text input can already be filled on page
        // load, so we update filter on init:
        this.updateFilter();

        this.baseInput.addEventListener(
            'input',
            debounce(this.updateFilter.bind(this), DEBOUNCE_TIME),
        );
    }

    updateFilter() {
        const rows = this.targetTable.querySelectorAll(
            `
                :scope > tr:not(.data-table-filter--skip),
                :scope > tbody > tr:not(.data-table-filter--skip)
            `
        );

        const regex = buildRegexp(this.baseInput.value);

        for (const row of rows) {
            const cells = row.querySelectorAll(
                ':scope > td:not(.data-table-filter--skip)'
            );
            const visibleRow = [...cells].some(cell => cell.innerText.match(regex))
            row.classList.toggle('table-filter--hidden', !visibleRow);
        }

        highlightTablesRows(this.targetTable);
    }
}

init();

function init() {
    const inputs = document.querySelectorAll('[data-table-filter]');

    for (const input of inputs) {
        new TableFilter(input);
    }
}
