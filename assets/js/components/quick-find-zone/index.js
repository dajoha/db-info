import { EditorView } from 'codemirror';
import { drawSelection, dropCursor, keymap, placeholder } from '@codemirror/view';
import { EditorState } from '@codemirror/state';
import { history, defaultKeymap, historyKeymap } from '@codemirror/commands';
import { autocompletion, closeBracketsKeymap, completionKeymap, CompletionContext } from '@codemirror/autocomplete';
import { KeyBinding } from '@codemirror/view';

import { createTheme } from '../../vendor-specific/codemirror/theme';
import specificTheme from './theme';
import appDataProvider from '../../service/app-data-provider';
import appApiClient from "../../service/app-api-client";
import {appKeymap} from "../../vendor-specific/codemirror";

/** @typedef {import('../../vendor-specific/codemirror/theme').ThemeOptions} ThemeOptions */

export const EVENT_FOCUS = 'quick-find-zone:focus';

/** @type {ThemeOptions} */
const themeOptions = {
    background: 'var(--main-bg-color)',
};

class QuickFindZone {
    /**
     * @param {HTMLFormElement} form
     */
    constructor(form) {
        this.form = form;
        /** @type {?HTMLElement} */
        this.tableRichInput = form.querySelector('.quick-find-zone__rich-table-input');
        this.basicTableInput = form.querySelector('.quick-find-zone__basic-table-input');

        this.setupTableRichInput();

        this.form.addEventListener('submit', this.beforeSubmit.bind(this));

        this.basicTableInput.addEventListener(EVENT_FOCUS, _ => {
            this.editorView.focus();
        });
    }

    beforeSubmit() {
        this.basicTableInput.value = this.editorView.state.doc.toString();
    }

    submit() {
        this.beforeSubmit();
        this.form.submit();
    }

    setupTableRichInput() {
        /** @type {KeyBinding[]} appKeymap */
        const customKeymap = [
            {
                key: 'Enter',
                run: _ => {
                    this.submit();
                    return true;
                },
            },
        ];

        /** @type {HTMLElement} */
        const placeholderElement = document.createElement('span');
        // TODO: actually coupled with text input placeholder style; use css variables:
        placeholderElement.style.fontSize = '0.8em';
        const tableName = appDataProvider.tableName;
        placeholderElement.innerHTML = 'Table' + (tableName ? ` (${tableName})` : '');

        this.editorView = new EditorView({
            extensions: [
                history(),
                drawSelection(),
                dropCursor(),
                placeholder(placeholderElement),
                // Forbid multiline:
                EditorState.transactionFilter.of(tr => tr.newDoc.lines > 1 ? [] : tr),
                autocompletion({
                    override: [this.autoCompleteTableAlias.bind(this)],
                    defaultKeymap: false,
                }),
                keymap.of([
                    ...completionKeymap,
                    ...customKeymap,
                    ...appKeymap(),
                    ...closeBracketsKeymap,
                    ...defaultKeymap,
                    ...historyKeymap,
                ]),
                createTheme(themeOptions),
                specificTheme,
            ],
            parent: this.tableRichInput,
        });

        // Once the codemirror input has been loaded, the basic text input must be hidden visually:
        this.basicTableInput.style.display = 'none';
    }

    /**
     * @param {CompletionContext} context
     */
    autoCompleteTableAlias(context) {
        let word = context.matchBefore(/[a-zA-Z0-9_]*/);

        return appApiClient.getTableAliasCompletions()
        .then(data => {
            data = data.map(
                /** @param {Object} completionInfo */
                completionInfo => {
                    let info = null;
                    let htmlInfo = completionInfo.info;
                    if (htmlInfo) {
                        info = _ => {
                            const element = document.createElement('span');
                            element.innerHTML = htmlInfo;
                            return element;
                        }
                    }
                    return {
                        ...completionInfo,
                        ...{info},
                    };
                }
            )
            return {
                from: word.from,
                options: data,
            };
        });
    }
}

function init() {
    const quickFindZones = document.querySelectorAll('.quick-find-zone');

    for (const quickFindZone of quickFindZones) {
        new QuickFindZone(quickFindZone);
    }
}

init();
