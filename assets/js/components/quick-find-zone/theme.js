import { EditorView } from 'codemirror';

export default EditorView.theme({
    '.cm-scroller': {
        overflow: 'hidden',
    },
    '.cm-completionIcon': {
        width: '2.5em',
    },
});
