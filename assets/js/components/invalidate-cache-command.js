init();

function init() {
    const elements = document.querySelectorAll(
        '[data-cache-invalidate--url]'
    );

    for (const element of elements) {
        setupInvalidateCacheCommand(element);
    }
}

/**
 * @param {HTMLElement} element
 */
function setupInvalidateCacheCommand(element) {
    const url = element.getAttribute('data-cache-invalidate--url');

    element.addEventListener('click', async () => {
        await fetch(url, {
            'method': 'POST',
        });
        location.reload();
    });
}
