import { parseUrl} from '../../util';
import { EventEmitter } from '../../util/event-emitter';
import * as extensions from './extensions';
import {showErrorModal} from "../modal/error-modal";

export const EVENT_FETCH = 'fetch';

/**
 * @typedef {import('../../general/error').ErrorMessage} ErrorMessage
 */

class DataFetcher {
    /**
     * @param {HTMLElement} baseElement
     */
    constructor(baseElement) {
        /** @type {HTMLElement} */
        this.baseElement = baseElement;

        /** @type {string} */
        this.id = baseElement.attributes.getNamedItem('data-fetch-html--id').value;

        /** @type {string} */
        this.initialUrl = baseElement.attributes.getNamedItem('data-fetch-html--url')?.value;
        if (!this.initialUrl) {
            throw(`Fetch-html id "${this.id}" has no url, ignoring`);
        }
        /** @type {URL} */
        this.url = parseUrl(this.initialUrl);

        /** @type {?HTMLElement} */
        this.overlayContainer = null;
        const parent = this.baseElement.parentElement;
        if (parent.classList.contains('overlay-container')) {
            this.overlayContainer = parent;
        }

        this.reflectInUrl = this.baseElement.hasAttribute('data-fetch-html--reflect-in-url');

        this.eventEmitter = new EventEmitter();

        new extensions.RefetchButtonsExtension(this);
        new extensions.ReloadButtonsExtension(this);
        new extensions.FilterEditorExtension(this);
        new extensions.PaginationExtension(this);
        new extensions.StickyExtension(this);
        new extensions.HighlightTableRowsExtension(this);
        new extensions.CopyButtonsExtension(this);
        new extensions.TransposedCheckboxesExtension(this);
        new extensions.FirstRowUrlExtension(this);
    }

    /**
     * @param {string} event
     * @param {function} callback
     */
    subscribe(event, callback) {
        this.eventEmitter.subscribe(event, callback);
    }

    /**
     * @param {string} event
     * @param {array} args
     */
    dispatch(event, args = []) {
        this.eventEmitter.dispatch(event, args);
    }

    async fetch() {
        let timer = null;
        if (this.overlayContainer) {
            timer = setTimeout(_ => {
                this.overlayContainer.classList.add('overlay-container--loading');
            }, 300);
        }

        const response = await fetch(this.url, {
            'headers': {
                'Db-Info-Fetch': '1',
            }
        });

        if (response.ok) {
            const rawHtml = await response.text();
            this.updateDataView(rawHtml);

            this.dispatch(EVENT_FETCH, [{response}]);
        } else {
            let wasHandled = false;
            if (response.headers.get('Content-Type') === 'application/json') {
                /** @type {ErrorMessage} error */
                const error = await response.json();
                if (error.error_message) {
                    showErrorModal({ errorMessage: error.error_message });
                    wasHandled = true;
                }
            }

            if (!wasHandled) {
                showErrorModal({ headerMessage: 'An unhandled error has occurred.' });
            }
        }

        if (this.overlayContainer) {
            clearTimeout(timer);
            this.overlayContainer.classList.remove('overlay-container--loading');
        }
    }

    /**
     * @param {string} rawHtml
     */
    updateDataView(rawHtml) {
        this.baseElement.innerHTML = rawHtml;

        const showIfSomeDataElements = document.querySelectorAll(
            `[data-fetch-html--show-if-some-data="${this.id}"]`
        );
        const isVisible = rawHtml.trim() !== '';

        for (const showIfSomeDataElement of showIfSomeDataElements) {
            showIfSomeDataElement.classList.toggle('fetch-html--shown', isVisible);
        }
    }
}

init();

function init() {
    const dataFetcherElements = document.querySelectorAll('[data-fetch-html--id]');

    for (const dataFetcherElement of dataFetcherElements) {
        let dataFetcher;
        try {
            dataFetcher = new DataFetcher(dataFetcherElement);
        } catch (e) {
            console.warn(e);
            continue;
        }

        void dataFetcher.fetch();
    }
}
