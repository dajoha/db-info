export const URL_PARAM_SEARCH = 'search';
export const URL_PARAM_SORT_REGEX = /^sort\[\d+(?:-(?:asc|desc))?]$/i;
