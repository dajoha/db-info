// Handles pagination interface.

/**
 * A struct which holds pagination information, retrieved from an HTTP response's headers.
 *
 * @typedef {{
 *     nbRows: number,
 *     totalCount: number,
 *     pageNumber: number,
 *     pageSize: number,
 *     nbPages: number,
 * }} PaginationInfos
 */

import { EVENT_FETCH } from '../index';
import {setButtonLoadingUntilFinished} from "../../../general/loading-button";

const URL_PARAM_PAGE = 'page';

export class PaginationExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        /** @type {?PaginationInfos} */
        this.pagination = null;

        this.setupPaginationButtons();

        this.dataFetcher.subscribe(EVENT_FETCH, data => {
            /** @type {Response} response */
            const response = data.response;
            this.pagination = getPaginationInfosFromResponse(response);
            this.updatePaginationInfoView();
        });
    }

    fetch() {
        return this.dataFetcher.fetch();
    }

    setupPaginationButtons() {
        for (const previousButton of this.findFirstButtons()) {
            previousButton.addEventListener('click', _ => {
                if (this.changePageInUrl(_ => 1)) {
                    const promise = this.fetch();
                    setButtonLoadingUntilFinished(previousButton, promise);
                }
            })
        }

        for (const previousButton of this.findPreviousButtons()) {
            previousButton.addEventListener('click', _ => {
                if (this.changePageInUrl(page => page - 1)) {
                    const promise = this.fetch();
                    setButtonLoadingUntilFinished(previousButton, promise);
                }
            })
        }

        for (const nextButton of this.findNextButtons()) {
            nextButton.addEventListener('click', _ => {
                if (this.changePageInUrl(page => page + 1)) {
                    const promise = this.fetch();
                    setButtonLoadingUntilFinished(nextButton, promise);
                }
            })
        }

        for (const lastButton of this.findLastButtons()) {
            lastButton.addEventListener('click', _ => {
                if (this.changePageInUrl((_, pagination) => pagination.nbPages)) {
                    const promise = this.fetch();
                    setButtonLoadingUntilFinished(lastButton, promise);
                }
            })
        }
    }

    /**
     * @return NodeListOf<HTMLElement>
     */
    findFirstButtons() {
        return document.querySelectorAll(`[data-fetch-html--page-first="${this.dataFetcher.id}"]`);
    }

    /**
     * @return NodeListOf<HTMLElement>
     */
    findPreviousButtons() {
        return document.querySelectorAll(`[data-fetch-html--page-previous="${this.dataFetcher.id}"]`);
    }

    /**
     * @return NodeListOf<HTMLElement>
     */
    findNextButtons() {
        return document.querySelectorAll(`[data-fetch-html--page-next="${this.dataFetcher.id}"]`);
    }

    /**
     * @return NodeListOf<HTMLElement>
     */
    findLastButtons() {
        return document.querySelectorAll(`[data-fetch-html--page-last="${this.dataFetcher.id}"]`);
    }

    /**
     * Updates the pagination view from `this.pagination`.
     */
    updatePaginationInfoView() {
        // Update text info about "nb pages/total pages" :
        {
            const content = this.pagination ? `${this.pagination.pageNumber}/${this.pagination.nbPages}` : '';
            const paginationInfoElements = document.querySelectorAll(
                `[data-fetch-html--pagination-info="${this.dataFetcher.id}"]`
            );
            for (const paginationInfoElement of paginationInfoElements) {
                paginationInfoElement.textContent = content;
            }
        }

        // Update text info about number of results:
        {
            let content = '';
            if (this.pagination) {
                if (this.pagination.totalCount === 0) {
                    content = 'No result';
                } else if (this.pagination.totalCount === 1) {
                    content = '1 result';
                } else if (this.pagination.totalCount > 1) {
                    content = `${this.pagination.totalCount} results`;
                }
            }
            const resultsCountElements = document.querySelectorAll(
                `[data-fetch-html--results-count="${this.dataFetcher.id}"]`
            );
            for (const resultsCountElement of resultsCountElements) {
                resultsCountElement.textContent = content;
            }
        }

        // Update "disabled" attributes of pagination buttons:
        const canGoPreviousPage = this.pagination && this.pagination.pageNumber > 1;
        const canGoNextPage = this.pagination && this.pagination.pageNumber < this.pagination.nbPages;
        for (const firstButton of this.findFirstButtons()) {
            firstButton.toggleAttribute('disabled', !canGoPreviousPage);
        }
        for (const previousButton of this.findPreviousButtons()) {
            previousButton.toggleAttribute('disabled', !canGoPreviousPage);
        }
        for (const nextButton of this.findNextButtons()) {
            nextButton.toggleAttribute('disabled', !canGoNextPage);
        }
        for (const lastButton of this.findLastButtons()) {
            lastButton.toggleAttribute('disabled', !canGoNextPage);
        }

        // Update the browser url if `this.dataFetcher.reflectInUrl` is `true`:
        if (this.pagination && this.dataFetcher.reflectInUrl) {
            const url = new URL(window.location.href);
            url.searchParams.set(URL_PARAM_PAGE, `${this.pagination.pageNumber}-${this.pagination.pageSize}`);
            window.history.replaceState(null, '', url.toString());
        }
    }

    /**
     * @param {function(number, PaginationInfos): number} pageModifier
     * @return boolean
     */
    changePageInUrl(pageModifier) {
        if (!this.pagination) {
            return false;
        }

        const pageNumber = pageModifier(this.pagination.pageNumber, this.pagination);
        const pageSize = this.pagination.pageSize;

        if (pageNumber <= 0 || pageNumber > this.pagination.nbPages) {
            return false;
        }

        this.dataFetcher.url.searchParams.set(URL_PARAM_PAGE, `${pageNumber}-${pageSize}`);

        return true;
    }
}

/**
 * Parses an HTTP response's headers and returns an object which holds pagination information.
 *
 * @param {Response} response
 *
 * @return ?PaginationInfos
 */
function getPaginationInfosFromResponse(response) {
    const getHeader = name => {
        const value = response.headers.get(name);
        if (value === null) {
            throw 'NO_HEADER';
        }
        return parseInt(value);
    }

    try {
        return {
            nbRows: getHeader('Db-Info-Nb-Rows'),
            totalCount: getHeader('Db-Info-Total-Rows'),
            pageNumber: getHeader('Db-Info-Page'),
            pageSize: getHeader('Db-Info-Page-Size'),
            nbPages: getHeader('Db-Info-Total-Pages'),
        };
    } catch (e) {
        if (e === 'NO_HEADER') {
            return null;
        } else {
            throw e;
        }
    }
}
