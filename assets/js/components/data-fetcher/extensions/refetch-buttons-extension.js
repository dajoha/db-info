import { parseUrl } from '../../../util';
import { EVENT_FETCH } from '../index';
import { URL_PARAM_SORT_REGEX } from '../util';

export class RefetchButtonsExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        this.setupRefetchButtons();

        this.dataFetcher.subscribe(EVENT_FETCH, this.setupRefetchButtons.bind(this));
    }

    setupRefetchButtons() {
        /**
         * @param {string} attribute
         * @param {function(MouseEvent): boolean} eventTester
         */
        const setupListeners = (attribute, eventTester) => {
            const refetchButtons = this.dataFetcher.baseElement.querySelectorAll(
                `[${attribute}]`
            );
            for (const refetchButton of refetchButtons) {
                refetchButton.addEventListener('click',
                    /** @param {MouseEvent} ev */
                    ev => {
                        eventTester(ev) && this.fetchFromAttribute(refetchButton, attribute);
                    }
                );
            }

        };

        setupListeners(
            'data-fetch-html--refetch-on-click',
            (ev) => !ev.shiftKey && !ev.ctrlKey && !ev.altKey,
        );

        setupListeners(
            'data-fetch-html--refetch-on-shift-click',
            (ev) => ev.shiftKey && !ev.ctrlKey && !ev.altKey,
        );

        setupListeners(
            'data-fetch-html--refetch-on-control-click',
            (ev) => !ev.shiftKey && ev.ctrlKey && !ev.altKey,
        );

        setupListeners(
            'data-fetch-html--refetch-on-control-shift-click',
            (ev) => ev.shiftKey && ev.ctrlKey && !ev.altKey,
        );
    }

    /**
     * @param {HTMLElement} button
     * @param {string} attribute
     */
    fetchFromAttribute(button, attribute) {
        const url = button.attributes.getNamedItem(attribute)?.value;
        this.dataFetcher.url = parseUrl(url);
        void this.dataFetcher.fetch();
        if (this.dataFetcher.reflectInUrl) {
            this.reflectSortsInUrl();
        }
    }

    reflectSortsInUrl() {
        const url = new URL(window.location.href);

        // Remove all previous sort params:
        const keys = [...url.searchParams.keys()];
        for (const key of keys) {
            if (key.match(URL_PARAM_SORT_REGEX)) {
                url.searchParams.delete(key);
            }
        }

        // Add up-to-date sort params:
        for (const [key, value] of this.dataFetcher.url.searchParams.entries()) {
            if (key.match(URL_PARAM_SORT_REGEX)) {
                url.searchParams.append(key, value);
            }
        }

        window.history.replaceState(null, '', url.toString());
    }
}
