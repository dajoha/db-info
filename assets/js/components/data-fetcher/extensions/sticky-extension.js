import { EVENT_FETCH } from '../index';
import { setupSticky } from '../../../behaviours/is-sticky-class';

export class StickyExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        this.dataFetcher.subscribe(EVENT_FETCH, _ => {
            setupSticky(this.dataFetcher.baseElement);
        });
    }
}
