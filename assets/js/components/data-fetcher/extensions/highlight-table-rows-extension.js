// Allows to re-highlight table rows when some new content is fetched.

import { EVENT_FETCH } from '../index';
import { highlightTablesRows } from '../../../behaviours/highlight-table-rows';

export class HighlightTableRowsExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        this.dataFetcher.subscribe(EVENT_FETCH, _ => {
            highlightTablesRows(this.dataFetcher.baseElement);
        });
    }
}
