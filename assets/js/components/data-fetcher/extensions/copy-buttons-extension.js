import { EVENT_FETCH } from '../index';
import { setupCopyColumnButtons } from '../../../actions/copy-column';
import {setupCopyValueButtons} from "../../../actions/copy-value";

export class CopyButtonsExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        this.dataFetcher.subscribe(EVENT_FETCH, _ => {
            setupCopyColumnButtons(this.dataFetcher.baseElement);
            setupCopyValueButtons(this.dataFetcher.baseElement);
        });
    }
}
