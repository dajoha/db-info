// Handles the reload button.

import {setButtonLoadingUntilFinished} from "../../../general/loading-button";
import {registerRefreshDataShortcutAction} from "../../../global-shortcuts";

export class ReloadButtonsExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        /** @type {NodeListOf<HTMLElement>} */
        const reloadButtons=  document.querySelectorAll(
            `[data-fetch-html--reload="${this.dataFetcher.id}"]`
        );

        let allowRefreshDataShortcut = false;

        for (const reloadButton of reloadButtons) {
            if (reloadButton.dataset['filterEditor-RefreshDataKeyShortcut'] === '1') {
                allowRefreshDataShortcut = true;
            }
            reloadButton.addEventListener('click', async () => {
                const promise = this.dataFetcher.fetch();
                setButtonLoadingUntilFinished(reloadButton, promise);
            })
        }

        if (allowRefreshDataShortcut) {
            registerRefreshDataShortcutAction(() => {
                void this.dataFetcher.fetch();
            })
        }
    }
}
