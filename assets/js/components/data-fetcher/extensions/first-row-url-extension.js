import { EVENT_FETCH } from '../index';
import {URL_PARAM_SEARCH, URL_PARAM_SORT_REGEX} from '../util';
import appDataProvider from '../../../service/app-data-provider';
import {getRouter} from '../../../service/app-router';

export class FirstRowUrlExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        /** @type {NodeListOf<HTMLAnchorElement>} */
        this.anchor_elements = document.querySelectorAll(
            `a[data-fetch-html--first-row-url="${dataFetcher.id}"]`
        );

        if (this.anchor_elements.length !== 0) {
            this.dataFetcher.subscribe(EVENT_FETCH, this.handleFetch.bind(this));
        }
    }

    handleFetch() {
        /** @type {Record<string, string>} */
        const query = {};

        for (const [key, value] of this.dataFetcher.url.searchParams.entries()) {
            if (key !== URL_PARAM_SEARCH && !key.match(URL_PARAM_SORT_REGEX)) {
                continue;
            }
            if (value.trim() !== '') {
                query[key] = value;
            }
        }

        const firstRowUrl = getRouter().generate('get_first_row', {
            params: {
                project: appDataProvider.projectName,
                tableAlias: appDataProvider.tableName,
            },
            query,
        });

        for (const anchor of this.anchor_elements) {
            anchor.href = firstRowUrl;
        }
    }
}
