import * as filter_editor from '../../filter-editor';
import { URL_PARAM_SEARCH } from '../util';

export class FilterEditorExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        /** @type {?string} */
        this.searchExpression = null;

        const filterEditor = document.querySelector(
            `[data-fetch-html--filter-editor="${this.dataFetcher.id}"]`
        );

        if (!filterEditor) {
            return;
        }

        filterEditor.addEventListener(
            filter_editor.EVENT_SUBMIT,
            this.handleSubmit.bind(this),
            false,
        );
    }

    /**
     * @param {FilterEditorSubmitEvent} event
     */
    handleSubmit(event) {
        this.searchExpression = event.detail.searchExpression;

        if (this.dataFetcher.reflectInUrl) {
            this.reflectSearchExpressionInUrl()
        }

        // Refetch data with search expression updated:
        this.dataFetcher.url.searchParams.set(URL_PARAM_SEARCH, this.searchExpression);
        void this.dataFetcher.fetch();
    }

    reflectSearchExpressionInUrl() {
        const url = new URL(window.location.href);

        if (this.searchExpression.trim() === '') {
            url.searchParams.delete(URL_PARAM_SEARCH);
        } else {
            url.searchParams.set(URL_PARAM_SEARCH, this.searchExpression);
        }

        window.history.replaceState(null, '', url.toString());
    }
}
