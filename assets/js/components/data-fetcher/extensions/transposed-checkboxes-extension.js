// Handles the "transposed" checkboxes.

export class TransposedCheckboxesExtension {
    /**
     * @param {DataFetcher} dataFetcher
     */
    constructor(dataFetcher) {
        this.dataFetcher = dataFetcher;

        /** @type {NodeListOf<HTMLInputElement>} */
        const checkboxes=  document.querySelectorAll(
            `[data-fetch-html--transposed="${this.dataFetcher.id}"]`
        );

        for (const checkbox of checkboxes) {
            this.updateUrl(this.dataFetcher.url, checkbox)

            checkbox.addEventListener('change', () => {
                this.updateUrl(this.dataFetcher.url, checkbox)
                void this.dataFetcher.fetch();
            })
        }
    }

    /**
     * @param {URL} url
     * @param {HTMLInputElement} checkbox
     */
    updateUrl(url, checkbox) {
        if (checkbox.checked) {
            url.searchParams.set('transposed', 'true');
        } else {
            url.searchParams.delete('transposed');
        }
    }
}
