/**
 * @param {HTMLElement} baseElement
 */
export function highlightTablesRows(baseElement) {
    let tables = baseElement.querySelectorAll('[data-highlight-table-rows]');
    if (baseElement.matches('[data-highlight-table-rows]')) {
        tables = [baseElement, ...tables];
    }

    for (const table of tables) {
        const rows = table.querySelectorAll(`
            :scope > tr,
            :scope > tbody > tr
        `);

        let currentHighlight = true;
        for (const row of rows) {
            if (window.getComputedStyle(row).display === 'none') {
                continue;
            }
            row.classList.toggle('highlight-row', currentHighlight);
            currentHighlight = !currentHighlight;
        }
    }
}

init();

function init() {
    highlightTablesRows(document.body);
}
