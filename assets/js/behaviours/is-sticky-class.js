const observer = new IntersectionObserver(
    ([e]) => {
        e.target.classList.toggle('is-sticky', e.intersectionRatio < 1)
    },
    {
        rootMargin: '-1px 1000000px 1000000px 1000000px',
        threshold: [1],
    },
);

setupSticky(document);

/**
 * @param {Document|Element} baseElement
 */
export function setupSticky(baseElement) {
    const elements = baseElement.querySelectorAll('.sticky');
    for (const element of elements) {
        observer.observe(element);
    }
}
