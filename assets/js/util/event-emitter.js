export class EventEmitter {
    constructor() {
        /**
         * @type {Object.<string, function[]>} subscribers
         * @private
         */
        this.subscribers = {};
    }

    /**
     * @param {string} event
     * @param {function} callback
     */
    subscribe(event, callback) {
        if (!this.subscribers[event]) {
            this.subscribers[event] = [];
        }

        this.subscribers[event].push(callback);
    }

    /**
     * @param {string} event
     * @param {array} args
     */
    dispatch(event, args = []) {
        const callbacks = this.subscribers[event] || [];

        for (const callback of callbacks) {
            callback(...args);
        }
    }
}
