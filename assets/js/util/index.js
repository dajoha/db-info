/**
 * @param {function} callback
 * @param {number} time
 * @returns {function(...[*]): void}
 */
export function debounce(callback, time) {
    let timout;
    return (...args) => {
        clearTimeout(timout);
        timout = setTimeout(_ => { callback.apply(this, args); }, time);
    };
}

/**
 * From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
 *
 * @param {string} string
 * @returns {string}
 */
export function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}

/**
 * @param {string}       variableName
 * @param {?HTMLElement} element
 */
export function getCssVariable(variableName, element = null) {
    element ||= document.querySelector(':root');
    return window.getComputedStyle(element).getPropertyValue(variableName);
}

/**
 * @param {string} url
 * @return {URL}
 */
export function parseUrl(url) {
    const host = window.location.protocol + '//' + window.location.host;

    return new URL(url, host);
}

/**
 * https://stackoverflow.com/a/3028037/3271687
 *
 * @param {HTMLElement} element
 * @param {function} action
 */
export function onClickOutside(element, action) {
    const isVisible = elem => !!elem && !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );

    const outsideClickListener = event => {
        if (!element.contains(event.target) && isVisible(element)) {
            action();
            removeClickListener();
        }
    }

    const removeClickListener = () => {
        document.removeEventListener('click', outsideClickListener);
    }

    document.addEventListener('click', outsideClickListener);
}

/**
 * @param {HTMLElement} el
 * @return {{top: number, left: number, bottom: number, right: number}}
 */
export function getOffset(el) {
    const rect = el.getBoundingClientRect();

    return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY,
        right: rect.right + window.scrollX,
        bottom: rect.bottom + window.scrollY,
    };
}

/**
 * @return {{width: number, height: number}}
 */
export function getViewPortSize() {
    const width = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    const height = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

    return { width, height };
}
