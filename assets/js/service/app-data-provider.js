/**
 * @typedef {{
 *     projectName: ?string,
 *     tableName: ?string,
 * }} AppData
 */

/**
 * @see src/Frontend/Service/JsAppDataProvider.php
 */
class AppDataProvider {
    constructor() {
        /** @type {AppData} */
        this.data = window.app_data;
    }

    /**
     * @return {?string}
     */
    get projectName() {
        return this.data.projectName;
    }

    /**
     * @return {?string}
     */
    get tableName() {
        return this.data.tableName;
    }
}

export default new AppDataProvider();
