/**
 * Internal API client.
 */

import appDataProvider from './app-data-provider';

class AppApiClient {
    /**
     * @typedef {{
     *     input: string,
     *     cursor: number,
     *     tableName: string=,
     *     projectName: string=,
     * }} TableFilterCompletionsParams
     */

    /**
     * @typedef {?{
     *     completeFrom: number,
     *     completions: array,
     * }} TableFilterCompletionsOutput
     */

    /**
     * @typedef {{
     *     projectName: string=,
     * }} TableAliasCompletionsParams
     */

    /**
     * @param {TableFilterCompletionsParams} params
     * @return {Promise<TableFilterCompletionsOutput>}
     */
    async getTableFilterCompletions(params) {
        const projectName = encodeURI(params.projectName ?? appDataProvider.projectName);
        const tableName = encodeURI(params.tableName ?? appDataProvider.tableName);

        const baseUrl = `/internal/projects/${projectName}/tables/${tableName}/filter-autocomplete`;
        const url = baseUrl + '?' + new URLSearchParams({
            input: params.input,
            cursor: params.cursor,
        });

        const response = await fetch(url);

        return await response.json();
    }

    /**
     * @param {TableAliasCompletionsParams} params
     */
    async getTableAliasCompletions(params = {}) {
        const projectName = encodeURI(params.projectName ?? appDataProvider.projectName);

        const url = `/internal/projects/${projectName}/table-alias-autocomplete`;

        const response = await fetch(url);

        return await response.json();
    }
}

export default new AppApiClient();
