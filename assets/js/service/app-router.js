/**
 * @typedef {'get_first_row'} RouteName
 */

/**
 * @typedef {{
 *     params?: Record<string, string>,
 *     query?: Record<string, string>,
 * }} GetRouteOptions
 */

/**
 * @typedef {{
 *     path: string,
 * }} RouteDefinition
 */

/**
 * @type {Record<RouteName, RouteDefinition>}
 */
const ROUTES = {
    'get_first_row': { path: 'projects/{project}/tables/{tableAlias}/seek/first' },
};

/**
 * @return {AppRouter}
 */
export function getRouter() {
    return router ??= new AppRouter(ROUTES);
}

let router = null;

class AppRouter {
    /**
     * @param {Record<RouteName, RouteDefinition>} routeDefinitions
     */
    constructor(routeDefinitions) {
        /**
         * @type {Record<RouteName, Route>}
         */
        this.routes = {};

        for (const routeName in routeDefinitions) {
            const routeDefinition = routeDefinitions[routeName];
            this.routes[routeName] = new Route(routeName, routeDefinition);
        }
    }

    /**
     * @param {RouteName} routeName
     * @param {GetRouteOptions?} options
     * @return {string}
     */
    generate(routeName, options = {}) {
        const route = this.routes[routeName];

        if (route === undefined) {
            throw `Unknown route name: "${routeName}"`;
        }

        return route.generate(options);
    }
}

class Route {
    /**
     * @param {string} name
     * @param {RouteDefinition} routeDefinition
     */
    constructor(name, routeDefinition) {
        this.name = name;
        this.parts = parsePath(routeDefinition.path);
    }

    /**
     * @param {GetRouteOptions?} options
     * @return {string}
     */
    generate(options = {}) {
        let output = '';
        const params = options.params || {};
        const query = options.query || {};

        // Base url:
        if (this.parts.length > 0) {
            output += this.parts[0];
            for (let i = 1; i < this.parts.length; i += 2) {
                const placeholder = this.parts[i];
                const value = params[placeholder];
                if (value === undefined) {
                    throw `Route "${this.name}": placeholder "${placeholder}" is required`;
                }
                output += value + this.parts[i + 1];
            }
        }

        // Normalize leading slash:
        output = '/' + output.replace(/^\/+/, '');

        // Query string:
        if (Object.keys(query).length > 0) {
            const queryParams = new URLSearchParams(options.query);
            output += `?${queryParams.toString()}`;
        }

        return output;
    }
}

/**
 * @param {string} input
 * @return {string[]}
 */
function parsePath(input) {
    const parts = input.split(/({[^}]+})/g);

    // Strip starting '{' and trailing '}' for placeholders:
    for (let i = 1; i < parts.length; i += 2) {
        parts[i] = parts[i].substring(1, parts[i].length - 1);
    }

    return parts;
}
