import { getCssVariable } from '../util';

setupCopyValueButtons(document);

/**
 * @param {Document|Element} rootElement
 */
export function setupCopyValueButtons(rootElement) {
    const copyValueButtons = rootElement.querySelectorAll('[data-action--copy-value]');

    copyValueButtons.forEach(button => {
        const getButtonAttr = attr => button.attributes.getNamedItem(attr).value;

        const selector = getButtonAttr('data-action--copy-value');
        const selectedElement = rootElement.querySelector(selector);

        const effectSelector = getButtonAttr('data-action--copy-value__effect');
        const effectElement = rootElement.querySelector(effectSelector);
        effectElement.classList.add('action--copy-value--effect');

        button.addEventListener('click', _ => {
            const text = selectedElement.textContent;

            navigator.clipboard.writeText(text).then(function () {
                effectElement.style.setProperty('--transition-time', 0);
                effectElement.classList.add('action-done');
                setTimeout(() => {
                    const time = getCssVariable('--transition-time-config', effectElement);
                    effectElement.style.setProperty('--transition-time', time);
                    effectElement.classList.remove('action-done');
                }, 100);
            });
        });
    });
}
