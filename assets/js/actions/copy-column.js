import { getCssVariable } from '../util';

setupCopyColumnButtons(document);

/**
 * @param {Document|Element} rootElement
 */
export function setupCopyColumnButtons(rootElement) {
    const copyColumnButtons = rootElement.querySelectorAll('[data-action--copy-column]');

    for (const button of copyColumnButtons) {
        button.addEventListener('click', _ => {
            const copyColumnAttr = button.getAttribute('data-action--copy-column');
            let columnNumber = parseInt(copyColumnAttr);

            if (isNaN((columnNumber))) {
                const th = button.closest('th');
                if (!th) {
                    console.warn("Did not find a parent <th> element, aborting");
                    return;
                }

                const th_parent = th.parentNode;
                const th_siblings = [...th_parent.children];
                columnNumber = th_siblings.indexOf(th);
            }

            const table = button.closest('table');
            if (!table) {
                console.warn("Did not find a parent <table> element, aborting");
                return;
            }

            const tds = table.querySelectorAll(
                `tr > td:nth-child(${columnNumber + 1})`
            );
            const values = table.querySelectorAll(
                `tr > td:nth-child(${columnNumber + 1}) .data-action--copy-column--value`
            );

            if (tds.length === 0 || values.length === 0) {
                console.warn('Nothing to copy');
                return;
            }

            const text = [ ...values ]
                .map(value => value.innerText)
                .join("\n")
            ;

            for (const td of tds) {
                td.classList.add('action--copy-value--effect');
            }

            const time = getCssVariable('--transition-time-config', tds[0]);

            navigator.clipboard.writeText(text).then(function () {
                for (const td of tds) {
                    td.style.setProperty('--transition-time', 0);
                    td.classList.add('action-done');
                }
                setTimeout(() => {
                    for (const td of tds) {
                        td.style.setProperty('--transition-time', time);
                        td.classList.remove('action-done');
                    }
                }, 100);
            });
        });
    }
}
