/**
 * @typedef {{
 *     key: string,
 *     action: function(),
 * }} KeyShortcut
 */

import * as quickFindZone from "./components/quick-find-zone";

/**
 * @type {KeyShortcut[]}
 */
const globalShortcuts = [
    {
        key: '@',
        action() {
            /** @type HTMLElement */
            const input = document.querySelector('.key-shortcut__row-search');
            if (input !== null) {
                input.focus();
            }
        },
    },
    {
        key: '#',
        action() {
            /** @type HTMLElement */
            const input = document.querySelector('.key-shortcut__table-search');
            if (input !== null) {
                /** @var {Event} event */
                const event = new Event(quickFindZone.EVENT_FOCUS);
                input.dispatchEvent(event);
            }
        },
    },
    {
        key: '/',
        action() {
            if (searchShortcutAction !== null) {
                searchShortcutAction();
            }
        }
    },
    {
        key: 'r',
        action() {
            if (refreshDataShortcutAction !== null) {
                refreshDataShortcutAction();
            }
        }
    },
];

/** @type {null|function()} */
let searchShortcutAction = null;

/** @type {null|function()} */
let refreshDataShortcutAction = null;

let shortcutsLock = 0;
let singleKeyShortcutsLock = 0;

export function lockGlobalShortcuts() {
    shortcutsLock++;
}

export function unlockGlobalShortcuts() {
    shortcutsLock--;
}

export function lockGlobalSingleKeyShortcuts() {
    singleKeyShortcutsLock++;
}

export function unlockGlobalSingleKeyShortcuts() {
    singleKeyShortcutsLock--;
}

/**
 * @param {function()} action
 */
export function registerSearchShortcutAction(action) {
    if (searchShortcutAction === null) {
        searchShortcutAction = action;
    }
}

/**
 * @param {function()} action
 */
export function registerRefreshDataShortcutAction(action) {
    if (refreshDataShortcutAction === null) {
        refreshDataShortcutAction = action;
    }
}

function initGlobalShortcuts() {
    window.addEventListener('focusin', function() {
        lockGlobalSingleKeyShortcuts();
    });

    window.addEventListener('focusout', function() {
        unlockGlobalSingleKeyShortcuts();
    });

    window.addEventListener('keydown', function(event) {
        if (shortcutsLock !== 0 || singleKeyShortcutsLock !== 0) {
            return;
        }

        for (const shortcut of globalShortcuts) {
            if (matchShortcut(event, shortcut)) {
                event.preventDefault();
                shortcut.action();
                break;
            }
        }
    });

    /** @type HTMLElement */
    const input = document.querySelector('.search-key-shortcut');
    if (input !== null) {
        registerSearchShortcutAction(() => {
            input.focus();
        });
    }
}

/**
 * @param {KeyboardEvent} e
 * @param {KeyShortcut} shortcut
 *
 * @return {boolean}
 */
function matchShortcut(e, shortcut) {
    if (e.ctrlKey || e.altKey) {
        return false;
    }

    return e.key === shortcut.key;
}

initGlobalShortcuts();
