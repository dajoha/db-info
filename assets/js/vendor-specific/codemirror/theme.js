import { EditorView } from 'codemirror';
import { Extension } from '@codemirror/state';

// Greatly inspired by https://github.com/codemirror/theme-one-dark/blob/main/src/one-dark.ts

/**
 * @typedef {{
 *     darkBackground: string=,
 *     guttersFg: string=,
 *     cursorFg: string=,
 *     searchMatchSelectedBg: string=,
 *     selectionMatchBg: string=,
 *     matchingBracketBg: string=,
 *     foldPlaceholderFg: string=,
 *     selectionBg: string=,
 *     white: string=,
 *     searchMatchBg: string=,
 *     background: string=,
 *     activeLineBg: string=,
 *     highlightBackground: string=,
 *     tooltipBg: string=,
 * }} ThemeOptions
 */

/** @type {Extension} */
let defaultTheme = null;

/** @type {ThemeOptions} */
const DEFAULT_THEME_OPTIONS = {
    white: 'white',
    darkBackground: '#21252b',
    highlightBackground: '#915230',
    background: '#333',
    tooltipBg: '#555',
    selectionBg: '#a85010',
    cursorFg: '#abb2bf',
    guttersFg: '#7d8799',
    foldPlaceholderFg: '#ddd',
    searchMatchBg: '#72a1ff59',
    searchMatchSelectedBg: '#6199ff2f',
    activeLineBg: '#6699ff0b',
    selectionMatchBg: '#aafe661a',
    matchingBracketBg: '#bad0f847',
}

/**
 * @param {ThemeOptions} themeOptions
 * @return {Extension}
 */
export function createTheme(themeOptions = {}) {
    /** @type {ThemeOptions} */
    const options = { ...DEFAULT_THEME_OPTIONS, ...themeOptions };

    const spec = {
        '&': {
            color: options.white,
            backgroundColor: options.background,
        },

        '.cm-content': {
            caretColor: options.cursorFg,
        },
        '.cm-cursor, .cm-dropCursor': {
            borderLeftColor: options.cursorFg,
        },

        '&.cm-focused > .cm-scroller > .cm-selectionLayer .cm-selectionBackground': {
            backgroundColor: options.selectionBg,
        },
        '.cm-selectionBackground': {
            backgroundColor: options.selectionBg,
        },
        '.cm-content ::selection': {
            backgroundColor: options.selectionBg,
        },

        '.cm-panels': {
            backgroundColor: options.darkBackground,
            color: options.white,
        },
        '.cm-panels.cm-panels-top': {
            borderBottom: '2px solid black',
        },
        '.cm-panels.cm-panels-bottom': {
            borderTop: '2px solid black',
        },

        '.cm-searchMatch': {
            backgroundColor: options.searchMatchBg,
            outline: '1px solid #457dff',
        },
        '.cm-searchMatch.cm-searchMatch-selected': {
            backgroundColor: options.searchMatchSelectedBg,
        },

        '.cm-activeLine': {
            backgroundColor: options.activeLineBg,
        },
        '.cm-selectionMatch': {
            backgroundColor: options.selectionMatchBg,
        },

        '&.cm-focused .cm-matchingBracket, &.cm-focused .cm-nonmatchingBracket': {
            backgroundColor: options.matchingBracketBg,
        },

        '.cm-gutters': {
            backgroundColor: options.background,
            color: options.guttersFg,
            border: 'none',
        },

        '.cm-activeLineGutter': {
            backgroundColor: options.highlightBackground,
        },

        '.cm-foldPlaceholder': {
            backgroundColor: 'transparent',
            border: 'none',
            color: options.foldPlaceholderFg,
        },

        '.cm-tooltip': {
            border: 'none',
            backgroundColor: options.tooltipBg,
        },
        '.cm-tooltip .cm-tooltip-arrow:before': {
            borderTopColor: 'transparent',
            borderBottomColor: 'transparent',
        },
        '.cm-tooltip .cm-tooltip-arrow:after': {
            borderTopColor: options.tooltipBg,
            borderBottomColor: options.tooltipBg,
        },
        '.cm-tooltip-autocomplete': {
            '& > ul > li[aria-selected]': {
                backgroundColor: options.highlightBackground,
                color: options.white,
            }
        },
        '.cm-tooltip.cm-completionInfo': {
            maxWidth: '600px !important',
        },
    };

    return EditorView.theme(spec, { dark: true });
}

/**
 * @return {Extension}
 */
export function getDefaultTheme() {
    if (defaultTheme === null) {
        defaultTheme = createTheme();
    }

    return defaultTheme;
}
