import {KeyBinding} from '@codemirror/view';

/**
 * @return {KeyBinding[]}
 */
export function appKeymap() {
    return [
        {
            key: 'Escape',
            run: editorView => {
                editorView.contentDOM.blur();
                return true;
            },
        },
    ];
}
