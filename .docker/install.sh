#!/bin/bash

set -e

php_docker() {
	docker compose exec php "$@"
}

cd "$(dirname "$(realpath "${BASH_SOURCE[0]}")" )"

docker compose up -d
php_docker composer install
php_docker yarn
php_docker yarn dev
