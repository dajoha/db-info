<?php

declare(strict_types=1);

namespace App\Domain\Merger\Combinators;

use App\Domain\Merger\Base\MergerInterface;
use App\Domain\Merger\Merger;

class MergePath implements MergerInterface
{
    /**
     * @param list<string|int> $path
     */
    public function __construct(protected array $path, protected MergerInterface $innerMerger)
    {
    }

    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool
    {
        return $this->path === $path
            && $this->innerMerger->supports($a, $b, $path, $context)
        ;
    }

    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): mixed
    {
        return $this->innerMerger->merge($a, $b, $path, $context, $merger);
    }
}
