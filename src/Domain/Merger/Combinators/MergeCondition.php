<?php

declare(strict_types=1);

namespace App\Domain\Merger\Combinators;

use App\Domain\Merger\Base\MergerInterface;
use App\Domain\Merger\Merger;

class MergeCondition implements MergerInterface
{
    /**
     * @var callable(mixed $a, mixed $b, list<string|int> $path): bool
     */
    protected $supports;

    /**
     * @param callable(mixed $a, mixed $b, list<string|int> $path): bool  $supports
     */
    public function __construct(callable $supports, protected MergerInterface $innerMerger)
    {
        $this->supports = $supports;
    }


    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool
    {
        return $this->innerMerger->supports($a, $b, $path, $context)
            && ($this->supports)($a, $b, $path)
        ;
    }

    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): mixed
    {
        return $this->innerMerger->merge($a, $b, $path, $context, $merger);
    }
}
