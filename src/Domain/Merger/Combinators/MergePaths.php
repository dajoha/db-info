<?php

declare(strict_types=1);

namespace App\Domain\Merger\Combinators;

use App\Domain\Merger\Base\MergerInterface;
use App\Domain\Merger\Merger;

class MergePaths implements MergerInterface
{
    /**
     * @param array<string, MergerInterface> $mergers
     */
    public function __construct(protected array $mergers)
    {
    }

    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool
    {
        $joinedPath = implode('.', $path);
        $merger = $this->mergers[$joinedPath] ?? null;

        if ($merger === null || !$merger->supports($a, $b, $path, $innerContext)) {
            return false;
        }

        $context = [
            'merger' => $merger,
            'inner_context' => $innerContext,
        ];

        return true;
    }

    /**
     * @param array{
     *     merger: MergerInterface,
     *     inner_context: mixed,
     * } $context
     */
    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): mixed
    {
        $mergeStrategy = $context['merger'];

        return $mergeStrategy->merge($a, $b, $path, $context['inner_context'], $merger);
    }
}
