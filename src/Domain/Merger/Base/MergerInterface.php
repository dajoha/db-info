<?php

declare(strict_types=1);

namespace App\Domain\Merger\Base;

use App\Domain\Merger\Merger;

interface MergerInterface
{
    /**
     * @param list<(int|string)> $path
     */
    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool;

    /**
     * @param list<(int|string)> $path
     */
    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): mixed;
}
