<?php

declare(strict_types=1);

namespace App\Domain\Merger\Base;

abstract class AbstractListMerger implements MergerInterface
{
    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool
    {
        return
            is_array($a)
            && is_array($b)
            && array_is_list($a)
            && array_is_list($b)
        ;
    }
}
