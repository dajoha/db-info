<?php

declare(strict_types=1);

namespace App\Domain\Merger\Exception;

use Exception;

class MergerException extends Exception
{
}
