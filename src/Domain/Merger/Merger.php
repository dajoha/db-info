<?php

declare(strict_types=1);

namespace App\Domain\Merger;

use App\Domain\Merger\Base\MergerInterface;
use App\Domain\Merger\Exception\MergerException;

class Merger
{
    /**
     * @var MergerInterface[]
     */
    protected array $mergers;

    /**
     * @param MergerInterface[] $mergers
     */
    public function __construct(array $mergers = [])
    {
        $this->mergers = $mergers;
    }

    /**
     * @param list<string|int> $path
     *
     * @throws MergerException
     */
    public function merge(mixed $a, mixed $b, array $path = []): mixed {
        foreach ($this->mergers as $merger) {
            if ($merger->supports($a, $b, $path, $context)) {
                return $merger->merge($a, $b, $path, $context, $this);
            }
        }

        throw new MergerException('Cannot merge data: no suitable merger was found');
    }
}
