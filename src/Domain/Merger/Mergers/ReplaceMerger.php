<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers;

use App\Domain\Merger\Base\MergerInterface;
use App\Domain\Merger\Merger;

class ReplaceMerger implements MergerInterface
{
    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool
    {
        return true;
    }

    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger,): mixed
    {
        return $b;
    }
}
