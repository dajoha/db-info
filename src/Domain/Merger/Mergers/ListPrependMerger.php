<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers;

use App\Domain\Merger\Base\AbstractListMerger;
use App\Domain\Merger\Merger;

class ListPrependMerger extends AbstractListMerger
{
    /**
     * @return array<mixed, mixed>
     */
    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): array
    {
        return [...$b, ...$a];
    }
}
