<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers;

use App\Domain\Merger\Base\MergerInterface;
use App\Domain\Merger\Exception\MergerException;
use App\Domain\Merger\Merger;

class ArrayRecursiveMerger implements MergerInterface
{
    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool
    {
        return
            is_array($a)
            && is_array($b)
            && !array_is_list($a)
            && !array_is_list($b)
        ;
    }

    /**
     * @param array<string, mixed> $a
     * @param array<string, mixed> $b
     *
     * @return array<string, mixed>
     *
     * @throws MergerException
     */
    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): array
    {
        $merged = $a;

        foreach ($b as $key => $value) {
            $path[] = $key;
            $merged[$key] = $merger->merge($merged[$key] ?? null, $value, $path);
            array_pop($path);
        }

        return $merged;
    }
}
