<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger;

use App\Domain\Merger\Exception\MergerException;
use App\Domain\Merger\Mergers\DynamicMerger\Command\CommandInterface;
use App\Domain\Merger\Util\PathUtil;

class DynamicMergerException extends MergerException
{
    /**
     * @param list<string|int> $path
     * @param class-string<CommandInterface>|null $commandClass
     */
    public function __construct(
        public string $errorMessage = "",
        public readonly ?array $path = null,
        public readonly ?string $commandClass = null,
        public readonly ?string $commandName = null,
    ) {
        $context = "Dynamic merger: ";

        if ($path !== null) {
            $context .= 'At '.PathUtil::toString($path);
        }

        if ($this->commandName !== null) {
            $context .= "Command \"$this->commandName\": ";
        }

        parent::__construct("$context: $errorMessage");
    }
}
