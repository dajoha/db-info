<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger\Command;

use App\Domain\Merger\Merger;

class ReplaceCommand extends AbstractCommand
{
    public const string NAME = 'replace';

    public static function process(mixed $value, mixed $param, array $path, Merger $merger): mixed
    {
        return $param;
    }
}
