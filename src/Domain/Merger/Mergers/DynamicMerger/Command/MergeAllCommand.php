<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger\Command;

use App\Domain\Merger\Exception\MergerException;
use App\Domain\Merger\Merger;

class MergeAllCommand extends AbstractCommand
{
    public const string NAME = 'merge-all';

    /**
     * @throws MergerException
     */
    public static function process(mixed $value, mixed $param, array $path, Merger $merger): mixed
    {
        if (
            !is_array($value)
            || !is_array($param)
            || array_is_list($value)
            || array_is_list($param)
        ) {
            self::throw("Expect associative array value", $path);
        }

        foreach ($param as $k => $v) {
            $path[] = $k;
            $value[$k] = $merger->merge($value[$k] ?? null, $v, $path);
            array_pop($path);
        }

        return $value;
    }
}
