<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger\Command;

use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\DynamicMerger\DynamicMergerException;

class MergeUniqueValuesCommand extends AbstractCommand
{
    public const string NAME = 'merge-unique-values';

    /**
     * @return array<mixed, mixed>
     *
     * @throws DynamicMergerException
     */
    public static function process(mixed $value, mixed $param, array $path, Merger $merger): array
    {
        if (
            !is_array($value)
            || !is_array($param)
            || !array_is_list($value)
            || !array_is_list($param)
        ) {
            self::throw("Expect list array value", $path);
        }

        return array_values(array_unique([...$value, ...$param]));
    }
}
