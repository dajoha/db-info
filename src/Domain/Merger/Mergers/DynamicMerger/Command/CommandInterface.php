<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger\Command;

use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\DynamicMerger\DynamicMergerException;

interface CommandInterface
{
    public static function getName(): string;

    /**
     * @param mixed $value The value to process
     * @param mixed $param The parameter given to the command
     * @param list<string|int> $path
     * @param Merger $merger
     *
     * @return mixed
     *
     * @throws DynamicMergerException
     */
    public static function process(mixed $value, mixed $param, array $path, Merger $merger): mixed;
}
