<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger\Command;

use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\DynamicMerger\DynamicMergerException;

class PrependCommand extends AbstractCommand
{
    public const string NAME = 'prepend';

    /**
     * @return array<mixed, mixed>
     *
     * @throws DynamicMergerException
     */
    public static function process(mixed $value, mixed $param, array $path, Merger $merger): array
    {
        $param = (array) $param;

        if (
            !is_array($value)
            || !array_is_list($value)
            || !array_is_list($param)
        ) {
            self::throw("Expect list array value", $path);
        }

        return [...$param, ...$value];
    }
}
