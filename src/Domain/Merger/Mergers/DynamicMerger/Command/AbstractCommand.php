<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger\Command;

use App\Domain\Merger\Mergers\DynamicMerger\DynamicMergerException;

abstract class AbstractCommand implements CommandInterface
{
    /**
     * To be overridden in child classes:
     */
    public const string NAME = '';

    public static function getName(): string
    {
        return static::NAME;
    }

    /**
     * @param list<string|int> $path
     *
     * @throws DynamicMergerException
     *
     * @return never-return
     */
    protected static function throw(string $message, ?array $path): void
    {
        throw new DynamicMergerException($message, $path, static::class);
    }
}
