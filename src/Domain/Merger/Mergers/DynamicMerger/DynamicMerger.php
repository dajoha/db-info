<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers\DynamicMerger;

use App\Domain\Merger\Base\MergerInterface;
use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\DynamicMerger\Command\AppendCommand;
use App\Domain\Merger\Mergers\DynamicMerger\Command\CommandInterface;
use App\Domain\Merger\Mergers\DynamicMerger\Command\MergeAllCommand;
use App\Domain\Merger\Mergers\DynamicMerger\Command\MergeFirstLevelCommand;
use App\Domain\Merger\Mergers\DynamicMerger\Command\MergeUniqueValuesCommand;
use App\Domain\Merger\Mergers\DynamicMerger\Command\PrependCommand;
use App\Domain\Merger\Mergers\DynamicMerger\Command\ReplaceCommand;

class DynamicMerger implements MergerInterface
{
    protected const string DEFAULT_COMMAND_PREFIX = '+';

    protected const array COMMAND_CLASSES = [
        AppendCommand::NAME => AppendCommand::class,
        MergeAllCommand::NAME => MergeAllCommand::class,
        MergeFirstLevelCommand::NAME => MergeFirstLevelCommand::class,
        MergeUniqueValuesCommand::NAME => MergeUniqueValuesCommand::class,
        PrependCommand::NAME => PrependCommand::class,
        ReplaceCommand::NAME => ReplaceCommand::class,
    ];

    public function __construct(protected string $commandPrefix = self::DEFAULT_COMMAND_PREFIX)
    {
    }

    public function supports(mixed $a, mixed $b, array $path, mixed &$context = null): bool
    {
        return is_array($b) && $this->isCommand(array_keys($b)[0] ?? null);
    }

    /**
     * @param array<string, mixed> $b
     *
     * @throws DynamicMergerException
     */
    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): mixed
    {
        try {
            foreach ($b as $prefixedCommand => $param) {
                $commandClass = $this->getCommandClass($prefixedCommand);
                $a = $commandClass::process($a, $param, $path, $merger);
            }
        } catch (DynamicMergerException $e) {
            $path = $e->path ?? $path;
            $commandName = $e->commandName === null ? null : $this->commandPrefix.$e->commandName;
            throw new DynamicMergerException($e->errorMessage, $path, commandName: $commandName);
        }

        return $a;
    }

    /**
     * @return class-string<CommandInterface>
     *
     * @throws DynamicMergerException
     */
    protected function getCommandClass(string $prefixedCommand): string
    {
        $commandName = $this->stripCommandPrefix($prefixedCommand);
        $commandClass = self::COMMAND_CLASSES[$commandName] ?? null;

        if ($commandClass === null) {
            throw new DynamicMergerException("Unknown command: \"$prefixedCommand\"");
        }

        return $commandClass;
    }

    /**
     * @throws DynamicMergerException
     */
    protected function stripCommandPrefix(string $prefixedCommand): string
    {
        if (!str_starts_with($prefixedCommand, $this->commandPrefix)) {
            throw new DynamicMergerException("Expected command, got \"$prefixedCommand\"");
        }

        return substr($prefixedCommand, strlen($this->commandPrefix));
    }

    protected function isCommand(mixed $value): bool
    {
        return is_string($value) && str_starts_with($value, $this->commandPrefix);
    }
}
