<?php

declare(strict_types=1);

namespace App\Domain\Merger\Mergers;

use App\Domain\Merger\Base\AbstractListMerger;
use App\Domain\Merger\Merger;

class ListAppendMerger extends AbstractListMerger
{
    /**
     * @return array<mixed, mixed>
     */
    public function merge(mixed $a, mixed $b, array $path, mixed $context, Merger $merger): array
    {
        return [...$a, ...$b];
    }
}
