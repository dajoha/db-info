<?php

declare(strict_types=1);

namespace App\Domain\Merger\Util;

class PathUtil
{
    private function __construct()
    {
    }

    /**
     * @param list<string|int> $path
     *
     * @return string
     */
    public static function toString(array $path): string
    {
        return '(root)'.implode(
            '',
            array_map(
                fn(string|int $part) => is_integer($part) ? "[$part]" : ".$part",
                $path,
            ),
        );
    }
}
