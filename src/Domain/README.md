Each sub-folder here is intended to be as isolated as possible, in a way that it would only imply
minor changes to implement it as a bundle, if it would make sense.
