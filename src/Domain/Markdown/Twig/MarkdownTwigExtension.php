<?php

declare(strict_types=1);

namespace App\Domain\Markdown\Twig;

use App\Domain\Markdown\MarkdownException;
use App\Domain\Markdown\Service\MarkdownToHtmlRenderer;
use League\CommonMark\Exception\CommonMarkException;
use Psr\Cache\InvalidArgumentException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MarkdownTwigExtension extends AbstractExtension
{
    public function __construct(
        protected readonly MarkdownToHtmlRenderer $markdownRenderer,
        protected readonly string $twigDefaultPath,
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'render_markdown_file',
                $this->renderMarkdownFile(...),
                ['is_safe' => ['html']],
            )
        ];
    }

    /**
     * Renders a markdown file into HTML. The filename is relative to the default twig template
     * directory.
     *
     * @throws CommonMarkException
     * @throws InvalidArgumentException
     * @throws MarkdownException
     */
    public function renderMarkdownFile(string $filename): string
    {
        $filename = "{$this->twigDefaultPath}/{$filename}";

        return $this->markdownRenderer->renderFile($filename);
    }
}
