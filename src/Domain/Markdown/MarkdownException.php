<?php

declare(strict_types=1);

namespace App\Domain\Markdown;

use Exception;

class MarkdownException extends Exception
{
}
