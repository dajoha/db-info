<?php

declare(strict_types=1);

namespace App\Domain\Markdown\Service;

use App\Domain\Markdown\MarkdownException;
use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Exception\CommonMarkException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;

readonly class MarkdownToHtmlRenderer
{
    protected CommonMarkConverter $converter;

    public function __construct(protected CacheInterface $markdownCachePool)
    {
        $this->converter = new CommonMarkConverter();
    }

    /**
     * @throws InvalidArgumentException
     * @throws CommonMarkException
     * @throws MarkdownException
     */
    public function renderFile(string $filename, bool $cached = true): string
    {
        if (!$cached) {
            return $this->renderFileNoCache($filename);
        }

        return $this->markdownCachePool->get(
            self::getCacheKeyForFile($filename),
            fn() => $this->renderFileNoCache($filename),
        );
    }

    protected static function getCacheKeyForFile(string $filename): string
    {
        $filename = base64_encode($filename);

        return "file-$filename";
    }

    /**
     * @throws CommonMarkException
     * @throws MarkdownException
     */
    protected function renderFileNoCache(string $filename): string
    {
        $content = file_get_contents($filename);

        if ($content === false) {
            throw new MarkdownException("Unable to read file \"$filename\"");
        }

        return $this->renderNoCache($content);
    }

    /**
     * @throws CommonMarkException
     */
    public function renderNoCache(string $input): string
    {
        return $this->converter->convert($input)->getContent();
    }
}
