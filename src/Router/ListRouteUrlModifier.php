<?php

declare(strict_types=1);

namespace App\Router;

use App\Controller\Request\QueryParam;
use App\Controller\Request\RequestSortsProvider;
use App\Exception\AppException;
use App\Exception\InternalException;
use App\Frontend\Model\ListColumn;
use App\Repository\QueryPart\Sort\Sort;
use App\Repository\QueryPart\Sort\OptSortDirection;
use App\Repository\QueryPart\Sort\Sorts;
use App\Service\Http\Exception\NoCurrentRequestException;
use App\Service\Http\RequestProvider;
use Doctrine\DBAL\Schema\Column;

class ListRouteUrlModifier
{
    public function __construct(
        protected RequestProvider $requestProvider,
        protected RequestSortsProvider $requestSortsProvider,
    ) {
    }

    /**
     * @throws InternalException
     * @throws NoCurrentRequestException
     */
    public function getUrlWithSortToggledSingle(
        ListColumn $listColumn,
        int $toggleDirection,
    ): string
    {
        $this->assertIsSortable($listColumn);

        if ($listColumn->sort) {
            $columnName = $listColumn->sort->columnName;
            $direction = $listColumn->sort->direction;
        } else {
            // Because $listColumn is sortable, $listColumn->column is defined:
            /** @var Column $column */
            $column = $listColumn->column;

            $columnName = $column->getName();
            $direction = OptSortDirection::None;
        }

        $newDirection = $toggleDirection >+ 0
            ? $direction->getNextValue()
            : $direction->getPreviousValue()
        ;

        $sort = new Sort($columnName, $newDirection, 1);
        $sorts = new Sorts([$sort]);

        return $this->getUrlFromSorts($sorts);
    }

    /**
     * @throws InternalException
     * @throws AppException
     */
    public function getUrlWithSortToggledMultiple(
        ListColumn $listColumn,
        int $toggleDirection,
    ): string
    {
        $this->assertIsSortable($listColumn);

        $sorts = clone $this->requestSortsProvider->getSortsFromRequestOrThrow();

        if ($listColumn->sort) {
            $sort = $listColumn->sort;
        } else {
            // Because $listColumn is sortable, $listColumn->column is defined:
            /** @var Column $column */
            $column = $listColumn->column;

            $sort = new Sort(
                $column->getName(),
                OptSortDirection::None,
                $sorts->getMaxPosition() + 1,
            );
        }
        $sort = $toggleDirection >= 0 ? $sort->withNextDirection() : $sort->withPreviousDirection();
        $sorts->setOrAdd($sort);

        return $this->getUrlFromSorts($sorts);
    }

    /**
     * @throws NoCurrentRequestException
     */
    protected function getUrlFromSorts(Sorts $sorts): string
    {
        $request = $this->requestProvider->getCurrentRequestOrThrow();
        $query = $request->query->all();
        $query[QueryParam::LIST_SORT] = $sorts->toQueryStringArray();
        $url = Url::fromRequest($request);
        $url->setQuery($query);

        return $url->toString();
    }

    /**
     * @throws InternalException
     */
    protected function assertIsSortable(ListColumn $listColumn): void
    {
        if (!$listColumn->isSortable) {
            throw new InternalException('List column is not sortable');
        }
    }
}
