<?php

declare(strict_types=1);

namespace App\Router\Model;

use App\Exception\NotImplementedException;
use App\Router\UrlEncodable;
use App\Router\Util\UrlUtil;
use Doctrine\DBAL\Types\Type;

readonly class DatabaseValue implements UrlEncodable
{
    public function __construct(public mixed $value, public Type $type)
    {
    }

    /**
     * @throws NotImplementedException
     */
    public function urlEncode(): string
    {
        return UrlUtil::encodeDatabaseValue($this->value, $this->type);
    }
}
