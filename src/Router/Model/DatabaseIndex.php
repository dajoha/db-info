<?php

declare(strict_types=1);

namespace App\Router\Model;

use App\Router\UrlEncodable;
use ArrayAccess;
use Countable;
use Stringable;

/**
 * A list of database values which represent an index for a table. Each value is bounded to a
 * column type (@see DatabaseValue}, in order to be able to encode them correctly into a URL.
 *
 * @implements ArrayAccess<int, DatabaseValue>
 */
class DatabaseIndex implements ArrayAccess, Countable, Stringable, UrlEncodable
{
    /**
     * @param list<DatabaseValue> $databaseValues
     */
    public function __construct(public array $databaseValues)
    {
    }

    /**
     * @param list<DatabaseValue> $databaseValues
     */
    public static function from(array $databaseValues): self
    {
        return new self($databaseValues);
    }

    public function __toString(): string
    {
        return $this->urlEncode();
    }

    public function isEmpty(): bool
    {
        return count($this) === 0;
    }

    public function urlEncode(): string
    {
        return iter($this->databaseValues)
            ->map(fn(DatabaseValue $value) => $value->urlEncode())
            ->join('/');
    }

    public function offsetExists(mixed $offset): bool
    {
        return key_exists($offset, $this->databaseValues);
    }

    public function offsetGet(mixed $offset): ?DatabaseValue
    {
        return $this->databaseValues[$offset] ?? null;
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
    }

    public function offsetUnset(mixed $offset): void
    {
    }

    public function count(): int
    {
        return count($this->databaseValues);
    }
}
