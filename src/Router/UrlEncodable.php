<?php

declare(strict_types=1);

namespace App\Router;

/**
 * Implementing this interface indicates that the object can be stringified in order to be provided
 * in a URL.
 */
interface UrlEncodable
{
    public function urlEncode(): string;
}
