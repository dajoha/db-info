<?php

declare(strict_types=1);

namespace App\Router;

use App\Exception\NotImplementedException;
use App\Project\Data\TableInfo;
use App\Project\Exception\ProjectDoesNotExistException;
use App\Project\Project;
use App\Project\ProjectManager;
use App\Router\Model\DatabaseValue;
use App\Router\Model\DatabaseIndex;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Routing\RouterInterface;

class AppRouter
{
    public function __construct(
        protected RouterInterface $router,
        protected ProjectManager $projectManager,
    ) {
    }

    /**
     * @param array<string, mixed> $extraFields
     *
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws ProjectDoesNotExistException
     */
    public function getTablePath(Project|string $project, string $tableAlias, array $extraFields = []): string
    {
        if (is_string($project)) {
            $project = $this->projectManager->getProject($project);
        }

        $tableInfo = $project->getTableInfoFromAlias($tableAlias);

        return $this->getTablePathFromTableInfo($tableInfo, $extraFields);
    }

    /**
     * @throws ProjectDoesNotExistException
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws NotImplementedException
     */
    public function getTableRowPath(
        Project|string $project,
        string $tableAlias,
        DatabaseIndex|DatabaseValue $databaseIndex,
    ): string
    {
        if (is_string($project)) {
            $project = $this->projectManager->getProject($project);
        }

        $tableInfo = $project->getTableInfoFromAlias($tableAlias);

        return $this->getTableRowPathFromTableInfo($tableInfo, $databaseIndex);
    }

    /**
     * @throws ProjectDoesNotExistException
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws NotImplementedException
     */
    public function getTableRowByFieldPath(
        Project|string $project,
        string $tableAlias,
        string $columnAlias,
        DatabaseValue $databaseValue,
    ): string
    {
        if (is_string($project)) {
            $project = $this->projectManager->getProject($project);
        }

        $tableInfo = $project->getTableInfoFromAlias($tableAlias);

        return $this->getTableRowByFieldPathFromTableInfo($tableInfo, $columnAlias, $databaseValue);
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws ProjectDoesNotExistException
     */
    public function getTableStructurePath(Project|string $project, string $tableAlias): string
    {
        if (is_string($project)) {
            $project = $this->projectManager->getProject($project);
        }

        $tableInfo = $project->getTableInfoFromAlias($tableAlias);

        return $this->getTableStructurePathFromTableInfo($tableInfo);
    }

    /**
     * @param array<string, mixed> $extraFields
     */
    public function getTablePathFromTableInfo(TableInfo $tableInfo, array $extraFields = []): string
    {
        return $this->router->generate('list_rows', [
            'project' => $tableInfo->project->getName(),
            'tableAlias' => $tableInfo->name,
            'sort' => $tableInfo->getDefaultSorts()->toQueryStringArray(),
            ...$extraFields,
        ]);
    }

    /**
     * @throws NotImplementedException
     */
    public function getTableRowPathFromTableInfo(
        TableInfo $tableInfo,
        DatabaseIndex|DatabaseValue $databaseIndex,
    ): string
    {
        return $this->router->generate('get_row', [
            'project' => $tableInfo->project->getName(),
            'tableAlias' => $tableInfo->name,
            'ids' => $databaseIndex->urlEncode(),
        ]);
    }

    /**
     * @throws NotImplementedException
     */
    public function getTableRowByFieldPathFromTableInfo(
        TableInfo $tableInfo,
        string $columnAlias,
        DatabaseValue $databaseValue,
    ): string
    {
        return $this->router->generate('get_row_by', [
            'project' => $tableInfo->project->getName(),
            'tableAlias' => $tableInfo->name,
            'columnAlias' => $columnAlias,
            'value' => $databaseValue->urlEncode(),
        ]);
    }

    public function getTableStructurePathFromTableInfo(TableInfo $tableInfo): string
    {
        return $this->router->generate('get_table_structure', [
            'project' => $tableInfo->project->getName(),
            'tableAlias' => $tableInfo->name,
        ]);
    }

    public function getInvalidateCachePath(?Project $project, ?TableInfo $tableInfo): string
    {
        if ($tableInfo !== null) {
            return $this->router->generate('cache_invalidate_table', [
                'project' => $tableInfo->project->getName(),
                'tableAlias' => $tableInfo->name,
            ]);
        }

        if ($project !== null) {
            return $this->router->generate('cache_invalidate_project', [
                'project' => $project->getName(),
            ]);
        }

        return $this->router->generate('cache_invalidate_all');
    }
}
