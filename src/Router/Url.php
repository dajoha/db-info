<?php

declare(strict_types=1);

namespace App\Router;

use Symfony\Component\HttpFoundation\Request;

class Url
{
    /**
     * @param array<string, string> $query
     */
    public function __construct(
        protected string $path,
        protected array $query = [],
    ) {
    }

    public static function fromRequest(Request $request): self
    {
        return new self($request->getPathInfo(), $request->query->all());
    }

    public function toString(): string
    {
        $url = $this->path;
        if (!empty($this->query)) {
            $query = http_build_query($this->query);
            $query = preg_replace(['/%5B/', '/%5D/'], ['[', ']'], $query);
            $url .= "?$query";
        }

        return $url;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): Url
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return array<string, string>
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array<string, string> $query
     */
    public function setQuery(array $query): Url
    {
        $this->query = $query;

        return $this;
    }
}
