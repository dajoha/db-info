<?php

declare(strict_types=1);

namespace App\Router\Util;

use App\Exception\NotImplementedException;
use App\Project\Data\TableInfo;
use App\Project\Model\Exception\BadPathJoinColumnException;
use App\Project\Model\Path\ColumnPath;
use Dajoha\Iter\Generator\Zip;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types;
use Doctrine\DBAL\Types\Type;
use Psr\Cache\InvalidArgumentException;

/**
 * Util methods which concern the generation/parse of application route URLs.
 */
class UrlUtil
{
    private function __construct()
    {
    }

    /**
     * Decodes a value provided in a URL for a given database table column. Indeed some values like
     * binary values or dates/times cannot be provided as raw values in URLs, so they must be
     * decoded when retrieving them.
     *
     * @throws NotImplementedException
     */
    public static function decodeDatabaseValue(string $urlValue, Type|Column $typeOrColumn): string|int|bool|float
    {
        $type = $typeOrColumn instanceof Column ? $typeOrColumn->getType() : $typeOrColumn;

        $typeClass = get_class($type);

        return match ($typeClass) {
            Types\AsciiStringType::class,
            Types\DateImmutableType::class,
            Types\DateTimeImmutableType::class,
            Types\DateTimeType::class,
            Types\DateTimeTzImmutableType::class,
            Types\DateTimeTzType::class,
            Types\DateType::class,
            Types\GuidType::class,
            Types\StringType::class,
            Types\TextType::class,
            Types\TimeImmutableType::class,
            Types\TimeType::class,
                => $urlValue,

            Types\BigIntType::class,
            Types\IntegerType::class,
            Types\SmallIntType::class,
                => (int) $urlValue,

            Types\FloatType::class,
            Types\SmallFloatType::class,
                => (float) $urlValue,

            Types\BinaryType::class,
                => hex2bin($urlValue),

            Types\BooleanType::class,
                => $urlValue === 'true',

            default => throw new NotImplementedException(
                sprintf('Parsing url value for type \"%s\" is not implemented', $typeClass)
            )
        };
    }

    /**
     * Decodes a value like {@see self::decodeDatabaseValue()}, but instead of directly providing
     * the column type, given a table info with column path instead.
     *
     * @throws InvalidArgumentException
     * @throws NotImplementedException
     * @throws BadPathJoinColumnException
     * @throws Exception
     */
    public static function decodeDatabaseValueFromPath(
        string $urlValue,
        TableInfo $tableInfo,
        string|ColumnPath $columnPath,
    ): string|int|bool|float
    {
        if (is_string($columnPath)) {
            $columnPath = $tableInfo->createColumnPath($columnPath);
        }

        $lastTableInfo = $columnPath->getLastTableInfo();
        $column = $lastTableInfo->getColumn($columnPath->getValueColumnName());

        if ($column !== null) {
            return self::decodeDatabaseValue($urlValue, $column);
        }

        return $urlValue;
    }

    /**
     * Decodes a list of values separated by '/', by assuming that they match the count and types of
     * the primary key defined for the given table.
     *
     * @return list<mixed>
     * @throws Exception
     */
    public static function decodePrimaryKeyIndex(TableInfo $tableInfo, string $urlIndex): array
    {
        $urlValues = explode('/', $urlIndex);

        $columns = iter($tableInfo->getPrimaryColumnNames() ?? [])
            ->map(fn($columnName) => $tableInfo->getColumnOrThrow($columnName));

        return Zip::new($urlValues, $columns)
            ->map(function (array $data) {
                /** @var string $urlValue */
                /** @var Column $column */
                [$urlValue, $column] = $data;

                return UrlUtil::decodeDatabaseValue($urlValue, $column);
            })
            ->toValues()
        ;
    }

    /**
     * Encodes a value in order to use it in a URL for a given database table column. Indeed some
     * values like binary values or dates/times cannot be provided as raw values in URLs, so they
     * must be transformed.
     *
     * @throws NotImplementedException
     */
    public static function encodeDatabaseValue(mixed $value, Type|Column $typeOrColumn): string
    {
        $type = $typeOrColumn instanceof Column ? $typeOrColumn->getType() : $typeOrColumn;

        $typeClass = get_class($type);

        return match ($typeClass) {
            Types\AsciiStringType::class,
            Types\BigIntType::class,
            Types\DateImmutableType::class,
            Types\DateTimeImmutableType::class,
            Types\DateTimeType::class,
            Types\DateTimeTzImmutableType::class,
            Types\DateTimeTzType::class,
            Types\DateType::class,
            Types\DecimalType::class,
            Types\FloatType::class,
            Types\GuidType::class,
            Types\IntegerType::class,
            Types\SmallFloatType::class,
            Types\SmallIntType::class,
            Types\StringType::class,
            Types\TextType::class,
            Types\TimeImmutableType::class,
            Types\TimeType::class,
                => (string) $value,

            Types\BinaryType::class,
                => self::stringifyBinary($value),

            Types\BooleanType::class,
                => self::stringifyBool($value),

            default => throw new NotImplementedException(
                sprintf('Stringify to url value for type \"%s\" is not implemented', $typeClass)
            )
        };
    }

    /**
     * @param bool $bool
     */
    protected static function stringifyBool(mixed $bool): string
    {
        return $bool ? 'true' : 'false';
    }

    /**
     * @param string $value
     */
    protected static function stringifyBinary(mixed $value): string
    {
        return bin2hex($value);
    }
}
