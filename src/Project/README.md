Gives access to centralized information about projects and their tables:
 * database schema information;
 * project configuration;
 * project's table configuration;
 * table's column configuration...

A central service here is `ProjectManager`, which provides some (cached) `Project` instances, which
in turn provide all project-related information (like listed just above).
