<?php

declare(strict_types=1);

namespace App\Project\Model\Path;

use App\Project\Data\TableInfo;

class JoinColumnPath extends AbstractPath implements PathInterface
{
    public static function fromString(
        string $columnAliases,
        bool $aliasAllowed,
        TableInfo $tableInfo,
    ): self
    {
        $columnAliases = explode('.', $columnAliases);

        return new self($columnAliases, $aliasAllowed, $tableInfo);
    }

    protected function getJoinColumnAliases(): array
    {
        return $this->columnAliases;
    }
}
