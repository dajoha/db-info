<?php

declare(strict_types=1);

namespace App\Project\Model\Path;

use App\Project\Data\TableInfo;
use App\Project\Model\Exception\BadPathJoinColumnException;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;

class ColumnPath extends AbstractPath implements PathInterface
{
    protected ?string $valueColumnName = null;

    public static function fromString(
        string $columnAliases,
        bool $aliasAllowed,
        TableInfo $tableInfo,
    ): self
    {
        $columnAliases = explode('.', $columnAliases);

        return new self($columnAliases, $aliasAllowed, $tableInfo);
    }

    /**
     * @throws BadPathJoinColumnException
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function getValueColumnName(): string
    {
        if ($this->valueColumnName !== null) {
            return $this->valueColumnName;
        }

        $this->getJoinColumnNames();

        $this->valueColumnName = $this->columnAliases[count($this->columnAliases) - 1];
        if ($this->aliasAllowed) {
            $this->valueColumnName = $this->lastTableInfo->getColumnNameFromAlias($this->valueColumnName);
        }

        return $this->valueColumnName;
    }

    protected function getJoinColumnAliases(): array
    {
        return array_slice($this->columnAliases, 0, -1);
    }
}
