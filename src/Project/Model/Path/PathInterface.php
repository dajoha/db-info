<?php

declare(strict_types=1);

namespace App\Project\Model\Path;

use App\Project\Data\TableInfo;
use Stringable;

interface PathInterface extends Stringable
{
    public function toString(): string;

    public function getTableInfo(): TableInfo;

    /**
     * @return string[]
     */
    public function getJoinColumnNames(): array;
}
