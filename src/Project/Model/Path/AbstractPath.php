<?php

declare(strict_types=1);

namespace App\Project\Model\Path;

use App\Project\Data\TableInfo;
use App\Project\Model\Exception\BadPathJoinColumnException;
use App\Project\Project;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;

abstract class AbstractPath implements PathInterface
{
    /** @var string[]|null  */
    protected ?array $joinColumnNames = null;

    protected ?TableInfo $lastTableInfo = null;

    protected readonly Project $project;

    /**
     * @return string[]
     */
    abstract protected function getJoinColumnAliases(): array;

    public function __construct(
        /** @var string[] */
        public readonly array $columnAliases,
        public readonly bool $aliasAllowed,
        public readonly TableInfo $tableInfo,
    ) {
        $this->project = $this->tableInfo->project;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return implode('.', $this->columnAliases);
    }

    /**
     * @return string[]
     *
     * @phpstan-assert !null $this->lastTableInfo
     *
     * @throws Exception
     * @throws BadPathJoinColumnException
     * @throws InvalidArgumentException
     */
    public function getJoinColumnNames(): array
    {
        if ($this->joinColumnNames !== null) {
            return $this->joinColumnNames;
        }

        $tableInfo = $this->tableInfo;
        $joinColumnNames = [];

        foreach ($this->getJoinColumnAliases() as $joinColumnAlias) {
            $joinColumnName = $joinColumnAlias;
            if ($this->aliasAllowed) {
                $joinColumnName = $tableInfo->getJoinColumnNameFromAlias($joinColumnAlias);
            }
            $joinColumnNames[] = $joinColumnName;
            $joinColumn = $tableInfo->getJoinColumn($joinColumnName);
            if ($joinColumn === null) {
                throw new BadPathJoinColumnException($this, $joinColumnName);
            }
            $tableInfo = $this->project->getTableInfo($joinColumn->tableName);
        }

        $this->lastTableInfo = $tableInfo;
        $this->joinColumnNames = $joinColumnNames;

        return $this->joinColumnNames;
    }

    /**
     * @throws BadPathJoinColumnException
     * @throws Exception|InvalidArgumentException
     */
    public function getLastTableInfo(): TableInfo
    {
        // Set $this->>lastTableInfo is not already defined:
        $this->getJoinColumnNames();

        return $this->lastTableInfo;
    }

    public function getTableInfo(): TableInfo
    {
        return $this->tableInfo;
    }
}
