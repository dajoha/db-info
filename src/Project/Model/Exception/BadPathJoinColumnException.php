<?php

declare(strict_types=1);

namespace App\Project\Model\Exception;

use App\Exception\AppException;
use App\Project\Model\Path\PathInterface;
use Throwable;

class BadPathJoinColumnException extends AppException
{
    public function __construct(
        public readonly PathInterface $path,
        public readonly string $badColumnName,
        int $code = 0,
        ?Throwable $previous = null,
    )
    {
        $tableName = $this->path->getTableInfo()->name;
        $message = "In table \"$tableName\": Path \"$path\": \"$badColumnName\" is not a join column name";
        parent::__construct($message, $code, $previous);
    }
}
