<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Project\Project;

interface ProjectAwareExceptionInterface
{
    public function getProject(): Project;
}
