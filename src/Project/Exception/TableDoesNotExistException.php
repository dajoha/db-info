<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Exception\AppException;
use App\Project\Project;
use Throwable;

class TableDoesNotExistException extends AppException implements ProjectAwareExceptionInterface
{
    public function __construct(
        public readonly Project $project,
        public readonly string $tableName,
        int $code = 0,
        ?Throwable $previous = null,
    )
    {
        $projectName = $project->getName();
        $message = "Table \"$tableName\" does not exist in project \"$projectName\"";

        parent::__construct($message, $code, $previous);
    }

    public function getProject(): Project
    {
        return $this->project;
    }
}
