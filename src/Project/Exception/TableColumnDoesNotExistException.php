<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Exception\AppException;
use Throwable;

class TableColumnDoesNotExistException extends AppException
{
    public function __construct(
        public readonly string $tableName,
        public readonly string $columnName,
        int $code = 0,
        ?Throwable $previous = null,
    )
    {
        $message = "Column \"$columnName\" does not exist in table \"$tableName\"";
        parent::__construct($message, $code, $previous);
    }
}
