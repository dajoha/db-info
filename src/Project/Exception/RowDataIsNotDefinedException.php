<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Exception\AppException;
use Throwable;

class RowDataIsNotDefinedException extends AppException
{
    public function __construct(
        public readonly string $tableName,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message = "Row data has not been retrieved from the database (table name: \"$tableName\")";
        parent::__construct($message, $code, $previous);
    }
}
