<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Exception\AppException;
use Throwable;

class ProjectDoesNotExistException extends AppException
{
    public function __construct(
        public readonly string $projectName,
        int $code = 0,
        ?Throwable $previous = null,
    )
    {
        $message = "Project \"$projectName\" does not exist";
        parent::__construct($message, $code, $previous);
    }
}
