<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Exception\AppException;
use Doctrine\DBAL\Exception\ConnectionException;
use Throwable;

/**
 * A wrapper for {@see ConnectionException}.
 */
class DatabaseConnectionException extends AppException
{
    public function __construct(
        Throwable $previous,
        int $code = 0,
    ) {
        parent::__construct($previous->getMessage(), $code, $previous);
    }
}
