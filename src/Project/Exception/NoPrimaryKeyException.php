<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Exception\AppException;
use Throwable;

class NoPrimaryKeyException extends AppException
{
    public function __construct(
        public readonly string $tableName,
        int $code = 0,
        ?Throwable $previous = null,
    )
    {
        $message = "No primary key was found for table \"$tableName\"";
        parent::__construct($message, $code, $previous);
    }
}
