<?php

declare(strict_types=1);

namespace App\Project\Exception;

use App\Exception\AppException;
use Throwable;

class ForeignKeyWasNotFoundException extends AppException
{
    public function __construct(
        public readonly string $tableName,
        public readonly string $columnName,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message = "No foreign key was found for table \"$tableName\", column \"$columnName\"";
        parent::__construct($message, $code, $previous);
    }
}
