<?php

declare(strict_types=1);

namespace App\Project\Data;

use App\Cache\Cache;
use App\Cache\CacheKey\Table\JoinPathsForListQueryCacheKey;
use App\Cache\CacheKey\Table\TableJoinColumnsCacheKey;
use App\Cache\CacheKey\Table\TableListColumnsCacheKey;
use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\Definition\Enum\ListColumnsMode;
use App\Configuration\Model\SortDirection;
use App\Configuration\ProjectConfig;
use App\Frontend\Model\JoinColumn;
use App\Frontend\Model\TableListColumn;
use App\Frontend\ValueTransformer\TransformerChain;
use App\Helper\StringFormatter;
use App\Project\Exception\ForeignKeyWasNotFoundException;
use App\Project\Exception\NoPrimaryKeyException;
use App\Project\Exception\TableColumnDoesNotExistException;
use App\Project\Exception\TableDoesNotExistException;
use App\Project\Model\Exception\BadPathJoinColumnException;
use App\Project\Model\Path\ColumnPath;
use App\Project\Model\Path\JoinColumnPath;
use App\Project\Project;
use App\Repository\Helper\QueryHelper;
use App\Repository\QueryPart\Sort\Sort;
use App\Repository\QueryPart\Sort\Sorts;
use Doctrine\DBAL\Exception as DbalException;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Doctrine\DBAL\Schema\Index;
use Doctrine\DBAL\Schema\Table;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\InvalidArgumentException;
use RuntimeException;

/**
 * Used everywhere to hold information about a given table. Its mixes information given by Doctrine
 * DBAL, and the "db_info" config.
 *
 * It's possible to retrieve a cached version of a "TableInfo", by using {@see Project::getTableInfo()}.
 *
 * @phpstan-import-type Type_UserConfig_TableListColumns from ConfigurationSchema
 */
class TableInfo
{
    public readonly ProjectConfig $projectConfig;

    /** @var ForeignKeyConstraint[]|null */
    protected ?array $joinableColumns = null;

    /** @var array<string, JoinColumn>|null */
    protected ?array $joinColumns = null;

    /** @var TableListColumn[]|null  */
    protected ?array $listColumns = null;

    /** @var string[]|null */
    protected ?array $columnNames = null;

    /** @var array<string, true>|null */
    protected ?array $columnNamesByKey = null;

    /** @var array<string, Column> */
    protected ?array $columnsByName = null;

    /**
     * Primary key is bool which indicates if it's quoted or non-quoted version.
     *
     * @var array<int, null|string[]>
     */
    protected array $primaryKeyColumnNames = [];

    /**
     * Non-quoted primary key column names, stored as array keys.
     *
     * @var array<string, true>
     */
    protected ?array $primaryKeyColumnNamesAsKeys = null;

    /** @var array<string, StringFormatter> */
    protected array $stringFormatters = [];

    /** @var array<string, ?TransformerChain> */
    protected array $transformerChains = [];

    protected ?Sorts $defaultSorts = null;

    public function __construct(
        public readonly Project $project,
        public readonly string $name,
        protected readonly Cache $cache,
    ) {
        $this->projectConfig = $this->project->config;
    }

    /**
     * @throws DbalException
     */
    public function __sleep(): array
    {
        $this->getColumnNames();
        $this->getStringFormatter('short');
        $this->getStringFormatter('tiny');

        return [
            'name',
            'columnNames',
            'formatStrings',
        ];
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Returns the related Doctrine table instance.
     *
     * @throws DbalException
     */
    public function getTable(): Table
    {
        return $this->project->getTable($this->name);
    }

    /**
     * @throws DbalException
     */
    public function getColumn(string $columnName): ?Column
    {
        return $this->getColumnsByName()[$columnName] ?? null;
    }

    /**
     * @throws DbalException
     * @throws TableColumnDoesNotExistException
     */
    public function getColumnOrThrow(string $columnName): Column
    {
        $column = $this->getColumnsByName()[$columnName] ?? null;

        if ($column === null) {
            throw new TableColumnDoesNotExistException($this->name, $columnName);
        }

        return $column;
    }

    /**
     * @return array<string, Column>
     * @throws DbalException
     */
    public function getColumnsByName(): array
    {
        if ($this->columnsByName !== null) {
            return $this->columnsByName;
        }

        $this->columnsByName = iter($this->getTable()->getColumns())
            ->mapKeys(fn($_, Column $column) => $column->getName())
            ->toArray();

        return $this->columnsByName;
    }

    /**
     * @throws TableDoesNotExistException
     * @throws InvalidArgumentException
     */
    public function assertTableExists(): void
    {
        if (!$this->project->tableNameExists($this->name)) {
            throw new TableDoesNotExistException($this->project, $this->name);
        }
    }

    /**
     * Returns the names of all the columns in the table.
     *
     * @return string[]
     * @throws DbalException
     */
    public function getColumnNames(): array
    {
        if ($this->columnNames !== null) {
            return $this->columnNames;
        }

        $this->columnNames = iter($this->getTable()->getColumns())
            ->map(fn(Column $column) => $column->getName())
            ->toValues()
        ;

        return $this->columnNames;
    }

    /**
     * Gets the Doctrine "Column" instance of a column, from a column name or alias.
     *
     * @throws DbalException
     * @throws TableColumnDoesNotExistException
     */
    public function getColumnFromAliasOrThrow(string $columnAlias): Column
    {
        $columnName = $this->getColumnNameFromAlias($columnAlias);


        return $this->getColumnOrThrow($columnName);
    }

    /**
     * Gets the Doctrine primary key instance for this table.
     *
     * @throws DbalException|NoPrimaryKeyException
     */
    public function getPrimaryKeyOrThrow(): Index
    {
        $primaryKey = $this->getTable()->getPrimaryKey();
        if (null === $primaryKey) {
            throw new NoPrimaryKeyException($this->name);
        }

        return $primaryKey;
    }

    /**
     * Gets the list of the primary key's column names.
     *
     * @return string[]
     *
     * @throws DbalException
     * @throws NoPrimaryKeyException
     */
    public function getPrimaryColumnNamesOrThrow(bool $quoted = false): array
    {
        $columnNames = $this->getPrimaryColumnNames($quoted);
        if ($columnNames === null) {
            throw new NoPrimaryKeyException($this->name);
        }

        return $columnNames;
    }

    /**
     * Gets the list of the primary key's column names. Returns `null` instead of throwing an
     * exception when there is no primary key.
     *
     * @return string[]|null
     *
     * @throws DbalException
     */
    public function getPrimaryColumnNames(bool $quoted = false): ?array
    {
        if (key_exists((int) $quoted, $this->primaryKeyColumnNames)) {
            return $this->primaryKeyColumnNames[$quoted];
        }

        $primaryKey = $this->getTable()->getPrimaryKey();
        if ($quoted) {
            $columnNames = $primaryKey?->getQuotedColumns($this->project->getPlatform());
        } else {
            $columnNames = $primaryKey?->getColumns();
        }

        $this->primaryKeyColumnNames[$quoted] = $columnNames;

        return $columnNames;
    }

    /**
     * Checks if a given column name is a primary key.
     *
     * @throws DbalException
     */
    public function hasPrimaryKey(string $columnName): bool
    {
        return key_exists($columnName, $this->getPrimaryColumnNamesAsKeys());
    }

    /**
     * Gets the list of the primary key's column names, as array keys.
     *
     * @return array<string, true>
     *
     * @throws DbalException
     */
    public function getPrimaryColumnNamesAsKeys(): array
    {
        if ($this->primaryKeyColumnNamesAsKeys !== null) {
            return $this->primaryKeyColumnNamesAsKeys;
        }

        $columnNames = $this->getPrimaryColumnNames() ?? [];
        $this->primaryKeyColumnNamesAsKeys = array_fill_keys($columnNames, true);

        return $this->primaryKeyColumnNamesAsKeys;
    }

    /**
     * Returns true if the primary key is only composed by one column.
     *
     * @throws DbalException
     *
     * @noinspection PhpUnused Used in Twig template
     */
    public function isSinglePrimaryKey(): bool
    {
        $columns = $this->getTable()->getPrimaryKey()?->getColumns() ?? [];

        return count($columns) === 1;
    }

    /**
     * Returns the primary key's column name, only if it's a single-column key.
     *
     * @throws DbalException
     */
    public function getSinglePrimaryColumnName(bool $quoted = false): ?string
    {
        $columns = $this->getPrimaryColumnNames($quoted);

        return $columns === null || count($columns) !== 1 ? null : $columns[0];
    }

    /**
     * Finds the Doctrine foreign key definition for the given column. For now, foreign keys with
     * multiple columns are not handled.
     *
     * @throws DbalException
     */
    public function findForeignKeyFor(string $columnName): ?ForeignKeyConstraint
    {
        return $this->getForeignKeysByColumnName()[$columnName] ?? null;
    }

    /**
     * Returns information on each foreign table referencing the actual table.
     *
     * @return array<string, list<array{
     *     localColumnName: string,
     *     foreignColumnName: string,
     * }>>
     *
     * @throws InvalidArgumentException
     *
     * @noinspection PhpUnused Used in Twig templates
     */
    public function getRelatedTables(): array
    {
        return $this->project->getRelatedTablesByTargetTable()[$this->name] ?? [];
    }

    /**
     * @return string[]
     */
    public function getAliases(): array
    {
        return $this->projectConfig->getTableConfig($this->name)[K::TABLE__ALIASES] ?? [];
    }

    /**
     * Returns the "best alias" of the table, aka. either the first table alias, or the table name
     * itself if no aliases are defined for this table.
     */
    public function getBestAlias(): string
    {
        return $this->projectConfig->getTableBestAlias($this->name);
    }

    /**
     * Returns the "best alias" for the given column name, aka. either the first column alias, or
     * the column name itself if no aliases are defined for this column. Aliases are handled for the
     * input parameter.
     *
     * @throws DbalException
     */
    public function getColumnBestAlias(string $columnAlias): string
    {
        $columnName = $this->getColumnNameFromAlias($columnAlias);

        return $this->projectConfig->getColumnBestAlias($this->name, $columnName);
    }

    /**
     * Resolves the real column name for the given column alias. Real column names have priority
     * over aliases.
     *
     * @throws DbalException
     */
    public function getColumnNameFromAlias(string $columnAlias): string
    {
        if ($this->columnNameExists($columnAlias)) {
            return $columnAlias;
        }

        return $this->projectConfig->getColumnNameFromAlias($this->name, $columnAlias);
    }

    public function getTransformerChainForColumn(string $columnName): ?TransformerChain
    {
        if (isset($this->transformerChains[$columnName])) {
            return $this->transformerChains[$columnName];
        }

        $chains = $this->projectConfig->getColumnsTransformerChains();
        $transformerChain = $chains[$this->name][$columnName] ?? null;

        if ($transformerChain === null) {
            $this->transformerChains[$columnName] = null;
        } else {
            $this->transformerChains[$columnName] = unserialize($transformerChain);
        }

        return $this->transformerChains[$columnName];
    }

    /**
     * @return list<string|int>|null
     */
    public function getPossibleValuesForColumn(string $columnName): ?array
    {
        $projectPossibleValues = $this->projectConfig->getColumnsPossibleValues();

        return $projectPossibleValues[$this->name][$columnName] ?? null;
    }

    /**
     * Resolves the real column name for the given join-column alias. Real column names have
     * priority over aliases.
     *
     * @throws DbalException
     */
    public function getJoinColumnNameFromAlias(string $joinColumnAlias): string
    {
        if ($this->columnNameExists($joinColumnAlias)) {
            return $joinColumnAlias;
        }

        return $this->projectConfig->getJoinColumnName($this->name, $joinColumnAlias);
    }

    /**
     * Returns the (general) list of columns to hide, as the keys of an array.
     *
     * @return array<string, true>
     */
    public function getHiddenColumns(): array
    {
        return $this->projectConfig->getTableHiddenColumns($this->name);
    }

    /**
     * Returns the list of columns to hide in listings, as the keys of an array. Only used in list
     * mode `ListColumnsMode::All`. They include the general list of hidden columns defined for the
     * table.
     *
     * @see ListColumnsMode
     *
     * @return array<string, true>
     */
    public function getListHiddenColumns(): array
    {
        return $this->projectConfig->getTableListHiddenColumns($this->name);
    }

    /**
     * Returns the list column definitions by column name. Note that all the definitions are not
     * present, but only those for which the 'column' property has been defined.
     *
     * @see ListColumnsMode
     *
     * @return array<string, array{
     *             column: ?string,
     *             format: ?string,
     *             title: ?string,
     *         }>
     */
    public function getListColumnsByColumn(): array
    {
        return $this->projectConfig->getTableListColumnsByColumn($this->name);
    }

    /**
     * Returns the `JoinColumn` information struct for the given column name (aliases are not
     * handled). If the given column is not a foreign key, then returns `null`.
     *
     * @throws InvalidArgumentException
     */
    public function getJoinColumn(string $columnName): ?JoinColumn
    {
        return $this->getAllJoinColumns()[$columnName] ?? null;
    }

    /**
     * Returns the `JoinColumn` information struct for the given column name (aliases are not
     * handled). If the given column is not a foreign key, then throws an exception.
     *
     * @throws ForeignKeyWasNotFoundException
     * @throws InvalidArgumentException
     */
    public function getJoinColumnOrThrow(string $columnName): JoinColumn
    {
        $joinColumn = $this->getJoinColumn($columnName);

        if ($joinColumn === null) {
            throw new ForeignKeyWasNotFoundException($this->name, $columnName);
        }

        return $joinColumn;
    }

    /**
     * Return the names of all the columns linked to a (simple) foreign key.
     *
     * @return string[]
     *
     * @throws DbalException
     */
    public function getJoinColumnNames(): array
    {
        return array_keys($this->getForeignKeysByColumnName());
    }

    /**
     * Returns all the `JoinColumn` information structs for the table (one element per foreign key).
     *
     * @return array<string, JoinColumn>
     *
     * @throws InvalidArgumentException
     */
    public function getAllJoinColumns(): array
    {
        if ($this->joinColumns !== null) {
            return $this->joinColumns;
        }

        $this->joinColumns = $this->cache->get(
            new TableJoinColumnsCacheKey($this),
            function(CacheItemInterface $item) {
                $item->expiresAfter(3600);

                return iter($this->getForeignKeysByColumnName())
                    ->mapKeyValues(
                        function ($columnName) {
                            /** @var JoinColumn $joinColumn */
                            $joinColumn = $this->_getJoinColumn($columnName);

                            return [$columnName, $joinColumn];
                        },
                    )
                    ->toArray()
                ;
            },
        );

        return $this->joinColumns;
    }

    /**
     * Returns the `JoinColumn` information struct for the given column name (aliases are not
     * handled). If the given column is not a foreign key, then returns `null`.
     *
     * @throws DbalException
     * @throws TableColumnDoesNotExistException
     */
    protected function _getJoinColumn(string $columnName): ?JoinColumn
    {
        $foreignKey = $this->findForeignKeyFor($columnName);

        if ($foreignKey === null) {
            return null;
        }

        $foreignTableName = $foreignKey->getForeignTableName();
        // We already know that there is one and only one column, so [0] is ok:
        $foreignColumnName = $foreignKey->getForeignColumns()[0];
        $foreignTableInfo = $this->project->getTableInfo($foreignTableName);
        $foreignTableBestAlias = $foreignTableInfo->getBestAlias();
        $foreignPrimaryColumnName = $foreignTableInfo->getSinglePrimaryColumnName();
        $foreignColumnIsPrimaryKey = $foreignColumnName === $foreignPrimaryColumnName;
        $foreignColumnType = $foreignTableInfo->getColumnOrThrow($foreignColumnName)->getType();

        return new JoinColumn(
            $foreignTableName,
            $foreignTableBestAlias,
            $foreignColumnName,
            $foreignColumnIsPrimaryKey,
            $foreignColumnType,
        );
    }

    /**
     * Returns the list of `TableListColumn` information structs, used in Twig templates to render
     * the table listings. Depending on the list mode, the number of columns will largely differ.
     *
     * @return TableListColumn[]
     * @throws DbalException
     * @throws InvalidArgumentException
     *
     * @noinspection PhpDocRedundantThrowsInspection
     */
    public function getTableListColumns(): array
    {
        if ($this->listColumns !== null) {
            return $this->listColumns;
        }

        $this->listColumns = $this->cache->get(
            new TableListColumnsCacheKey($this),
            function(CacheItemInterface $item) {
                $item->expiresAfter(3600);
                return $this->_getTableListColumns();
            },
        );

        return $this->listColumns;
    }

    /**
     * No-cache version.
     *
     * @return TableListColumn[]
     * @throws DbalException
     *@see self::getTableListColumns()
     *
     */
    protected function _getTableListColumns(): array
    {
        $listColumnsConfig = $this->getListColumnsConfig();
        $mode = ListColumnsMode::from($listColumnsConfig[K::TABLE__LIST_COLUMNS__MODE]);
        $listColumns = null;

        switch ($mode) {
            case ListColumnsMode::Summary:
                $formatString = $this->getSummaryTiny() ?? $this->getSummaryShort();
                // @phpstan-ignore argument.type (OK Validated in config: $formatString cannot be `null`)
                $listColumns = [ new TableListColumn('summary', $formatString) ];
                break;
            case ListColumnsMode::Format:
                $formatString = $listColumnsConfig[K::TABLE__LIST_COLUMNS__FORMAT];
                // @phpstan-ignore argument.type (OK Validated in config: $formatString cannot be `null`)
                $listColumns = [ new TableListColumn('summary', $formatString) ];
                break;
            case ListColumnsMode::All:
                $columnsToHide = $this->getListHiddenColumns();
                $listColumns = iter($this->getTable()->getColumns())
                    ->filter(fn(Column $column) => !key_exists($column->getName(), $columnsToHide))
                    ->map(function (Column $column) {
                        $columnName = $column->getName();
                        $listColumnDef = $this->getListColumnsByColumn()[$columnName] ?? null;
                        $format = $listColumnDef[K::TABLE__LIST_COLUMNS__COLUMNS__FORMAT] ?? null;
                        $format ??= '{' . $columnName . '}';
                        $columnBestAlias = $this->getColumnBestAlias($columnName);

                        return new TableListColumn(title: $columnBestAlias, format: $format, column: $column);
                    })
                    ->toValues()
                ;
                break;
            case ListColumnsMode::Listed:
                $columnsToShow = $listColumnsConfig[K::TABLE__LIST_COLUMNS__COLUMNS];
                $listColumns = iter($columnsToShow)
                    ->map(function ($columnConfig) {
                        $columnAlias = $columnConfig[K::TABLE__LIST_COLUMNS__COLUMNS__COLUMN] ?? null;

                        [$columnName, $column] = [null, null];
                        if ($columnAlias !== null) {
                            $column = $this->getColumnFromAliasOrThrow($columnAlias);
                            $columnName = $column->getName();
                        }

                        return new TableListColumn(
                            title: $columnConfig[K::TABLE__LIST_COLUMNS__COLUMNS__TITLE]
                                ?? $this->getColumnBestAlias($columnName), // @phpstan-ignore argument.type (OK: Validated by schema)
                            format: $columnConfig[K::TABLE__LIST_COLUMNS__COLUMNS__FORMAT]
                                ?? '{'.$columnName.'}',
                            column: $column,
                        );
                    })
                    ->toValues()
                ;
                break;
        }

        return $listColumns;
    }

    /**
     * @throws DbalException
     * @throws BadPathJoinColumnException
     * @throws InvalidArgumentException
     */
    public function getTableInfoFromJoinColumnPath(JoinColumnPath $joinColumnPath): ?TableInfo
    {
        $tableInfo = $this;
        foreach ($joinColumnPath->getJoinColumnNames() as $joinColumnName) {
            $joinColumn = $this->getJoinColumn($joinColumnName);
            if ($joinColumn === null) {
                return null;
            }
            $tableInfo = $this->project->getTableInfo($joinColumn->tableName);
        }

        return $tableInfo;
    }

    /**
     * Returns all the join-column paths needed to format the values of a row, when listing the
     * table on frontend.
     *
     * @param string $formatStyle The style needed for summaries, typically = 'tiny'
     *
     * @return array<string, string[]> All the string arrays are guaranteed to be non-empty.
     *
     * @throws DbalException
     * @throws InvalidArgumentException
     *
     * @noinspection PhpDocRedundantThrowsInspection
     */
    public function getJoinColumnPathsForListQuery(string $formatStyle = 'tiny'): array
    {
        return $this->cache->get(
            new JoinPathsForListQueryCacheKey($this, $formatStyle),
            function(CacheItemInterface $item) use ($formatStyle) {
                $item->expiresAfter(3600);
                return $this->_getJoinColumnPathsForListQuery($formatStyle);
            },
        );
    }

    public function getDefaultSorts(): Sorts
    {
        if ($this->defaultSorts === null) {
            $defaultSorts = [];
            $position = 0;
            $defaultColumnOrders = $this->projectConfig->getTableDefaultColumnOrders($this->name);

            /**
             * @var string $columnName
             * @var SortDirection $direction
             */
            foreach ($defaultColumnOrders as [$columnName, $direction]) {
                $optDirection = $direction->toOptSortDirection();
                $defaultSorts[] = new Sort($columnName, $optDirection, $position++);
            }

            $this->defaultSorts = new Sorts($defaultSorts);
        }

        return $this->defaultSorts;
    }

    public function getDefaultPaginationSize(): int
    {
        return $this->projectConfig->getDefaultPaginationSize($this->name);
    }

    /**
     * No-cache version of {@see self::getJoinColumnPathsForListQuery()}.
     *
     * @return array<string, string[]>
     *
     * @throws DbalException
     * @throws InvalidArgumentException
     */
    protected function _getJoinColumnPathsForListQuery(string $formatStyle): array
    {
        // Add joins needed for list column formats:
        $joins = iter($this->getTableListColumns())
            ->map(fn(TableListColumn $listColumn) => StringFormatter::findPlaceholders($listColumn->format))
            ->flatten()
            ->map(fn($pathString) => $this->createColumnPath($pathString)->getJoinColumnNames())
            ->filter(fn(array $joinColumnNames) => !empty($joinColumnNames))
            ->mapKeys(fn($_, $joinColumnNames) => QueryHelper::getJoinId($joinColumnNames))
            ->toArray()
        ;

        // Add joins needed for the summaries of join columns (columns with a FK):
        foreach ($this->getAllJoinColumns() as $joinColumnName => $joinColumn) {
            $tableInfo = $this->project->getTableInfo($joinColumn->tableName);
            $formatString = $tableInfo->getSummary($formatStyle);
            if ($formatString === null) {
                continue;
            }
            $joinsNeeded = iter($tableInfo->getJoinsNeededForFormat($formatString))
                ->map(fn(array $columnNames) => [$joinColumnName, ...$columnNames])
                ->mapKeys(fn($_, $columnNames) => QueryHelper::getJoinId($columnNames))
                ->toArray()
            ;
            $joins = array_merge($joins, $joinsNeeded);
        }

        return $joins;
    }

    /**
     * @return list<string[]>
     */
    public function getJoinsNeededForFormat(string $formatString): array
    {
        $joins = iter($this->getStringFormatter($formatString)->placeholders)
            ->map(fn(string $placeholder) => $this->createColumnPath($placeholder)->getJoinColumnNames())
            ->mapKeys(fn($_, array $joinColumnNames) => QueryHelper::getJoinId($joinColumnNames))
            ->toArray();

        return array_values($joins);
    }

    public function createColumnPath(string $pathString, bool $aliasAllowed = true): ColumnPath
    {
        return ColumnPath::fromString($pathString, $aliasAllowed, $this);
    }

    public function createJoinColumnPath(
        string $pathString,
        bool $aliasAllowed = true,
    ): JoinColumnPath
    {
        return JoinColumnPath::fromString($pathString, $aliasAllowed, $this);
    }

    /**
     * Returns the list column config of the table.
     *
     * @return Type_UserConfig_TableListColumns
     */
    public function getListColumnsConfig(): array
    {
        return $this->projectConfig->getTableListColumnsConfig($this->name);
    }

    public function getStringFormatter(string $formatString): StringFormatter
    {
        if (!key_exists($formatString, $this->stringFormatters)) {
            $this->stringFormatters[$formatString] = new StringFormatter($formatString);
        }

        return $this->stringFormatters[$formatString];
    }

    public function hasSummary(): bool
    {
        return $this->getSummaryShort() !== null || $this->getSummaryTiny() !== null;
    }

    public function getSummary(string $formatStyle): ?string
    {
        return match ($formatStyle) {
            'short' => $this->getSummaryShort() ?? $this->getSummaryTiny(),
            'tiny' => $this->getSummaryTiny() ?? $this->getSummaryShort(),
            default => throw new RuntimeException("Bad format style: \"$formatStyle\""),
        };
    }

    /**
     * Returns the short summary format string of the table, if defined.
     */
    public function getSummaryShort(): ?string
    {
        return $this->projectConfig->getTableSummaryShort($this->name);
    }

    /**
     * Returns the tiny summary format string of the table, if defined.
     */
    public function getSummaryTiny(): ?string
    {
        return $this->projectConfig->getTableSummaryTiny($this->name);
    }

    /**
     * Returns the column aliases for the table.
     *
     * @return array<string, string>
     */
    public function getColumnAliases(): array
    {
        return $this->projectConfig->getColumnAliases()[$this->name] ?? [];
    }

    /**
     * Returns the join column aliases for the table.
     *
     * @return array<string, string>
     */
    public function getJoinColumnAliases(): array
    {
        return $this->projectConfig->getJoinColumnAliases()[$this->name] ?? [];
    }

    /**
     * @throws DbalException
     * @throws TableColumnDoesNotExistException
     */
    public function assertColumnExists(string $columnName): void
    {
        if (!$this->getTable()->hasColumn($columnName)) {
            throw new TableColumnDoesNotExistException($this->name, $columnName);
        }
    }

    /**
     * @return array<string, ForeignKeyConstraint>
     *
     * @throws DbalException
     */
    protected function getForeignKeysByColumnName(): array
    {
        if ($this->joinableColumns !== null) {
            return $this->joinableColumns;
        }

        $table = $this->project->getTable($this->name);
        $foreignKeys = $table->getForeignKeys();
        $this->joinableColumns = [];

        foreach ($foreignKeys as $foreignKey) {
            $localColumns = $foreignKey->getLocalColumns();
            $foreignColumns = $foreignKey->getForeignColumns();
            if (count($localColumns) !== 1 || count($foreignColumns) !== 1) {
                continue;
            }
            $localColumn = $localColumns[0];
            $this->joinableColumns[$localColumn] = $foreignKey;
        }

        return $this->joinableColumns;
    }

    /**
     * @throws DbalException
     */
    protected function columnNameExists(string $columnName): bool
    {
        return key_exists($columnName, $this->getColumnNamesByKey());
    }

    /**
     * @return array<string, true>
     *
     * @throws DbalException
     */
    protected function getColumnNamesByKey(): array
    {
        if ($this->columnNamesByKey === null) {
            $this->columnNamesByKey = array_fill_keys($this->getColumnNames(), true);
        }

        return $this->columnNamesByKey;
    }
}
