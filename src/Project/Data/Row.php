<?php

declare(strict_types=1);

namespace App\Project\Data;

use App\Exception\NotImplementedException;
use App\Helper\CacheBag;
use App\Project\Exception\ForeignKeyWasNotFoundException;
use App\Project\Exception\RowDataIsNotDefinedException;
use App\Project\Exception\TableColumnDoesNotExistException;
use App\Project\Model\Path\PathInterface;
use App\Project\Project;
use App\Router\AppRouter;
use App\Router\Model\DatabaseValue;
use App\Router\Model\DatabaseIndex;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;

/**
 * Represents a row in a DB table, holding the data itself and also metadata/services which help
 * to simplify its representation.
 */
class Row
{
    protected CacheBag $cache;

    /** @var array<string, Row> */
    protected array $joinedRows = [];

    /**
     * @param array<string, mixed>|null $data
     */
    public function __construct(
        public readonly Project $project,
        public readonly TableInfo $tableInfo,
        protected ?array $data,
        protected readonly AppRouter $appRouter,
    ) {
        $this->cache = new CacheBag();
    }

    /**
     * @return  array<string, mixed>
     *
     * @throws RowDataIsNotDefinedException
     */
    public function getDataOrThrow(): array
    {
        if (!$this->hasData()) {
            throw new RowDataIsNotDefinedException($this->getTableName());
        }

        return $this->data;
    }

    /**
     * @phpstan-assert-if-true !null $this->data
     */
    public function hasData(): bool
    {
        return $this->data !== null;
    }

    /**
     * @param array<string, mixed> $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * Returns the primary key values of the row, which can easily be formatted as a string for use
     * in a URI.
     *
     * @throws Exception
     * @throws RowDataIsNotDefinedException
     * @throws TableColumnDoesNotExistException
     */
    public function getPrimaryKeyIndex(): DatabaseIndex
    {
        return $this->cache->get('primary_key_index', function() {
            $columnNames = $this->tableInfo->getPrimaryColumnNames() ?? [];
            $data = $this->getDataOrThrow();
            $values = iter($columnNames)->map(function (string $name) use ($data) {
                $column = $this->tableInfo->getColumnOrThrow($name);

                return new DatabaseValue($data[$name], $column->getType());
            });

            return DatabaseIndex::from($values->toValues());
        });
    }

    /**
     * Gets the value of the given column (column aliases are handled).
     *
     * @throws TableColumnDoesNotExistException
     * @throws RowDataIsNotDefinedException
     * @throws Exception
     */
    public function getValue(string $columnAlias): mixed
    {
        $columnName = $this->tableInfo->getColumnNameFromAlias($columnAlias);
        $data = $this->getDataOrThrow();
        if (!key_exists($columnName, $data)) {
            throw new TableColumnDoesNotExistException($this->getTableName(), $columnName);
        }

        return $data[$columnName];
    }

    /**
     * Gets the URL to the detailed row view.
     *
     * @throws Exception
     * @throws RowDataIsNotDefinedException
     * @throws TableColumnDoesNotExistException
     * @throws NotImplementedException
     */
    public function getUrl(): ?string
    {
        $primaryKeyIndex = $this->getPrimaryKeyIndex();
        if ($primaryKeyIndex->isEmpty()) {
            return null;
        }

        return $this->appRouter->getTableRowPathFromTableInfo($this->tableInfo, $primaryKeyIndex);
    }

    public function getTableName(): string
    {
        return $this->tableInfo->name;
    }

    /**
     * Returns a new row containing the data of the joined table referenced by `$path`.
     *
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     * @throws RowDataIsNotDefinedException
     */
    public function getJoinedRow(PathInterface $path): ?Row
    {
        $row = $this;

        foreach ($path->getJoinColumnNames() as $joinColumnName) {
            $row = $row->joinedOne($joinColumnName, false);
            if ($row === null) {
                return null;
            }
        }

        return $row;
    }

    /**
     * Adds the data of some joined rows. The join path can have multiple levels, which will create
     * nested joined rows.
     *
     * @param string[]             $path
     * @param array<string, mixed> $data
     *
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     * @throws InvalidArgumentException
     */
    public function addJoinedRow(array $path, array $data): void
    {
        assert(!empty($path));

        $isLastColumn = count($path) === 1;
        $firstColumnName = $path[0];

        // If the row has not been already defined, add a "phantom" row, without data:
        if (!key_exists($firstColumnName, $this->joinedRows)) {
            $joinColumn = $this->tableInfo->getJoinColumnOrThrow($firstColumnName);
            $this->joinedRows[$firstColumnName] = $this->createRow($joinColumn->tableName, null);
        }

        if ($isLastColumn) {
            $this->joinedRows[$firstColumnName]->setData($data);
        } else {
            array_shift($path);
            $this->joinedRows[$firstColumnName]->addJoinedRow($path, $data);
        }
    }

    /**
     * Used by @see getJoinedRow(): gets one one-level joined row.
     *
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     * @throws RowDataIsNotDefinedException
     */
    protected function joinedOne(string $columnNameAlias, bool $aliasAllowed = true): ?Row
    {
        $columnName = $aliasAllowed
            ? $this->tableInfo->getJoinColumnNameFromAlias($columnNameAlias)
            : $columnNameAlias;

        if (key_exists($columnName, $this->joinedRows) && $this->joinedRows[$columnName]->hasData()) {
            return $this->joinedRows[$columnName];
        }

        $foreignKey = $this->tableInfo->findForeignKeyFor($columnName);
        if (null === $foreignKey) {
            throw new ForeignKeyWasNotFoundException($this->getTableName(), $columnName);
        }

        $platform = $this->project->getPlatform();
        $foreignTableName = $foreignKey->getForeignTableName();
        $quotedForeignKeyColumnName = $foreignKey->getQuotedForeignColumns($platform)[0];

        $data = $this->project->getRepository()
            ->createQueryBuilder($foreignTableName)
            ->where("$quotedForeignKeyColumnName = :foreignKeyValue")
            ->setParameter('foreignKeyValue', $this->getDataOrThrow()[$columnName])
            ->setMaxResults(1)
            ->fetchAssociative()
        ;

        if (false === $data) {
            return null;
        }

        return $this->createRow($foreignTableName, $data);
    }

    /**
     * @param array<string, mixed> $data
     */
    protected function createRow(string $tableName, ?array $data): Row
    {
        return new Row(
            $this->project,
            $this->project->getTableInfo($tableName),
            $data,
            $this->appRouter,
        );
    }
}
