<?php

declare(strict_types=1);

namespace App\Project;

use App\Cache\Cache;
use App\Configuration\Service\ConfigProvider;
use App\Doctrine\ConnectionFactoryInterface;
use App\Project\Exception\ProjectDoesNotExistException;
use App\Project\Factory\RowFactory;
use Doctrine\DBAL\Exception;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Provides {@see Project} instances.
 */
class ProjectManager
{
    /** @var Project[] */
    protected array $projects = [];

    public function __construct(
        protected readonly Cache $cache,
        protected readonly ConfigProvider $configProvider,
        #[Autowire(lazy: true)]
        protected readonly RowFactory $rowFactory,
        protected readonly ConnectionFactoryInterface $connectionFactory,
    ) {
    }

    /**
     * @throws ProjectDoesNotExistException
     * @throws Exception
     */
    public function getProject(string $projectName): Project
    {
        $project = $this->projects[$projectName] ?? null;
        if ($project !== null) {
            return $project;
        }

        $projectConfig = $this->configProvider->getProjectConfig($projectName);
        if ($projectConfig === null) {
            throw new ProjectDoesNotExistException($projectName);
        }

        $this->projects[$projectName] = new Project(
            $projectConfig,
            $this->connectionFactory,
            $this->rowFactory,
            $this->cache,
        );

        return $this->projects[$projectName];
    }
}
