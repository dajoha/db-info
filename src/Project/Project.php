<?php

declare(strict_types=1);

namespace App\Project;

use App\Cache\Cache;
use App\Cache\CacheKey\Project\JoinsByTargetTableCacheKey;
use App\Cache\CacheKey\Project\SortedTableAliasesCacheKey;
use App\Cache\CacheKey\Project\SortedTableNamesCacheKey;
use App\Cache\CacheKey\Project\TableNamesByKeyCacheKey;
use App\Configuration\ProjectConfig;
use App\Doctrine\ConnectionFactoryInterface;
use App\Helper\CacheBag;
use App\Project\Data\TableInfo;
use App\Project\Exception\TableDoesNotExistException;
use App\Project\Factory\RowFactory;
use App\Repository\ProjectRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Tools\DsnParser;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\InvalidArgumentException;

/**
 * The central service which makes the bridge with Doctrine DBAL, for a given project.
 *
 * @psalm-import-type Params from DriverManager
 */
class Project
{
    /**
     * Mappings for database URL schemes, e.g.: "mysql://....." will be understood as "pdo_mysql://...."
     */
    protected const array SCHEME_MAPPINGS = [
        'mysql' => 'pdo_mysql',
        'postgres' => 'pdo_pgsql',
    ];

    /** @phpstan-var Params $connectionParams */
    protected readonly array $connectionParams;

    protected ?Connection $connection = null;

    /** @var AbstractSchemaManager<AbstractPlatform>|null $schemaManager */
    protected ?AbstractSchemaManager $schemaManager = null;

    protected ?ProjectRepository $repository = null;

    protected readonly CacheBag $tableCache;

    protected readonly CacheBag $tableInfoCache;

    /** @var array<string, TableInfo>|null */
    protected ?array $allTableInfos = null;

    /**
     * @throws Exception
     */
    public function __construct(
        public readonly ProjectConfig $config,
        protected readonly ConnectionFactoryInterface $connectionFactory,
        protected readonly RowFactory $rowFactory,
        protected readonly Cache $cache,
    ) {
        $databaseUrl = $this->config->getDatabaseUrl();
        $this->connectionParams = (new DsnParser(self::SCHEME_MAPPINGS))->parse($databaseUrl);
        $this->tableCache = new CacheBag();
        $this->tableInfoCache = new CacheBag();

        // Map the ENUM type (not supported by DBAL by default) to the string mapping type,
        // see https://symfony.com/doc/current/doctrine/dbal.html#registering-custom-mapping-types-in-the-schematool
        $this->getPlatform()->registerDoctrineTypeMapping('enum', 'string');
        $this->getPlatform()->registerDoctrineTypeMapping('geometry', 'string');
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getName(): string
    {
        return $this->config->getProjectName();
    }

    /**
     * Gets a Doctrine connection.
     */
    public function getConnection(): Connection
    {
        if ($this->connection === null) {
            $this->connection = $this->connectionFactory->getConnection($this->connectionParams);
        }

        return $this->connection;
    }

    /**
     * Gets the Doctrine schema manager.
     *
     * @return AbstractSchemaManager<AbstractPlatform>
     *
     * @throws Exception
     */
    public function getSchemaManager(): AbstractSchemaManager
    {
        if ($this->schemaManager === null) {
            $this->schemaManager = $this->getConnection()->createSchemaManager();
        }

        return $this->schemaManager;
    }

    /**
     * Gets the Doctrine platform (aka. mysql, postgres, etc...).
     *
     * @throws Exception
     */
    public function getPlatform(): AbstractPlatform
    {
        return $this->getConnection()->getDatabasePlatform();
    }

    public function getRepository(): ProjectRepository
    {
        if ($this->repository === null) {
            $this->repository = new ProjectRepository($this, $this->rowFactory);
        }

        return $this->repository;
    }

    /**
     * Returns `TableInfo` instances for each table, sorted by the table's "best alias".
     *
     * @return array<string, TableInfo>
     *
     * @throws InvalidArgumentException
     */
    public function getAllTableInfos(): array
    {
        if ($this->allTableInfos === null) {
            $this->allTableInfos = iter($this->getSortedTableNames())
                ->mapKeyValues(fn($i, string $tableName) => [$tableName, $this->getTableInfo($tableName)])
                ->toArray()
            ;
        }

        return $this->allTableInfos;
    }

    /**
     * Returns all table names, sorted by name.
     *
     * @return string[]
     *
     * @throws InvalidArgumentException
     */
    public function getSortedTableNames(): array
    {
        return $this->cache->get(
            new SortedTableNamesCacheKey($this),
            fn() => $this->_getSortedTableNames(),
        );
    }

    /**
     * No-cache version.
     *
     * @see self::getSortedTableNames()
     *
     * @return string[]
     *
     * @throws Exception
     */
    protected function _getSortedTableNames(): array
    {
        $tableNames = iter($this->getSchemaManager()->listTables())
            ->map(fn(Table $table) => $table->getName())
            ->toValues()
        ;

        sort($tableNames);

        return $tableNames;
    }

    /**
     * Returns all table aliases (aliases only, no real table names), key-sorted. Keys are aliases,
     * values are real table names.
     *
     * @return string[]
     *
     * @throws InvalidArgumentException
     */
    public function getSortedTableAliases(): array
    {
        return $this->cache->get(
            new SortedTableAliasesCacheKey($this),
            fn() => $this->_getSortedTableAliases(),
        );
    }

    /**
     * No-cache version.
     *
     * {@see self::getSortedTableAliases()}
     *
     * @return string[]
     *
     * @throws InvalidArgumentException
     */
    protected function _getSortedTableAliases(): array
    {
        $tableAliases = iter($this->getAllTableInfos())
            ->map(function (TableInfo $tableInfo) {
                $aliases = $tableInfo->getAliases();
                return array_fill_keys($aliases, $tableInfo->name);
            })
            ->flatten()
            ->toArray()
        ;

        ksort($tableAliases);

        return $tableAliases;
    }

    /**
     * Returns the Doctrine `Table` instance for the given table name (aliases not handled).
     *
     * @throws Exception
     */
    public function getTable(string $tableName): Table
    {
        return $this->tableCache->get(
            $tableName,
            function() use($tableName) {
                return $this->getSchemaManager()->introspectTable($tableName);
            }
        );
    }

    /**
     * Returns the `TableInfo` instance for the given table name (aliases not handled).
     */
    public function getTableInfo(string $tableName): TableInfo
    {
        return new TableInfo($this, $tableName, $this->cache);
    }

    /**
     * Returns the `TableInfo` instance for the given table name or alias. Real table names have
     * priority over aliases.
     *
     * @throws InvalidArgumentException
     */
    public function getTableInfoFromAlias(string $tableAlias): TableInfo
    {
        $tableName = $this->getTableNameFromAlias($tableAlias);

        return $this->getTableInfo($tableName);
    }

    /**
     * Returns the `TableInfo` instance for the given table name or alias. Real table names have
     * priority over aliases. Throws an exception if the table does not exist.
     *
     * @throws TableDoesNotExistException
     * @throws InvalidArgumentException
     */
    public function getTableInfoFromAliasOrThrow(string $tableAlias): TableInfo
    {
        $tableName = $this->getTableNameFromAlias($tableAlias);
        $tableInfo = $this->getTableInfo($tableName);
        $tableInfo->assertTableExists();

        return $tableInfo;
    }

    /**
     * Returns the real name of a table. If a real table name is given, it will always be returned
     * unchanged, even if an alias with the same name exists.
     *
     * @throws InvalidArgumentException
     */
    public function getTableNameFromAlias(string $tableAlias): string
    {
        if ($this->tableNameExists($tableAlias)) {
            return $tableAlias;
        } else {
            return $this->config->getTableNameFromAlias($tableAlias);
        }
    }

    /**
     * Quote a table name or a column name in order to insert it literally in a raw SQL query.
     *
     * @throws Exception
     */
    public function quote(string $identifier): string
    {
        return $this->getPlatform()->quoteIdentifier($identifier);
    }

    /**
     * Returns information on each foreign table referencing each table:
     * - The level-1 key is the name of a table being referenced;
     * - The level-2 key is the name of a table referencing the level-1 table;
     * - The value is an array containing the names of the columns implicated in the join.
     *
     * NOTE: Only handles simple foreign key, e.g. one local column to one foreign column.
     *
     * @return array<string, array<string, list<array{
     *      localColumnName: string,
     *      foreignColumnName: string,
     *  }>>>
     *
     * @throws InvalidArgumentException
     */
    public function getRelatedTablesByTargetTable(): array
    {
        return $this->cache->get(
            new JoinsByTargetTableCacheKey($this),
            function(CacheItemInterface $item) {
                $item->expiresAfter(3600);
                return $this->_getRelatedTablesByTargetTable();
            },
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    public function tableNameExists(string $tableName): bool
    {
        return key_exists($tableName, $this->getTableNamesByKey());
    }

    /**
     * No-cache version.
     *
     * @return array<string, array<string, list<array{
     *     localColumnName: string,
     *     foreignColumnName: string,
     * }>>>
     *
     * @see self::getRelatedTablesByTargetTable()
     *
     * @throws Exception
     */
    protected function _getRelatedTablesByTargetTable(): array
    {
        $joins = [];

        foreach ($this->getSchemaManager()->listTables() as $table) {
            $tableName = $table->getName();
            foreach ($table->getForeignKeys() as $foreignKey) {
                $columnNames = $foreignKey->getLocalColumns();
                $foreignColumnNames = $foreignKey->getForeignColumns();
                if (count($columnNames) !== 1 || count($foreignColumnNames) !== 1) {
                    continue; // Skip unhandled cases
                }
                $columnName = $columnNames[0];
                $foreignColumnName = $foreignColumnNames[0];
                $foreignTableName = $foreignKey->getForeignTableName();

                $joins[$foreignTableName][$tableName][] = [
                    'localColumnName' => $columnName,
                    'foreignColumnName' => $foreignColumnName,
                ];
            }
        }

        return $joins;
    }

    /**
     * Returns all the table names as key values.
     *
     * @return array<string, true>
     *
     * @throws InvalidArgumentException
     */
    protected function getTableNamesByKey(): array
    {
        // @phpstan-ignore return.type (false positive)
        return $this->cache->get(
            new TableNamesByKeyCacheKey($this),
            fn() => $this->_getTableNamesByKey(),
        );
    }

    /**
     * No-cache version.
     *
     * @return array<string, true>
     *
     * @throws Exception
     */
    protected function _getTableNamesByKey(): array
    {
        $tableNames = iter($this->getSchemaManager()->listTables())
            ->map(fn(Table $table) => $table->getName())
            ->toValues();

        return array_fill_keys($tableNames, true);
    }
}
