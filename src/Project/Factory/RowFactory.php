<?php

declare(strict_types=1);

namespace App\Project\Factory;

use App\Project\Data\Row;
use App\Project\Project;
use App\Router\AppRouter;

/**
 * Creates {@see Row} instances.
 */
readonly class RowFactory
{
    public function __construct(protected AppRouter $appRouter)
    {
    }

    /**
     * @param array<string, mixed>|null $data
     *
     * Creates a new `Row`.
     */
    public function create(Project $project, string $tableName, ?array $data): Row
    {
        return new Row(
            $project,
            $project->getTableInfo($tableName),
            $data,
            $this->appRouter,
        );
    }
}
