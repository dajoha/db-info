<?php

declare(strict_types=1);

namespace App;

use App\DependencyInjection\DbInfoExtension;
use App\Frontend\ValueTransformer\DependencyInjection\Pass\RegisterValueTransformersPass;
use App\Repository\QuickFind\DependencyInjection\Pass\RegisterQuickFindOptionsAliasesPass;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    protected function build(ContainerBuilder $container): void
    {
        $dbInfoExtension = new DbInfoExtension();
        $container->registerExtension($dbInfoExtension);

        $container->addCompilerPass(new RegisterValueTransformersPass());
        $container->addCompilerPass(new RegisterQuickFindOptionsAliasesPass());
    }
}
