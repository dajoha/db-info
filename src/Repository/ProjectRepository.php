<?php

declare(strict_types=1);

namespace App\Repository;

use App\Configuration\ProjectConfig;
use App\Helper\VarNameGenerator;
use App\Project\Data\Row;
use App\Project\Data\TableInfo;
use App\Project\Exception\ForeignKeyWasNotFoundException;
use App\Project\Exception\NoPrimaryKeyException;
use App\Project\Exception\TableColumnDoesNotExistException;
use App\Project\Factory\RowFactory;
use App\Project\Model\Exception\BadPathJoinColumnException;
use App\Project\Project;
use App\Repository\Exception\PageSizeIsTooLargeException;
use App\Repository\Exception\WrongPrimaryKeyValueException;
use App\Repository\JoinQueryBuilder\JoinQueryBuilder;
use App\Repository\Model\QueryJoinInfo;
use App\Repository\QueryPart\Filter\Filters;
use App\Repository\QueryPart\Pagination\Pagination;
use App\Repository\QueryPart\Sort\Sorts;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\SearchExpressionParser;
use Dajoha\Iter\Generator\Zip;
use Doctrine\DBAL\Exception as DbalException;
use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Psr\Cache\InvalidArgumentException;

/**
 * Creates database queries.
 */
class ProjectRepository
{
    protected readonly ProjectConfig $projectConfig;

    public function __construct(
        protected Project $project,
        protected RowFactory $rowFactory,
    ) {
        $this->projectConfig = $this->project->config;
    }

    /**
     * Finds a single row by its id (primary key).
     *
     * @param list<int|string>|int|string $id
     *
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws NoPrimaryKeyException
     * @throws WrongPrimaryKeyValueException
     */
    public function find(string $tableAlias, array|int|string $id): ?Row
    {
        $idValues = (array) $id;

        $tableInfo = $this->project->getTableInfoFromAlias($tableAlias);
        $tableName = $tableInfo->name;
        $quotedPrimaryColumnNames = $tableInfo->getPrimaryColumnNamesOrThrow(quoted: true);
        if (count($quotedPrimaryColumnNames) !== count($idValues)) {
            throw new WrongPrimaryKeyValueException($tableName, $idValues);
        }

        $varName = new VarNameGenerator('value_');
        $infos = new Zip($quotedPrimaryColumnNames, $idValues, $varName->newName(...));
        $queryBuilder = $this->createQueryBuilder($tableName);

        foreach ($infos as [ $quotedColumnName, $value, $var ]) {
            $queryBuilder
                ->where("$quotedColumnName = :$var")
                ->setParameter($var, $value);
        }

        return $this->fetchRow($tableName, $queryBuilder);
    }

    /**
     * Finds a single row by searching by a custom column. Typically, this result should be unique,
     * but if it's not the case, the first result will be returned without error.
     *
     * @throws DbalException
     * @throws TableColumnDoesNotExistException
     * @throws InvalidArgumentException
     */
    public function findBy(string $tableAlias, string $columnAlias, string|int|bool|float $value): ?Row
    {
        $tableInfo = $this->project->getTableInfoFromAlias($tableAlias);
        $tableName = $tableInfo->name;
        $columnName = $tableInfo->getColumnNameFromAlias($columnAlias);
        $quotedColumnName = $this->project->quote($columnName);

        $tableInfo->assertColumnExists($columnName);

        $queryBuilder = $this->createQueryBuilder($tableName)
            ->where("$quotedColumnName = :value")
            ->setParameter('value', $value)
        ;

        return $this->fetchRow($tableName, $queryBuilder);
    }

    /**
     * Finds the first row which satisfies the given criteria.
     *
     * @param array<string, string> $filters
     *
     * @throws BadPathJoinColumnException
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws PageSizeIsTooLargeException
     * @throws Exception
     */
    public function findFirst(
        string $tableAlias,
        array $filters = [],
        ?string $searchExpression = null,
        ?Sorts $sorts = null,
    ): ?Row
    {
        $tableInfo = $this->project->getTableInfoFromAlias($tableAlias);

        if ($searchExpression !== null) {
            $searchExpression = SearchExpressionParser::parse($searchExpression);
        }

        $queryBuilder = $this->createListQueryBuilder($tableInfo, $filters, $searchExpression, $sorts)
            ->select('*')
            ->setMaxResults(1)
        ;

        return $this->fetchRow($tableInfo->name, $queryBuilder);
    }

    /**
     * Returns a list of rows, being the results of the given search. It will add all the joins, and
     * select all the fields needed to:
     *  - format the list on frontend (including summaries of joined columns);
     *  - add `WHERE` clauses for filters.
     *
     * @param array<string, string|int|bool|float> $filters Keys are column name paths, values are
     *                                                      values to search.
     * @param int|null $totalCount Is set to the total number of results, without limit. Useful for
     *                             user information.
     *
     * @return Row[]
     *
     * @throws BadPathJoinColumnException
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws PageSizeIsTooLargeException
     * @throws Exception
     */
    public function list(
        string $tableAlias,
        array $filters = [],
        ?string $searchExpression = null,
        ?Sorts $sorts = null,
        ?Pagination $pagination = null,
        ?int &$totalCount = null,
    ): array
    {
        // Get table info for the main table:
        $tableInfo = $this->project->getTableInfoFromAlias($tableAlias);

        // Parse the search expression, if present:
        if ($searchExpression !== null) {
            $searchExpression = SearchExpressionParser::parse($searchExpression);
        }

        // Get the total count of results:
        $qb = $this->createListQueryBuilder($tableInfo, $filters, $searchExpression)
            ->select('COUNT(*)');
        $totalCount = $qb->fetchNumeric()[0]; // @phpstan-ignore offsetAccess.nonOffsetAccessible (OK: always have 1 row)

        // Create the base query builder:
        $queryBuilder = $this->createListQueryBuilder(
            $tableInfo,
            $filters,
            $searchExpression,
            $sorts,
            $pagination,
        );

        // Select all the columns of the main table:
        $queryBuilder->selectAllFieldsForMainTable();

        // Add the joins, and select the fields needed to format the list on frontend:
        /** @var QueryJoinInfo[] $joinsInfos */
        $joinsInfos = [];
        $joinColumnPaths = $tableInfo->getJoinColumnPathsForListQuery();
        foreach ($joinColumnPaths as $joinColumnPath) {
            $joinInfo = $queryBuilder->registerJoin($joinColumnPath);
            $queryBuilder->selectAllFieldsForTable($joinInfo->tableAlias, $joinInfo->tableInfo);
            $joinsInfos[] = $joinInfo;
        }

        // Fetch results:
        $results = $queryBuilder->fetchAllAssociative();

        // Construct the result rows:
        $rows = [];
        foreach ($results as $values) {
            $columnAliasIndex = 1;

            // This closure takes all the fields for the given table, while updating
            // $columnAliasIndex :
            $getTableData = function(TableInfo $tableInfo) use($values, &$columnAliasIndex) {
                $tableData = [];
                foreach ($tableInfo->getColumnNames() as $columnName) {
                    $tableData[$columnName] = $values["col_alias__$columnAliasIndex"];
                    ++$columnAliasIndex;
                }
                return $tableData;
            };

            // Create the main row:
            $mainTableData = $getTableData($tableInfo);
            $row = $this->rowFactory->create($this->project, $tableInfo->name, $mainTableData);

            // Add joined rows to the main row:
            foreach ($joinsInfos as $joinInfos) {
                $joinTableData = $getTableData($joinInfos->tableInfo);
                $row->addJoinedRow($joinInfos->joinPath, $joinTableData);
            }

            $rows[] = $row;
        }

        return $rows;
    } // list()

    /**
     * @param array<string, string|int|bool|float> $filters
     *
     * @throws BadPathJoinColumnException
     * @throws DbalException
     * @throws ForeignKeyWasNotFoundException
     * @throws InvalidArgumentException
     * @throws PageSizeIsTooLargeException
     */
    public function createListQueryBuilder(
        TableInfo $tableInfo,
        array $filters,
        ?ExpressionInterface $searchExpression = null,
        ?Sorts $sorts = null,
        ?Pagination $pagination = null,
    ): JoinQueryBuilder
    {
        $queryBuilder = new JoinQueryBuilder($tableInfo);

        // Add query filters:
        $filters = new Filters($filters, $tableInfo);
        $queryBuilder->addFilters($filters);

        // Add search expression:
        if ($searchExpression !== null) {
            $queryBuilder->addSearchExpression($searchExpression);
        }

        // Add sorts (ORDER BY clauses):
        if ($sorts !== null) {
            $queryBuilder->addSorts($sorts);
        }

        // Add pagination:
        if ($pagination !== null) {
            $queryBuilder->setPagination($pagination);
        }

        return $queryBuilder;
    }

    /**
     * Creates a pre-configured query builder:
     *  - Sets the "FROM" clause if the table name is given;
     *  - Selects "*";
     *  - Limits the results, accordingly to the config key `db_info.list_max_results`.
     *
     * @param string|null $tableAlias Warning: the alias is not quoted
     *
     * @throws DbalException
     */
    public function createQueryBuilder(
        ?string $tableName = null,
        ?string $tableAlias = null,
    ): QueryBuilder
    {
        $queryBuilder = $this->project->getConnection()->createQueryBuilder();

        if ($tableName !== null) {
            /** @noinspection PhpDqlBuilderUnknownModelInspection */
            $queryBuilder
                ->from($this->project->quote($tableName), $tableAlias)
                ->select('*')
                ->setMaxResults($this->getListMaxResults())
            ;
        }

        return $queryBuilder;
    }

    public function getListMaxResults(): int
    {
        return $this->projectConfig->getListMaxResults();
    }

    /**
     * @throws DbalException
     */
    protected function fetchRow(string $tableName, QueryBuilder $queryBuilder): ?Row
    {
        $data = $queryBuilder->fetchAssociative();

        if ($data === false) {
            return null;
        }

        return $this->rowFactory->create($this->project, $tableName, $data);
    }
}
