<?php

declare(strict_types=1);

namespace App\Repository\QueryPart\Pagination;

use App\Repository\Exception\BadPaginationValueException;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Symfony\Component\HttpFoundation\Request;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Sequence\preceded;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\integer;

/**
 * Pagination information, which can be parsed from strings, and used in queries via the
 * `ProjectRepository` service.
 *
 * @see \App\Repository\ProjectRepository
 */
class Pagination
{
    public const int DEFAULT_SIZE = 100;

    public readonly int $offset;

    public function __construct(
        public readonly int $page,
        public readonly int $size,
    ) {
        $this->offset = ($this->page - 1) * $this->size;
    }

    /**
     * Tries to parse pagination information from the query parameter "?page". If not present,
     * returns `null` but if the parse failed, then throws an exception.
     *
     * @throws BadPaginationValueException
     */
    public static function fromRequestOrThrow(Request $request, int $defaultSize): ?self
    {
        $page = $request->query->get('page');

        return $page === null ? null : self::fromStringOrThrow((string) $page, $defaultSize);
    }

    /**
     * Parses pagination information.
     *
     * Format: PAGE|PAGE-PAGE_SIZE
     * Example:
     *  "2"    => page 2 with default page size
     *  "2-15" => page 2 with page size = 15
     *
     * @throws BadPaginationValueException
     */
    public static function fromStringOrThrow(string $string, int $defaultSize): self
    {
        $parseResult = self::getParser()->parse($string);
        if (!$parseResult->isSuccess()) {
            throw new BadPaginationValueException($string, "Parse failed");
        }

        $output = $parseResult->output;
        $page = $output[0];
        $size = $output[1] ?? $defaultSize;

        if ($page <= 0) {
            throw new BadPaginationValueException($string, "Page number cannot be zero or negative");
        }
        if ($size <= 0) {
            throw new BadPaginationValueException($string, "Page size cannot be zero or negative");
        }

        return new self($page, $size);
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return "{$this->page}-{$this->size}";
    }

    /**
     * Returns the total number of result pages, for the given total number of results.
     */
    public function getTotalPages(int $totalCount): int
    {
        return (int) ceil($totalCount / $this->size);
    }

    /**
     * Returns the parser for pagination information.
     */
    protected static function getParser(): ParserInterface
    {
        static $parser = null;

        if ($parser === null) {
            $parser = seq(
                integer(),
                opt(preceded('-', integer())),
            )->complete();
        }

        return $parser;
    }
}
