<?php

declare(strict_types=1);

namespace App\Repository\QueryPart\Sort;

use App\Controller\Request\QueryParam;
use App\Exception\AppException;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Symfony\Component\HttpFoundation\Request;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Sequence\preceded;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\integer;

/**
 * List of {@see Sort}s, typically retrieved from the request.
 */
class Sorts
{
    /** @var Sort[] */
    protected array $sorts = [];

    /**
     * @param Sort[] $sorts
     */
    public function __construct(array $sorts = [])
    {
        foreach ($sorts as $sort) {
            $this->sorts[$sort->columnName] = $sort;
        }

        $this->rearrange();
    }

    public function __clone(): void
    {
        $this->sorts = array_map(fn(Sort $sort) => clone $sort, $this->sorts);
    }

    /**
     * @throws AppException
     */
    public static function fromRequestOrThrow(Request $request): self
    {
        return self::fromArrayOrThrow($request->get(QueryParam::LIST_SORT) ?? []);
    }

    /**
     * @param array<int|string, string> $queryStringArray
     *
     * @throws AppException
     */
    public static function fromArrayOrThrow(array $queryStringArray): self
    {
        $sorts = [];
        $positionDirectionParser = self::getPositionDirectionParser();

        foreach ($queryStringArray as $positionDirection => $columnName) {
            if (empty($columnName)) {
                throw new AppException("Sort component: column name is empty");
            }

            $parseResult = $positionDirectionParser->parse((string) $positionDirection);
            if (!$parseResult->isSuccess()) {
                throw new AppException("Bad sort component for column \"$columnName\": \"$positionDirection\"");
            }

            [$position, $direction] = $parseResult->output;
            $sorts[] = new Sort($columnName, $direction, $position);
        }

        return new self($sorts);
    }

    public static function getPositionDirectionParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $parser = seq(
            integer(),
            opt(preceded('-', OptSortDirection::getQueryStringParser()))
                ->map(fn($direction) => $direction ?? OptSortDirection::Asc),
        )->complete();

        return $parser;
    }

    /**
     * @return array<string, string>
     */
    public function toQueryStringArray(): array
    {
        $query = [];

        foreach ($this->sorts as $sort) {
            $query[$sort->getQueryStringKey()] = $sort->columnName;
        }

        return $query;
    }

    public function count(): int
    {
        return count($this->sorts);
    }

    /**
     * @return Sort[]
     */
    public function getAll(): array
    {
        return $this->sorts;
    }

    public function get(string $columnName): ?Sort
    {
        return $this->sorts[$columnName] ?? null;
    }

    public function setOrAdd(Sort $sort): self
    {
        $this->sorts[$sort->columnName] = $sort;
        $this->rearrange();

        return $this;
    }

    public function getMaxPosition(): ?int
    {
        if (empty($this->sorts)) {
            return null;
        }

        return iter($this->sorts)->map(fn(Sort $sort) => $sort->position)->max();
    }

    protected function rearrange(): void
    {
        // Sort by position:
        uasort($this->sorts, fn(Sort $a, Sort $b) => $a->position <=> $b->position);

        // Remove sorts with `None` direction:
        $this->sorts = array_filter(
            $this->sorts,
            fn(Sort $sort) => $sort->direction !== OptSortDirection::None,
        );

        // Renumber:
        $position = 0;
        foreach ($this->sorts as $sort) {
            $sort->position = ++$position;
        }
    }
}
