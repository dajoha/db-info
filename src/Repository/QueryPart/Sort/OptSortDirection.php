<?php

declare(strict_types=1);

namespace App\Repository\QueryPart\Sort;

use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\String\iStr;

enum OptSortDirection: string
{
    case None = 'none';
    case Asc = 'asc';
    case Desc = 'desc';

    public function getNextValue(): OptSortDirection
    {
        return match ($this) {
            self::None => self::Asc,
            self::Asc => self::Desc,
            self::Desc => self::None,
        };
    }

    public function getPreviousValue(): OptSortDirection
    {
        return match ($this) {
            self::None => self::Desc,
            self::Asc => self::None,
            self::Desc => self::Asc,
        };
    }

    public static function getQueryStringParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $parser = alt(
            iStr(self::Asc->value)->value(self::Asc),
            iStr(self::Desc->value)->value(self::Desc),
        );

        return $parser;
    }

    public function toSql(): ?string
    {
        return match ($this) {
            self::None => null,
            self::Asc => 'ASC',
            self::Desc => 'DESC',
        };
    }
}
