<?php

declare(strict_types=1);

namespace App\Repository\QueryPart\Sort;

class Sort
{
    public function __construct(
        public string $columnName,
        public OptSortDirection $direction,
        public int $position,
    ) {
    }

    public function withNextDirection(): Sort
    {
        return new self($this->columnName, $this->direction->getNextValue(), $this->position);
    }

    public function withPreviousDirection(): Sort
    {
        return new self($this->columnName, $this->direction->getPreviousValue(), $this->position);
    }

    public function getQueryStringKey(): string
    {
        $position = $this->position;
        $direction = $this->direction->value;

        return "$position-$direction";
    }
}
