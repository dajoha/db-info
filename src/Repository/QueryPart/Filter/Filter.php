<?php

declare(strict_types=1);

namespace App\Repository\QueryPart\Filter;

use App\Project\Data\TableInfo;
use App\Project\Model\Path\ColumnPath;

readonly class Filter
{
    public ColumnPath $columnPath;
    public mixed $value;

    public function __construct(string $fieldPath, mixed $value, TableInfo $tableInfo)
    {
        $this->columnPath = ColumnPath::fromString(
            $fieldPath,
            aliasAllowed: true,
            tableInfo: $tableInfo,
        );
        $this->value = $value;
    }
}
