<?php

declare(strict_types=1);

namespace App\Repository\QueryPart\Filter;

use App\Project\Data\TableInfo;

/**
 * A list of {@see Filter} instances, typically created from the request's query string.
 */
readonly class Filters
{
    /** @var Filter[] */
    public array $filters;

    /**
     * @param array<string, string|int|bool|float> $fields
     */
    public function __construct(array $fields, TableInfo $tableInfo)
    {
        $this->filters = iter($fields)
            ->map(fn(mixed $value, string $fieldPath) => new Filter($fieldPath, $value, $tableInfo))
            ->toValues();
    }
}
