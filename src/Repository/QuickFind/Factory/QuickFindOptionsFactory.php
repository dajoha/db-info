<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\Factory;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\ProjectConfig;
use App\Configuration\Service\ConfigProvider;
use App\Repository\QuickFind\DependencyInjection\Pass\RegisterQuickFindOptionsAliasesPass;
use App\Repository\QuickFind\Exception\UnknownQuickFindOptionAliasException;
use App\Repository\QuickFind\QuickFindOption\QuickFindOptionInterface;

/**
 * Creates quick-find options for a given project.
 *
 * @phpstan-import-type Type_UserConfig_QuickFindOptions from ConfigurationSchema
 * @phpstan-import-type Type_InternalConfig_QuickFindOptionsConfig from RegisterQuickFindOptionsAliasesPass
 */
class QuickFindOptionsFactory
{
    /**
     * Quick-find options by table name.
     * @var array<string, QuickFindOptionInterface[]>
     */
    protected array $quickFindOptions;

    /** @var Type_UserConfig_QuickFindOptions[]|null */
    protected ?array $defaultQuickFindOptionsDefinitions = null;

    public function __construct(
        protected readonly ConfigProvider $configProvider,
        protected readonly ProjectConfig $projectConfig,
        /** @var Type_InternalConfig_QuickFindOptionsConfig $quickFindOptionsConfig */
        protected readonly array $quickFindOptionsConfig,
    ) {
        $this->quickFindOptions = [];
    }

    /**
     * Returns the resolved quick-find options for the given table. There are 3 levels of
     * definitions, each one overriding the previous one:
     *
     *  - Application-level defaults (configured in `db_info.table_defaults.quick_find_options`);
     *  - Project-level defaults (configured in `db_info.projects.<PROJECT_NAME>.table_defaults.quick_find_options`);
     *  - Table-specific options (configured in `db_info.projects.<PROJECT_NAME>.tables.<TABLE_NAME>.quick_find_options`).
     *
     * @return QuickFindOptionInterface[]
     *
     * @throws UnknownQuickFindOptionAliasException
     */
    public function getQuickFindOptionsForTable(string $tableName): array
    {
        if (!key_exists($tableName, $this->quickFindOptions)) {
            $quickFindOptionsDefinitions =
                $this->projectConfig->getQuickFindOptionsDefinitions($tableName)
                ?? $this->getDefaultQuickFindOptionsDefinitions()
            ;
            $this->quickFindOptions[$tableName] = $this->create($quickFindOptionsDefinitions);
        }

        return $this->quickFindOptions[$tableName];
    }

    /**
     * Creates an array of `QuickFindOptionInterface`, based on the given definitions.
     *
     * @phpstan-param Type_UserConfig_QuickFindOptions[] $quickFindOptionsDefinitions
     *
     * @return QuickFindOptionInterface[]
     *
     * @throws UnknownQuickFindOptionAliasException
     */
    public function create(array $quickFindOptionsDefinitions): array
    {
        $quickFindOptions = [];

        foreach ($quickFindOptionsDefinitions as $quickFindOptionsDefinition) {
            $class = $quickFindOptionsDefinition[K::QUICK_FIND_OPTIONS__CLASS];
            if (str_starts_with($class, '@')) {
                $alias = substr($class, 1);
                $class = $this->quickFindOptionsConfig['aliases'][$alias] ?? null;
                if (null === $class) {
                    throw new UnknownQuickFindOptionAliasException($alias);
                }
            }
            $parameters = $quickFindOptionsDefinition[K::QUICK_FIND_OPTIONS__PARAMETERS];
            $quickFindOptions[] = new $class(...$parameters);
        }

        return $quickFindOptions;
    }

    /**
     * Returns the default quick-find definitions for the project. First search for project-level
     * defaults, then fallbacks to application-level defaults.
     *
     * @return Type_UserConfig_QuickFindOptions[]
     */
    protected function getDefaultQuickFindOptionsDefinitions(): array
    {
        if ($this->defaultQuickFindOptionsDefinitions === null) {
            $this->defaultQuickFindOptionsDefinitions =
                $this->projectConfig->getDefaultQuickFindOptionsDefinitions()
                ?? $this->configProvider->getDefaultQuickFindOptionsDefinitions()
                ?? [];
        }

        return $this->defaultQuickFindOptionsDefinitions;
    }
}
