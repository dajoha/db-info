<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\Exception;

use App\Exception\AppException;
use App\Repository\QuickFind\QuickFindOption\QuickFindOptionInterface;
use Throwable;

class ColumnNameIsNotSetException extends AppException
{
    public function __construct(
        public readonly QuickFindOptionInterface $quickFindOption,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $class = get_class($this->quickFindOption);
        $message = "Column name was not set in method \"supports()\" of quick find option \"$class\"";
        parent::__construct($message, $code, $previous);
    }
}
