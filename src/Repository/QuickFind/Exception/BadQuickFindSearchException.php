<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\Exception;

use App\Exception\AppException;
use Throwable;

class BadQuickFindSearchException extends AppException
{
    public function __construct(
        public readonly string $tableName,
        public readonly string $search,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message = "Bad quick find search for table \"$tableName\", search = \"$search\"";
        parent::__construct($message, $code, $previous);
    }
}
