<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\Exception;

use App\Exception\AppException;
use Throwable;

class UnknownQuickFindOptionAliasException extends AppException
{
    public function __construct(
        public readonly string $alias = "",
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message = "Unknown quick find option alias: \"$alias\"";
        parent::__construct($message, $code, $previous);
    }
}
