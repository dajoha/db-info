<?php

declare(strict_types=1);

namespace App\Repository\QuickFind;

use App\Configuration\Service\ConfigProvider;
use App\Project\Data\Row;
use App\Project\Factory\RowFactory;
use App\Project\Project;
use App\Repository\QuickFind\DependencyInjection\Pass\RegisterQuickFindOptionsAliasesPass;
use App\Repository\QuickFind\Exception\BadQuickFindSearchException;
use App\Repository\QuickFind\Exception\ColumnNameIsNotSetException;
use App\Repository\QuickFind\Exception\UnknownQuickFindOptionAliasException;
use App\Repository\QuickFind\Factory\QuickFindOptionsFactory;
use App\Repository\QuickFind\QuickFindOption\QuickFindOptionInterface;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Psr\Cache\InvalidArgumentException;

/**
 * @phpstan-import-type Type_InternalConfig_QuickFindOptionsConfig from RegisterQuickFindOptionsAliasesPass
 */
class QuickFindRepository
{
    /** @var array<string, QuickFindOptionsFactory> */
    protected array $optionsFactories = [];

    public function __construct(
        protected readonly RowFactory $rowFactory,
        protected readonly ConfigProvider $configProvider,
        /** @var Type_InternalConfig_QuickFindOptionsConfig $quickFindOptionsConfig */
        protected readonly array $quickFindOptionsConfig,
    ) {
    }

    /**
     * Finds a row in a table, by performing a "quick find" search. It is ruled by
     * `QuickFindOptionInterface` services, which can be configured differently for each table.
     *
     * @throws BadQuickFindSearchException
     * @throws Exception
     * @throws UnknownQuickFindOptionAliasException
     * @throws InvalidArgumentException
     * @throws ColumnNameIsNotSetException
     */
    public function quickFind(Project $project, string $tableAlias, string $search): ?Row
    {
        $tableInfo = $project->getTableInfoFromAlias($tableAlias);
        $tableName = $tableInfo->name;

        $quickFindOptionsFactory = $this->getQuickFindOptionsFactory($project);
        $quickFindOptions = $quickFindOptionsFactory->getQuickFindOptionsForTable($tableName);

        $data = false;
        $atLeastOneOptionSupportsSearch = false;

        foreach ($quickFindOptions as $quickFindOption) {
            /** @var string|null $columnName */
            $columnName = null;
            if (!$quickFindOption->supports($search, $project, $tableName, $columnName)) {
                continue;
            }
            $atLeastOneOptionSupportsSearch = true;

            if ($columnName === null) {
                throw new ColumnNameIsNotSetException($quickFindOption);
            }

            $data = $this
                ->createQueryBuilder($project, $tableName, $quickFindOption, $columnName, $search)
                ->fetchAssociative();

            if ($data !== false) {
                break;
            }
        }

        if (!$atLeastOneOptionSupportsSearch) {
            throw new BadQuickFindSearchException($tableName, $search);
        }

        if ($data === false) {
            return null;
        }

        return $this->rowFactory->create($project, $tableName, $data);
    }

    /**
     * @throws Exception
     */
    protected function createQueryBuilder(
        Project $project,
        string $tableName,
        QuickFindOptionInterface $quickFindOption,
        string $columnName,
        string $search,
    ): QueryBuilder {
        $queryBuilder = $project->getRepository()->createQueryBuilder($tableName);

        $quotedColumnName = $project->quote($columnName);

        // Allow the quick find option to alter the search string:
        $quickFindOption->alterSearchBeforeQuery($search, $project, $tableName, $columnName);

        $queryBuilder
            ->andWhere("$quotedColumnName = :search")
            ->setParameter('search', $search)
        ;

        return $queryBuilder;
    }

    protected function getQuickFindOptionsFactory(Project $project): QuickFindOptionsFactory
    {
        if (!isset($this->optionsFactories[$project->getName()])) {
            $this->optionsFactories[$project->getName()] = new QuickFindOptionsFactory(
                $this->configProvider,
                $project->config,
                $this->quickFindOptionsConfig,
            );
        }

        return $this->optionsFactories[$project->getName()];
    }
}
