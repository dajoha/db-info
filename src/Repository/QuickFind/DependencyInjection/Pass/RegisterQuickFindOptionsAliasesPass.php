<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\DependencyInjection\Pass;

use App\Repository\QuickFind\QuickFindOption\QuickFindOptionInterface;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\DependencyInjection\Attribute\AsAlias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Registers all services implementing "QuickFindOptionInterface" and annotated with "AsAlias" into
 * the Symfony parameter "quick_find_options_config.aliases", in order to use these aliases in the
 * "db_info" configuration key.
 *
 * @see QuickFindOptionInterface
 * @see AsAlias
 *
 * @phpstan-type Type_InternalConfig_QuickFindOptionsConfig array{
 *     aliases: array<string, class-string<QuickFindOptionInterface>>,
 * }
 */
class RegisterQuickFindOptionsAliasesPass implements CompilerPassInterface
{
    /**
     * @throws ReflectionException
     */
    public function process(ContainerBuilder $container): void
    {
        $aliases = [];

        foreach ($container->getDefinitions() as $definition) {
            if (!$definition->hasTag('quick_find_option') || $definition->isAbstract()) {
                continue;
            }

            /** @var ?class-string $className */
            $className = $definition->getClass();
            if ($className === null) {
                continue;
            }

            $class = new ReflectionClass($className);

            foreach ($class->getAttributes(AsAlias::class) as $attribute) {
                /** @var AsAlias $asAlias */
                $asAlias = $attribute->newInstance();
                $alias = $asAlias->id;
                if ($alias !== null) {
                    $aliases[$alias] = $className;
                }
            }
        }

        $container->setParameter('quick_find_options_config', [
            'aliases' => $aliases,
        ]);
    }
}
