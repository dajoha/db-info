<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\QuickFindOption;

use App\Project\Project;
use Symfony\Component\DependencyInjection\Attribute\AsAlias;

#[AsAlias('quick_find_option.uuid')]
class UuidQuickFindOption extends AbstractQuickFindOption
{
    public const string DEFAULT_COLUMN_NAME = 'uuid';

    protected const string UUID_REGEX = '/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/';

    public function __construct(protected string $columnName = self::DEFAULT_COLUMN_NAME)
    {
    }

    public function supports(
        string $search,
        Project $project,
        string $tableName,
        ?string &$columnName,
    ): bool
    {
        if (preg_match(self::UUID_REGEX, $search) !== 1) {
            return false;
        }

        $columnName = $this->columnName;

        return true;
    }
}
