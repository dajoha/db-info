<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\QuickFindOption;

use App\Exception\NotImplementedException;
use App\Project\Exception\NoPrimaryKeyException;
use App\Project\Exception\TableColumnDoesNotExistException;
use App\Project\Project;
use App\Router\Util\UrlUtil;
use Doctrine\DBAL\Exception;
use Symfony\Component\DependencyInjection\Attribute\AsAlias;

#[AsAlias('quick_find_option.primary_key')]
class PrimaryKeyQuickFindOption extends AbstractQuickFindOption
{
    /**
     * @throws NoPrimaryKeyException
     * @throws Exception
     */
    public function supports(
        string $search,
        Project $project,
        string $tableName,
        ?string &$columnName,
    ): bool
    {
        $primaryKey = $project->getTableInfo($tableName)->getPrimaryKeyOrThrow();
        $columnNames = $primaryKey->getColumns();

        if (count($columnNames) !== 1) {
            return false;
        }

        $columnName = $columnNames[0];

        return true;
    }

    /**
     * @throws Exception
     * @throws TableColumnDoesNotExistException
     * @throws NotImplementedException
     */
    public function alterSearchBeforeQuery(
        string &$search,
        Project $project,
        string $tableName,
        string $columnName,
    ): void
    {
        $column = $project->getTableInfo($tableName)->getColumnOrThrow($columnName);
        $decodedSearch = UrlUtil::decodeDatabaseValue($search, $column);
        $search = (string) $decodedSearch;
    }
}
