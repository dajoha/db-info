<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\QuickFindOption;

use App\Project\Project;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

/**
 * Represents a way to quickly find a row in a table.
 */
#[AutoconfigureTag('quick_find_option')]
interface QuickFindOptionInterface
{
    /**
     * Must return `true` if the search string is eligible for this quick find option. If it's the
     * case, the param $columnName must be set to the column name to search.
     *
     * @param string|null $columnName On match, must be set with the non-quoted column name to search
     */
    public function supports(
        string $search,
        Project $project,
        string $tableName,
        ?string &$columnName,
    ): bool;

    /**
     * A hook which allows to alter the initial search, before the real query is executed.
     */
    public function alterSearchBeforeQuery(
        string &$search,
        Project $project,
        string $tableName,
        string $columnName,
    ): void;
}
