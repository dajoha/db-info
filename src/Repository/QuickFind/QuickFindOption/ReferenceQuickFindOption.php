<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\QuickFindOption;

use App\Project\Project;
use Doctrine\DBAL\Exception;
use Symfony\Component\DependencyInjection\Attribute\AsAlias;

#[AsAlias('quick_find_option.reference')]
class ReferenceQuickFindOption extends AbstractQuickFindOption
{
    public const array DEFAULT_COLUMN_NAMES = [
        'reference',
        'ref',
    ];

    protected const string REFERENCE_REGEX = '/^[a-zA-Z0-9]+(-[a-zA-Z0-9]+){3,}$/';

    public function __construct(
        /** @var string[] $columnNames */
        protected array $columnNames = self::DEFAULT_COLUMN_NAMES
    ) {
    }

    /**
     * @throws Exception
     */
    public function supports(
        string $search,
        Project $project,
        string $tableName,
        ?string &$columnName,
    ): bool
    {
        if (preg_match(self::REFERENCE_REGEX, $search) !== 1) {
            return false;
        }

        $table = $project->getTable($tableName);
        foreach ($this->columnNames as $columnNameToSearch) {
            if ($table->hasColumn($columnNameToSearch)) {
                $columnName = $columnNameToSearch;
                return true;
            }
        }

        return false;
    }
}
