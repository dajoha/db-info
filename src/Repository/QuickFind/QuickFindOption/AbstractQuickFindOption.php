<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\QuickFindOption;

use App\Project\Project;

abstract class AbstractQuickFindOption implements QuickFindOptionInterface
{
    public function alterSearchBeforeQuery(
        string &$search,
        Project $project,
        string $tableName,
        string $columnName,
    ): void
    {
    }
}
