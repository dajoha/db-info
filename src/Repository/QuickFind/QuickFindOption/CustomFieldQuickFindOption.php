<?php

declare(strict_types=1);

namespace App\Repository\QuickFind\QuickFindOption;

use App\Project\Project;
use Symfony\Component\DependencyInjection\Attribute\AsAlias;

#[AsAlias('quick_find_option.custom_field')]
class CustomFieldQuickFindOption extends AbstractQuickFindOption
{
    public function __construct(
        protected string $columnName,
        protected string $regex = '/.*/',
        protected ?string $replace = null,
    )  {
    }

    public function supports(
        string $search,
        Project $project,
        string $tableName,
        ?string &$columnName,
    ): bool
    {
        if (preg_match($this->regex, $search) !== 1) {
            return false;
        }

        $columnName = $this->columnName;

        return true;
    }

    public function alterSearchBeforeQuery(
        string &$search,
        Project $project,
        string $tableName,
        string $columnName,
    ): void
    {
        if ($this->replace !== null) {
            $search = preg_replace($this->regex, $this->replace, $search);
        }
    }
}
