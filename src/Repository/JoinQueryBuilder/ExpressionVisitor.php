<?php

declare(strict_types=1);

namespace App\Repository\JoinQueryBuilder;

use App\Project\Exception\ForeignKeyWasNotFoundException;
use App\Project\Model\Exception\BadPathJoinColumnException;
use App\Project\Model\Path\ColumnPath;
use App\Project\Project;
use App\Repository\SearchExpression\Ast\BinaryOperationExpr;
use App\Repository\SearchExpression\Ast\CollectionExpr;
use App\Repository\SearchExpression\Ast\DatabasePathExpr;
use App\Repository\SearchExpression\Ast\FunctionCallExpr;
use App\Repository\SearchExpression\Ast\LogicalValueExpr;
use App\Repository\SearchExpression\Ast\Model\Operator;
use App\Repository\SearchExpression\Ast\NumberExpr;
use App\Repository\SearchExpression\Ast\StringExpr;
use App\Repository\SearchExpression\Exception\SearchExpressionException;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\VisitorInterface;
use App\Util\PlatformUtil;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Psr\Cache\InvalidArgumentException;

/**
 * Transforms a {@see ExpressionInterface} into raw SQL.
 *
 * Actually supports Mysql/MariaDb and PostgreSQL.
 *
 * Automatically adds the needed `JOIN`s to the given {@see JoinQueryBuilder}, when a compound
 * expression is encountered like `product.brand.name`.
 */
class ExpressionVisitor implements VisitorInterface
{
    protected AbstractPlatform $platform;

    /**
     * @throws Exception
     */
    public function __construct(
        protected JoinQueryBuilder $queryBuilder,
        protected Project $project,
    ) {
        $this->platform = $this->project->getPlatform();
    }

    public function visitStr(StringExpr $stringExpr): string
    {
        $output = $this->platform->quoteStringLiteral($stringExpr->value);

        return self::mayAddParentheses($stringExpr, $output);
    }

    public function visitNumber(NumberExpr $numberExpr): string
    {
        return self::mayAddParentheses($numberExpr, (string) $numberExpr->value);
    }

    public function visitLogicalValue(LogicalValueExpr $logicalValueExpr): string
    {
        return self::mayAddParentheses($logicalValueExpr, $logicalValueExpr->logicalValue->value);
    }

    public function visitCollection(CollectionExpr $collectionExpr): string
    {
        $values = iter($collectionExpr->values)
            ->map(fn(ExpressionInterface $value) => $value->accept($this))
            ->join(', ')
        ;

        return "($values)";
    }

    /**
     * @throws BadPathJoinColumnException
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     * @throws InvalidArgumentException
     */
    public function visitDatabasePath(DatabasePathExpr $pathExpr): string
    {
        $columnPath = new ColumnPath(
            $pathExpr->columnAliases,
            aliasAllowed: true,
            tableInfo: $this->queryBuilder->tableInfo,
        );

        $joinInfo = $this->queryBuilder->registerJoin($columnPath->getJoinColumnNames());

        $quotedColumnName = $this->platform->quoteIdentifier($columnPath->getValueColumnName());
        $output = "$joinInfo->tableAlias.$quotedColumnName";

        return self::mayAddParentheses($pathExpr, $output);
    }

    /**
     * @throws SearchExpressionException
     */
    public function visitBinaryOperation(BinaryOperationExpr $operationExpr): string
    {
        $lhs = $operationExpr->operand1->accept($this);
        $rhs = $operationExpr->operand2->accept($this);
        $operator = $operationExpr->operator;

        $operatorStr = match ($operator) {
            Operator::Mul    => '*',
            Operator::Div    => '/',
            Operator::Add    => '+',
            Operator::Sub    => '-',
            Operator::Eq     => '=',
            Operator::Neq    => '!=',
            Operator::Gte    => '>=',
            Operator::Lte    => '<=',
            Operator::Gt     => '>',
            Operator::Lt     => '<',
            Operator::And    => 'AND',
            Operator::Or     => 'OR',
            Operator::In     => 'IN',
            Operator::Is     => 'IS',
            Operator::IsNot  => 'IS NOT',
            Operator::Like, Operator::ILike, Operator::SimpleLike => 'LIKE',
        };

        // `Operator::SimpleLike` is a special operator where the rhs has to be transformed:
        if ($operator === Operator::SimpleLike) {
            $rhs = $this->getRhsForSimpleLikeOperator($operationExpr->operand2);
        }

        if (in_array($operator, [Operator::SimpleLike, Operator::ILike])) {
            if (PlatformUtil::implementsILike($this->platform)) {
                $output = "$lhs ILIKE $rhs";
            } else {
                $output = "LOWER($lhs) LIKE " . strtolower($rhs);
            }
        } else {
            $output = "$lhs $operatorStr $rhs";
        }

        return self::mayAddParentheses($operationExpr, $output);
    }

    public function visitFunctionCall(FunctionCallExpr $functionCallExpr): string
    {
        $functionName = $functionCallExpr->name;
        $arguments = iter($functionCallExpr->arguments)
            ->map(fn(ExpressionInterface $expression) => $expression->accept($this))
            ->join(', ')
        ;

        return "$functionName($arguments)";
    }

    public static function mayAddParentheses(ExpressionInterface $expression, string $output): string
    {
        return $expression->hasParentheses() ? "($output)" : $output;
    }

    /**
     * Transforms the given string value into a `ILIKE` pattern, by escaping potential wild-char
     * characters, and prefixing and suffixing by '%'.
     *
     * Compatible with PostgreSQL and MySQL/MariaDB.
     *
     * @throws SearchExpressionException
     */
    protected function getRhsForSimpleLikeOperator(ExpressionInterface $rightExpr): string
    {
        if (
            !$rightExpr instanceof StringExpr
            && !$rightExpr instanceof NumberExpr
            && !$rightExpr instanceof DatabasePathExpr
        ) {
            $operator = Operator::SimpleLike->value;
            throw new SearchExpressionException(
                "Operator \"$operator\": right operand must be a string, a number, or an identifier to stringify",
                $rightExpr,
            );
        }

        $pattern = str_replace(['\\', '%', '_'], ['\\\\', '\\%', '\\_'], (string) $rightExpr);
        $pattern = "%$pattern%";

        $str = new StringExpr($pattern);

        $span = $rightExpr->getSpan();
        if ($span !== null) { // Should be always non-null
            $str->setSpan($span);
        }

        return $str->accept($this);
    }
}
