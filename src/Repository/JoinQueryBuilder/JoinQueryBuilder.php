<?php

declare(strict_types=1);

namespace App\Repository\JoinQueryBuilder;

use App\Configuration\ProjectConfig;
use App\Project\Data\TableInfo;
use App\Project\Exception\ForeignKeyWasNotFoundException;
use App\Project\Model\Exception\BadPathJoinColumnException;
use App\Project\Project;
use App\Repository\Exception\PageSizeIsTooLargeException;
use App\Repository\Helper\QueryHelper;
use App\Repository\Model\QueryJoinInfo;
use App\Repository\QueryPart\Filter\Filters;
use App\Repository\QueryPart\Pagination\Pagination;
use App\Repository\QueryPart\Sort\Sorts;
use App\Repository\SearchExpression\ExpressionInterface;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Psr\Cache\InvalidArgumentException;

/**
 * A query builder which makes it easy to:
 *  - join (potentially nested) tables,
 *  - prevent joining the same table multiple times,
 *  - add ordering with automatic join if needed,
 *  - add filters (WHERE expressions) with automatic join if needed,
 *  - add pagination
 */
class JoinQueryBuilder extends QueryBuilder
{
    protected readonly Project $project;

    protected readonly ProjectConfig $projectConfig;

    protected QueryHelper $queryHelper;

    protected string $mainTableSqlAlias;

    protected ?ExpressionVisitor $expressionVisitor = null;

    /**
     * @throws Exception
     */
    public function __construct(public TableInfo $tableInfo)
    {
        $this->project = $this->tableInfo->project;
        $this->projectConfig = $this->project->config;

        parent::__construct($this->project->getConnection());
        $this->queryHelper = new QueryHelper();
        $this->mainTableSqlAlias = $this->queryHelper->getOrCreateTableAlias($tableInfo->name);

        $quotedTableName = $this->project->quote($tableInfo->name);
        /** @noinspection PhpDqlBuilderUnknownModelInspection */
        $this
            ->from($quotedTableName, $this->mainTableSqlAlias)
            ->setMaxResults($this->tableInfo->getDefaultPaginationSize())
        ;
    }

    /**
     * @throws Exception
     */
    public function selectAllFieldsForMainTable(): self
    {
        return $this->selectAllFieldsForTable($this->mainTableSqlAlias, $this->tableInfo);
    }

    /**
     * @param string $tableSqlAlias Safe table alias name
     *
     * @throws Exception
     */
    public function selectAllFieldsForTable(string $tableSqlAlias, TableInfo $tableInfo): self
    {
        foreach ($tableInfo->getTable()->getColumns() as $column) {
            $quotedColumnName = $this->project->quote($column->getName());
            $columnAlias = $this->queryHelper->newColumnAlias();
            $this->addSelect("$tableSqlAlias.$quotedColumnName AS $columnAlias");
        }

        return $this;
    }

    /**
     * Adds a join to the query builder, only if not already present.
     *
     * @param string[] $path
     *
     * @throws Exception|InvalidArgumentException
     * @throws ForeignKeyWasNotFoundException
     */
    public function registerJoin(array $path): QueryJoinInfo
    {
        $joinTableInfo = $this->tableInfo;
        $parentTableSqlAlias = $this->mainTableSqlAlias;
        $currentPath = [];

        // The next loop is guaranteed to have at least one iteration, so $joinTableSqlAlias is
        // guaranteed to be not `null` after it.
        $joinTableSqlAlias = null;

        // Join each table of the join path:
        foreach ($path as $pathColumnName) {
            $currentPath[] = $pathColumnName;
            $joinId = QueryHelper::getJoinId($currentPath);
            $joinColumn = $joinTableInfo->getJoinColumnOrThrow($pathColumnName);
            $joinTableSqlAlias = $this->queryHelper->getTableAlias($joinId);

            if ($joinTableSqlAlias === null) {
                $joinTableSqlAlias = $this->queryHelper->getOrCreateTableAlias($joinColumn->tableName, $joinId);
                $quotedColumnName = $this->project->quote($pathColumnName);
                $quotedJoinColumnName = $this->project->quote($joinColumn->columnName);
                $this->leftJoin(
                    $parentTableSqlAlias,
                    $joinColumn->tableName,
                    $joinTableSqlAlias,
                    "$joinTableSqlAlias.$quotedJoinColumnName = $parentTableSqlAlias.$quotedColumnName",
                );
            }

            $joinTableInfo = $this->project->getTableInfo($joinColumn->tableName);
            $parentTableSqlAlias = $joinTableSqlAlias;
        }

        return new QueryJoinInfo(
            $path,
            $joinTableInfo,
            $joinTableSqlAlias ?? $this->mainTableSqlAlias
        );
    }

    /**
     * @throws BadPathJoinColumnException
     * @throws InvalidArgumentException
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     */
    public function addFilters(Filters $filters): self
    {
        foreach ($filters->filters as $filter) {
            $columnPath = $filter->columnPath;

            // If needed, add the necessary "JOIN" clauses:
            $joinInfo = $this->registerJoin($columnPath->getJoinColumnNames());

            $quotedTableAlias = $this->project->quote($joinInfo->tableAlias);
            $quotedColumnName = $this->project->quote($columnPath->getValueColumnName());
            $quotedFullColumnName = "$quotedTableAlias.$quotedColumnName";
            $placeholder = $this->queryHelper->newPlaceholder();

            $this
                ->andWhere("$quotedFullColumnName = :$placeholder")
                ->setParameter($placeholder, $filter->value)
            ;
        }

        return $this;
    }

    /**
     * Adds a search expression to the query, and joins all the tables needed by the expression.
     *
     * @throws Exception
     */
    public function addSearchExpression(ExpressionInterface $searchExpression): self
    {
        $whereExpression = $searchExpression->accept($this->getExpressionVisitor());

        return $this->andWhere($whereExpression);
    }

    /**
     * @throws Exception
     */
    public function addSorts(Sorts $sorts): self
    {
        foreach ($sorts->getAll() as $sort) {
            $direction = $sort->direction->toSql();
            $quotedColumnName = $this->project->quote($sort->columnName);
            $this->addOrderBy("$this->mainTableSqlAlias.$quotedColumnName", $direction);
        }

        return $this;
    }

    /**
     * @throws PageSizeIsTooLargeException
     */
    public function setPagination(Pagination $pagination): self
    {
        $listMaxResults = $this->projectConfig->getListMaxResults();
        if ($pagination->size > $listMaxResults) {
            throw new PageSizeIsTooLargeException($pagination->size, $listMaxResults);
        }

        $this
            ->setFirstResult($pagination->offset)
            ->setMaxResults($pagination->size)
        ;

        return $this;
    }

    /**
     * @throws Exception
     */
    protected function getExpressionVisitor(): ExpressionVisitor
    {
        if ($this->expressionVisitor === null) {
            $this->expressionVisitor = new ExpressionVisitor($this, $this->project);
        }

        return $this->expressionVisitor;
    }
}
