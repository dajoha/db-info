<?php

declare(strict_types=1);

namespace App\Repository\Helper;

use Dajoha\Iter\Generator\Counter;
use Iterator;

/**
 * Helps to generate various names and aliases, for a specific database query.
 *
 * IMPORTANT: different queries must never use the same instance of this class.
 */
class QueryHelper
{
    public const string PLACEHOLDER_PREFIX = 'placeholder' . self::COUNTER_SEPARATOR;
    public const string TABLE_ALIAS_PREFIX = '';
    public const string COLUMN_ALIAS_PREFIX = 'col_alias' . self::COUNTER_SEPARATOR;

    public const string COUNTER_SEPARATOR = '__';

    protected ?Iterator $placeholderGenerator = null;

    /** @var Iterator[] */
    protected array $tableAliasGenerators = [];

    /** @var array<string, string> */
    protected array $tableAliases = [];

    protected ?Iterator $columnAliasGenerator = null;

    /**
     * Creates a new placeholder name, which can be used safely in a SQL query.
     */
    public function newPlaceholder(): string
    {
        if ($this->placeholderGenerator === null) {
            $this->placeholderGenerator = Counter::from(1)->map(
                fn(int $n) => self::PLACEHOLDER_PREFIX . $n
            );
        }

        $current = $this->placeholderGenerator->current();
        $this->placeholderGenerator->next();

        return $current;
    }

    /**
     * Creates a new table alias if needed. The returned alias is safe (no need to quote it).
     *
     * @param string|null $joinId If given, the same alias will be returned when called several
     *                            times, when the same table name and join id are given.
     */
    public function getOrCreateTableAlias(string $tableName, ?string $joinId = null): string
    {
        if ($joinId !== null && key_exists($joinId, $this->tableAliases)) {
            return $this->tableAliases[$joinId];
        }

        $tableName = self::safeIdentifier($tableName);

        if (!key_exists($tableName, $this->tableAliasGenerators)) {
            $this->tableAliasGenerators[$tableName] = Counter::from(1)->map(fn(int $n) =>
                self::TABLE_ALIAS_PREFIX
                . $tableName
                . self::COUNTER_SEPARATOR
                . $n
            );
        }

        $current = $this->tableAliasGenerators[$tableName]->current();
        $this->tableAliasGenerators[$tableName]->next();

        if ($joinId !== null) {
            $this->tableAliases[$joinId] = $current;
        }

        return $current;
    }

    /**
     * Generates a new column alias; the returned alias name is safe (no need to quote it).
     */
    public function newColumnAlias(): string
    {
        if ($this->columnAliasGenerator === null) {
            $this->columnAliasGenerator = Counter::from(1)->map(
                fn(int $n) => self::COLUMN_ALIAS_PREFIX . $n
            );
        }

        $current = $this->columnAliasGenerator->current();
        $this->columnAliasGenerator->next();

        return $current;
    }

    /**
     * Returns a previously created table alias, by its $joinId.
     */
    public function getTableAlias(string $joinId): ?string
    {
        return $this->tableAliases[$joinId] ?? null;
    }

    /**
     * @param string[] $columnNames
     */
    public static function getJoinId(array $columnNames): string
    {
        return implode("\x00", $columnNames);
    }

    protected static function safeIdentifier(string $unsafeIdentifier): string
    {
        /** @var string */
        return preg_replace('/[^a-zA-Z0-9_]/', '_', $unsafeIdentifier);
    }
}
