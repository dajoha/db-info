<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression;

use App\Repository\SearchExpression\Ast\BinaryOperationExpr;
use App\Repository\SearchExpression\Ast\CollectionExpr;
use App\Repository\SearchExpression\Ast\DatabasePathExpr;
use App\Repository\SearchExpression\Ast\FunctionCallExpr;
use App\Repository\SearchExpression\Ast\LogicalValueExpr;
use App\Repository\SearchExpression\Ast\Model\Operator;
use App\Repository\SearchExpression\Ast\NumberExpr;
use App\Repository\SearchExpression\Ast\StringExpr;
use App\Repository\SearchExpression\Exception\SearchExpressionException;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Expression\ExpressionParser;
use Dajoha\ParserCombinator\Stream\LocatedStream\LocatedSpan;
use Dajoha\ParserCombinator\Stream\LocatedStream\LocatedStream;
use Dajoha\ParserCombinator\Stream\StringStream\StringSpan;
use Exception;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Map\value;
use function Dajoha\ParserCombinator\Parser\Misc\recursive;
use function Dajoha\ParserCombinator\Parser\Sequence\delimited;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\iStr;
use function Dajoha\ParserCombinator\Parser\String\spaced;
use function Dajoha\ParserCombinator\Parser\String\spaces1;

class SearchExpressionParser
{
    /**
     * @throws Exception
     */
    public static function parse(string $input): ?ExpressionInterface
    {
        try {
            $stream = new LocatedStream($input);
            return self::getParser()->parse($stream)->output;
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (SearchExpressionException $e) {
            // Add location information if possible:
            if ($e->expression !== null) {
                $span = $e->expression->getSpan(); // @phpstan-ignore method.notFound (TODO)
                $location = null;
                if ($span instanceof StringSpan) {
                    $location = "At position {$span->start->charPosition}";
                } else if ($span instanceof LocatedSpan) {
                    $location = "At line {$span->start->line}, column {$span->start->column}";
                }
                if ($location !== null) {
                    $message = "$location: {$e->getMessage()}";
                    $e = new SearchExpressionException(
                        $message,
                        $e->expression,
                        $e->getCode(),
                        $e->getPrevious(),
                    );
                }
            }

            throw $e;
        }
    }

    public static function getParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $parser = self::expression()
            ->spaced()
            ->complete()
        ;

        return $parser;
    }

    public static function expression(): ParserInterface
    {
        $recursiveParser = recursive();

        $parser = ExpressionParser::precedenceParser(
            alt(
                self::terminal(),
                delimited('(', $recursiveParser, ')')
                    ->map(fn(ExpressionInterface $expression) => $expression->withParentheses()),
            ),
            [
                self::binaryOperations([
                    ['*', Operator::Mul],
                    ['/', Operator::Div],
                ]),
                self::binaryOperations([
                    ['+', Operator::Add],
                    ['-', Operator::Sub],
                ]),
                self::binaryOperations([
                    [['=', '=='], Operator::Eq],
                    ['!=', Operator::Neq],
                    ['>=', Operator::Gte],
                    ['<=', Operator::Lte],
                    ['>', Operator::Gt],
                    ['<', Operator::Lt],
                    ['IN', Operator::In],
                    [self::isNotOperator(), Operator::IsNot],
                    ['IS', Operator::Is],
                    ['LIKE', Operator::Like],
                    ['ILIKE', Operator::ILike],
                    ['~', Operator::SimpleLike],
                ]),
                self::binaryOperations([
                    [['&&', '&', 'AND'], Operator::And],
                ]),
                self::binaryOperations([
                    [['||', '|', 'OR'], Operator::Or],
                ]),
            ],
        );

        $recursiveParser->setParser(spaced($parser));

        return $parser;
    }

    public static function literal(): ParserInterface
    {
        static $parser = null;
        if ($parser !== null) {
            return $parser;
        }

        $parser = alt(
            NumberExpr::getParser(),
            StringExpr::getParser(),
            LogicalValueExpr::getParser(),
        );

        return $parser;
    }

    public static function terminal(): ParserInterface
    {
        static $parser = null;
        if ($parser !== null) {
            return $parser;
        }

        $parser = alt(
            self::literal(),
            FunctionCallExpr::getParser(),
            DatabasePathExpr::getParser(),
            CollectionExpr::getParser(),
        );

        return $parser;
    }

    public static function isNotOperator(): ParserInterface
    {
        return seq(
            ParserUtil::wordAnyCase('IS'),
            spaces1(),
            ParserUtil::wordAnyCase('NOT'),
        )->toString();
    }

    /**
     * @param list<array{
     *     string|ParserInterface|(string|ParserInterface)[],
     *     Operator
     * }> $operationsDefinitions
     */
    protected static function binaryOperations(array $operationsDefinitions): callable
    {
        $alternatives = array_map(
            function ($def) {
                $operatorParsers = (array) $def[0];
                $operatorParsers = array_map(
                    fn(string|ParserInterface $p) => is_string($p) ? iStr($p) : $p,
                    $operatorParsers,
                );
                $operatorParser = count($operatorParsers) === 1
                    ? $operatorParsers[0]
                    : alt(...$operatorParsers);

                return value(
                    spaced($operatorParser), // Operator
                    $def[1], // Matching enum value
                );
            },
            $operationsDefinitions,
        );

        return ExpressionParser::leftAssocBinaryOper(
            alt(...$alternatives),
            fn($op, $a, $b) => new BinaryOperationExpr($op, $a, $b),
        );
    }
}
