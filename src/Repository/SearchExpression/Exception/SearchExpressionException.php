<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Exception;

use App\Exception\AppException;
use App\Repository\SearchExpression\ExpressionInterface;
use Throwable;

class SearchExpressionException extends AppException
{
    public function __construct(
        string $message = "",
        public readonly ?ExpressionInterface $expression = null,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        parent::__construct($message, $code, $previous);
    }
}
