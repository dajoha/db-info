<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\Interface\SingleParserInterface;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\VisitorInterface;
use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\Multi\separated1;
use function Dajoha\ParserCombinator\Parser\String\identifier;

class DatabasePathExpr
    extends AbstractExpression
    implements ExpressionInterface, SingleParserInterface
{
    public function __construct(
        /** @var string[] */
        public readonly array $columnAliases,
    ) {
    }

    public function __toString(): string
    {
        return implode('.', $this->columnAliases);
    }

    public static function getParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $parser = separated1(identifier(), '.')
            ->map(fn(array $columnAliases) => new self($columnAliases))
        ;

        return $parser;
    }

    public function accept(VisitorInterface $visitor): mixed
    {
        return $visitor->visitDatabasePath($this);
    }
}
