<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\ExpressionInterface;
use Dajoha\ParserCombinator\Stream\Base\Span\SpanAwareTrait;

abstract class AbstractExpression implements ExpressionInterface
{
    use SpanAwareTrait;

    protected bool $hasParentheses = false;

    public function withParentheses(): static
    {
        $this->hasParentheses = true;

        return $this;
    }

    public function hasParentheses(): bool
    {
        return $this->hasParentheses;
    }
}
