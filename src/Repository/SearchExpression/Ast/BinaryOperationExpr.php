<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\Model\Operator;
use App\Repository\SearchExpression\Exception\SearchExpressionException;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\VisitorInterface;

class BinaryOperationExpr
    extends AbstractExpression
    implements ExpressionInterface
{
    /**
     * @throws SearchExpressionException
     */
    public function __construct(
        public readonly Operator $operator,
        public readonly ExpressionInterface $operand1,
        public readonly ExpressionInterface $operand2,
    ) {
        $this->checkValidity();
    }

    public function accept(VisitorInterface $visitor): mixed
    {
        return $visitor->visitBinaryOperation($this);
    }

    /**
     * @throws SearchExpressionException
     */
    protected function checkValidity(): void
    {
        // Collections:
        if ($this->operand1 instanceof CollectionExpr) {
            throw new SearchExpressionException(
                'List is not allowed at this place',
                $this->operand1,
            );
        }
        if ($this->operator !== Operator::In && $this->operand2 instanceof CollectionExpr) {
            throw new SearchExpressionException(
                'List is not allowed at this place',
                $this->operand2,
            );
        }

        // "LIKE" operator:
        if (
            in_array($this->operator, [ Operator::Like, Operator::ILike ], true)
            && !$this->operand2 instanceof StringExpr
        ) {
            $operator = $this->operator->value;
            throw new SearchExpressionException(
                "Operator \"$operator\": right operand must be a string",
                $this->operand2,
            );
        }
    }
}
