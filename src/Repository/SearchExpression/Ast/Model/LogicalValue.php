<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast\Model;

enum LogicalValue: string
{
    case False = 'FALSE';
    case True = 'TRUE';
    case Null = 'NULL';
    case Unknown = 'UNKNOWN';

    public static function tryFromAnyCase(string $s): ?self
    {
        return self::tryFrom(strtoupper($s));
    }
}
