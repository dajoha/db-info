<?php

namespace App\Repository\SearchExpression\Ast\Model;

enum Operator: string
{
    case Mul = '*';
    case Div = '/';
    case Add = '+';
    case Sub = '-';
    case Gt = '>';
    case Gte = '>=';
    case Lt = '<';
    case Lte = '<=';
    case Eq = '=';
    case Neq = '!=';
    case And = '&';
    case Or = '|';
    case In = 'IN';
    case Is = 'IS';
    case IsNot = 'IS NOT';
    case Like = 'LIKE';
    case ILike = 'ILIKE';
    case SimpleLike = '~';
}
