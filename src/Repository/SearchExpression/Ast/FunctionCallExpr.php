<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\Interface\SingleParserInterface;
use App\Repository\SearchExpression\Exception\SearchExpressionException;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\SearchExpressionParser;
use App\Repository\SearchExpression\VisitorInterface;
use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Misc\recursive;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Multi\separated1;
use function Dajoha\ParserCombinator\Parser\Sequence\delimited;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\space;
use function Dajoha\ParserCombinator\Parser\String\spaced;

class FunctionCallExpr
    extends AbstractExpression
    implements ExpressionInterface, SingleParserInterface
{
    /**
     * @param list<ExpressionInterface> $arguments
     *
     * @throws SearchExpressionException
     */
    public function __construct(
        public readonly string $name,
        public readonly array $arguments,
    ) {
        $this->checkValidity();
    }

    public function accept(VisitorInterface $visitor): mixed
    {
        return $visitor->visitFunctionCall($this);
    }

    public static function getParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $separator = alt(
            spaced(','),
            many1(space()),
        );

        $expression = recursive();

        $parser =
            seq(
                identifier(),
                delimited(
                    spaced('('),
                    separated1($expression, $separator)
                        ->followedBy(opt($separator)),
                    spaced(')'),
                )
            )
        ;

        $parser = $parser->map(function (array $sequence) {
            [$functionName, $arguments] = $sequence;

            return new self($functionName, $arguments);
        });

        $expression->setParser(SearchExpressionParser::expression());

        return $parser;
    }

    /**
     * @throws SearchExpressionException
     */
    protected function checkValidity(): void
    {
        // TODO: check if function name is not a reserved keyword

        // Collections:
        foreach ($this->arguments as $argument) {
            if ($argument instanceof CollectionExpr) {
                throw new SearchExpressionException(
                    'List is not allowed at this place',
                    $argument,
                );
            }
        }
    }
}
