<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\Interface\SingleParserInterface;
use App\Repository\SearchExpression\Exception\SearchExpressionException;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\SearchExpressionParser;
use App\Repository\SearchExpression\VisitorInterface;
use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Misc\recursive;
use function Dajoha\ParserCombinator\Parser\Multi\many1;
use function Dajoha\ParserCombinator\Parser\Multi\separated1;
use function Dajoha\ParserCombinator\Parser\Sequence\delimited;
use function Dajoha\ParserCombinator\Parser\String\space;
use function Dajoha\ParserCombinator\Parser\String\spaced;

class CollectionExpr
    extends AbstractExpression
    implements SingleParserInterface
{
    /**
     * @param ExpressionInterface[] $values
     *
     * @throws SearchExpressionException
     */
    public function __construct(public readonly array $values)
    {
        $this->checkValidity();
    }

    public static function getParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $separator = alt(
            spaced(','),
            many1(space()),
        );

        $expression = recursive();

        $parser = delimited(
            spaced('['),
            separated1($expression, $separator)
                ->followedBy(opt($separator)),
            spaced(']'),
        );

        $parser = $parser->map(fn(array $values) => new self($values));

        $expression->setParser(SearchExpressionParser::expression());

        return $parser;
    }

    public function accept(VisitorInterface $visitor): mixed
    {
        return $visitor->visitCollection($this);
    }

    /**
     * @throws SearchExpressionException
     */
    protected function checkValidity(): void
    {
        foreach ($this->values as $value) {
            if ($value instanceof CollectionExpr) {
                throw new SearchExpressionException('Collections cannot be nested', $value);
            }
        }
    }
}
