<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\Interface\LiteralInterface;
use App\Repository\SearchExpression\VisitorInterface;
use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\String\float;

class NumberExpr
    extends AbstractExpression
    implements LiteralInterface
{
    public readonly float $value;

    public function __construct(int|float $value)
    {
        $this->value = (float) $value;
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public function getLiteralValue(): int|float
    {
        return $this->value;
    }

    public static function getParser(): ParserInterface
    {
        return float()->map(fn($v) => new self($v));
    }

    public function accept(VisitorInterface $visitor): mixed
    {
        return $visitor->visitNumber($this);
    }
}
