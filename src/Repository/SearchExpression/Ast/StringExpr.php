<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\Interface\LiteralInterface;
use App\Repository\SearchExpression\VisitorInterface;
use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\String\delimitedStr;

class StringExpr
    extends AbstractExpression
    implements LiteralInterface
{
    public function __construct(public readonly string $value)
    {
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function getLiteralValue(): string
    {
        return $this->value;
    }

    public static function getParser(): ParserInterface
    {
        return delimitedStr("'", escapeChar: "\\")->map(fn($v) => new self($v));
    }

    public function accept(VisitorInterface $visitor): mixed
    {
        return $visitor->visitStr($this);
    }
}
