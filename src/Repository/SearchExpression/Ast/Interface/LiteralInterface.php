<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast\Interface;

use App\Repository\SearchExpression\ExpressionInterface;

interface LiteralInterface extends ExpressionInterface, SingleParserInterface
{
    public function getLiteralValue(): bool|int|float|string;
}
