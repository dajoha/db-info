<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast\Interface;

use Dajoha\ParserCombinator\Base\ParserInterface;

interface SingleParserInterface
{
    public static function getParser(): ParserInterface;
}
