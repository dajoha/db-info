<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\Model\LogicalValue;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\VisitorInterface;
use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\Character\alpha;
use function Dajoha\ParserCombinator\Parser\Multi\many1;

class LogicalValueExpr
    extends AbstractExpression
    implements ExpressionInterface
{
    public function __construct(public readonly LogicalValue $logicalValue)
    {
    }

    public function __toString(): string
    {
        return $this->logicalValue->value;
    }

    public static function getParser(): ParserInterface
    {
        return many1(alpha())
            ->toString()
            ->map(function (string $v) {
                $value = LogicalValue::tryFromAnyCase($v);

                return $value === null ? null : new self($value);
            })
            ->verify(fn($v) => $v !== null);
    }

    public function accept(VisitorInterface $visitor): mixed
    {
        return $visitor->visitLogicalValue($this);
    }
}
