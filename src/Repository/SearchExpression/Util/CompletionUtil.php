<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression\Util;

class CompletionUtil
{
    private function __construct()
    {
    }

    /**
     * Checks that at the end of the input, we are inside a string or not. E.g. if the input is
     * "v = 'hell", then it will return `true` because a single quote has been opened but not
     * closed. Useful for completion.
     *
     * @phpstan-assert-if-true int $stringStartPosition
     */
    public static function insideString(string $input, ?int &$stringStartPosition = null): bool
    {
        $insideString = false;
        $skipNextChar = false;

        for ($i = 0; $i < strlen($input); $i++) {
            if ($skipNextChar) {
                $skipNextChar = false;
                continue;
            }

            switch ($input[$i]) {
                case '\\':
                    $skipNextChar = true;
                    break;
                case '\'':
                    $insideString = !$insideString;
                    $stringStartPosition = $insideString ? $i + 1 : null;
                    break;
            }
        }

        return $insideString;
    }

    /**
     * @template T of string|int
     *
     * @phpstan-param T $value
     *
     * @return (T is string ? string : int)
     *
     * Escapes a string in order to be usable as a search expression string (without quotes around).
     * Leaves int values as they are.
     */
    public static function escapeValue(string|int $value): string|int
    {
        return is_string($value) ? self::escapeString($value) : $value;
    }

    /**
     * Escapes a string in order to be usable as a search expression string (without quotes around).
     */
    public static function escapeString(string $value): string
    {
        return str_replace(['\\', '\''], ['\\\\', '\\\''], $value);
    }
}
