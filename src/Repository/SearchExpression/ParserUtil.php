<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression;

use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\String\identifier;

class ParserUtil
{
    public static function wordAnyCase(string $word): ParserInterface
    {
        return identifier()->verify(fn($s) => strcasecmp($s, $word) === 0);
    }
}
