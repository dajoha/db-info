<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression;

use App\Repository\SearchExpression\Ast\BinaryOperationExpr;
use App\Repository\SearchExpression\Ast\CollectionExpr;
use App\Repository\SearchExpression\Ast\DatabasePathExpr;
use App\Repository\SearchExpression\Ast\FunctionCallExpr;
use App\Repository\SearchExpression\Ast\LogicalValueExpr;
use App\Repository\SearchExpression\Ast\NumberExpr;
use App\Repository\SearchExpression\Ast\StringExpr;

interface VisitorInterface
{
    public function visitStr(StringExpr $stringExpr): mixed;
    public function visitNumber(NumberExpr $numberExpr): mixed;
    public function visitLogicalValue(LogicalValueExpr $logicalValueExpr): mixed;
    public function visitCollection(CollectionExpr $collectionExpr): mixed;
    public function visitDatabasePath(DatabasePathExpr $pathExpr): mixed;
    public function visitBinaryOperation(BinaryOperationExpr $operationExpr): mixed;
    public function visitFunctionCall(FunctionCallExpr $functionCallExpr): mixed;
}
