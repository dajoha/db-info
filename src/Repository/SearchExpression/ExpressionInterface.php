<?php

declare(strict_types=1);

namespace App\Repository\SearchExpression;

use Dajoha\ParserCombinator\Stream\Base\Span\SpanAwareInterface;

interface ExpressionInterface extends SpanAwareInterface
{
    public function withParentheses(): static;

    public function hasParentheses(): bool;

    public function accept(VisitorInterface $visitor): mixed;
}
