<?php

declare(strict_types=1);

namespace App\Repository\Exception;

use App\Exception\AppException;
use Throwable;

class WrongPrimaryKeyValueException extends AppException
{
    /**
     * @param list<int|string> $values
     */
    public function __construct(
        public readonly string $tableName,
        array $values,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $values = implode('/', $values);
        $message = "Table \"$tableName\": wrong primary key value(s): $values";

        parent::__construct($message, $code, $previous);
    }
}
