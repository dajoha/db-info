<?php

declare(strict_types=1);

namespace App\Repository\Exception;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Exception\AppException;
use Throwable;

class PageSizeIsTooLargeException extends AppException
{
    public function __construct(
        public readonly int $size,
        public readonly int $listMaxResults,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $configPath = [
            K::ROOT,
            K::PROJECTS,
            '<PROJECT_NAME>',
            K::LIST_MAX_RESULTS,
        ];
        $configKey = implode('.', $configPath);

        $message =
            "The page size \"$size\" is too large (max: $listMaxResults). ".
            "You can configure it via the configuration key \"$configKey\".";

        parent::__construct($message, $code, $previous);
    }
}
