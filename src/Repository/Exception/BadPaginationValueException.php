<?php

declare(strict_types=1);

namespace App\Repository\Exception;

use App\Exception\AppException;
use Throwable;

class BadPaginationValueException extends AppException
{
    public function __construct(
        public readonly string $paginationValue,
        public readonly string $parseMessage,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message = "Bad pagination value (\"$this->paginationValue\"): $this->parseMessage";
        parent::__construct($message, $code, $previous);
    }
}
