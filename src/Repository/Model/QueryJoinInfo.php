<?php

declare(strict_types=1);

namespace App\Repository\Model;

use App\Project\Data\TableInfo;
use App\Repository\JoinQueryBuilder\JoinQueryBuilder;

/**
 * Stores information about a specific table join, for {@see JoinQueryBuilder}. The main information
 * is the table sql alias, which can be reused if already present.
 */
readonly class QueryJoinInfo
{
    public function __construct(
        /**
         * The full path to the joined table, relative to the query's main table
         *
         * @var string[]
         */
        public array $joinPath,

        /** The `TableInfo` of the joined table */
        public TableInfo $tableInfo,

        /** The SQL alias of the joined table */
        public string $tableAlias,
    ) {
    }
}
