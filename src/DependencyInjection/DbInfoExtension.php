<?php

declare(strict_types=1);

namespace App\DependencyInjection;

use Exception;
use LogicException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

/**
 * @phpstan-import-type Type_SymfonyConfiguration from Configuration
 */
class DbInfoExtension extends Extension
{
    public const string CONTAINER_PARAM__MAIN_CONFIG_FILE = 'main_configuration_file';
    /**
     * @var ?Type_SymfonyConfiguration
     */
    protected ?array $config = null;

    /**
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        /** @phpstan-ignore assign.propertyType (OK: Validated by Symfony) */
        $this->config ??= $this->processConfiguration($configuration, $configs);

        $container->setParameter(
            self::CONTAINER_PARAM__MAIN_CONFIG_FILE,
            $this->config[ConfigurationKeys::CONFIG_FILE],
        );
    }

    /**
     * @return Type_SymfonyConfiguration
     */
    public function getConfig(): array
    {
        if ($this->config === null) {
            throw new LogicException('Should only be called after DbInfoExtension has been loaded');
        }

        return $this->config;
    }
}
