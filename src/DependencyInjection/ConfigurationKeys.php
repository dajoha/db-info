<?php

declare(strict_types=1);

namespace App\DependencyInjection;

class ConfigurationKeys
{
    public const string CONFIG_FILE = 'config_file';
}
