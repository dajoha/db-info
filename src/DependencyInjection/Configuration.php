<?php

declare(strict_types=1);

namespace App\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Real configuration is defined in directory "src/Configuration".
 *
 * @phpstan-type Type_SymfonyConfiguration array{
 *     config_file: string,
 * }
 */
class Configuration implements ConfigurationInterface
{
    public const string ROOT_NAME = 'db_info';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NAME);
        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode(ConfigurationKeys::CONFIG_FILE)
                    ->isRequired()
                    ->validate()
                        ->ifTrue(fn($v) => !is_string($v))
                        ->thenInvalid('Invalid type, expected string')
                    ->end()
                ->end()
        ;

        return $treeBuilder;
    }
}
