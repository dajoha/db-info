<?php

declare(strict_types=1);

namespace App\Http;

use Symfony\Component\HttpFoundation\Response;

class NoContentResponse extends Response
{
    /**
     * @param array<string, string|string[]|null> $headers
     */
    public function __construct(array $headers = [])
    {
        parent::__construct(null, Response::HTTP_NO_CONTENT, $headers);
    }
}
