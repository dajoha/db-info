<?php

declare(strict_types=1);

namespace App\Service\Http;

use App\Service\Http\Exception\NoCurrentRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class RequestProvider
{
    public function __construct(protected RequestStack $requestStack)
    {
    }

    public function getCurrentRequest(): ?Request
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * @throws NoCurrentRequestException
     */
    public function getCurrentRequestOrThrow(): Request
    {
        $currentRequest = $this->requestStack->getCurrentRequest();

        if ($currentRequest === null) {
            throw new NoCurrentRequestException();
        }

        return $currentRequest;
    }
}
