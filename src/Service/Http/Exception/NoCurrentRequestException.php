<?php

declare(strict_types=1);

namespace App\Service\Http\Exception;

use App\Exception\AppException;

class NoCurrentRequestException extends AppException
{
    public function __construct()
    {
        parent::__construct("No current request");
    }
}
