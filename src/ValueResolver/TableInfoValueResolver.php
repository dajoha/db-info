<?php

declare(strict_types=1);

namespace App\ValueResolver;

use App\Exception\InternalException;
use App\Project\Data\TableInfo;
use App\Project\Exception\ProjectDoesNotExistException;
use App\Project\Exception\TableDoesNotExistException;
use App\Project\ProjectManager;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

#[AutoconfigureTag('controller.argument_value_resolver', ['priority' => 150])]
readonly class TableInfoValueResolver implements ValueResolverInterface
{
    public const string ATTRIBUTE_PROJECT = 'project';
    public const string ATTRIBUTE_TABLE_ALIAS = 'tableAlias';

    public function __construct(protected ProjectManager $projectManager)
    {
    }

    /**
     * @return iterable<TableInfo>
     *
     * @throws InternalException
     * @throws InvalidArgumentException
     * @throws TableDoesNotExistException
     * @throws ProjectDoesNotExistException
     * @throws Exception
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $argumentType = $argument->getType();
        if (!$argumentType || !is_a($argumentType, TableInfo::class, true)) {
            return [];
        }

        $projectName = $request->attributes->get(self::ATTRIBUTE_PROJECT);
        if (!is_string($projectName)) {
            return [];
        }

        $project = $this->projectManager->getProject($projectName);

        $tableAlias = $request->attributes->get(self::ATTRIBUTE_TABLE_ALIAS);
        if (!is_string($tableAlias)) {
            throw new InternalException('Attribute "tableAlias" must be provided in the request');
        }

        yield $project->getTableInfoFromAliasOrThrow($tableAlias);
    }
}
