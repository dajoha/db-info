<?php

declare(strict_types=1);

namespace App\ValueResolver;

use App\Project\Exception\ProjectDoesNotExistException;
use App\Project\Project;
use App\Project\ProjectManager;
use Doctrine\DBAL\Exception;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

#[AutoconfigureTag('controller.argument_value_resolver', ['priority' => 150])]
readonly class ProjectValueResolver implements ValueResolverInterface
{
    public function __construct(protected ProjectManager $projectManager)
    {
    }

    /**
     * @return iterable<Project>
     *
     * @throws Exception
     * @throws ProjectDoesNotExistException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $argumentType = $argument->getType();
        if (!$argumentType || !is_a($argumentType, Project::class, true)) {
            return [];
        }

        // Get the value from the request, based on the argument name:
        $projectName = $request->attributes->get($argument->getName());
        if (!is_string($projectName)) {
            return [];
        }

        yield $this->projectManager->getProject($projectName);
    }
}
