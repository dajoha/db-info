<?php

declare(strict_types=1);

namespace App\Controller\Request;

use App\Exception\AppException;
use App\Repository\QueryPart\Sort\Sorts;
use App\Service\Http\RequestProvider;

class RequestSortsProvider
{
    protected ?Sorts $sorts = null;

    public function __construct(protected RequestProvider $requestProvider)
    {
    }

    /**
     * @throws AppException
     */
    public function getSortsFromRequestOrThrow(): Sorts
    {
        if ($this->sorts === null) {
            $request = $this->requestProvider->getCurrentRequestOrThrow();
            $this->sorts = Sorts::fromRequestOrThrow($request);
        }

        return $this->sorts;
    }
}
