<?php

declare(strict_types=1);

namespace App\Controller\Request;

class QueryParam
{
    public const string FORMAT = 'format';
    public const string FORMAT__HTML_DATA = 'html-data';
    public const string LIST_FIELD = 'field';
    public const string LIST_SEARCH = 'search';
    public const string LIST_SORT = 'sort';
    public const string LIST_TRANSPOSED = 'transposed';
}
