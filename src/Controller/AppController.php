<?php

declare(strict_types=1);

namespace App\Controller;

use App\Configuration\Service\ConfigProvider;
use App\Controller\Exception\QueryStringException;
use App\Controller\Request\QueryParam;
use App\Controller\Request\RequestSortsProvider;
use App\Exception\AppException;
use App\Exception\NotImplementedException;
use App\Exception\ValidationException;
use App\Frontend\Factory\ListColumnsFactory;
use App\Frontend\Factory\TableStructureFactory;
use App\Project\Data\TableInfo;
use App\Project\Exception\NoPrimaryKeyException;
use App\Project\Exception\TableColumnDoesNotExistException;
use App\Project\Model\Exception\BadPathJoinColumnException;
use App\Project\Project;
use App\Repository\Exception\BadPaginationValueException;
use App\Repository\Exception\PageSizeIsTooLargeException;
use App\Repository\Exception\WrongPrimaryKeyValueException;
use App\Repository\QueryPart\Pagination\Pagination;
use App\Repository\QuickFind\Exception\BadQuickFindSearchException;
use App\Repository\QuickFind\Exception\ColumnNameIsNotSetException;
use App\Repository\QuickFind\Exception\UnknownQuickFindOptionAliasException;
use App\Repository\QuickFind\QuickFindRepository;
use App\Router\Util\UrlUtil;
use App\Twig\TwigContextProvider;
use App\Validation\Validator;
use App\Validation\Validator\Filters as FiltersConstraint;
use Doctrine\DBAL\Exception as DbalException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Attribute\Route;

/**
 * @phpstan-import-type Type_TwigBaseContext from TwigContextProvider
 */
class AppController extends AbstractController
{
    public const string HEADER_NB_ROWS = self::HEADER_PREFIX . 'Nb-Rows';
    public const string HEADER_TOTAL_ROWS = self::HEADER_PREFIX . 'Total-Rows';
    public const string HEADER_PAGE = self::HEADER_PREFIX . 'Page';
    public const string HEADER_PAGE_SIZE = self::HEADER_PREFIX . 'Page-Size';
    public const string HEADER_TOTAL_PAGES = self::HEADER_PREFIX . 'Total-Pages';

    protected const string HEADER_PREFIX = 'Db-Info-';

    public function __construct(protected readonly TwigContextProvider $twigContextProvider)
    {
    }

    #[Route('', name: 'home')]
    public function home(ConfigProvider $configProvider): Response
    {
        return $this->render('pages/home.html.twig', $this->getTwigContext(extraContext: [
            'project_names' => $configProvider->getProjectNames(),
        ]));
    }

    /**
     * @throws InvalidArgumentException
     */
    #[Route('projects/{project}', name: 'project_home')]
    public function projectHome(Project $project): Response
    {
        return $this->render('pages/project_home.html.twig', $this->getTwigContext(
            project: $project,
            extraContext: [
                'table_infos' => $project->getAllTableInfos(),
            ],
        ));
    }

    /**
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws NoPrimaryKeyException
     * @throws WrongPrimaryKeyValueException
     */
    #[Route('projects/{project}/tables/{tableAlias}/get/{ids<.+>}', name: 'get_row')]
    public function getRow(Project $project, TableInfo $tableInfo, string $ids): Response
    {
        $indexValues = UrlUtil::decodePrimaryKeyIndex($tableInfo, $ids);

        $row = $project->getRepository()->find($tableInfo->name, $indexValues);

        return $this->render('pages/view_row.html.twig', $this->getTwigContext(
            tableInfo: $tableInfo,
            extraContext: [
                'row' => $row,
            ],
        ));
    }

    /**
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws TableColumnDoesNotExistException
     * @throws NotImplementedException
     */
    #[Route('projects/{project}/tables/{tableAlias}/by/{columnAlias}/{value}', name: 'get_row_by')]
    public function getRowBy(
        Project $project,
        TableInfo $tableInfo,
        string $columnAlias,
        string $value,
    ): Response
    {
        $column = $tableInfo->getColumnFromAliasOrThrow($columnAlias);
        $value = UrlUtil::decodeDatabaseValue($value, $column);

        $row = $project->getRepository()->findBy($tableInfo->name, $columnAlias, $value);

        return $this->render('pages/view_row.html.twig', $this->getTwigContext(
            tableInfo: $tableInfo,
            extraContext: [
                'row' => $row,
            ],
        ));
    }

    /**
     * @param array<string, mixed> $fields
     *
     * @throws AppException
     * @throws BadPaginationValueException
     * @throws BadPathJoinColumnException
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws PageSizeIsTooLargeException
     * @throws QueryStringException
     * @throws ValidationException
     */
    #[Route('projects/{project}/tables/{tableAlias}', name: 'list_rows')]
    public function listRows(
        Project $project,
        TableInfo $tableInfo,
        #[MapQueryParameter(name: QueryParam::FORMAT)] ?string $format,
        #[MapQueryParameter(name: QueryParam::LIST_FIELD)] ?array $fields,
        #[MapQueryParameter(name: QueryParam::LIST_SEARCH)] ?string $searchExpression,
        #[MapQueryParameter(name: QueryParam::LIST_TRANSPOSED)] ?bool $transposed,
        Request $request,
        Validator $validator,
        ListColumnsFactory $listColumnsFactory,
        RequestSortsProvider $requestSortsProvider,
    ): Response
    {
        $validator->validateOrThrow(
            $fields,
            new FiltersConstraint(),
            '"field[]" query parameters',
        );

        $fields = iter($fields ?? [])
            ->map(function(string $value, string $columnPath) use ($tableInfo) {
                return UrlUtil::decodeDatabaseValueFromPath($value, $tableInfo, $columnPath);
            })
            ->toArray()
        ;

        $repository = $project->getRepository();
        $transposed ??= false;

        $pagination = Pagination::fromRequestOrThrow(
            $request,
            $tableInfo->getDefaultPaginationSize(),
        );

        switch ($format) {
            case QueryParam::FORMAT__HTML_DATA:
                $sorts = $requestSortsProvider->getSortsFromRequestOrThrow();

                $rows = $repository->list(
                    $tableInfo->name,
                    $fields,
                    $searchExpression,
                    $sorts,
                    $pagination,
                    $totalRows,
                );
                $nbRows = count($rows);

                $listColumns = $listColumnsFactory->createListColumns($tableInfo, $sorts);

                $template = match ($transposed) {
                    true => 'include/rows_list_transposed.html.twig',
                    false => 'include/rows_list.html.twig',
                };

                $response = $this->render($template, $this->getTwigContext(
                    tableInfo: $tableInfo,
                    extraContext: [
                        'list_columns' => $listColumns,
                        'pagination' => $pagination,
                        'rows' => $rows,
                        'nb_rows' => $nbRows,
                        'total_rows' => $totalRows,
                        'is_filtered' => !empty($fields),
                    ],
                ));

                $this->addListHeaders($response, $nbRows, $totalRows, $pagination);

                break;
            case null:
                $response = $this->render('pages/list_rows.html.twig', $this->getTwigContext(
                    tableInfo: $tableInfo,
                    extraContext: [
                        'pagination' => $pagination,
                        'search_expression' => $searchExpression,
                        'is_filtered' => !empty($fields),
                    ],
                ));

                break;
            default:
                throw new QueryStringException('format');
        }

        return $response;
    }

    /**
     * @throws BadQuickFindSearchException
     * @throws ColumnNameIsNotSetException
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws UnknownQuickFindOptionAliasException
     */
    #[Route('projects/{project}/tables/{tableAlias}/find/{search}', name: 'quick_find_row')]
    #[Route('projects/{project}/tables/{tableAlias}/{search}', name: 'quick_find_row_short')]
    public function searchRow(
        Project $project,
        TableInfo $tableInfo,
        string $search,
        QuickFindRepository $quickFindRepository,
    ): Response
    {
        $row = $quickFindRepository->quickFind($project, $tableInfo->name, $search);

        return $this->render('pages/view_row.html.twig', $this->getTwigContext(
            tableInfo: $tableInfo,
            extraContext: [
                'row' => $row,
            ],
        ));
    }

    /**
     * @param array<string, mixed> $fields
     *
     * @throws AppException
     * @throws BadPathJoinColumnException
     * @throws DbalException
     * @throws InvalidArgumentException
     * @throws PageSizeIsTooLargeException
     * @throws ValidationException
     */
    #[Route('projects/{project}/tables/{tableAlias}/seek/first', name: 'get_first_row')]
    public function getFirstRow(
        Project $project,
        TableInfo $tableInfo,
        #[MapQueryParameter(name: QueryParam::LIST_FIELD)] ?array $fields,
        #[MapQueryParameter(name: QueryParam::LIST_SEARCH)] ?string $searchExpression,
        Validator $validator,
        RequestSortsProvider $requestSortsProvider,
    ): Response
    {
        $validator->validateOrThrow($fields, new FiltersConstraint(), '"field[]" query parameters');

        $fields ??= [];
        $sorts = $requestSortsProvider->getSortsFromRequestOrThrow();

        $row = $project->getRepository()->findFirst(
            $tableInfo->name,
            $fields,
            $searchExpression,
            $sorts,
        );

        return $this->render('pages/view_row.html.twig', $this->getTwigContext(
            tableInfo: $tableInfo,
            extraContext: [
                'row' => $row,
                'is_first_row_page' => true,
            ],
        ));
    }

    /**
     * @throws InvalidArgumentException
     * @throws DbalException
     */
    #[Route('projects/{project}/table-structures/{tableAlias}', name: 'get_table_structure')]
    public function getTableStructure(
        TableInfo $tableInfo,
        TableStructureFactory $tableStructureFactory,
    ): Response {
        $tableStructure = $tableStructureFactory->createTableStructure($tableInfo);

        return $this->render('pages/view_table_structure.html.twig', $this->getTwigContext(
            tableInfo: $tableInfo,
            extraContext: [
                'table_structure' => $tableStructure,
            ],
        ));
    }

    protected function addListHeaders(
        Response $response,
        int $nbRows,
        int $totalRows,
        ?Pagination $pagination,
    ): Response
    {
        $response->headers->set(self::HEADER_NB_ROWS, (string) $nbRows);
        $response->headers->set(self::HEADER_TOTAL_ROWS, (string) $totalRows);

        if ($pagination !== null) {
            $headers = [
                self::HEADER_PAGE => $pagination->page,
                self::HEADER_PAGE_SIZE => $pagination->size,
                self::HEADER_TOTAL_PAGES => $pagination->getTotalPages($totalRows),
            ];
            $response->headers->add(array_map(strval(...), $headers));
        }

        return $response;
    }

    /**
     * @param array<string, mixed> $extraContext
     *
     * @return Type_TwigBaseContext|array<string, mixed>
     */
    public function getTwigContext(
        ?Project $project = null,
        ?TableInfo $tableInfo = null,
        array $extraContext = [],
    ): array {
        return $this->twigContextProvider->getTwigContext($project, $tableInfo, $extraContext);
    }
}
