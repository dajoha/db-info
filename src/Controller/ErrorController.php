<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Request\QueryParam;
use App\ErrorHandler\AppErrorHandler;
use App\Project\Exception\ProjectAwareExceptionInterface;
use App\Twig\TwigContextProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Controller\ErrorController as BaseErrorController;
use Throwable;

#[AsController]
class ErrorController extends AbstractController
{
    public function __construct(
        #[Autowire(service: 'error_controller')]
        protected readonly BaseErrorController $baseErrorController,
        protected readonly RequestStack $requestStack,
        protected readonly TwigContextProvider $twigContextProvider,
    ) {
    }

    public function __invoke(Throwable $exception): Response
    {
        $currentRequest = $this->requestStack->getCurrentRequest();
        $format = $currentRequest?->query?->getString(QueryParam::FORMAT);

        // Transform some external exceptions into app exceptions:
        $exception = AppErrorHandler::toAppException($exception);

        if (!AppErrorHandler::isAppException($exception)) {
            return ($this->baseErrorController)($exception);
        }

        // The query parameter "?format=html-data" is used by javascript to fetch html data, but on
        // error the javascript code checks for json data if provided, in order to display the error
        // message in a modal window:
        if ($format === QueryParam::FORMAT__HTML_DATA) {
            $data = AppErrorHandler::getJsonError($exception);
            return new JsonResponse($data, 500);
        } else {
            // Try to retrieve the current project, in order to propose more controls in the navbar
            // of the error page:
            $project = null;
            if ($exception instanceof ProjectAwareExceptionInterface) {
                $project = $exception->getProject();
            }

            return $this->render('pages/error.html.twig', $this->twigContextProvider->getTwigContext(
                project: $project,
                extraContext: [
                    'error_message' => $exception->getMessage(),
                ]
            ));
        }
    }
}
