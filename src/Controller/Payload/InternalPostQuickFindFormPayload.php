<?php

declare(strict_types=1);

namespace App\Controller\Payload;

use Symfony\Component\Serializer\Annotation\SerializedName;

readonly class InternalPostQuickFindFormPayload
{
    public function __construct(
        public string $project,
        #[SerializedName('current_table')]
        public ?string $currentTableName,
        #[SerializedName('table_alias')]
        public string $tableAlias,
        public string $search,
    ) {
    }
}
