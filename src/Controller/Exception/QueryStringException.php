<?php

declare(strict_types=1);

namespace App\Controller\Exception;

use App\Exception\AppException;
use Throwable;

class QueryStringException extends AppException
{
    public function __construct(
        public readonly string $parameter,
        int $code = 0,
        ?Throwable $previous = null,
    )
    {
        $message = "Bad parameter value in query string for \"$parameter\"";
        parent::__construct($message, $code, $previous);
    }
}
