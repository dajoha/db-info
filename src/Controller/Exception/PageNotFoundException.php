<?php

declare(strict_types=1);

namespace App\Controller\Exception;

use App\Exception\AppException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * A wrapper for {@see NotFoundHttpException}.
 */
class PageNotFoundException extends AppException
{
    public function __construct(
        int $code = 404,
        ?Throwable $previous = null,
    ) {
        $message = "Page not found";

        parent::__construct($message, $code, $previous);
    }
}
