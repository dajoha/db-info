<?php

declare(strict_types=1);

namespace App\Controller;

use App\Cache\Cache;
use App\Http\NoContentResponse;
use App\Project\Exception\TableDoesNotExistException;
use App\Project\Project;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;

#[Route('cache/invalidate')]
class CacheInvalidateController extends AbstractController
{
    public function __construct(protected Cache $cache)
    {
    }

    #[Route('', name: 'cache_invalidate_all', methods: 'POST')]
    public function invalidateAll(): NoContentResponse
    {
        $this->cache->clear();

        return new NoContentResponse();
    }

    /**
     * @throws InvalidArgumentException
     */
    #[Route('/projects/{project}', name: 'cache_invalidate_project', methods: 'POST')]
    public function invalidateProject(Project $project): NoContentResponse
    {
        $this->cache->clearProject($project);

        return new NoContentResponse();
    }

    /**
     * @throws InvalidArgumentException
     * @throws TableDoesNotExistException
     */
    #[Route('/projects/{project}/tables/{tableAlias}', name: 'cache_invalidate_table', methods: 'POST')]
    public function invalidateTable(Project $project, string $tableAlias): NoContentResponse
    {
        $tableInfo = $project->getTableInfoFromAliasOrThrow($tableAlias);
        $this->cache->clearTable($tableInfo);

        return new NoContentResponse();
    }
}
