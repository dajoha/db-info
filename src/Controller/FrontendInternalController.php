<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\Payload\InternalPostQuickFindFormPayload;
use App\Frontend\Service\AutoCompletion\AutoCompletionService;
use App\Project\Exception\ProjectDoesNotExistException;
use App\Project\Project;
use App\Router\AppRouter;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('internal/')]
class FrontendInternalController extends AbstractController
{
    /**
     * @throws InvalidArgumentException
     */
    #[Route(
        'projects/{project}/tables/{tableName}/filter-autocomplete',
        name: 'internal_table_filter_completions',
    )]
    public function getTableFilterCompletions(
        Project $project,
        string $tableName,
        #[MapQueryParameter] string $input,
        #[MapQueryParameter(name: 'cursor')] int $cursorPosition,
        AutoCompletionService $autoCompletionService,
        SerializerInterface $serializer,
    ): JsonResponse
    {
        $completions = $autoCompletionService->getTableFilterCompletions(
            $project,
            $tableName,
            $input,
            $cursorPosition,
        );
        $completions = $serializer->serialize($completions, 'json');

        return new JsonResponse($completions, json: true);
    }

    #[Route(
        'projects/{project}/table-alias-autocomplete',
        name: 'internal_table_alias_completions',
    )]
    public function getTableAliasCompletions(
        Project $project,
        AutoCompletionService $autoCompletionService,
        SerializerInterface $serializer,
    ): JsonResponse
    {
        $suggestions = $autoCompletionService->getTableAliasCompletions($project);
        $suggestions = $serializer->serialize($suggestions, 'json');

        return new JsonResponse($suggestions, json: true);
    }

    /**
     * @throws Exception
     * @throws ProjectDoesNotExistException
     * @throws InvalidArgumentException
     */
    #[Route(
        'quick-find',
        name: 'internal_post_quick_find_form',
        methods: 'POST',
    )]
    public function postQuickFindForm(
        #[MapRequestPayload]
        InternalPostQuickFindFormPayload $payload,
        AppRouter $appRouter,
    ): RedirectResponse
    {
        $tableAlias = empty($payload->tableAlias) ? $payload->currentTableName : $payload->tableAlias;
        if (empty($tableAlias)) {
            throw new BadRequestException("Expect a table name, unless the search is performed from a table-related page");
        }

        if (empty($payload->search)) {
            return $this->redirect($appRouter->getTablePath($payload->project, $tableAlias));
        } else {
            return $this->redirectToRoute('quick_find_row_short', [
                'project' => $payload->project,
                'tableAlias' => $tableAlias,
                'search' => $payload->search,
            ]);
        }
    }
}
