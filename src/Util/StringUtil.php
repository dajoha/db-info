<?php

declare(strict_types=1);

namespace App\Util;

class StringUtil
{
    private function __construct()
    {
    }

    /**
     * Strip the given $needle at the end of the $haystack if present.
     */
    public static function stripEnd(string $haystack, string $needle): string
    {
        if (str_ends_with($haystack, $needle)) {
            $haystack = substr($haystack, 0, strlen($haystack) - strlen($needle));
        }

        return $haystack;
    }

    public static function isIdentifier(string $name): bool
    {
        return preg_match('/^[a-zA_Z_-][a-zA_Z0-9_-]*$/', $name) === 1;
    }
}
