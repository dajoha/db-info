<?php

declare(strict_types=1);

namespace App\Util;

use RuntimeException;

class ClassUtil
{
    private function __construct()
    {
    }

    public static function getBaseNamespace(string $namespace, string $tailToStrip): string
    {
        if (!str_ends_with($namespace, $tailToStrip)) {
            throw new RuntimeException("Bad namespace: \"$namespace\"");
        }

        return substr($namespace, 0, strlen($namespace) - strlen($tailToStrip));
    }

    public static function getClassName(string $fqcn): string
    {
        $parts = explode('\\', $fqcn);

        return $parts[count($parts) - 1];
    }
}
