<?php

declare(strict_types=1);

namespace App\Util;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\PostgreSQLPlatform;

class PlatformUtil
{
    private function __construct()
    {
    }

    public static function implementsILike(AbstractPlatform $platform): bool
    {
        return $platform instanceof PostgreSQLPlatform;
    }
}
