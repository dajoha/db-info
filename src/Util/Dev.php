<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace App\Util;

class Dev
{
    private function __construct()
    {
    }

    protected const string DEFAULT_CHRONO_NAME = 'default';

    /** @var array<string, float> */
    static array $chronos = [];

    /** @var array<string, float> */
    static array $cumulateChronos = [];

    /** @var array<string, float> */
    static array $currentCumulateChronos = [];

    public static function startChrono(string $name = self::DEFAULT_CHRONO_NAME): void
    {
        self::$chronos[$name] = microtime(true);
    }

    public static function getChrono(string $name = self::DEFAULT_CHRONO_NAME): float
    {
        return microtime(true) - self::$chronos[$name];
    }

    public static function printChrono(string $name = self::DEFAULT_CHRONO_NAME): void
    {
        echo self::getChrono($name) . "\n";
    }

    public static function dumpChrono(string $name = self::DEFAULT_CHRONO_NAME): void
    {
        dump(self::getChrono($name));
    }

    public static function startCumulateChrono(string $name = self::DEFAULT_CHRONO_NAME): void
    {
        self::$currentCumulateChronos[$name] = microtime(true);
    }

    public static function stopCumulateChrono(string $name = self::DEFAULT_CHRONO_NAME): void
    {
        $end = microtime(true);
        self::$cumulateChronos[$name] ??= 0;
        self::$cumulateChronos[$name] += $end - self::$currentCumulateChronos[$name];
    }

    public static function getCumulateChrono(string $name = self::DEFAULT_CHRONO_NAME): float
    {
        return self::$cumulateChronos[$name] ?? 0;
    }

    public static function printCumulateChrono(string $name = self::DEFAULT_CHRONO_NAME): void
    {
        echo self::getCumulateChrono($name) . "\n";
    }

    public static function dumpCumulateChrono(string $name = self::DEFAULT_CHRONO_NAME): void
    {
        dump(self::getCumulateChrono($name));
    }

    public static function getTime(callable $callable): float
    {
        $start = microtime(true);
        $callable();

        return microtime(true) - $start;
    }

    public static function printTime(callable $callable): void
    {
        $start = microtime(true);
        $callable();

        echo microtime(true) - $start . "\n";
    }

    public static function dumpTime(callable $callable): void
    {
        $start = microtime(true);
        $callable();

        dump(microtime(true) - $start . "\n");
    }
}
