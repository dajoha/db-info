<?php

declare(strict_types=1);

namespace App\Util;

use Dajoha\Iter\IteratorInterface;
use Exception;
use HaydenPierce\ClassFinder\ClassFinder;
use ReflectionClass;

class ClassFinderUtil
{
    private function __construct()
    {
    }

    /**
     * @template T
     *
     * @param string               $namespace
     * @param class-string<T>|null $filterInstanceOfClass
     *
     * @return ($filterInstanceOfClass is null
     *     ? IteratorInterface<int, class-string>
     *     : IteratorInterface<int, class-string<T>>
     * )
     *
     * @throws Exception
     */
    public static function findClassesInNamespace(
        string $namespace,
        ?string $filterInstanceOfClass = null,
    ): IteratorInterface {
        // Find all classes in the resulting base namespace:
        ClassFinder::enablePSR4Support(); // Enabled by default, just to be sure
        ClassFinder::disablePSR4Vendors();

        /** @var array<int, class-string> $classes */
        $classes = ClassFinder::getClassesInNamespace(
            $namespace,
            ClassFinder::RECURSIVE_MODE,
        );

        $iter = iter($classes);

        if ($filterInstanceOfClass !== null) {
            // Keep only concrete classes which are instances of the given class/trait:
            /** @var IteratorInterface<int, class-string<T>> $iter */
            $iter = $iter->filter(function ($class) use ($filterInstanceOfClass) {
                $rc = new ReflectionClass($class);

                return
                    $rc->implementsInterface($filterInstanceOfClass)
                    && !$rc->isAbstract();
            });
        }

        return $iter;
    }
}
