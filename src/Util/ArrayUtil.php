<?php

declare(strict_types=1);

namespace App\Util;

class ArrayUtil
{
    private function __construct()
    {
    }

    /**
     * @param array<mixed, mixed> $array1
     * @param array<mixed, mixed> $array2
     * @return array<mixed, mixed>
     */
    public static function recursiveMerge(array $array1, array $array2): array
    {
        if (!self::isAssociative($array1) || !self::isAssociative($array2)) {
            return $array2;
        }

        return self::_recursiveMerge($array1, $array2);
    }

    /**
     * @param array<mixed, mixed> $array1
     * @param array<mixed, mixed> $array2
     * @return array<mixed, mixed>
     */
    protected static function _recursiveMerge(array $array1, array $array2): array
    {
        $merged = $array1;

        foreach ($array2 as $key => $value) {
            if (self::isAssociative($merged[$key] ?? null) && self::isAssociative($value)) {
                $merged[$key] = self::recursiveMerge($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    protected static function isAssociative(mixed $array): bool
    {
        return
            is_array($array)
            && (
                empty($array)
                || array_keys($array) !== range(0, count($array) - 1)
            )
        ;
    }
}
