<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Project;

use App\Cache\CacheKey\Base\AbstractProjectRelatedCacheKey;

class SortedTableAliasesCacheKey extends AbstractProjectRelatedCacheKey
{
    public const string PREFIX = 'sorted_table_aliases';
}
