<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Project;

use App\Cache\CacheKey\Base\AbstractProjectRelatedCacheKey;

class JoinsByTargetTableCacheKey extends AbstractProjectRelatedCacheKey
{
    public const string PREFIX = 'joins_by_target_table';
}
