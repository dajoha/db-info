<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Project;

use App\Cache\CacheKey\Base\AbstractProjectRelatedCacheKey;

class SortedTableNamesCacheKey extends AbstractProjectRelatedCacheKey
{
    public const string PREFIX = 'sorted_table_names';
}
