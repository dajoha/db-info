<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Project;

use App\Cache\CacheKey\Base\AbstractProjectRelatedCacheKey;

class TableNamesByKeyCacheKey extends AbstractProjectRelatedCacheKey
{
    public const string PREFIX = 'table_names_by_key';
}
