<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Configuration;

use App\Cache\CacheKey\Base\AbstractCacheKey;

class ConfigurationCacheKey extends AbstractCacheKey
{
    public const string PREFIX = 'configuration';
}
