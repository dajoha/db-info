<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Configuration;

use App\Cache\CacheKey\Base\AbstractCacheKey;

class ConfigurationFilesTimestampsCacheKey extends AbstractCacheKey
{
    public const string PREFIX = 'configuration_files_timestamps';
}
