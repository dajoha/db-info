<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Base;

use App\Cache\Cache;
use App\Project\Project;

abstract class AbstractProjectRelatedCacheKey extends AbstractCacheKey
{
    public function __construct(protected Project $project)
    {
    }

    public function getParts(): array
    {
        return [
            self::getPrefix(),
            $this->project->getName(),
        ];
    }

    public function getTags(): ?array
    {
        return [
            Cache::getProjectTag($this->project),
        ];
    }
}
