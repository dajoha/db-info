<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Base;

abstract class AbstractCacheKey implements CacheKeyInterface
{
    public const string PREFIX = 'default';

    public static function getPrefix(): string {
        return static::PREFIX;
    }

    public function getParts(): array
    {
        return [self::getPrefix()];
    }

    public function getTags(): ?array
    {
        return [];
    }
}
