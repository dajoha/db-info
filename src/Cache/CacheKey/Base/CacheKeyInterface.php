<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Base;

use Stringable;

interface CacheKeyInterface
{
    /**
     * @return list<string|Stringable>
     */
    public function getParts(): array;

    /**
     * @return string[]|null
     */
    public function getTags(): ?array;
}
