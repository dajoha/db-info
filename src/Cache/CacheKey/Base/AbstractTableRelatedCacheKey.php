<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Base;

use App\Cache\Cache;
use App\Project\Data\TableInfo;

abstract class AbstractTableRelatedCacheKey extends AbstractProjectRelatedCacheKey
{
    public function __construct(protected TableInfo $tableInfo)
    {
        parent::__construct($tableInfo->project);
    }

    public function getParts(): array
    {
        return [
            self::getPrefix(),
            $this->project->getName(),
            $this->tableInfo->name,
        ];
    }

    public function getTags(): ?array
    {
        return [
            ...(parent::getTags() ?? []),
            Cache::getTableTag($this->tableInfo),
        ];
    }
}
