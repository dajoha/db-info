<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Table;

use App\Cache\CacheKey\Base\AbstractTableRelatedCacheKey;
use App\Project\Data\TableInfo;

class JoinPathsForListQueryCacheKey extends AbstractTableRelatedCacheKey
{
    public const string PREFIX = 'join_paths_for_list_query';

    public function __construct(TableInfo $tableInfo, protected string $formatStyle)
    {
        parent::__construct($tableInfo);
    }

    public function getParts(): array
    {
        return [...parent::getParts(), $this->formatStyle];
    }
}
