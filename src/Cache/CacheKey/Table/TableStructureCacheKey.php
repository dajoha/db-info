<?php

declare(strict_types=1);

namespace App\Cache\CacheKey\Table;

use App\Cache\CacheKey\Base\AbstractTableRelatedCacheKey;

class TableStructureCacheKey extends AbstractTableRelatedCacheKey
{
    public const string PREFIX = 'table_structure';
}
