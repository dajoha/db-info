<?php

declare(strict_types=1);

namespace App\Cache;

use App\Cache\CacheKey\Base\CacheKeyInterface;
use App\Project\Data\TableInfo;
use App\Project\Project;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Contracts\Cache\CallbackInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

/**
 * A wrapper around Symfony cache.
 */
class Cache
{
    protected const string CACHE_KEY_SEPARATOR = "\0";

    public function __construct(
        #[Autowire(service: TagAwareCacheInterface::class)]
        protected TagAwareCacheInterface&AdapterInterface $symfonyCache,
    ) {
    }

    /**
     * @template T
     *
     * @param CacheKeyInterface $cacheKey
     * @param (callable(CacheItemInterface,bool):T)|CallbackInterface<T> $callback
     *
     * @return T
     *
     * @throws InvalidArgumentException
     */
    public function get(CacheKeyInterface $cacheKey, callable|CallbackInterface $callback): mixed
    {
        $key = $this->stringifyKey($cacheKey);

        $tags = $cacheKey->getTags();

        if ($tags === null) {
            $finalCallback = $callback;
        } else {
            $finalCallback = function(ItemInterface $item, bool &$save) use($tags, $callback) {
                $item->tag($tags);

                return $callback($item, $save);
            };
        }

        return $this->symfonyCache->get($key, $finalCallback);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getItem(CacheKeyInterface $cacheKey): CacheItemInterface
    {
        $key = $this->stringifyKey($cacheKey);

        return $this->symfonyCache->getItem($key);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getItemValue(CacheKeyInterface $cacheKey): mixed
    {
        return $this->getItem($cacheKey)->get();
    }

    /**
     * @throws InvalidArgumentException
     */
    public function set(CacheKeyInterface $cacheKey, mixed $value): void
    {
        $item = $this->getItem($cacheKey)->set($value);
        $this->symfonyCache->save($item);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function delete(CacheKeyInterface $cacheKey): void
    {
        $key = $this->stringifyKey($cacheKey);
        $this->symfonyCache->deleteItem($key);
    }

    /**
     * Clears all the application cache.
     */
    public function clear(): void
    {
        $this->symfonyCache->clear();
    }

    /**
     * Clears the cache for the given project.
     *
     * @throws InvalidArgumentException
     */
    public function clearProject(Project $project): void
    {
        $this->symfonyCache->invalidateTags([self::getProjectTag($project)]);
    }

    /**
     * Clears the cache for the given table in a specific project.
     *
     * @throws InvalidArgumentException
     */
    public function clearTable(TableInfo $tableInfo): void
    {
        $this->symfonyCache->invalidateTags([self::getTableTag($tableInfo)]);
    }

    /**
     * Creates a cache key tag.
     *
     * @param array<mixed, mixed> $data
     */
    public static function getTag(array $data): string
    {
        return base64_encode(serialize($data));
    }

    /**
     * Creates the cache key tag for the given project.
     */
    public static function getProjectTag(Project $project): string
    {
        return self::getTag([
            'project' => $project->getName(),
        ]);
    }

    /**
     * Creates the cache key tag for the given project table.
     */
    public static function getTableTag(TableInfo $tableInfo): string
    {
        return self::getTag([
            'project' => $tableInfo->project->getName(),
            'table' => $tableInfo->name,
        ]);
    }

    protected function stringifyKey(CacheKeyInterface $cacheKey): string
    {
        return implode(self::CACHE_KEY_SEPARATOR, $cacheKey->getParts());
    }
}
