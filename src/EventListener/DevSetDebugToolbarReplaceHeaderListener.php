<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\DependencyInjection\Attribute\When;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Dev: allows to update Symfony debug toolbar information when an AJAX request is sent.
 */
#[When('dev')]
#[AsEventListener]
readonly class DevSetDebugToolbarReplaceHeaderListener
{
    public function __construct(protected KernelInterface $kernel)
    {
    }

    public function __invoke(ResponseEvent $event): void
    {
        if (!$this->kernel->isDebug()) {
            // Just in case, should never happen because this service is already limited to dev
            // environment, thanks to attribute #[When] above:
            return;
        }

        $response = $event->getResponse();
        $response->headers->set('Symfony-Debug-Toolbar-Replace', '1');
    }
}
