<?php

declare(strict_types=1);

namespace App\Validation\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * @see Filters
 */
class FiltersValidator extends AbstractValidator
{
    protected function doValidate(mixed $value, Constraint $constraint): void
    {
        if (!is_array($value)) {
            throw new UnexpectedValueException($value, 'array');
        }

        foreach (array_keys($value) as $key) {
            if (!is_string($key) || empty($key)) {
                $this->context->buildViolation('Keys must be non-empty strings')->addViolation();
                break;
            }
        }
    }
}
