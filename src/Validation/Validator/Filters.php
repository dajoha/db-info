<?php

declare(strict_types=1);

namespace App\Validation\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint for a list of filters, typically given in the query string.
 *
 * @see \App\Controller\AppController::listRows()
 * @see \App\Repository\ProjectRepository::createListQueryBuilder()
 */
class Filters extends Constraint
{
    public function validatedBy(): string
    {
        return FiltersValidator::class;
    }
}
