<?php

declare(strict_types=1);

namespace App\Validation\Validator;

use App\Util\StringUtil as Str;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Simplifies validation for child classes.
 */
abstract class AbstractValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        // Check the constraint:
        $constraintClass = Str::stripEnd(static::class, 'Validator');
        if (!is_a($constraint, $constraintClass, true)) {
            throw new UnexpectedTypeException($constraint, $constraintClass);
        }

        // Allow empty values:
        if (null === $value || '' === $value) {
            return;
        }

        $this->doValidate($value, $constraint);
    }

    abstract protected function doValidate(mixed $value, Constraint $constraint): void;
}
