<?php

declare(strict_types=1);

namespace App\Validation;

use App\Exception\ValidationException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Wrapper for Symfony validator.
 */
readonly class Validator
{
    public function __construct(protected ValidatorInterface $validator)
    {
    }

    /**
     * Wrapper for {@see ValidatorInterface::validate}:
     *  - Throws a `ValidationException` if at least one error is present;
     *  - Adds an optional context message to the exception.
     *
     * @param Constraint|Constraint[]|null $constraints
     *
     * @throws ValidationException
     */
    public function validateOrThrow(
        mixed $value,
        Constraint|array|null $constraints = null,
        ?string $contextMessage = null,
    ): void
    {
        $errors = $this->validator->validate($value, $constraints);
        if ($errors->count() === 0) {
            return;
        }

        $message = (string) $errors[0]?->getMessage();
        if ($contextMessage !== null) {
            $message = "$contextMessage: $message";
        }

        throw new ValidationException($message);
    }
}
