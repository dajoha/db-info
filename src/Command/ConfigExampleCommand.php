<?php

declare(strict_types=1);

namespace App\Command;

use App\Configuration\Definition\ConfigurationSchema;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

#[AsCommand(
    'db-info:config:example',
    "Dump an auto-generated configuration schema example (yaml format)",
)]
class ConfigExampleCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $example = ConfigurationSchema::getSchema()->getExampleValue();
        echo Yaml::dump($example, inline: 10);

        return self::SUCCESS;
    }
}
