<?php

declare(strict_types=1);

namespace App\Command;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\Service\ConfigProvider;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

#[AsCommand(
    'db-info:config:dump',
    "Dump the current parsed db-info configuration",
)]
class ConfigDumpCommand extends Command
{
    protected const string FORMAT_YAML = 'yaml';
    protected const string FORMAT_JSON = 'json';

    protected const array FORMATS = [self::FORMAT_YAML, self::FORMAT_JSON];

    public function __construct(protected ConfigProvider $configProvider)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $possibleFormats = iter(self::FORMATS)->map(fn($f) => "\"$f\"")->join(', ');

        $this
            ->addOption(
                'project',
                'p',
                InputOption::VALUE_REQUIRED,
                'An optional project name',
            )
            ->addOption(
                'format',
                'f',
                InputOption::VALUE_REQUIRED,
                "Format (Possible values: $possibleFormats)",
                self::FORMAT_YAML,
                [self::FORMAT_YAML, self::FORMAT_JSON],
            )
        ;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $projectName = $input->getOption('project');
        $format = $input->getOption('format');

        $config = $this->configProvider->getPreProcessedConfig();

        if ($projectName !== null) {
            $config = $config[K::ROOT][K::PROJECTS][$projectName] ?? null;
            if ($config === null) {
                throw new InvalidOptionException("Project not found: \"$projectName\"");
            }
        }

        echo match ($format) {
            self::FORMAT_YAML => Yaml::dump($config, inline: 10),
            self::FORMAT_JSON => json_encode($config, JSON_PRETTY_PRINT) . "\n",
            default => throw new InvalidOptionException("Bad format option: \"$format\""),
        };

        return self::SUCCESS;
    }
}
