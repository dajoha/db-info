<?php

declare(strict_types=1);

namespace App\Command;

use App\Frontend\ValueTransformer\DependencyInjection\Pass\RegisterValueTransformersPass;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

#[AsCommand(
    'db-info:value-transformer:list',
    'List value transformers',
)]
class ValueTransformersListCommand extends Command
{
    /**
     * @param array<string, string> $transformerClasses Keys are transformer names, values are classes
     */
    public function __construct(
        #[Autowire(param: RegisterValueTransformersPass::CONTAINER_PARAM__TRANSFORMER_CLASSES)]
        protected readonly array $transformerClasses,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $transformerClasses = $this->transformerClasses;
        ksort($transformerClasses);

        $rows = iter($transformerClasses)
            ->map(fn($className, $name) => [$name, $className])
            ->toValues()
        ;

        $io->table(['Name', 'Class'], $rows);

        return self::SUCCESS;
    }
}
