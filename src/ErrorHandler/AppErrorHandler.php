<?php

declare(strict_types=1);

namespace App\ErrorHandler;

use App\Controller\Exception\PageNotFoundException;
use App\Exception\AppException;
use App\Project\Exception\DatabaseConnectionException;
use App\Project\Exception\DoctrineException;
use Dajoha\Schema\Exception\SchemaException;
use Dajoha\Schema\Exception\ValidationException;
use Doctrine\DBAL\Exception as DbalException;
use Doctrine\DBAL\Exception\ConnectionException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * @phpstan-type Type_JsonError array{
 *     success: bool,
 *     error_message: string,
 *     class: class-string,
 *     stack_trace: string[],
 * }
 */
class AppErrorHandler
{
    /**
     * @var ?callable
     */
    protected $previousExceptionHandler;

    private function __construct()
    {
    }

    public static function register(): void
    {
        $errorHandler = new self;
        $previousExceptionHandler = set_exception_handler($errorHandler->handleException(...));
        $errorHandler->setPreviousExceptionHandler($previousExceptionHandler);
    }

    public function handleException(Throwable $exception): void
    {
        if (self::handleAppException($exception)) {
            return;
        }

        $this->callPreviousExceptionHandler($exception);
    }

    public static function handleAppException(Throwable $exception): bool
    {
        if (!self::isAppException($exception)) {
            return false;
        }

        // Retrieve header "Db-Info-Fetch":
        $fetch = $_SERVER['HTTP_DB_INFO_FETCH'] ?? null;

        if ($fetch === '1') {
            header('Content-Type: application/json', response_code: 500);
            echo json_encode(self::getJsonError($exception));
        } else {
            $errorMessage = $exception->getMessage();
            include __DIR__.'/../../templates/pages/error.html.php';
        }

        return true;
    }

    /**
     * @param Throwable $exception
     * @return Type_JsonError
     */
    public static function getJsonError(Throwable $exception): array
    {
        return [
            'success' => false,
            'error_message' => $exception->getMessage(),
            'class' => get_class($exception),
            'stack_trace' => self::generateCallTrace($exception),
        ];
    }

    public static function isAppException(Throwable $exception): bool
    {
        return
            $exception instanceof SchemaException
            || $exception instanceof ValidationException
            || $exception instanceof AppException
        ;
    }

    /**
     * Converts an external exception to an application exception.
     *
     * @param Throwable $exception The original exception to convert
     *
     * @return Throwable The converted exception if applicable; otherwise the original exception
     */
    public static function toAppException(Throwable $exception): Throwable
    {
        if ($exception instanceof NotFoundHttpException) {
            return new PageNotFoundException(previous: $exception);
        } elseif ($exception instanceof ConnectionException) {
            return new DatabaseConnectionException($exception);
        } elseif ($exception instanceof DbalException) {
            return new DoctrineException($exception);
        }

        return $exception;
    }

    protected function setPreviousExceptionHandler(?callable $previousExceptionHandler): void
    {
        $this->previousExceptionHandler = $previousExceptionHandler;
    }

    protected function callPreviousExceptionHandler(Throwable $exception): void
    {
        if (is_callable($this->previousExceptionHandler)) {
            ($this->previousExceptionHandler)($exception);
        }
    }

    /**
     * @return string[]
     */
    protected static function generateCallTrace(Throwable $exception): array
    {
        $trace = explode("\n", $exception->getTraceAsString());
        // reverse array to make steps line up chronologically
        $trace = array_reverse($trace);
        array_shift($trace); // remove {main}
        array_pop($trace); // remove call to this method
        $length = count($trace);
        $result = [];

        for ($i = 0; $i < $length; $i++) {
            $result[] = substr($trace[$i], strpos($trace[$i], ' ') + 1);
        }

        return $result;
    }}
