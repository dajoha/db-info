<?php

declare(strict_types=1);

namespace App\Frontend\Model;

use Doctrine\DBAL\Types\Type;

/**
 * Information mainly used to render the summary of a joined value in a row.
 */
readonly class JoinColumn
{
    public function __construct(
        public string $tableName,
        public string $tableBestAlias,
        public string $columnName,
        public bool $columnIsPrimaryKey,
        public Type $columnType,
    ) {
    }
}
