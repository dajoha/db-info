<?php

declare(strict_types=1);

namespace App\Frontend\Model;

use App\Repository\QueryPart\Sort\Sort;
use Doctrine\DBAL\Schema\Column;

/**
 * A {@see TableListColumn} which adds information about the current request, like sorting.
 */
readonly class ListColumn extends TableListColumn
{
    public function __construct(
        string $title,
        string $format,
        ?Column $column,
        public ?Sort $sort,
        public bool $showSortPosition,
    ) {
        parent::__construct($title, $format, $column);
    }

    public static function fromTableListColumn(
        TableListColumn $tableListColumn,
        ?Sort $sort,
        bool $showSortPosition,
    ): ListColumn {
        return new self(
            $tableListColumn->title,
            $tableListColumn->format,
            $tableListColumn->column,
            $sort,
            $showSortPosition,
        );
    }
}
