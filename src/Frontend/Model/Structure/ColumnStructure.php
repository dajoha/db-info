<?php

declare(strict_types=1);

namespace App\Frontend\Model\Structure;

readonly class ColumnStructure
{
    public string $fullType;

    public function __construct(
        public string $name,
        public string $type,
        public bool $isNullable,
        public ?int $length,
        public bool $isPrimary,
        public bool $isAutoIncrement,
        public ?SingleForeignKeyInfo $singleForeignKey,
    ) {
        $fullType = $type;
        if ($length !== null && $length !== 0) {
            $fullType .= "($length)";
        }
        $this->fullType = $fullType;
    }
}
