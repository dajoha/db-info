<?php

declare(strict_types=1);

namespace App\Frontend\Model\Structure;

class IndexStructure
{
    /**
     * @param string[] $columnNames
     */
    public function __construct(
        public string $name,
        public array $columnNames,
        public bool $isPrimary,
        public bool $isUnique,
    ) {
    }
}
