<?php

declare(strict_types=1);

namespace App\Frontend\Model\Structure;

readonly class ForeignKeyStructure
{
    /**
     * @param list<ForeignKeyBinding> $columnBindings
     */
    public function __construct(
        public string $name,
        public string $foreignTableName,
        public array $columnBindings,
        public string $foreignTableLink,
    ) {
    }
}
