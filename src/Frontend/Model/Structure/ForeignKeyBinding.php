<?php

declare(strict_types=1);

namespace App\Frontend\Model\Structure;

class ForeignKeyBinding
{
    public function __construct(
        public string $localColumnName,
        public string $foreignColumnName,
    ) {
    }
}
