<?php

declare(strict_types=1);

namespace App\Frontend\Model\Structure;

readonly class SingleForeignKeyInfo
{
    public function __construct(
        public string $tableName,
        public string $columnName,
        public string $link,
    ) {
    }
}
