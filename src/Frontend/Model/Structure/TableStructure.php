<?php

declare(strict_types=1);

namespace App\Frontend\Model\Structure;

readonly class TableStructure
{
    /**
     * @param array<string, ColumnStructure> $columns
     * @param array<string, ForeignKeyStructure> $foreignKeys
     * @param array<string, IndexStructure> $indexes
     */
    public function __construct(
        public array $columns,
        public array $foreignKeys,
        public array $indexes,
    ) {
    }
}
