<?php

declare(strict_types=1);

namespace App\Frontend\Model;

use Doctrine\DBAL\Schema\Column;

/**
 * Holds the needed information on how to render values for a particular table column, in a table
 * list.
 */
readonly class TableListColumn
{
    public bool $isSortable;

    public function __construct(
        /** Title of the column */
        public string $title,
        public string $format,
        public ?Column $column = null,
    ) {
        $this->isSortable = $column !== null;
    }
}
