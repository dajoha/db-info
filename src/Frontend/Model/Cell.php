<?php

declare(strict_types=1);

namespace App\Frontend\Model;

use App\Frontend\Factory\CellsFactory;

/**
 * Holds all the information needed by Twig templates in order to render a value in a row.
 *
 * Cells are typically created from factory {@see CellsFactory}.
 */
readonly class Cell
{
    public function __construct(
        public ?string $columnName,
        public ?string $title,
        public mixed $rawValue,
        public string $htmlValue,
        public bool $visible,
        public ?JoinColumn $joinColumn,
        public ?string $url,
    ) {
    }

    public function getTitleOrColumnName(): ?string
    {
        return $this->title ?? $this->columnName;
    }
}
