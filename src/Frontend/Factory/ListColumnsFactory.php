<?php

declare(strict_types=1);

namespace App\Frontend\Factory;

use App\Frontend\Model\ListColumn;
use App\Frontend\Model\TableListColumn;
use App\Project\Data\TableInfo;
use App\Repository\QueryPart\Sort\Sorts;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;

/**
 * See {@see ListColumn}.
 */
class ListColumnsFactory
{
    /**
     * @return ListColumn[]
     *
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function createListColumns(TableInfo $tableInfo, Sorts $sorts): array
    {
        // Frontend: determine whether to display the sorting position:
        $showSortPosition = $sorts->count() > 1;

        return iter($tableInfo->getTableListColumns())
            ->map(function (TableListColumn $tableListColumn) use ($sorts, $showSortPosition) {
                $columnName = $tableListColumn->column?->getName();
                // Testing "$columnName !== null" is redundant, but it helps static code analysis:
                $isSortable = $tableListColumn->isSortable && $columnName !== null;

                return ListColumn::fromTableListColumn(
                    $tableListColumn,
                    sort: $isSortable ? $sorts->get($columnName) : null,
                    showSortPosition: $isSortable ? $showSortPosition : false,
                );
            })
            ->toValues()
        ;
    }
}
