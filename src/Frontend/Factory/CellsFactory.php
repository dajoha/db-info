<?php

/** @noinspection PhpClassCanBeReadonlyInspection Lazy proxy */

declare(strict_types=1);

namespace App\Frontend\Factory;

use App\Exception\NotImplementedException;
use App\Frontend\Model\Cell;
use App\Frontend\Model\JoinColumn;
use App\Frontend\Model\TableListColumn;
use App\Frontend\Service\HtmlFormatter;
use App\Project\Data\Row;
use App\Project\Exception\ForeignKeyWasNotFoundException;
use App\Project\Exception\ProjectDoesNotExistException;
use App\Project\Exception\RowDataIsNotDefinedException;
use App\Router\AppRouter;
use App\Router\Model\DatabaseValue;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Schema\Column;
use Psr\Cache\InvalidArgumentException;

/**
 * Creates {@see Cell} instances.
 */
class CellsFactory
{
    public function __construct(
        protected readonly AppRouter $appRouter,
        protected readonly HtmlFormatter $htmlFormatter,
    ) {
    }

    /**
     * Used to render a detailed row view.
     *
     * @return Cell[]
     *
     * @throws RowDataIsNotDefinedException
     */
    public function createCellsFromRow(Row $row): array
    {
        $hiddenColumns = $row->tableInfo->getHiddenColumns();

        return iter($row->getDataOrThrow())
            ->map(function(mixed $value, string $columnName) use ($row, $hiddenColumns) {
                $visible = !key_exists($columnName, $hiddenColumns);
                $column = $row->tableInfo->getColumnOrThrow($columnName);
                $joinColumn = $row->tableInfo->getJoinColumn($columnName);
                $url = $this->generateUrl($row, $column, $value, $joinColumn);

                $htmlValue = $this->htmlFormatter->transformTableValueHtml(
                    $row->tableInfo,
                    $columnName,
                    $value,
                );

                return new Cell(
                    $columnName,
                    null,
                    $value,
                    $htmlValue,
                    $visible,
                    $joinColumn,
                    $url,
                );
            })
            ->mapKeys(fn($key, Cell $cell) => $cell->columnName)
            ->toArray()
        ;
    }

    /**
     * Used to render a row in a table list.
     *
     * The number of cells returned must be exactly the same as the number of `TableListColumn`s
     * returned by {@see \App\Project\Data\TableInfo::getTableListColumns()}.
     *
     * @return Cell[]
     *
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function createListCellsFromRow(Row $row): array
    {
        return iter($row->tableInfo->getTableListColumns())
            ->map(fn(TableListColumn $listColumn) => $this->createListCell($listColumn, $row))
            ->toValues()
        ;
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws RowDataIsNotDefinedException
     * @throws ForeignKeyWasNotFoundException
     * @throws ProjectDoesNotExistException
     * @throws NotImplementedException
     */
    protected function createListCell(TableListColumn $listColumn, Row $row): Cell
    {
        $column = $listColumn->column;
        $joinColumn = null;
        $url = null;
        $columnName = null;
        $title = null;
        $rawValue = null;

        if ($column === null) {
            $title = $listColumn->title;
        } else {
            $columnName = $column->getName();
            $rawValue = $row->getDataOrThrow()[$columnName];
            $joinColumn = $row->tableInfo->getJoinColumn($columnName);
            $url = $this->generateUrl($row, $column, $rawValue, $joinColumn);
        }

        // The HTML-formatted value, possibly transformed:
        $htmlValue = $this->htmlFormatter->formatRowHtml($row, $listColumn->format);

        return new Cell(
            $columnName,
            $title,
            $rawValue,
            $htmlValue,
            true, // Visibility is not used in this case
            $joinColumn,
            $url,
        );
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws ProjectDoesNotExistException
     * @throws NotImplementedException
     */
    protected function generateUrl(
        Row $row,
        Column $column,
        mixed $value,
        ?JoinColumn $joinColumn,
    ): ?string
    {
        if (empty($value)) {
            return null;
        }

        $columnName = $column->getName();

        // If the column is the primary key of the current table:
        if ($columnName === $row->tableInfo->getSinglePrimaryColumnName()) {
            return $this->appRouter->getTableRowPathFromTableInfo(
                $row->tableInfo,
                new DatabaseValue($value, $column->getType()),
            );
        }

        if ($joinColumn === null) {
            return null;
        }

        // If the column is the primary key of the joined table:
        if ($joinColumn->columnIsPrimaryKey) {
            return $this->appRouter->getTableRowPath(
                $row->project,
                $joinColumn->tableBestAlias,
                new DatabaseValue($value, $joinColumn->columnType),
            );
        }

        return $this->appRouter->getTableRowByFieldPath(
            $row->project,
            $joinColumn->tableBestAlias,
            $joinColumn->columnName,
            new DatabaseValue($value, $joinColumn->columnType),
        );
    }
}
