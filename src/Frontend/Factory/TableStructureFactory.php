<?php

declare(strict_types=1);

namespace App\Frontend\Factory;

use App\Cache\Cache;
use App\Cache\CacheKey\Table\TableStructureCacheKey;
use App\Frontend\Model\Structure\ColumnStructure;
use App\Frontend\Model\Structure\ForeignKeyBinding;
use App\Frontend\Model\Structure\ForeignKeyStructure;
use App\Frontend\Model\Structure\IndexStructure;
use App\Frontend\Model\Structure\SingleForeignKeyInfo;
use App\Frontend\Model\Structure\TableStructure;
use App\Project\Data\TableInfo;
use App\Project\Exception\ProjectDoesNotExistException;
use App\Router\AppRouter;
use Dajoha\Iter\Generator\Zip;
use Doctrine\DBAL\Exception as DbalException;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Doctrine\DBAL\Schema\Index;
use Doctrine\DBAL\Types\Type;
use Psr\Cache\InvalidArgumentException;

/**
 * Creates {@see TableStructure} instances.
 */
class TableStructureFactory
{
    public function __construct(
        protected AppRouter $router,
        protected Cache $cache,
    ) {
    }

    /**
     * @throws DbalException
     * @throws InvalidArgumentException
     *
     * @noinspection PhpDocRedundantThrowsInspection
     */
    public function createTableStructure(TableInfo $tableInfo): TableStructure
    {
        return $this->cache->get(
            new TableStructureCacheKey($tableInfo),
            fn() => new TableStructure(
                $this->createColumnStructures($tableInfo),
                $this->createForeignKeyStructures($tableInfo),
                $this->createIndexStructures($tableInfo),
            ),
        );
    }

    /**
     * @return array<string, ColumnStructure>
     *
     * @throws DbalException
     */
    protected function createColumnStructures(TableInfo $tableInfo): array
    {
        $dbalTable = $tableInfo->getTable();

        return iter($dbalTable->getColumns())
            ->map(fn(Column $column) => $this->createColumnStructure($tableInfo, $column))
            ->mapKeys(fn($k, ColumnStructure $column) => $column->name)
            ->toArray()
        ;
    }

    /**
     * @throws DbalException
     * @throws ProjectDoesNotExistException
     * @throws InvalidArgumentException
     */
    protected function createColumnStructure(TableInfo $tableInfo, Column $column): ColumnStructure
    {
        $columnName = $column->getName();

        $foreignKey = null;
        $dbalForeignKey = $tableInfo->findForeignKeyFor($columnName);
        if ($dbalForeignKey !== null) {
            $foreignColumnName = $dbalForeignKey->getForeignColumns()[0];
            $foreignTableName = $dbalForeignKey->getForeignTableName();
            $foreignLink = $this->router->getTableStructurePath($tableInfo->project, $foreignTableName);

            $foreignKey = new SingleForeignKeyInfo(
                $foreignTableName,
                $foreignColumnName,
                $foreignLink,
            );
        }

        return new ColumnStructure(
            $columnName,
            Type::lookupName($column->getType()),
            !$column->getNotnull(),
            $column->getLength(),
            $tableInfo->hasPrimaryKey($columnName),
            $column->getAutoincrement(),
            $foreignKey,
        );
    }

    /**
     * @return array<string, ForeignKeyStructure>
     *
     * @throws DbalException
     */
    protected function createForeignKeyStructures(TableInfo $tableInfo): array
    {
        $dbalTable = $tableInfo->getTable();

        return iter($dbalTable->getForeignKeys())
            ->map(fn(ForeignKeyConstraint $fk) => $this->createForeignKeyStructure($tableInfo, $fk))
            ->mapKeys(fn($k, ForeignKeyStructure $fk) => $fk->name)
            ->toArray()
        ;
    }

    /**
     * @throws ProjectDoesNotExistException
     * @throws InvalidArgumentException
     * @throws DbalException
     */
    protected function createForeignKeyStructure(
        TableInfo $tableInfo,
        ForeignKeyConstraint $fk,
    ): ForeignKeyStructure
    {
        $bindings = Zip::new($fk->getLocalColumns(), $fk->getForeignColumns())
            ->map(fn($names) => new ForeignKeyBinding(...$names))
            ->toValues()
        ;

        $link = $this->router->getTableStructurePath($tableInfo->project, $tableInfo->name);

        return new ForeignKeyStructure(
            $fk->getName(),
            $fk->getForeignTableName(),
            $bindings,
            $link,
        );
    }

    /**
     * @return array<string, IndexStructure>
     *
     * @throws DbalException
     */
    protected function createIndexStructures(TableInfo $tableInfo): array
    {
        $dbalTable = $tableInfo->getTable();

        return iter($dbalTable->getIndexes())
            ->map(fn(Index $index) => new IndexStructure(
                $index->getName(),
                $index->getColumns(),
                $index->isPrimary(),
                $index->isUnique(),
            ))
            ->mapKeys(fn($k, IndexStructure $index) => $index->name)
            ->toArray()
        ;
    }
}
