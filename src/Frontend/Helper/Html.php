<?php

declare(strict_types=1);

namespace App\Frontend\Helper;

class Html
{
    public static function escapeAttr(mixed $htmlAttributeValue): string
    {
        return htmlentities(
            (string) $htmlAttributeValue,
            ENT_QUOTES | ENT_HTML401 | ENT_SUBSTITUTE | ENT_DISALLOWED,
            'UTF-8',
        );
    }
}
