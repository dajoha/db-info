<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer;

use App\Frontend\ValueTransformer\Transformer\Base\TransformerInterface;

readonly class TransformerChain
{
    public function __construct(
        /** @var TransformerInterface[] */
        protected array $transformers,
    ) {
    }

    public function transform(mixed $input, ?bool &$isHtmlSafe = false): string
    {
        return array_reduce(
            $this->transformers,
            function ($input, TransformerInterface $transformer) use (&$isHtmlSafe) {
                if ($transformer::isHtmlSafe()) {
                    $isHtmlSafe = true;
                }

                return $transformer->transform($input);
            },
            $input,
        );
    }
}
