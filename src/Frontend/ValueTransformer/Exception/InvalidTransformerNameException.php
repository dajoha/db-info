<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Exception;

use App\Exception\AppException;
use App\Frontend\ValueTransformer\Transformer\Base\TransformerInterface;
use Throwable;

class InvalidTransformerNameException extends AppException
{
    /**
     * @param class-string<TransformerInterface> $transformerClass
     */
    public function __construct(
        string $transformerClass,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $name = $transformerClass::getName();
        $message = "Invalid transformer name for class \"$transformerClass\": \"$name\"";
        parent::__construct($message, $code, $previous);
    }
}
