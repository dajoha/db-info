<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Exception;

use App\Exception\AppException;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Stream\LocatedStream\LocatedStream;
use Dajoha\ParserCombinator\Stream\StringStream\StringPosition;
use Dajoha\ParserCombinator\Stream\StringStream\StringStream;
use Throwable;

class BadTransformerChainExpressionException extends AppException
{
    public function __construct(
        public readonly ParseResult $parseResult,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        /** @var LocatedStream $stream */
        $stream = $this->parseResult->stream;
        /** @var StringPosition $position */
        $position = $stream->getPosition()->base;
        $pos = $position->charPosition;

        /** @var StringStream $stringStream */
        $stringStream = $stream->innerStream;
        $remaining = substr($stringStream->input, $stringStream->index);

        $message = "Transformer chain expression: Unexpected input at position $pos: '$remaining'";
        parent::__construct($message, $code, $previous);
    }
}
