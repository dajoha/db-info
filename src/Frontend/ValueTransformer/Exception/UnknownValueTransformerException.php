<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Exception;

use App\Exception\AppException;
use Throwable;

class UnknownValueTransformerException extends AppException
{
    public function __construct(
        public readonly string $transformerName,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message = "Unknown value transformer: '$this->transformerName'";
        parent::__construct($message, $code, $previous);
    }
}
