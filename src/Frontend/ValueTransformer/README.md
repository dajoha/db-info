A mini-language used to apply transformations on table fields, before displaying them on the frontend.

It can useful for example to:

* display php-serialized data in a readable way;
* highlight json data;
* truncate long values...

It is used in the db-info configuration at this path:

```
db_info.projects.<PROJECT>.tables.<TABLE>.columns.<COLUMN>.transform
```

#### Expression example:

* `unserialize | json_encode('pretty') | highlight('json')`  
  Describes a value transformer which first deserializes a value, then encodes it in json
  (because a PHP value is not displayable), then finally adds color highlighting.

### Dev: adding a new transformer

Just add a new class in `./Transformer`, by taking example from a simple transformer like
`./Transformer/UcFirstTransformer.php`. The class must implement `TransformerInterface` but it is
advised to extend `AbstractTransformer` instead.

Once created, run `./bin/console cache:clear` to register the new transformer into the cache.

More sophisticated examples (argument handling, html rendering) can be found in:
* `./Transformer/HighlightTransformer.php`
* `./Transformer/JsonEncodeTransformer/JsonEncodeTransformer.php`
