<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\DependencyInjection\Pass;

use App\Frontend\ValueTransformer\Exception\InvalidTransformerNameException;
use App\Frontend\ValueTransformer\Service\DevTransformerChainParser;
use App\Frontend\ValueTransformer\Transformer\Base\TransformerInterface;
use App\Util\ClassFinderUtil;
use App\Util\ClassUtil;
use App\Util\StringUtil;
use Exception;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Registers all concrete classes implementing {@see TransformerInterface} in the namespace
 * `App\Frontend\ValueTransformer`, and saves them into a container parameter.
 */
class RegisterValueTransformersPass implements CompilerPassInterface
{
    public const string CONTAINER_PARAM__TRANSFORMER_CLASSES = 'db_info.value_transformer_classes';

    /**
     * Keys are the transformer names {@see TransformerInterface::getName()}.
     * Values are the matching transformer classes.
     * @var array<string, class-string<TransformerInterface>>
     */
    protected ?array $transformersClasses = null;

    /**
     * @throws Exception
     */
    public function process(ContainerBuilder $container): void
    {
        // Get the base namespace where to search for classes implementing `TransformerInterface`:
        $valueTransformerBaseNamespace = ClassUtil::getBaseNamespace(
            __NAMESPACE__,
            '\DependencyInjection\Pass',
        );

        // Find all classes in the resulting base namespace:
        $transformersClasses = ClassFinderUtil::findClassesInNamespace(
            $valueTransformerBaseNamespace,
            TransformerInterface::class,
        )
            // Check if transformer names are valid:
            ->run(
                /**
                 * @param class-string<TransformerInterface> $transformerClass
                 * @throws InvalidTransformerNameException
                 */
                function(string $transformerClass) {
                    if (!StringUtil::isIdentifier($transformerClass::getName())) {
                        throw new InvalidTransformerNameException($transformerClass);
                    }
                }
            )
            // Set the array keys to the transformers names:
            ->mapKeys(
                /** @param class-string<TransformerInterface> $transformerClass */
                fn(int $k, string $transformerClass) => $transformerClass::getName()
            )
            ->toArray();

        $container->setParameter(self::CONTAINER_PARAM__TRANSFORMER_CLASSES, $transformersClasses);

        // Inject the transformer classes into the `DevTransformerChainParser` definition:
        if ($container->hasDefinition(DevTransformerChainParser::class)) {
            $container->findDefinition(DevTransformerChainParser::class)
                ->setArgument('$transformersClasses', $transformersClasses);
        }
    }
}
