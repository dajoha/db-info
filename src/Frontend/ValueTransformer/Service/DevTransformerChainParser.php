<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Service;

use App\Frontend\ValueTransformer\Parser\TransformerChainParser;

/**
 * Just for dev: in production, the {@see TransformerChainParser} is not needed as a service.
 */
class DevTransformerChainParser extends TransformerChainParser
{
}
