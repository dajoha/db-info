<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Parser;

use App\Frontend\ValueTransformer\Exception\BadTransformerChainExpressionException;
use App\Frontend\ValueTransformer\Exception\UnknownValueTransformerException;
use App\Frontend\ValueTransformer\Transformer\Base\TransformerInterface;
use App\Frontend\ValueTransformer\TransformerChain;
use Dajoha\ParserCombinator\Base\AbstractParser;
use Dajoha\ParserCombinator\Base\ParseResult;
use Dajoha\ParserCombinator\Base\ParserInterface;
use Dajoha\ParserCombinator\Stream\Base\StreamInterface;
use Dajoha\ParserCombinator\Stream\LocatedStream\LocatedStream;
use function Dajoha\ParserCombinator\Parser\Combinator\alt;
use function Dajoha\ParserCombinator\Parser\Combinator\opt;
use function Dajoha\ParserCombinator\Parser\Multi\separated0;
use function Dajoha\ParserCombinator\Parser\Multi\separated1;
use function Dajoha\ParserCombinator\Parser\Sequence\delimited;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\delimitedStr;
use function Dajoha\ParserCombinator\Parser\String\identifier;
use function Dajoha\ParserCombinator\Parser\String\integer;
use function Dajoha\ParserCombinator\Parser\String\spaced;

class TransformerChainParser extends AbstractParser
{
    protected ?ParserInterface $parser = null;

    public function __construct(
        /**
         * Map of transformer name => transformer class
         * @var array<string, class-string<TransformerInterface>>
         */
        protected readonly array $transformersClasses,
    ) {
    }

    /**
     * @param string|mixed[]|StreamInterface<mixed> $input
     *
     * @throws BadTransformerChainExpressionException
     * @throws UnknownValueTransformerException
     *
     * @noinspection PhpDocRedundantThrowsInspection
     */
    public function parse(string|array|StreamInterface $input): ParseResult
    {
        $input = new LocatedStream($input);
        $parseResult = $this->getParser()->parse($input);

        if (!$parseResult->isSuccess()) {
            throw new BadTransformerChainExpressionException($parseResult);
        }

        return $parseResult;
    }

    protected function getParser(): ParserInterface
    {
        if ($this->parser !== null) {
            return $this->parser;
        }

        $transformerArgument = alt(
            integer(),
            delimitedStr("'", escapeChar: '\\'),
        );

        $transformerParser =
            seq(
                identifier(),
                opt(delimited(
                    spaced('('),
                    separated0($transformerArgument, spaced(',')),
                    spaced(')'),
                ))->map(fn($args) => $args ?? []),
            )
            ->map(function (array $sequence) {
                [$transformerName, $arguments] = $sequence;

                $transformerClass = $this->transformersClasses[$transformerName] ?? null;
                if ($transformerClass === null) {
                    throw new UnknownValueTransformerException($transformerName);
                }

                return new $transformerClass(...$arguments);
            })
        ;

        $this->parser =
            separated1($transformerParser, spaced('|'))
            ->map(fn($transformers) => new TransformerChain($transformers))
            ->spaced()
            ->complete()
        ;

        return $this->parser;
    }

    public function getDescription(): string
    {
        return $this->getParser()->getDescription();
    }
}
