<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;
use DateTimeImmutable;
use Exception;

class FormatTimestampTransformer extends AbstractTransformer
{
    public const string DEFAULT_DATETIME_FORMAT = 'Y-m-d H:i:s';

    public function __construct(public readonly string $format = self::DEFAULT_DATETIME_FORMAT)
    {
    }

    public static function getName(): string
    {
        return 'format_timestamp';
    }

    public function transform(mixed $input): string
    {
        try {
            $dateTime = new DateTimeImmutable('@' . (int) $input);

            return $dateTime->format($this->format);
        } catch (Exception) {
            return '';
        }
    }
}
