<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class JsonDecodeTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        return 'json_decode';
    }

    public function transform(mixed $input): mixed
    {
        return json_decode((string) $input);
    }
}
