<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class CondHtmlTransformer extends AbstractTransformer
{
    public function __construct(
        public readonly mixed $ifValue,
        public readonly mixed $thenValue,
    ) {
    }

    public static function getName(): string
    {
        return 'cond_html';
    }

    public static function isHtmlSafe(): bool
    {
        return true;
    }

    public function transform(mixed $input): string
    {
        return $input ? $this->ifValue : $this->thenValue;
    }
}
