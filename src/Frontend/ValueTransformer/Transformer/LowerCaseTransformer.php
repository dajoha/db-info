<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class LowerCaseTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        return 'lower';
    }

    public function transform(mixed $input): string
    {
        return strtolower((string) $input);
    }
}
