<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\HighlightTransformer\Highlighter;

use DomainException;
use Exception;
use Highlight\Highlighter;
use function HighlightUtilities\getLanguageDefinitionPath;

readonly class ScrivoHighlighter implements HighlighterInterface
{
    public function __construct(protected string $language)
    {
        // Will throw an exception if the language does not exist:
        getLanguageDefinitionPath($this->language);
    }

    /**
     * @throws Exception
     */
    public function highlight(string $code): string
    {
        $hl = new Highlighter();

        try {
            $highlighted = $hl->highlight($this->language, $code);

            return <<< HTML
                <pre><code class="hljs {$highlighted->language}">{$highlighted->value}</code></pre>
                HTML;
        }
        catch (DomainException) {
            // Should never happen because it is checked in the constructor: thrown if the specified
            // language does not exist.
            return htmlentities($code);
        }
    }
}
