<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\HighlightTransformer\Highlighter;

interface HighlighterInterface
{
    public function highlight(string $code): string;
}
