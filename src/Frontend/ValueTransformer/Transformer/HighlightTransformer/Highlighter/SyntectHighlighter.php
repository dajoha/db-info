<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\HighlightTransformer\Highlighter;

use App\Exception\AppException;
use Syntect\Syntax;
use Syntect\Syntect;

class SyntectHighlighter implements HighlighterInterface
{
    protected Syntax $syntax;

    /**
     * @throws AppException
     */
    public function __construct(protected string $language)
    {
        $syntax = $this->getSyntax();

        if ($syntax === null) {
            throw new AppException("Unknown language for syntect highlighting: \"$this->language\"");
        }

        $this->syntax = $syntax;
    }

    public function __sleep(): array
    {
        return ['language'];
    }

    public function __wakeup(): void
    {
        // @phpstan-ignore assign.propertyType (OK: Cannot be null because it has been checked in constructor)
        $this->syntax = $this->getSyntax();
    }

    public static function isSupported(): bool
    {
        return class_exists(Syntect::class);
    }

    public function highlight(string $code): string
    {
        return Syntect::highlightHtml($code, $this->syntax);
    }

    protected function getSyntax(): ?Syntax
    {
        return Syntect::getSyntax($this->language);
    }
}
