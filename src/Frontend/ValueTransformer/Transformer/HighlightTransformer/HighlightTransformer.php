<?php

/** @noinspection PhpFullyQualifiedNameUsageInspection */

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\HighlightTransformer;

use App\Exception\AppException;
use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;
use App\Frontend\ValueTransformer\Transformer\HighlightTransformer\Highlighter\HighlighterInterface;
use App\Frontend\ValueTransformer\Transformer\HighlightTransformer\Highlighter\ScrivoHighlighter;
use App\Frontend\ValueTransformer\Transformer\HighlightTransformer\Highlighter\SyntectHighlighter;
use Exception;

/**
 * Highlights code by adding HTML colored tags by using either:
 *  - php_syntect extension (@see https://gitlab.com/dajoha/php-syntect) if available;
 *  - Scrivo highlight library otherwise (slower).
 */
class HighlightTransformer extends AbstractTransformer
{
    protected HighlighterInterface $highlighter;

    /**
     * @throws AppException
     */
    public function __construct(protected readonly string $language)
    {
        if (SyntectHighlighter::isSupported()) {
            $this->highlighter = new SyntectHighlighter($this->language);
        } else {
            $this->highlighter = new ScrivoHighlighter($this->language);
        }
    }

    public static function getName(): string
    {
        return 'highlight';
    }

    public static function isHtmlSafe(): bool
    {
        return true;
    }

    /**
     * @throws Exception
     */
    public function transform(mixed $input): string
    {
        return $this->highlighter->highlight((string) $input);
    }
}
