<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class UcWordsTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        /** @noinspection SpellCheckingInspection */
        return 'ucwords';
    }

    public function transform(mixed $input): string
    {
        return ucwords((string) $input);
    }
}
