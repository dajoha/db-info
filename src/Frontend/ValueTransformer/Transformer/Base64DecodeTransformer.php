<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class Base64DecodeTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        return 'base64_decode';
    }

    public function transform(mixed $input): string
    {
        return base64_decode((string) $input);
    }
}
