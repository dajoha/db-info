<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class TruncateTransformer extends AbstractTransformer
{
    protected readonly int $ellipsisLen;

    public function __construct(
        public readonly int $maxLength,
        public readonly string $ellipsis = '…',
    ) {
        $this->ellipsisLen = mb_strlen($this->ellipsis);
    }

    public static function getName(): string
    {
        return 'truncate';
    }

    public function transform(mixed $input): string
    {
        $input = (string) $input;

        if (mb_strlen($input) <= $this->maxLength) {
            return $input;
        }

        $truncated = mb_substr($input, 0, $this->maxLength - $this->ellipsisLen);

        return $truncated . $this->ellipsis;
    }
}
