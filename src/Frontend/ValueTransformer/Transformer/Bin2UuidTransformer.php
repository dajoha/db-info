<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class Bin2UuidTransformer extends AbstractTransformer
{
    public const string FLAG_SWAP_BACK = 'swap_back';

    protected bool $swapBack;

    public function __construct(?string $flag = null)
    {
        $this->swapBack = $flag === self::FLAG_SWAP_BACK;
    }

    public static function getName(): string
    {
        return 'bin_to_uuid';
    }

    public function transform(mixed $input): string
    {
        if ($this->swapBack) {
            $input = implode([
                substr($input, 4, 4),
                substr($input, 2, 2),
                substr($input, 0, 2),
                substr($input, 8),
            ]);
        }

        // @phpstan-ignore offsetAccess.nonOffsetAccessible (OK: format string is always valid)
        $string = unpack("H*", $input)[1];

        return preg_replace(
            '/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/',
            '$1-$2-$3-$4-$5',
            $string,
        );
    }
}
