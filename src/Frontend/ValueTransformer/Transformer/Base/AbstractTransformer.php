<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\Base;

abstract class AbstractTransformer implements TransformerInterface
{
    public static function isHtmlSafe(): bool
    {
        return false;
    }
}
