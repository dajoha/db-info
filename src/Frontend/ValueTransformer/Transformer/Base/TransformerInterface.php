<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\Base;

interface TransformerInterface
{
    public static function getName(): string;

    public static function isHtmlSafe(): bool;

    public function transform(mixed $input): mixed;
}
