<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\JsonEncodeTransformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class JsonEncodeTransformer extends AbstractTransformer
{
    protected const int DEFAULT_JSON_ENCODE_FLAGS = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;

    protected readonly int $jsonEncodeFlags;

    public function __construct(string ...$stringFlags)
    {
        $flags = 0;
        foreach ($stringFlags as $stringFlag) {
            $flags |= JsonEncodeTransformerFlag::from($stringFlag)->toJsonEncodeFlag();
        }
        $this->jsonEncodeFlags = $flags | self::DEFAULT_JSON_ENCODE_FLAGS;
    }

    public static function getName(): string
    {
        return 'json_encode';
    }

    public function transform(mixed $input): string
    {
        $result = json_encode($input, $this->jsonEncodeFlags);

        return is_string($result) ? $result : 'Unable to encode in json';
    }
}
