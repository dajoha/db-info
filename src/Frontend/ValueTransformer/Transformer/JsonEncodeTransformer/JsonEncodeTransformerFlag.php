<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer\JsonEncodeTransformer;

/**
 * A flag which can be used when using {@see JsonEncodeTransformer}.
 */
enum JsonEncodeTransformerFlag: string
{
    case Pretty = 'pretty';
    case ForceObject = 'force_object';

    /**
     * Transform the flag to a native php `json_encode` flag.
     */
    public function toJsonEncodeFlag(): int
    {
        return match($this) {
            self::Pretty => JSON_PRETTY_PRINT,
            self::ForceObject => JSON_FORCE_OBJECT,
        };
    }
}
