<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class UpperCaseTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        return 'upper';
    }

    public function transform(mixed $input): string
    {
        return strtoupper((string) $input);
    }
}
