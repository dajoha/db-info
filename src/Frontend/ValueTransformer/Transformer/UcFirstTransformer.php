<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class UcFirstTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        /** @noinspection SpellCheckingInspection */
        return 'ucfirst';
    }

    public function transform(mixed $input): string
    {
        return ucfirst((string) $input);
    }
}
