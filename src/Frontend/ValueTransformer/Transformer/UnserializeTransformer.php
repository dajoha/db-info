<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class UnserializeTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        return 'unserialize';
    }

    public function transform(mixed $input): mixed
    {
        return unserialize((string) $input);
    }
}
