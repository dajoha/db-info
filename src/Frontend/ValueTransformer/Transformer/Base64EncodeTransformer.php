<?php

declare(strict_types=1);

namespace App\Frontend\ValueTransformer\Transformer;

use App\Frontend\ValueTransformer\Transformer\Base\AbstractTransformer;

class Base64EncodeTransformer extends AbstractTransformer
{
    public static function getName(): string
    {
        return 'base64_encode';
    }

    public function transform(mixed $input): string
    {
        return base64_encode((string) $input);
    }
}
