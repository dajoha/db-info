<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableAlias\Enum;

/**
 * The auto-completion section.
 */
enum Section: string
{
    case TableAliases = 'Table aliases';
    case TableNames = 'Tables';
}
