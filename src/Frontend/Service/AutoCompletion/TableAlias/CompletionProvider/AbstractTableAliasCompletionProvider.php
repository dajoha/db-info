<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableAlias\CompletionProvider;

use App\Frontend\Service\AutoCompletion\TableAlias\Enum\Section;
use App\Frontend\Service\AutoCompletion\TableAlias\Model\CodeMirrorTableAliasCompletion;
use Twig\Environment;

abstract class AbstractTableAliasCompletionProvider implements TableAliasCompletionProviderInterface
{
    public function __construct(protected Environment $twig)
    {
    }

    protected function newCompletion(
        string $label,
        string $type,
        Section $section,
        ?int $boost = null,
        ?string $infoTableName = null,
    ): CodeMirrorTableAliasCompletion
    {
        return new CodeMirrorTableAliasCompletion(
            $this->twig,
            $label,
            $type,
            $section,
            $boost,
            $infoTableName,
        );
    }
}
