<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableAlias\CompletionProvider;

use App\Frontend\Service\AutoCompletion\TableAlias\Enum\Section;
use App\Project\Project;
use Psr\Cache\InvalidArgumentException;

class TableNamesCompletionProvider extends AbstractTableAliasCompletionProvider
{
    /**
     * @throws InvalidArgumentException
     */
    public function getCompletions(Project $project): iterable
    {
        return iter($project->getSortedTableNames())
            ->map(fn(string $tableName) => $this->newCompletion(
                label: $tableName,
                type: 'tableName',
                section: Section::TableNames,
            ))
        ;
    }
}
