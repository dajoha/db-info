<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableAlias\CompletionProvider;

use App\Frontend\Service\AutoCompletion\TableAlias\Enum\Section;
use App\Project\Project;
use Psr\Cache\InvalidArgumentException;

class TableAliasesCompletionProvider extends AbstractTableAliasCompletionProvider
{
    /**
     * @throws InvalidArgumentException
     */
    public function getCompletions(Project $project): iterable
    {
        return iter($project->getSortedTableAliases())
            ->map(fn(string $tableName, string $tableAlias) => $this->newCompletion(
                label: $tableAlias,
                type: 'tableAlias',
                section: Section::TableAliases,
                infoTableName: $tableName,
            ))
        ;
    }
}
