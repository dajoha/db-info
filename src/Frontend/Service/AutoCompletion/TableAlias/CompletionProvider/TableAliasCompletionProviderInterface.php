<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableAlias\CompletionProvider;

use App\Frontend\Service\AutoCompletion\AutoCompletionService;
use App\Frontend\Service\AutoCompletion\TableAlias\Model\CodeMirrorTableAliasCompletion;
use App\Project\Project;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

/**
 * Used in {@see AutoCompletionService} to retrieve CodeMirror completions.
 */
#[AutoconfigureTag('db_info.codemirror.completion_provider.table_alias')]
interface TableAliasCompletionProviderInterface
{
    /**
     * @return iterable<CodeMirrorTableAliasCompletion>
     */
    public function getCompletions(Project $project): iterable;
}
