<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableAlias\Model;

use App\Frontend\Service\AutoCompletion\Model\CodeMirrorCompletion;
use App\Frontend\Service\AutoCompletion\TableAlias\Enum\Section;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Annotation\Ignore;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * A serializable struct to be passed to the CodeMirror autocompletion extension.
 *
 * @see https://codemirror.net/docs/ref/#autocomplete.Completion
 */
readonly class CodeMirrorTableAliasCompletion extends CodeMirrorCompletion
{
    public function __construct(
        #[Ignore] protected Environment $twig,
        string $label,
        string $type,
        Section $section,
        ?int $boost = null,
        #[Ignore] public ?string $infoTableName = null,
    ) {
        parent::__construct($label, $type, $section, $boost);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    #[Context(self::SKIP_NULL)]
    public function getInfo(): ?string
    {
        if ($this->infoTableName === null) {
            return null;
        }

        return $this->twig->render(
            'include/js/components/quick_find_zone/completion_info.html.twig',
            [
                'completion' => $this,
            ],
        );
    }
}
