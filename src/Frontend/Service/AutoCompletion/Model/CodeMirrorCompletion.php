<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\Model;

use BackedEnum;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

/**
 * A serializable struct to be passed to the CodeMirror autocompletion extension.
 *
 * @see https://codemirror.net/docs/ref/#autocomplete.Completion
 */
readonly class CodeMirrorCompletion
{
    protected const array SKIP_NULL = [AbstractObjectNormalizer::SKIP_NULL_VALUES => true];

    public function __construct(
        protected string $label,
        protected ?string $type,
        protected BackedEnum $section,
        protected ?int $boost = null,
        protected ?string $info = null,
    ) {
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getSection(): BackedEnum
    {
        return $this->section;
    }

    #[Context(self::SKIP_NULL)]
    public function getBoost(): ?int
    {
        return $this->boost;
    }

    #[Context(self::SKIP_NULL)]
    public function getInfo(): ?string
    {
        return $this->info;
    }
}
