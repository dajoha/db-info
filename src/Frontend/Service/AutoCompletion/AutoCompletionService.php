<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion;

use App\Exception\AppException;
use App\Frontend\Service\AutoCompletion\TableAlias\CompletionProvider\TableAliasCompletionProviderInterface;
use App\Frontend\Service\AutoCompletion\TableAlias\Model\CodeMirrorTableAliasCompletion;
use App\Frontend\Service\AutoCompletion\TableFilter\CompletionProvider\TableFilterCompletionProviderInterface;
use App\Frontend\Service\AutoCompletion\TableFilter\Enum\Section;
use App\Frontend\Service\AutoCompletion\TableFilter\Model\CodeMirrorTableFilterCompletion;
use App\Frontend\Service\AutoCompletion\TableFilter\Model\CodeMirrorTableFilterCompletionsResult;
use App\Project\Project;
use App\Repository\SearchExpression\Util\CompletionUtil;
use Doctrine\DBAL\Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Attribute\AutowireIterator;
use Throwable;
use Twig\Environment;

/**
 * Provides auto-completion data for CodeMirror.
 */
readonly class AutoCompletionService
{
    public function __construct(
        /** @var iterable<TableFilterCompletionProviderInterface> */
        #[AutowireIterator('db_info.codemirror.completion_provider.table_filter')]
        protected iterable $tableFilterCompletionProviders,
        /** @var iterable<TableAliasCompletionProviderInterface> */
        #[AutowireIterator('db_info.codemirror.completion_provider.table_alias')]
        protected iterable $tableAliasCompletionProviders,
        protected Environment $twig,
    ) {
    }

    /**
     * Returns completion suggestions for a given input and cursor position.
     *
     * See {@see assets/js/components/filter-editor/index.js} for more detail about how the returned
     * values are used.
     *
     * @throws InvalidArgumentException
     */
    public function getTableFilterCompletions(
        Project $project,
        string $baseTableName,
        string $input,
        int $cursorPosition,
    ): ?CodeMirrorTableFilterCompletionsResult
    {
        $input = substr($input, 0, $cursorPosition);

        if (CompletionUtil::insideString($input, $stringPosition)) {
            return $this->getValueCompletions($project, $baseTableName, $input, $stringPosition);
        }

        $databasePathStartIndex = self::findTrailingDatabasePath($input);
        if ($databasePathStartIndex === null) {
            return null;
        }

        return $this->getDatabasePathCompletions(
            $project,
            $baseTableName,
            $input,
            $databasePathStartIndex,
        );
    }

    /**
     * Returns suggestions for a table name or alias actually being inserted.
     *
     * @return array<CodeMirrorTableAliasCompletion>
     */
    public function getTableAliasCompletions(Project $project): array
    {
        return iter($this->tableAliasCompletionProviders)
            ->map(fn(TableAliasCompletionProviderInterface $completionProvider)
                => $completionProvider->getCompletions($project)
            )
            ->flatten()
            ->toValues()
        ;
    }

    /**
     * Returns completion suggestions for a join path actually being inserted.
     *
     * @param string $baseTableName The name of the table on which the incomplete join path is
     *                              applied.
     *
     * @return array<CodeMirrorTableFilterCompletion>
     * @throws InvalidArgumentException
     */
    protected function getTableFilterCompletionsForColumnPath(
        Project $project,
        string $baseTableName,
        string $incompleteColumnPath,
    ): array
    {
        try {
            $baseTableInfo = $project->getTableInfo($baseTableName);
            $columnPath = $baseTableInfo->createColumnPath($incompleteColumnPath);

            // Get the `TableInfo` for the last complete sub-path, e.g., if the incomplete join path
            // is "employee.company.phone_nu", then the retrieved `TableInfo` is for
            // "employee.company". If there is no sub-path like in input "emplo", `$tableInfo` is
            // the base table info ($baseTableInfo).
            $tableInfo = $columnPath->getLastTableInfo();

            return iter($this->tableFilterCompletionProviders)
                ->map(fn(TableFilterCompletionProviderInterface $completionProvider)
                    => $completionProvider->getCompletions($tableInfo, $baseTableInfo)
                )
                ->flatten()
                ->toValues()
            ;
        }
        catch (AppException|Exception) {
            return [];
        }
    }

    /**
     * Finds the index of the trailing database path in the input string.
     *
     * @param string $input The input string to search.
     *
     * @return int|null The index of the trailing database path, or null if not found.
     */
    protected static function findTrailingDatabasePath(string $input): ?int
    {
        $pattern = '/\b[a-zA-Z_][a-zA-Z0-9_]*(?:\.[a-zA-Z_][a-zA-Z0-9_]*)*\.?$/';

        $result = preg_match($pattern, $input, $matches, PREG_OFFSET_CAPTURE);
        /** @var array<array{string, int}> $matches */

        return $result === 1 ? $matches[0][1] : null;
    }

    /**
     * @throws InvalidArgumentException
     */
    protected function getDatabasePathCompletions(
        Project $project,
        string $baseTableName,
        string $input,
        int $databasePathStartIndex,
    ): CodeMirrorTableFilterCompletionsResult
    {
        $databasePath = substr($input, $databasePathStartIndex);

        $completions = $this->getTableFilterCompletionsForColumnPath(
            $project,
            $baseTableName,
            $databasePath,
        );

        $fromComplete = $databasePathStartIndex;
        $lastPathIndex = strrpos($databasePath, '.');
        if ($lastPathIndex !== false) {
            $fromComplete += $lastPathIndex + 1;
        }

        return new CodeMirrorTableFilterCompletionsResult($fromComplete, $completions);
    }

    protected function getValueCompletions(
        Project $project,
        string $baseTableName,
        string $input,
        int $stringPosition,
    ): ?CodeMirrorTableFilterCompletionsResult
    {
        $inputBeforeString = substr($input, 0, $stringPosition - 1);
        $pattern = '/\b([a-zA-Z_][a-zA-Z0-9_]*(?:\.[a-zA-Z_][a-zA-Z0-9_]*)*)\s*==?\s*$/';

        if (preg_match($pattern, $inputBeforeString, $matches) !== 1) {
            return null;
        }

        $databasePath = $matches[1];
        $baseTableInfo = $project->getTableInfo($baseTableName);
        $columnPath = $baseTableInfo->createColumnPath($databasePath);

        try {
            $tableInfo = $columnPath->getLastTableInfo();
            $columnName = $columnPath->getValueColumnName();
            $possibleValues = $tableInfo->getPossibleValuesForColumn($columnName);
        } catch (Throwable) {
            return null;
        }

        if (empty($possibleValues)) {
            return null;
        }

        $completions = array_map(
            fn($value) => new CodeMirrorTableFilterCompletion(
                $this->twig,
                (string) CompletionUtil::escapeValue($value),
                null,
                Section::Values,
            ),
            $possibleValues,
        );

        return new CodeMirrorTableFilterCompletionsResult($stringPosition, $completions);
    }
}
