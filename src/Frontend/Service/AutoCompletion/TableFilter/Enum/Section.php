<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\Enum;

/**
 * The auto-completion section.
 */
enum Section: string
{
    case Aliases = 'Aliases';
    case Columns = 'Columns';
    case Values = 'Values';
}
