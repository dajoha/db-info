<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\Model;

readonly class CodeMirrorTableFilterCompletionsResult
{
    public function __construct(
        /**
         * The input byte position from where the text should be replaced by one of the proposed
         * completions
         */
        public int $completeFrom,
        /**
         * @var CodeMirrorTableFilterCompletion[]
         */
        public array $completions,
    ) {
    }
}
