<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\CompletionProvider;

use App\Frontend\Service\AutoCompletion\TableFilter\Enum\Section;
use App\Project\Data\TableInfo;
use Doctrine\DBAL\Exception;

/**
 * Provides column alias completion for the filter editor.
 */
class ColumnAliasesCompletionProvider extends AbstractTableFilterCompletionProvider
{
    /**
     * @throws Exception
     */
    public function getCompletions(TableInfo $tableInfo, TableInfo $baseTableInfo): iterable
    {
        $columnAliasesAsKeys = $tableInfo->getColumnAliases();

        // Remove column aliases which have the name of a real column:
        $columnNamesAsKeys = array_fill_keys($tableInfo->getColumnNames(), true);
        $columnAliasesAsKeys = array_diff_key($columnAliasesAsKeys, $columnNamesAsKeys);

        $columnAliases = array_keys($columnAliasesAsKeys);

        return iter($columnAliases)
            ->map(fn(string $columnAlias) => $this->newCompletion(
                label: $columnAlias,
                type: 'columnAlias',
                section: Section::Aliases,
                boost: 30,
                infoColumnName: $tableInfo->getColumnNameFromAlias($columnAlias),
            ))
        ;
    }
}
