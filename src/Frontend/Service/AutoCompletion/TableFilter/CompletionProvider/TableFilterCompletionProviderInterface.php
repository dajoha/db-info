<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\CompletionProvider;

use App\Frontend\Service\AutoCompletion\AutoCompletionService;
use App\Frontend\Service\AutoCompletion\TableFilter\Model\CodeMirrorTableFilterCompletion;
use App\Project\Data\TableInfo;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

/**
 * Used in {@see AutoCompletionService} to retrieve CodeMirror completions.
 */
#[AutoconfigureTag('db_info.codemirror.completion_provider.table_filter')]
interface TableFilterCompletionProviderInterface
{
    /**
     * @return iterable<CodeMirrorTableFilterCompletion>
     */
    public function getCompletions(TableInfo $tableInfo, TableInfo $baseTableInfo): iterable;
}
