<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\CompletionProvider;

use App\Frontend\Service\AutoCompletion\TableFilter\Enum\Section;
use App\Project\Data\TableInfo;
use Doctrine\DBAL\Exception;

class JoinColumnNamesCompletionProvider extends AbstractTableFilterCompletionProvider
{
    /**
     * @throws Exception
     */
    public function getCompletions(TableInfo $tableInfo, TableInfo $baseTableInfo): iterable
    {
        return iter($tableInfo->getJoinColumnNames())
            ->map(fn(string $joinColumnName) => $this->newCompletion(
                label: $joinColumnName,
                type: 'joinColumn',
                section: Section::Columns,
                // @phpstan-ignore property.nonObject (OK: Column name exists because we iterate on getJoinColumnNames())
                infoTableName: $tableInfo->getJoinColumn($joinColumnName)->tableName,
            ))
        ;
    }
}
