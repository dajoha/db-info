<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\CompletionProvider;

use App\Frontend\Service\AutoCompletion\TableFilter\Enum\Section;
use App\Project\Data\TableInfo;
use Doctrine\DBAL\Exception;

class ColumnNamesCompletionProvider extends AbstractTableFilterCompletionProvider
{
    /**
     * @throws Exception
     */
    public function getCompletions(TableInfo $tableInfo, TableInfo $baseTableInfo): iterable
    {
        $joinColumnNames = $tableInfo->getJoinColumnNames();
        $columnNames = $tableInfo->getColumnNames();

        // "Simple column names" are the names of columns which are not foreign keys:
        $simpleColumnNames = array_diff($columnNames, $joinColumnNames);

        return iter($simpleColumnNames)
            ->map(fn(string $columnName) => $this->newCompletion(
                label: $columnName,
                type: 'column',
                section: Section::Columns,
            ))
        ;
    }
}
