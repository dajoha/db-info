<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\CompletionProvider;

use App\Frontend\Service\AutoCompletion\TableFilter\Enum\Section;
use App\Frontend\Service\AutoCompletion\TableFilter\Model\CodeMirrorTableFilterCompletion;
use Twig\Environment;

abstract class AbstractTableFilterCompletionProvider implements TableFilterCompletionProviderInterface
{
    public function __construct(protected Environment $twig)
    {
    }

    protected function newCompletion(
        string $label,
        string $type,
        Section $section,
        ?int $boost = null,
        ?string $infoColumnName = null,
        ?string $infoTableName = null,
    ): CodeMirrorTableFilterCompletion
    {
        return new CodeMirrorTableFilterCompletion(
            $this->twig,
            $label,
            $type,
            $section,
            $boost,
            $infoColumnName,
            $infoTableName,
        );
    }
}
