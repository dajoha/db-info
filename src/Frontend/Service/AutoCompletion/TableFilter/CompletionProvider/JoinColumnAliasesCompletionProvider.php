<?php

declare(strict_types=1);

namespace App\Frontend\Service\AutoCompletion\TableFilter\CompletionProvider;

use App\Frontend\Model\JoinColumn;
use App\Frontend\Service\AutoCompletion\TableFilter\Enum\Section;
use App\Project\Data\TableInfo;
use Doctrine\DBAL\Exception;

class JoinColumnAliasesCompletionProvider extends AbstractTableFilterCompletionProvider
{
    /**
     * @throws Exception
     */
    public function getCompletions(TableInfo $tableInfo, TableInfo $baseTableInfo): iterable
    {
        $joinColumnAliasesAsKeys = $tableInfo->getJoinColumnAliases();

        // Remove join column aliases which have the name of a real column:
        $columnNamesAsKeys = array_fill_keys($tableInfo->getColumnNames(), true);
        $joinColumnAliasesAsKeys = array_diff_key($joinColumnAliasesAsKeys, $columnNamesAsKeys);

        $joinColumnAliases = array_keys($joinColumnAliasesAsKeys);

        return iter($joinColumnAliases)
            ->map(function (string $joinColumnAlias) use ($tableInfo) {
                $joinColumnName = $tableInfo->getJoinColumnNameFromAlias($joinColumnAlias);
                /** @var JoinColumn $joinColumn Not null because we iterate on column names provided by $tableInfo */
                $joinColumn = $tableInfo->getJoinColumn($joinColumnName);

                return $this->newCompletion(
                    label: $joinColumnAlias,
                    type: 'joinColumnAlias',
                    section: Section::Aliases,
                    boost: 60,
                    infoColumnName: $joinColumnName,
                    infoTableName: $joinColumn->tableName,
                );
            })
        ;
    }
}
