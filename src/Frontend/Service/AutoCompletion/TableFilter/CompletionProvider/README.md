Completion providers for the table search expression language.

It provides completions for the table column paths, like table columns, aliases, join columns. 

See `App\Repository\QueryPart\SearchExpression` for detail about search expressions.
