<?php

declare(strict_types=1);

namespace App\Frontend\Service;

use App\Frontend\Helper\Html;
use App\Helper\SplObjectCacheBag;
use App\Project\Data\Row;
use App\Project\Data\TableInfo;
use App\Project\Exception\ForeignKeyWasNotFoundException;
use App\Project\Exception\RowDataIsNotDefinedException;
use App\Project\Exception\TableColumnDoesNotExistException;
use App\Project\Model\Exception\BadPathJoinColumnException;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\BinaryType;
use Psr\Cache\InvalidArgumentException;
use RuntimeException;

/**
 * Methods to format data in HTML.
 */
class HtmlFormatter
{
    protected const string SUMMARY_STYLE_TINY = 'tiny';
    protected const string SUMMARY_STYLE_SHORT = 'short';

    protected SplObjectCacheBag $summaryTinyCache;

    protected SplObjectCacheBag $summaryShortCache;

    public function __construct()
    {
        $this->summaryTinyCache = new SplObjectCacheBag();
        $this->summaryShortCache = new SplObjectCacheBag();
    }

    /**
     * Formats the row to an HTML representation. Placeholders can be placed into curly brackets.
     *
     * HTML tags will only be used for transformed values ({@see TransformerInterface}). Text
     * between placeholders is not HTML-escaped, so it may be used to add formatted text.
     *
     * Format string example: "Product is {name}, from company {owner.company.name}"
     *
     * @throws ForeignKeyWasNotFoundException
     * @throws Exception
     * @throws RowDataIsNotDefinedException
     *
     * @noinspection PhpDocRedundantThrowsInspection
     */
    public function formatRowHtml(Row $row, string $formatString): string
    {
        $stringFormatter = $row->tableInfo->getStringFormatter($formatString);

        return $stringFormatter->format(
            fn($placeholder) => $this->resolveFormatPlaceholderHtml($row, $placeholder) ?? ''
        );
    }

    /**
     * Transforms a value according to the optional {@see TransformerChain} configured for the given
     * column. The returned value is HTML-safe (already escaped).
     *
     * @param bool &$wasTransformed Is set to `true` or `false` depending on if the value was
     *                              transformed or not
     *
     * @throws Exception
     * @throws TableColumnDoesNotExistException
     */
    public function transformTableValueHtml(
        TableInfo $tableInfo,
        string $columnName,
        mixed $rawValue,
        bool &$wasTransformed = false,
    ): string
    {
        if ($rawValue === null) {
            return <<< HTML
                <span class="field-value field-value--null">null</span>
                HTML;
        }

        $transformerChain = $tableInfo->getTransformerChainForColumn($columnName);
        $hasTransformerChain = $transformerChain !== null;
        $transformedValue = $rawValue;
        $isHtmlSafe = false;
        $title = null;

        if ($hasTransformerChain) {
            $transformedValue = $transformerChain->transform($rawValue, $isHtmlSafe);
        } else {
            $column = $tableInfo->getColumnOrThrow($columnName);
            if ($column->getType() instanceof BinaryType) {
                $isHtmlSafe = true;
                $rawValue = bin2hex($rawValue);
                $transformedValue = $rawValue;
                $hasTransformerChain = true;
                $title = "Binary value (represented in hexadecimal)";
            }
        }

        $htmlValue = $transformedValue;
        if (!$isHtmlSafe) {
            $htmlValue = htmlspecialchars(trim((string) $transformedValue));
        }

        if ($hasTransformerChain) {
            if ($title === null) {
                $rawValueInAttr = Html::escapeAttr($rawValue);
                $title = "Raw value: $rawValueInAttr";
            }
            $htmlValue = <<< HTML
                <span
                    class="field-value field-value--transformed"
                    title="$title"
                >$htmlValue</span>
                HTML;
        } else {
            $htmlValue = <<< HTML
                <span class="field-value">$htmlValue</span>
                HTML;
        }

        $wasTransformed = $hasTransformerChain;

        return $htmlValue;
    }

    /**
     * Used in Twig templates: format the row in a "short" or "tiny" summary style.
     *
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     * @throws RowDataIsNotDefinedException
     * @throws RuntimeException
     * @throws BadPathJoinColumnException
     * @throws InvalidArgumentException
     *
     * @noinspection PhpUnused Used in Twig template
     */
    public function getJoinedColumnSummaryHtml(
        Row $row,
        ?string $columnNameAliases,
        string $summaryStyle = self::SUMMARY_STYLE_SHORT,
        bool $aliasAllowed = true,
    ): ?string
    {
        if ($columnNameAliases === null) {
            // Perform null check because Cell::$columnName is nullable
            return null;
        }

        // Avoid to retrieve data from database when it's useless because there is no summary for
        // the given table:
        $joinColumnPath = $row->tableInfo->createJoinColumnPath($columnNameAliases, $aliasAllowed);
        $tableInfo = $row->tableInfo->getTableInfoFromJoinColumnPath($joinColumnPath);
        if ($tableInfo === null || !$tableInfo->hasSummary()) {
            return null;
        }

        $joined = $row->getJoinedRow($joinColumnPath);
        if ($joined === null) {
            return null;
        }

        return match ($summaryStyle) {
            self::SUMMARY_STYLE_SHORT  => $this->getSummaryShortHtml($joined),
            self::SUMMARY_STYLE_TINY => $this->getSummaryTinyHtml($joined),
            default => throw new RuntimeException("Bad summary style: \"$summaryStyle\""),
        };
    }

    /**
     * @throws RowDataIsNotDefinedException
     * @throws ForeignKeyWasNotFoundException
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws BadPathJoinColumnException
     * @throws TableColumnDoesNotExistException
     */
    protected function resolveFormatPlaceholderHtml(Row $row, string $path): ?string
    {
        $columnPath = $row->tableInfo->createColumnPath($path);

        $joinedRow = $row->getJoinedRow($columnPath);
        if ($joinedRow === null) {
            return null;
        }
        $data = $joinedRow->getDataOrThrow();

        $columnName = $columnPath->getValueColumnName();

        if (!key_exists($columnName, $data)) {
            return null;
        }

        $value = $data[$columnName];

        if (is_string($value) && preg_match('/^[0-9]+\.[0-9]+$/', $value) === 1) {
            $value = round((float) $value, 4);
        }

        $tableInfo = $columnPath->getLastTableInfo();

        return $this->transformTableValueHtml($tableInfo, $columnName, $value);
    }

    /**
     * Formats the row accordingly to the configured table prop "summary_tiny". If the property is
     * not defined, falls back to the property "summary_short".
     *
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     * @throws RowDataIsNotDefinedException
     */
    public function getSummaryTinyHtml(Row $row): ?string
    {
        return $this->summaryTinyCache->get($row, function() use ($row) {
            $formatString = $row->tableInfo->getSummaryTiny() ?? $row->tableInfo->getSummaryShort();

            return $formatString === null ? null : $this->formatRowHtml($row, $formatString);
        });
    }

    /**
     * Formats the row accordingly to the configured table prop "summary_short". If the property is
     * not defined, falls back to the property "summary_tiny".
     *
     * @throws Exception
     * @throws ForeignKeyWasNotFoundException
     * @throws RowDataIsNotDefinedException
     */
    public function getSummaryShortHtml(Row $row): ?string
    {
        return $this->summaryShortCache->get($row, function() use ($row) {
            $formatString = $row->tableInfo->getSummaryShort() ?? $row->tableInfo->getSummaryTiny();

            return $formatString === null ? null : $this->formatRowHtml($row, $formatString);
        });
    }
}
