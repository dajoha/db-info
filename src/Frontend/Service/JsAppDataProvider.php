<?php

declare(strict_types=1);

namespace App\Frontend\Service;

use App\Project\Data\TableInfo;
use App\Project\Project;

/**
 * Aims to provide app-level data for the frontend javascript. The provided data is an object
 * containing optional keys depending on the context.
 *
 * In the frontend, the given data is provided in `window.app_data`.
 *
 * The provided data must be coupled with {@see assets/js/service/app-data-provider.js}.
 */
class JsAppDataProvider
{
    public const string K_PROJECT_NAME = 'projectName';
    public const string K_TABLE_NAME = 'tableName';

    /**
     * @return array{
     *     projectName?: string,
     *     tableName?: string,
     * }
     */
    public function getData(
        ?Project $project = null,
        ?TableInfo $tableInfo = null,
    ): array
    {
        $data = [];

        if ($project !== null) {
            $data[self::K_PROJECT_NAME] = $project->getName();
        }

        if ($tableInfo !== null) {
            $data[self::K_TABLE_NAME] = $tableInfo->name;
        }

        return $data;
    }
}
