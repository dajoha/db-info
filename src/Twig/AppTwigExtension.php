<?php

declare(strict_types=1);

namespace App\Twig;

use App\Configuration\Service\ConfigProvider;
use App\Frontend\Factory\CellsFactory;
use App\Frontend\Service\HtmlFormatter;
use App\Project\ProjectManager;
use App\Router\AppRouter;
use App\Router\ListRouteUrlModifier;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * @phpstan-import-type Type_TwigBaseContext from TwigContextProvider
 */
class AppTwigExtension extends AbstractExtension
{
    protected int $autoIncrement = 0;

    /** @var array<string, int> */
    protected array $uniqueIds = [];

    protected readonly CellsFactory $cellsFactory;

    protected readonly ListRouteUrlModifier $listRouteUrlModifier;

    protected readonly ProjectManager $projectManager;

    protected readonly ConfigProvider $configProvider;

    protected readonly AppRouter $appRouter;

    protected readonly HtmlFormatter $htmlFormatter;

    public function __construct(
        #[Autowire(lazy: true)] CellsFactory $cellsFactory,
        #[Autowire(lazy: true)] ListRouteUrlModifier $listRouteUrlModifier,
        #[Autowire(lazy: true)] ProjectManager $projectManager,
        #[Autowire(lazy: true)] ConfigProvider $configProvider,
        #[Autowire(lazy: true)] AppRouter $appRouter,
        #[Autowire(lazy: true)] HtmlFormatter $htmlFormatter,
    ) {
        $this->cellsFactory = $cellsFactory;
        $this->listRouteUrlModifier = $listRouteUrlModifier;
        $this->projectManager = $projectManager;
        $this->configProvider = $configProvider;
        $this->appRouter = $appRouter;
        $this->htmlFormatter = $htmlFormatter;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'attr',
                self::getAttr(...),
                [
                    'needs_environment' => true,
                    'is_safe' => ['html'],
                ],
            ),
            new TwigFunction(
                'auto_id',
                $this->getAutoId(...),
            ),
            new TwigFunction(
                'cache_invalidation_path',
                $this->getCacheInvalidationPath(...),
                [
                    'needs_context' => true,
                ],
            ),
            new TwigFunction(
                'cells',
                $this->cellsFactory->createCellsFromRow(...),
            ),
            new TwigFunction(
                'class',
                self::getClasses(...),
            ),
            new TwigFunction(
                'list_cells',
                $this->cellsFactory->createListCellsFromRow(...),
            ),
            new TwigFunction(
                'new_auto_id',
                $this->getNewAutoId(...),
            ),
            new TwigFunction(
                'pluralize',
                $this->pluralize(...),
            ),
            new TwigFunction(
                'project_names',
                $this->getProjectNames(...),
            ),
            new TwigFunction(
                'row_joined_column_summary_html',
                $this->htmlFormatter->getJoinedColumnSummaryHtml(...),
            ),
            new TwigFunction(
                'row_summary_tiny_html',
                $this->htmlFormatter->getSummaryTinyHtml(...),
            ),
            new TwigFunction(
                'table_path',
                $this->appRouter->getTablePath(...),
            ),
            new TwigFunction(
                'table_structure_path',
                $this->appRouter->getTableStructurePath(...),
            ),
            new TwigFunction(
                'unique_id',
                $this->getUniqueId(...),
            ),
            new TwigFunction(
                'url_with_sort_toggled_multiple',
                $this->listRouteUrlModifier->getUrlWithSortToggledMultiple(...),
            ),
            new TwigFunction(
                'url_with_sort_toggled_single',
                $this->listRouteUrlModifier->getUrlWithSortToggledSingle(...),
            ),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('values', $this->values(...)),
            new TwigFilter('pluralize', $this->pluralize(...)),
            new TwigFilter('to_words', $this->toWords(...)),
        ];
    }

    public function getNewAutoId(): int
    {
        return ++$this->autoIncrement;
    }

    public function getAutoId(int $autoId, string $prefix = null): string
    {
        $prefix = $prefix === null ? '' : "$prefix-";

        return 'auto-id-' . $prefix . $autoId;
    }

    public function getUniqueId(string $prefix): string
    {
        $this->uniqueIds[$prefix] ??= 0;
        ++$this->uniqueIds[$prefix];

        return "{$prefix}_{$this->uniqueIds[$prefix]}";
    }

    /**
     * @param array<string, bool> $classes
     */
    public function getClasses(array $classes): string
    {
        return iter($classes)
            ->map(fn($visible, $class) => ['visible' => $visible, 'class' => $class])
            ->filter(fn($data) => $data['visible'])
            ->map(fn($data) => $data['class'])
            ->join(' ');
    }

    public function getAttr(Environment $env, string $name, mixed $value): ?string
    {
        if ($value === null) {
            return null;
        }
        $value = twig_escape_filter($env, $value, 'html_attr');

        return "$name=$value";
    }

    public function toWords(string $string): string
    {
        /** @var string $string */
        $string = preg_replace(
            '/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])|_+/',
            ' ',
            $string,
        );

        return strtolower($string);
    }

    /**
     * @phpstan-ignore-next-line
     */
    public function values(array $array): array
    {
        return array_values($array);
    }

    public function pluralize(string $string): string
    {
        if (str_ends_with($string, 's')) {
            return $string;
        }
        if (str_ends_with($string, 'y')) {
            return substr($string, 0, -1).'ies';
        }

        return $string . 's'; // To improve :)
    }

    /**
     * @return string[]
     */
    public function getProjectNames(): array
    {
        return $this->configProvider->getProjectNames();
    }

    /**
     * @param Type_TwigBaseContext $context
     */
    public function getCacheInvalidationPath(array $context): string
    {
        return $this->appRouter->getInvalidateCachePath($context['project'], $context['table']);
    }
}
