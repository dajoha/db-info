<?php

declare(strict_types=1);

namespace App\Twig;

use App\Frontend\Service\JsAppDataProvider;
use App\Project\Data\TableInfo;
use App\Project\Project;

/**
 * Provides needed common context for all twig pages (project, table, js data...).
 *
 * @phpstan-type Type_TwigBaseContext array{
 *     project: ?Project,
 *     table: ?TableInfo,
 *     js_app_data: array{
 *         projectName?: string,
 *         tableName?: string,
 *     },
 * }
 */
readonly class TwigContextProvider
{
    public function __construct(protected JsAppDataProvider $jsAppDataProvider)
    {
    }

    /**
     * @param array<string, mixed> $extraContext
     *
     * @return Type_TwigBaseContext|array<string, mixed>
     */
    public function getTwigContext(
        ?Project $project = null,
        ?TableInfo $tableInfo = null,
        array $extraContext = [],
    ): array {
        $project ??= $tableInfo?->project;

        return array_merge(
            [
                'project' => $project,
                'table' => $tableInfo,
                'js_app_data' => $this->jsAppDataProvider->getData($project, $tableInfo),
            ],
            $extraContext,
        );
    }
}
