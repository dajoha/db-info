<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DevTwigExtension extends AbstractExtension
{
    /** @var array<string, float> */
    protected array $benches = [];

    public function getFunctions(): array
    {
        return [
            new TwigFunction('start_bench', $this->startBench(...)),
            new TwigFunction('dump_bench', $this->dumpBench(...)),
        ];
    }

    public function startBench(string $name = 'default'): void
    {
        $this->benches[$name] = microtime(true);
    }

    public function dumpBench(string $name = 'default'): void
    {
        dump(microtime(true) - $this->benches[$name]);
    }
}
