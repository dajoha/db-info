<?php

declare(strict_types=1);

namespace App\Helper;

use Dajoha\ParserCombinator\Base\ParserInterface;
use function Dajoha\ParserCombinator\Parser\Multi\many0;
use function Dajoha\ParserCombinator\Parser\Sequence\seq;
use function Dajoha\ParserCombinator\Parser\String\anyStr;
use function Dajoha\ParserCombinator\Parser\String\delimitedStr;
use function Dajoha\ParserCombinator\Parser\Token\any;

/**
 * Preprocess a format string, in order to replace its placeholders more quickly after creation.
 * The placeholders are anything contained in curly brackets. Format string example:
 *
 *    "Hello {name}, you age is {age}"
 *
 * This kind of format string must be passed in the constructor to be pre-processed, then the
 * method `format()` allows to perform the real formatting process.
 */
readonly class StringFormatter
{
    /** @var string[] */
    public array $strings;

    /** @var string[] */
    public array $placeholders;

    public function __construct(public string $formatString)
    {
        $result = self::getParser()->parse($this->formatString);

        [$pairs, $remainder] = $result->output;
        $this->strings = [...array_column($pairs, 0), $remainder];
        $this->placeholders = array_column($pairs, 1);
    }

    /**
     * Shortcut function which returns all the placeholders found in the format string; only useful
     * if there is no need to keep the `FormatString` instance.
     *
     * @return string[]
     */
    public static function findPlaceholders(string $formatString): array
    {
        return (new self($formatString))->placeholders;
    }

    /**
     * Use the pre-processed format string to generate a formatted string, by using
     * `$placeholderProvider` to transform the placeholders into real values.
     *
     * @param callable(string $placeholder, int $key): string $placeholderProvider
     *
     * @return string
     */
    public function format(callable $placeholderProvider): string
    {
        return iter($this->strings)
            ->interleave(iter($this->placeholders)->map($placeholderProvider))
            ->join();
    }

    /**
     * Parser which separates placeholders.
     */
    protected static function getParser(): ParserInterface
    {
        static $parser = null;

        if ($parser !== null) {
            return $parser;
        }

        $rawString = anyStr(forbiddenChars: '{');
        $placeholder = delimitedStr('{', '}');
        $parser = seq(many0([$rawString, $placeholder]), many0(any())->toString());

        return $parser;
    }
}
