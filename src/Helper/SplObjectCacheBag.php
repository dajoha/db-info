<?php

declare(strict_types=1);

namespace App\Helper;

use SplObjectStorage;

/**
 * A memory cache with php objects as keys.
 */
class SplObjectCacheBag
{
    /** @var SplObjectStorage<object, mixed> */
    protected SplObjectStorage $values;

    public function __construct()
    {
        $this->values = new SplObjectStorage();
    }

    public function get(object $key, callable $getter): mixed
    {
        if (!$this->values->contains($key)) {
            $this->values[$key] = $getter();
        }

        return $this->values[$key];
    }
}
