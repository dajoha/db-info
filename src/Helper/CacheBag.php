<?php

declare(strict_types=1);

namespace App\Helper;

/**
 * The simplest memory cache ever.
 */
class CacheBag
{
    /** @var array<int|string, mixed> */
    protected array $values = [];

    public function get(int|string $key, callable $getter): mixed
    {
        if (!key_exists($key, $this->values)) {
            $this->values[$key] = $getter();
        }

        return $this->values[$key];
    }
}
