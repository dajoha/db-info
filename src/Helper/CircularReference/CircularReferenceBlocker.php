<?php

declare(strict_types=1);

namespace App\Helper\CircularReference;

class CircularReferenceBlocker
{
    /** @var array<string, true> $references */
    protected array $references = [];

    public function __construct(protected ?string $context = null)
    {
    }

    /**
     * @throws CircularReferenceException
     */
    public function addReferenceOrThrow(string $reference): static
    {
        if (array_key_exists($reference, $this->references)) {
            throw new CircularReferenceException($reference, $this->context);
        }

        $this->references[$reference] = true;

        return $this;
    }
}
