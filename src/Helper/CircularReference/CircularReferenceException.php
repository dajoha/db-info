<?php

declare(strict_types=1);

namespace App\Helper\CircularReference;

use App\Exception\AppException;
use Throwable;

class CircularReferenceException extends AppException
{
    public function __construct(
        string $reference,
        ?string $context = null,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message = $context === null ? '' : "$context: ";
        $message .= "Circular reference was found: \"$reference\"";

        parent::__construct($message, $code, $previous);
    }
}
