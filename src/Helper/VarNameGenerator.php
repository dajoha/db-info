<?php

declare(strict_types=1);

namespace App\Helper;

use Dajoha\Iter\Generator\Counter;
use Iterator;

/**
 * Helper to generate unique variable names, based on a given prefix and an automatic increment.
 */
class VarNameGenerator
{
    protected Iterator $generator;

    public function __construct(public readonly string $prefix)
    {
        $this->generator = Counter::from(1)
            ->map(fn(int $n) => $this->prefix . $n)
        ;
    }

    public function __toString(): string
    {
        return $this->getCurrent();
    }

    public function newName(): string
    {
        $this->generator->next();

        return $this->getCurrent();
    }

    public function getCurrent(): string
    {
        return $this->generator->current();
    }
}
