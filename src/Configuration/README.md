All of the configuration-related stuff.

For a each request the modification time of each user config file is checked; if one file has been
modified then the config is reprocessed.

Important classes:

* `./Definition/ConfigurationSchema.php` holds the configuration schema, which validates and
  normalizes the user configuration.
* `./Service/ConfigProvider.php` provides the processed configuration to the rest of the application.
