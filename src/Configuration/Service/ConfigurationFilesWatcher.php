<?php

declare(strict_types=1);

namespace App\Configuration\Service;

use App\Cache\Cache;
use App\Cache\CacheKey\Configuration\ConfigurationFilesTimestampsCacheKey;
use App\Configuration\Event\ConfigFilesChangedEvent;
use App\Configuration\EventListener\ConfigFilesChangedListener;
use App\Configuration\Model\FilesTimestampsBag;
use Psr\Cache\InvalidArgumentException;
use Psr\EventDispatcher\EventDispatcherInterface;

/**
 * Provides a way to check if user configuration files have been updated. If so, then an event
 * {@see ConfigFilesChangedEvent} is dispatched, which is listened by:
 *  - {@see ConfigFilesChangedListener} to invalidate config cache, and pre-process config again in
 *    order to retrieve the new list of user config files to watch.
 */
class ConfigurationFilesWatcher
{
    protected bool $alreadyChecked = false;

    public function __construct(
        protected Cache $cache,
        protected EventDispatcherInterface $eventDispatcher,
    ) {
    }

    /**
     * Sends an event {@see ConfigFilesChangedEvent} if user config files have been changed. It will
     * check for file changes only once per request, even if called multiple times.
     *
     * @throws InvalidArgumentException
     */
    public function checkFilesChanged(): void
    {
        // Run this check at most once per request:
        if ($this->alreadyChecked) {
            return;
        }
        $this->alreadyChecked = true;

        // Get the list of files to watch:
        $filesTimestampsBagItem = $this->cache->getItem(new ConfigurationFilesTimestampsCacheKey());

        if ($filesTimestampsBagItem->isHit()) {
            /** @var FilesTimestampsBag $filesTimestampsBag */
            $filesTimestampsBag = $filesTimestampsBagItem->get();
            $anyFileHasChanged = $filesTimestampsBag->anyFileHasChanged();
        } else {
            $anyFileHasChanged = true;
        }

        if ($anyFileHasChanged) {
            $this->eventDispatcher->dispatch(new ConfigFilesChangedEvent());
        }
    }
}
