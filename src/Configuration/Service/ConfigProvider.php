<?php

declare(strict_types=1);

namespace App\Configuration\Service;

use App\Cache\Cache;
use App\Cache\CacheKey\Configuration\ConfigurationCacheKey;
use App\Cache\CacheKey\Configuration\PreProcessedConfigurationCacheKey;
use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\ProjectConfig;
use Psr\Cache\InvalidArgumentException;

/**
 * Provides user configuration at different steps:
 *  - Pre-processed config ({@see self::getPreProcessedConfig()});
 *  - Fully processed and transformed config ({@See self::getConfig()});
 *  - Fully processed and transformed config for one specific project, wrapped into {@see ProjectConfig}.
 *
 * @phpstan-import-type Type_UserConfig_Root from ConfigurationSchema
 */
class ConfigProvider
{
    /**
     * @phpstan-var ?Type_UserConfig_Root
     */
    protected ?array $preProcessedConfig = null;

    /**
     * @var ?array<string, mixed>
     */
    protected ?array $config = null;

    /**
     * @var array<string, ProjectConfig>
     */
    protected array $projectConfigs = [];

    public function __construct(
        protected ConfigurationProcessor $configurationProcessor,
        protected ConfigurationFilesWatcher $configurationFilesWatcher,
        protected Cache $cache,
    ) {
    }

    /**
     * @return array<string, mixed>
     *
     * @throws InvalidArgumentException
     */
    public function getConfig(): array
    {
        if ($this->config !== null) {
            return $this->config;
        }

        // If user config files have been changed, invalidate cache and rebuild pre-processed config:
        $this->configurationFilesWatcher->checkFilesChanged();

        $this->config = $this->cache->get(
            new ConfigurationCacheKey(),
            function () {
                $preProcessedConfig = $this->getPreProcessedConfig();

                return $this->configurationProcessor->process($preProcessedConfig);
            },
        );

        return $this->config;
    }

    /**
     * @return Type_UserConfig_Root
     *
     * @throws InvalidArgumentException
     */
    public function getPreProcessedConfig(): array
    {
        if ($this->preProcessedConfig !== null) {
            return $this->preProcessedConfig;
        }

        // If user config files have been changed, invalidate cache and rebuild pre-processed config:
        $this->configurationFilesWatcher->checkFilesChanged();

        // Get the value without checking cache hit, because in all cases the cache value is already
        // set (thanks to call to "checkFilesChanged()" above):
        $this->preProcessedConfig = $this->cache->getItemValue(new PreProcessedConfigurationCacheKey());

        return $this->preProcessedConfig;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getProjectConfig(string $projectName): ?ProjectConfig
    {
        $projectConfig = $this->projectConfigs[$projectName] ?? null;
        if ($projectConfig !== null) {
            return $projectConfig;
        }

        $config = $this->getConfig();

        $projectConfigDef = $config[K::PROJECTS][$projectName] ?? null;
        if ($projectConfigDef === null) {
            return null;
        }

        $this->projectConfigs[$projectName] = new ProjectConfig(
            $config[K::TABLE_DEFAULTS] ?? [],
            $projectName,
            $projectConfigDef['config'],
            $projectConfigDef['tableNamesByAlias'],
            $projectConfigDef['tableBestAliases'],
            $projectConfigDef['columnAliases'],
            $projectConfigDef['columnBestAliases'],
            $projectConfigDef['joinColumnAliases'],
            $projectConfigDef['hiddenColumns'],
            $projectConfigDef['listHiddenColumns'],
            $projectConfigDef['listColumnsByColumn'],
            $projectConfigDef['columnsTransformerChains'],
            $projectConfigDef['columnsPossibleValues'],
        );

        return $this->projectConfigs[$projectName];
    }

    /**
     * @return string[]
     * @throws InvalidArgumentException
     */
    public function getProjectNames(): array
    {
        /** @phpstan-ignore return.type (OK: Validated by {@see ConfigurationSchema::getMainSchema}) */
        return array_keys($this->getConfig()[K::PROJECTS]);
    }

    /**
     * Application-level default quick-find options definitions.
     *
     * @return array<string, mixed>|null
     * @throws InvalidArgumentException
     */
    public function getDefaultQuickFindOptionsDefinitions(): ?array
    {
        return $this->getConfig()[K::TABLE_DEFAULTS][K::TABLE__QUICK_FIND_OPTIONS] ?? null;
    }
}
