<?php

declare(strict_types=1);

namespace App\Configuration\Service;

use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\Exception\MultipleColumnAliasDefinitionException;
use App\Configuration\Exception\MultipleTableAliasDefinitionException;
use App\Configuration\Exception\TransformerChainParseException;
use App\Configuration\Helper\ConfigurationPreProcessor;
use App\Configuration\Helper\ConfigurationTransformer;
use App\Configuration\Model\FilesTimestampsBag;
use App\DependencyInjection\DbInfoExtension;
use App\Domain\Merger\Exception\MergerException;
use App\Exception\InternalException;
use App\Frontend\ValueTransformer\DependencyInjection\Pass\RegisterValueTransformersPass;
use App\Frontend\ValueTransformer\Transformer\Base\TransformerInterface;
use App\Helper\CircularReference\CircularReferenceException;
use Dajoha\Schema\Exception\ValidationException;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Main service which processes user configuration. There two main steps:
 * - Pre-process, where config is validated and normalized;
 * - Process, where config is optimized for internal use.
 *
 * @phpstan-import-type Type_UserConfig_Root from ConfigurationSchema
 */
class ConfigurationProcessor
{
    /**
     * @param array<string, class-string<TransformerInterface>> $transformerClasses Map of transformer name => transformer class
     *
     */
    public function __construct(
        #[Autowire(param: DbInfoExtension::CONTAINER_PARAM__MAIN_CONFIG_FILE)]
        protected string $mainConfigFile,
        #[Autowire(param: RegisterValueTransformersPass::CONTAINER_PARAM__TRANSFORMER_CLASSES)]
        protected array $transformerClasses,
    ) {
    }

    /**
     * @return Type_UserConfig_Root
     *
     * @throws MergerException
     * @throws ValidationException
     * @throws CircularReferenceException
     * @throws InternalException
     */
    public function preProcess(?FilesTimestampsBag $filesTimestampsBag = null): array
    {
        $configurationProcessor = $this->newConfigurationPreprocessor();

        return $configurationProcessor->preProcessConfig($filesTimestampsBag);
    }

    /**
     * @param Type_UserConfig_Root $preProcessedConfig Should be the result of {@see self::preProcess()}
     *
     * @return array<string, mixed>
     *
     * @throws TransformerChainParseException
     * @throws MultipleTableAliasDefinitionException
     * @throws MultipleColumnAliasDefinitionException
     */
    public function process(array $preProcessedConfig): array
    {
        // Note: below the term "transformer" is used by 2 different scopes: "transformer classes"
        // are totally unrelated to "configuration transformer":
        $configurationTransformer = new ConfigurationTransformer(
            $preProcessedConfig,
            $this->transformerClasses,
        );

        return $configurationTransformer->getTransformedConfig();
    }

    public function newConfigurationPreprocessor(): ConfigurationPreProcessor
    {
        return new ConfigurationPreProcessor($this->mainConfigFile);
    }
}
