<?php

declare(strict_types=1);

namespace App\Configuration\Definition;

/**
 * Defines all the configuration keys used in the configuration tree "db_info". Nested keys
 * are separated by two underscores.
 */
class ConfigurationKeys
{
    public const string ROOT = 'db_info';
    public const string LIST_MAX_RESULTS = 'list_max_results';
    public const string TABLE_DEFAULTS = 'table_defaults';
    public const string PROJECTS = 'projects';
    public const string PROJECT__DATABASE_URL = 'database_url';
    public const string PROJECT__TABLES = 'tables';
    public const string PROJECT__ABSTRACT = 'abstract';
    public const string PROJECT__EXTENDS = 'extends';
    public const string TABLE__ALIASES = 'aliases';
    public const string TABLE__SUMMARY_SHORT = 'summary_short';
    public const string TABLE__SUMMARY_TINY = 'summary_tiny';
    public const string TABLE__COLUMNS = 'columns';
    public const string TABLE__COLUMNS__TRANSFORM = 'transform';
    public const string TABLE__COLUMNS__POSSIBLE_VALUES = 'possible_values';
    public const string TABLE__COLUMN_ALIASES = 'column_aliases';
    public const string TABLE__JOIN_COLUMN_ALIASES = 'join_column_aliases';
    public const string TABLE__HIDDEN_COLUMNS = 'hidden_columns';
    public const string TABLE__DEFAULT_COLUMN_ORDERS = 'default_column_orders';
    public const string TABLE__LIST_COLUMNS = 'list_columns';
    public const string TABLE__LIST_COLUMNS__MODE = 'mode';
    public const string TABLE__LIST_COLUMNS__COLUMNS = 'columns';
    public const string TABLE__LIST_COLUMNS__HIDE = 'hide';
    public const string TABLE__LIST_COLUMNS__FORMAT = 'format';
    public const string TABLE__LIST_COLUMNS__COLUMNS__TITLE = 'title';
    public const string TABLE__LIST_COLUMNS__COLUMNS__COLUMN = 'column';
    public const string TABLE__LIST_COLUMNS__COLUMNS__FORMAT = 'format';
    public const string TABLE__QUICK_FIND_OPTIONS = 'quick_find_options';
    public const string TABLE__DEFAULT_PAGINATION_SIZE = 'default_pagination_size';
    public const string QUICK_FIND_OPTIONS__CLASS = 'class';
    public const string QUICK_FIND_OPTIONS__PARAMETERS = 'parameters';
}
