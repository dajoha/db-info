<?php

declare(strict_types=1);

namespace App\Configuration\Definition\Enum;

/**
 * The different display modes for a table list.
 */
enum ListColumnsMode: string
{
    /** Only one summary column (use "summary_tiny" or "summary_short") */
    case Summary = 'summary';
    /** Only one summary column (use a custom format instead of "summary_tiny" or "summary_short") */
    case Format = 'format';
    /** Show all (not hidden) columns */
    case All = 'all';
    /** Show the listed columns only */
    case Listed = 'listed';

    /**
     * @return string[]
     */
    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
