<?php

declare(strict_types=1);

namespace App\Configuration\Definition;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\Enum\ListColumnsMode;
use App\Configuration\Helper\ConfigurationPreProcessor;
use App\Configuration\Model\SortDirection;
use App\Repository\QuickFind\QuickFindOption\QuickFindOptionInterface;
use Dajoha\Schema\Base\Context;
use Dajoha\Schema\Base\SchemaInterface;
use Dajoha\Schema\Expect as E;

/**
 * Defines the configuration schema for "db_info".
 *
 * @phpstan-type Type_UserConfig_Root array{
 *     db_info: Type_UserConfig_Main,
 * }
 *
 * @phpstan-type Type_UserConfig_Main array{
 *     projects: array<string, Type_UserConfig_Project>,
 *     table_defaults: array<string, mixed>,
 * }
 *
 * @phpstan-type Type_UserConfig_Project array{
 *     extends: string[],
 *     abstract: bool,
 *     database_url: string,
 *     tables: array<string, Type_UserConfig_Table>,
 *     table_defaults: array<string, mixed>,
 *     list_max_results: int,
 * }
 *
 * @phpstan-type Type_UserConfig_Table array{
 *     aliases?: string[],
 *     summary_short?: string,
 *     summary_tiny?: string,
 *     columns: array<string, mixed>,
 *     column_aliases: array<string, string[]>,
 *     join_column_aliases: array<string, string[]>,
 *     hidden_columns: string[],
 *     list_columns?: Type_UserConfig_TableListColumns,
 *     quick_find_options?: Type_UserConfig_QuickFindOptions,
 *     default_column_orders: mixed[],
 *     default_pagination_size?: int,
 * }
 *
 * @phpstan-type Type_UserConfig_QuickFindOptions array{
 *     class: class-string<QuickFindOptionInterface>,
 *     parameters: mixed[],
 * }
 *
 * @phpstan-type Type_UserConfig_TableListColumnsColumn array{
 *     title: ?string,
 *     column: ?string,
 *     format: ?string,
 * }
 *
 * @phpstan-type Type_UserConfig_TableListColumns array{
 *     mode: string,
 *     columns: Type_UserConfig_TableListColumnsColumn[],
 *     hide: string[]|null,
 *     format: ?string,
 * }
 */
class ConfigurationSchema
{
    public const int DEFAULT_LIST_MAX_RESULTS = 1000;

    protected const array MANDATORY_KEYS_LIST_COLUMNS_COLUMN = [
        K::TABLE__LIST_COLUMNS__COLUMNS__TITLE,
        K::TABLE__LIST_COLUMNS__COLUMNS__COLUMN,
    ];

    protected const array MANDATORY_KEYS_TABLE_SUMMARY_MODE = [
        K::TABLE__SUMMARY_TINY,
        K::TABLE__SUMMARY_SHORT,
    ];

    /**
     * The main configuration schema.
     */
    public static function getSchema(): SchemaInterface
    {
        static $schema = null;

        $schema ??= E::dict([
            K::ROOT => self::getMainSchema(),
        ]);

        return $schema;
    }

    /**
     * The "first pass" schema is a very basic version of the main configuration schema, intended
     * to be used at an early stage of the configuration processing:
     * {@see ConfigurationPreProcessor::preProcessConfig()}.
     *
     * See {@see self::getSchema()} for the whole configuration schema.
     */
    public static function getFirstPassSchema(): SchemaInterface
    {
        static $schema = null;

        $schema ??= E::dict([
            K::ROOT => self::getFirstPassMainSchema(),
        ]);

        return $schema;
    }

    protected static function getMainSchema(): SchemaInterface
    {
        return E::dict([
            K::PROJECTS => E::array()
                ->stringKeys(examplePrefix: 'project')
                ->values(self::getProjectSchema()),
            K::TABLE_DEFAULTS => self::getTableDefaultsSchema(),
        ]);
    }

    protected static function getProjectSchema(): SchemaInterface
    {
        return E::dict([
            K::PROJECT__EXTENDS => self::stringListDefaultEmpty(),
            K::PROJECT__ABSTRACT => E::bool()->default(false),
            K::PROJECT__DATABASE_URL => E::string()->required(),
            K::PROJECT__TABLES => E::array()
                ->stringKeys(examplePrefix: 'table')
                ->values(self::getTableSchema())
                ->default([]),
            K::TABLE_DEFAULTS => self::getTableDefaultsSchema(),
            K::LIST_MAX_RESULTS => E::integer()->min(1)->default(self::DEFAULT_LIST_MAX_RESULTS),
        ]);
    }

    protected static function getTableSchema(): SchemaInterface
    {
        return
            E::dict([
                K::TABLE__ALIASES => self::stringList(),
                K::TABLE__SUMMARY_SHORT => E::string(),
                K::TABLE__SUMMARY_TINY => E::string(),
                K::TABLE__COLUMNS => self::getTableColumnsSchema(),
                K::TABLE__COLUMN_ALIASES => self::aliasArray(examplePrefix: 'column'),
                K::TABLE__JOIN_COLUMN_ALIASES => self::aliasArray(examplePrefix: 'join_column'),
                K::TABLE__HIDDEN_COLUMNS => self::stringListDefaultEmpty(),
                K::TABLE__LIST_COLUMNS => self::getTableListColumnsSchema(),
                K::TABLE__QUICK_FIND_OPTIONS => self::getQuickFindOptionsSchema(),
                K::TABLE__DEFAULT_COLUMN_ORDERS => self::getTableDefaultColumnOrdersSchema(),
                K::TABLE__DEFAULT_PAGINATION_SIZE => self::getDefaultPaginationSizeSchema(),
            ])
            ->validate(self::validateTable(...))
        ;
    }

    /**
     * Validation for the config of a database table.
     */
    protected static function validateTable(mixed $tableConfig): bool|string
    {
        $listColumnsMode = $tableConfig[K::TABLE__LIST_COLUMNS][K::TABLE__LIST_COLUMNS__MODE] ?? null;

        if (
            $listColumnsMode !== ListColumnsMode::Summary->value
            || self::hasAnyMandatoryKey($tableConfig, self::MANDATORY_KEYS_TABLE_SUMMARY_MODE)
        ) {
            return true;
        }

        return sprintf(
            "When %s.%s = \"%s\", at least %s or %s must be defined",
            K::TABLE__LIST_COLUMNS,
            K::TABLE__LIST_COLUMNS__MODE,
            ListColumnsMode::Summary->value,
            K::TABLE__SUMMARY_TINY,
            K::TABLE__SUMMARY_SHORT,
        );
    }

    protected static function getTableColumnsSchema(): SchemaInterface
    {
        $valueSchema = E::dict([
            K::TABLE__COLUMNS__TRANSFORM => E::string()->nullable(),
            K::TABLE__COLUMNS__POSSIBLE_VALUES => E::list(E::anyOf(E::string(), E::integer())),
        ]);

        return
            E::array()
            ->stringKeys(examplePrefix: 'column')
            ->values($valueSchema)
            ->default([])
        ;
    }

    protected static function getTableDefaultsSchema(): SchemaInterface
    {
        return
            E::dict([
                K::TABLE__QUICK_FIND_OPTIONS => self::getQuickFindOptionsSchema(),
                K::TABLE__DEFAULT_PAGINATION_SIZE => self::getDefaultPaginationSizeSchema(),
            ])
            ->default([])
        ;
    }

    protected static function getQuickFindOptionsSchema(): SchemaInterface
    {
        return
            E::list(
                E::dict([
                    K::QUICK_FIND_OPTIONS__CLASS => E::string()->required(),
                    K::QUICK_FIND_OPTIONS__PARAMETERS => E::array()->default([]),
                ])
                ->normalize(
                    function ($value) {
                        return is_string($value) ? [ K::QUICK_FIND_OPTIONS__CLASS => $value ] : $value;
                    },
                    prepend: true,
                )
            )
        ;
    }

    protected static function getDefaultPaginationSizeSchema(): SchemaInterface
    {
        return E::integer(min: 1);
    }

    protected static function getTableDefaultColumnOrdersSchema(): SchemaInterface
    {
        // A sort direction, like "ASC", or "DESC":
        $sortDirectionSchema = E::enum(SortDirection::class)
            ->normalize(fn(string $dir) => strtolower($dir))
        ;

        $columnOrderSchema = E::tuple(E::string(), $sortDirectionSchema);

        return E::list($columnOrderSchema)
            ->default([])
            ->normalize(function (array $value) {
                if (empty($value) || is_array($value[0] ?? null)) {
                    return $value;
                } else {
                    return [$value];
                }
            })
        ;
    }

    protected static function getTableListColumnsSchema(): SchemaInterface
    {
        return
            E::dict([
                K::TABLE__LIST_COLUMNS__MODE => E::anyOf(...ListColumnsMode::values())
                    ->default(ListColumnsMode::All->value),
                K::TABLE__LIST_COLUMNS__COLUMNS => E::list(self::getTableListColumnsColumnSchema()),
                K::TABLE__LIST_COLUMNS__HIDE => self::stringListDefaultEmpty(),
                K::TABLE__LIST_COLUMNS__FORMAT => E::string(),
            ])
            ->normalize(function($value) {
                return is_string($value) ? [K::TABLE__LIST_COLUMNS__MODE => $value] : $value;
            })
            ->validate(self::validateTableListColumns(...))
        ;
    }

    /**
     * Custom validation for the table list config.
     */
    protected static function validateTableListColumns(mixed $listColumnsConfig, Context $context): bool
    {
        $mode = ListColumnsMode::tryFrom($listColumnsConfig[K::TABLE__LIST_COLUMNS__MODE]);
        if ($mode === null) {
            return true;
        }

        $addMandatoryError = function (string $key) use ($mode, $context) {
            $context->addError(sprintf(
                'Configuration key "%s" is mandatory for mode "%s"',
                $key,
                $mode->value
            ));
        };
        $addForbiddenError = function (string $key) use ($mode, $context) {
            $context->addError(sprintf(
                'Configuration key "%s" is forbidden for mode "%s"',
                $key,
                $mode->value,
            ));
        };

        if ($mode !== ListColumnsMode::All) {
            if (!empty($listColumnsConfig[K::TABLE__LIST_COLUMNS__HIDE])) {
                $addForbiddenError(K::TABLE__LIST_COLUMNS__HIDE);
            }
        }

        if (in_array($mode, [ListColumnsMode::Summary, ListColumnsMode::Format])) {
            if (!empty($listColumnsConfig[K::TABLE__LIST_COLUMNS__COLUMNS])) {
                $addForbiddenError(K::TABLE__LIST_COLUMNS__COLUMNS);
            }
        }

        if ($mode === ListColumnsMode::Format) {
            if (empty($listColumnsConfig[K::TABLE__LIST_COLUMNS__FORMAT])) {
                $addMandatoryError(K::TABLE__LIST_COLUMNS__FORMAT);
            }
        } elseif (!empty($listColumnsConfig[K::TABLE__LIST_COLUMNS__FORMAT])) {
            $addForbiddenError(K::TABLE__LIST_COLUMNS__FORMAT);
        }

        $columns = $listColumnsConfig[K::TABLE__LIST_COLUMNS__COLUMNS] ?? null;
        if ($mode === ListColumnsMode::Listed && empty($columns)) {
            $addMandatoryError(K::TABLE__LIST_COLUMNS__COLUMNS);
        }

        return true;
    }

    protected static function getTableListColumnsColumnSchema(): SchemaInterface
    {
        return
            E::dict([
                K::TABLE__LIST_COLUMNS__COLUMNS__TITLE => E::string()->nullable(),
                K::TABLE__LIST_COLUMNS__COLUMNS__COLUMN => E::string()->nullable(),
                K::TABLE__LIST_COLUMNS__COLUMNS__FORMAT => E::string()->nullable(),
            ])
            ->normalize(function($value) {
                if (is_string($value)) {
                    return [
                        K::TABLE__LIST_COLUMNS__COLUMNS__COLUMN => $value,
                        K::TABLE__LIST_COLUMNS__COLUMNS__FORMAT => '{'.$value.'}',
                    ];
                }

                return $value;
            })
            ->validate(fn($columnConfig, Context $context) => self::validateHasAnyMandatoryKey(
                $columnConfig,
                self::MANDATORY_KEYS_LIST_COLUMNS_COLUMN,
            ))
        ;
    }

    protected static function getFirstPassMainSchema(): SchemaInterface
    {
        return
            E::dict([
                K::PROJECTS => E::array()
                    ->stringKeys(examplePrefix: 'project')
                    ->values(self::getFirstPassProjectSchema()),
            ])
                ->allowExtraFields()
        ;
    }

    protected static function getFirstPassProjectSchema(): SchemaInterface
    {
        return
            E::dict([
                K::PROJECT__EXTENDS => self::stringListDefaultEmpty(),
                K::PROJECT__ABSTRACT => E::bool()->default(false),
            ])
                ->allowExtraFields()
        ;
    }

    protected static function aliasArray(?string $examplePrefix = null): SchemaInterface
    {
        return
            E::array()
            ->stringKeys($examplePrefix)
            ->values(self::stringListDefaultEmpty())
            ->default([])
        ;
    }

    protected static function stringList(): SchemaInterface
    {
        return
            E::list()
            ->castToArray()
            ->values(E::string()->notEmpty())
        ;
    }

    protected static function stringListDefaultEmpty(): SchemaInterface
    {
        return
            E::list()
            ->castToArray()
            ->values(E::string()->notEmpty())
            ->default([])
        ;
    }

    /**
     * Validates that at least one mandatory key is present and not empty.
     *
     * @param array<mixed, mixed> $dict
     * @param string[] $keys
     */
    protected static function validateHasAnyMandatoryKey(array $dict, array $keys): bool|string
    {
        if (!self::hasAnyMandatoryKey($dict, $keys)) {
            $keys = iter($keys)->map(fn($name) => "'$name'")->join(', ');

            return "At least one key in $keys must be defined";
        }

        return true;
    }

    /**
     * Checks if at least one mandatory key is present and not empty.
     *
     * @param array<mixed, mixed> $dict
     * @param string[] $keys
     */
    protected static function hasAnyMandatoryKey(array $dict, array $keys): bool
    {
        return iter($keys)->any(fn(string $key) => !empty($dict[$key] ?? null));
    }
}
