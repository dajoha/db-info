<?php

declare(strict_types=1);

namespace App\Configuration\EventListener;

use App\Cache\Cache;
use App\Cache\CacheKey\Configuration\ConfigurationCacheKey;
use App\Cache\CacheKey\Configuration\ConfigurationFilesTimestampsCacheKey;
use App\Cache\CacheKey\Configuration\PreProcessedConfigurationCacheKey;
use App\Configuration\Event\ConfigFilesChangedEvent;
use App\Configuration\Model\FilesTimestampsBag;
use App\Configuration\Service\ConfigurationProcessor;
use App\Domain\Merger\Exception\MergerException;
use App\Exception\InternalException;
use App\Helper\CircularReference\CircularReferenceException;
use Dajoha\Schema\Exception\ValidationException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

/**
 * Invalidates configuration cache when any user config file has been changed. We need to rebuild
 * pre-processed configuration as well, because this is where we register the new list of user
 * config files to watch for changes.
 */
#[AsEventListener(ConfigFilesChangedEvent::class)]
class ConfigFilesChangedListener
{
    public function __construct(
        protected ConfigurationProcessor $configurationProcessor,
        protected Cache $cache,
    ) {
    }

    /**
     * @throws InvalidArgumentException
     * @throws MergerException
     * @throws InternalException
     * @throws CircularReferenceException
     * @throws ValidationException
     */
    public function __invoke(): void
    {
        $filesTimestampsBag = new FilesTimestampsBag();

        $preProcessedConfig = $this->configurationProcessor->preProcess($filesTimestampsBag);
        $this->cache->set(new PreProcessedConfigurationCacheKey(), $preProcessedConfig);

        // Set the new list of config files to watch:
        $this->cache->set(new ConfigurationFilesTimestampsCacheKey(), $filesTimestampsBag);

        $this->cache->delete(new ConfigurationCacheKey());
    }
}
