<?php

declare(strict_types=1);

namespace App\Configuration;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\Definition\Enum\ListColumnsMode;
use App\Configuration\Model\SortDirection;
use App\Configuration\Service\ConfigProvider;
use App\Frontend\ValueTransformer\TransformerChain;
use App\Project\Data\TableInfo;
use App\Project\Project;
use App\Repository\QueryPart\Pagination\Pagination;

/**
 * A wrapper around "db_info.projects.<PROJECT_NAME>" configuration key. It is provided by the
 * service {@see ConfigProvider}.
 *
 * @phpstan-import-type Type_UserConfig_QuickFindOptions from ConfigurationSchema
 * @phpstan-import-type Type_UserConfig_TableListColumns from ConfigurationSchema
 */
class ProjectConfig
{
    public function __construct(
        /** @var array<string, mixed> */
        protected array $globalTableDefaults,
        protected string $projectName,
        /** @var array<string, mixed> */
        protected array $config,
        /** @var array<string, string> */
        protected array $tableNamesByAlias,
        /** @var array<string, mixed> */
        protected array $tableBestAliases,
        /** @var array<string, mixed> */
        protected array $columnAliases,
        /** @var array<string, mixed> */
        protected array $columnBestAliases,
        /** @var array<string, mixed> */
        protected array $joinColumnAliases,
        /** @var array<string, mixed>|null */
        protected ?array $hiddenColumns,
        /** @var array<string, mixed>|null */
        protected ?array $listHiddenColumns,
        /** @var array<string, mixed>|null */
        protected ?array $listColumnsByColumn,
        /** @var array<string, mixed> */
        protected array $columnsTransformerChains,
        /** @var array<string, mixed> */
        protected array $columnsPossibleValues,
    ) {
    }

    public function getProjectName(): string
    {
        return $this->projectName;
    }

    public function getDatabaseUrl(): string
    {
        return $this->config[K::PROJECT__DATABASE_URL];
    }

    /**
     * Don't use this method directly, use {@see Project::getTableNameFromAlias()} instead.
     */
    public function getTableNameFromAlias(string $alias): string
    {
        return $this->getTableNamesByAlias()[$alias] ?? $alias;
    }

    public function getTableBestAlias(string $tableAlias): string
    {
        return $this->getTableBestAliases()[$tableAlias] ?? $tableAlias;
    }

    public function getTableSummaryTiny(string $tableName): ?string
    {
        return $this->getTableConfig($tableName)[K::TABLE__SUMMARY_TINY] ?? null;
    }

    public function getTableSummaryShort(string $tableName): ?string
    {
        return $this->getTableConfig($tableName)[K::TABLE__SUMMARY_SHORT] ?? null;
    }

    /**
     * An array of tuples describing the default column orders for a table column. Each tuple is
     * composed of:
     *  - A column name;
     *  - A sort direction (ASC or DESC).
     *
     * @return array<array{string, SortDirection}>
     */
    public function getTableDefaultColumnOrders(string $tableName): array
    {
        return $this->getTableConfig($tableName)[K::TABLE__DEFAULT_COLUMN_ORDERS] ?? [];
    }

    public function getColumnBestAlias(string $tableName, string $columnName): string
    {
        return $this->getColumnBestAliases()[$tableName][$columnName] ?? $columnName;
    }

    /**
     * Don't use this method directly, use {@see TableInfo::getColumnNameFromAlias()} instead.
     */
    public function getColumnNameFromAlias(string $tableName, string $columnAlias): string
    {
        return $this->getColumnAliases()[$tableName][$columnAlias] ?? $columnAlias;
    }

    public function getJoinColumnName(string $tableName, string $joinColumnAlias): string
    {
        return $this->getJoinColumnAliases()[$tableName][$joinColumnAlias] ?? $joinColumnAlias;
    }

    /**
     * @return array<string, true>
     */
    public function getTableHiddenColumns(string $tableName): array
    {
        return $this->getHiddenColumns()[$tableName] ?? [];
    }

    /**
     * @return array<string, true>
     */
    public function getTableListHiddenColumns(string $tableName): array
    {
        return $this->getListHiddenColumns()[$tableName] ?? [];
    }

    /**
     * @return array<string, array{
     *             column: ?string,
     *             format: ?string,
     *             title: ?string,
     *         }>
     */
    public function getTableListColumnsByColumn(string $tableName): array
    {
        return $this->getListColumnsByColumn()[$tableName] ?? [];
    }

    /**
     * @return Type_UserConfig_TableListColumns
     */
    public function getTableListColumnsConfig(string $tableName): array
    {
        return $this->getTableConfig($tableName)[K::TABLE__LIST_COLUMNS] ?? [
            K::TABLE__LIST_COLUMNS__MODE => ListColumnsMode::All->value,
        ];
    }

    /**
     * @param string $tableName
     *
     * @return Type_UserConfig_QuickFindOptions[]|null
     */
    public function getQuickFindOptionsDefinitions(string $tableName): ?array
    {
        $tableQuickFindOptions = $this->getTableConfig($tableName)[K::TABLE__QUICK_FIND_OPTIONS] ?? null;

        // Can be empty array if config is not defined (to be improved), so we check for emptiness:
        return empty($tableQuickFindOptions) ? null : $tableQuickFindOptions;
    }

    public function getDefaultPaginationSize(string $tableName): int
    {
        return
            $this->getTableConfig($tableName)[K::TABLE__DEFAULT_PAGINATION_SIZE]
            ?? $this->config[K::TABLE_DEFAULTS][K::TABLE__DEFAULT_PAGINATION_SIZE]
            ?? $this->globalTableDefaults[K::TABLE__DEFAULT_PAGINATION_SIZE]
            ?? Pagination::DEFAULT_SIZE
        ;
    }

    /**
     * @return array<string, mixed>|null
     */
    public function getTableConfig(string $tableName): ?array
    {
        return $this->config[K::PROJECT__TABLES][$tableName] ?? null;
    }

    /**
     * Returns an array with:
     *  - Table alias as key,
     *  - Real table name as value.
     *
     * @return array<string, string>
     */
    public function getTableNamesByAlias(): array
    {
        return $this->tableNamesByAlias;
    }

    /**
     * Returns an array with:
     *  - Table alias as key,
     *  - The "best" alias as value, i.e. the first defined alias or the table name if there is no
     *    table alias.
     *
     * @return array<string, string>
     */
    public function getTableBestAliases(): array
    {
        return $this->tableBestAliases;
    }

    /**
     * Returns a 2-level array with:
     *  - Table name as primary key,
     *  - Column alias as secondary key,
     *  - Real column name as value.
     *
     * @return array<string, array<string, string>>
     */
    public function getColumnAliases(): array
    {
        return $this->columnAliases;
    }

    /**
     * Returns a 2-level array with:
     *  - Table name as primary key,
     *  - Column name as secondary key,
     *  - The best column alias as value.
     *
     * @return array<string, array<string, string>>
     */
    public function getColumnBestAliases(): array
    {
        return $this->columnBestAliases;
    }

    /**
     * Returns a 2-level array with:
     *  - Table name as primary key,
     *  - Join column alias as secondary key,
     *  - Real column name as value.
     *
     * @return array<string, array<string, string>>
     */
    public function getJoinColumnAliases(): array
    {
        return $this->joinColumnAliases;
    }

    /**
     * Returns an array with:
     *   - Table name as primary key,
     *   - An array of hidden columns as value.
     *
     * @return array<string, array<string, true>>
     */
    public function getHiddenColumns(): array
    {
        return $this->hiddenColumns ?? [];
    }

    /**
     * Returns an array with:
     *   - Table name as primary key,
     *   - An array of list hidden columns as value.
     *
     * @return array<string, array<string, true>>
     */
    public function getListHiddenColumns(): array
    {
        return $this->listHiddenColumns ?? [];
    }

    /**
     * Returns an array with:
     *   - Table name as primary key,
     *   - A column name as secondary key,
     *   - A table column definition as value.
     *
     * @return array<string, array<string, array{
     *             column: ?string,
     *             format: ?string,
     *             title: ?string,
     *         }>>
     */
    public function getListColumnsByColumn(): array
    {
        return $this->listColumnsByColumn ?? [];
    }

    /**
     * Project-level default quick-find options definitions.
     *
     * @return Type_UserConfig_QuickFindOptions|null
     */
    public function getDefaultQuickFindOptionsDefinitions(): ?array
    {
        return $this->config[K::TABLE_DEFAULTS][K::TABLE__QUICK_FIND_OPTIONS] ?? null;
    }

    public function getListMaxResults(): int
    {
        return $this->config[K::LIST_MAX_RESULTS];
    }

    /**
     * Returns a 2-level array with:
     *  - Table name as primary key,
     *  - Column name as secondary key,
     *  - Serialized transformer chain as value ({@see TransformerChain}).
     *
     * @return array<string, array<string, string>>
     */
    public function getColumnsTransformerChains(): array
    {
        return $this->columnsTransformerChains;
    }

    /**
     * Returns a 3-level array with:
     *  - Table name as primary key,
     *  - Column name as secondary key,
     *  - Array of possible values for this column (useful for text completion).
     *
     * @return array<string, array<string, list<string|int>>>
     */
    public function getColumnsPossibleValues(): array
    {
        return $this->columnsPossibleValues;
    }
}
