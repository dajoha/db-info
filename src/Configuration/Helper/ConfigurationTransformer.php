<?php

declare(strict_types=1);

namespace App\Configuration\Helper;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\Exception\MultipleColumnAliasDefinitionException;
use App\Configuration\Exception\MultipleTableAliasDefinitionException;
use App\Configuration\Exception\TransformerChainParseException;
use App\Configuration\Service\ConfigProvider;
use App\Frontend\ValueTransformer\Parser\TransformerChainParser;
use App\Frontend\ValueTransformer\Transformer\Base\TransformerInterface;
use App\Frontend\ValueTransformer\TransformerChain;
use Throwable;

/**
 * After being processed by {@see ConfigurationPreProcessor}, transforms the user config into data
 * ready to use for the php application.
 *
 * The final output provided by this class is intended to be passed to {@see ConfigProvider}.
 *
 * @phpstan-import-type Type_UserConfig_Root from ConfigurationSchema
 * @phpstan-import-type Type_UserConfig_Project from ConfigurationSchema
 */
class ConfigurationTransformer
{
    /**
     * @param Type_UserConfig_Root $config
     * @param array<string, class-string<TransformerInterface>> $transformerClasses
     */
    public function __construct(
        protected array $config,
        protected array $transformerClasses,
    ) {
    }

    /**
     * Returns the final config data to provide to {@see ConfigProvider}.
     *
     * @return array<string, mixed>
     *
     * @throws TransformerChainParseException
     * @throws MultipleTableAliasDefinitionException
     * @throws MultipleColumnAliasDefinitionException
     */
    public function getTransformedConfig(): array
    {
        // Value transformer: get the transformer chain parser. Services are not accessible from here,
        // so we create it by hand:
        $transformerChainParser = new TransformerChainParser($this->transformerClasses);

        $configProviderProjectConfigs = [];
        foreach ($this->config[K::ROOT][K::PROJECTS] as $projectName => $projectConfig) {
            $configProviderProjectConfigs[$projectName] = [
                'config' => $projectConfig,
                'tableNamesByAlias' => $this->getTableNamesByAlias($projectConfig),
                'tableBestAliases' => $this->getTableBestAliases($projectConfig),
                'columnAliases' => $this->getColumnAliases($projectConfig, K::TABLE__COLUMN_ALIASES),
                'columnBestAliases' => $this->getColumnBestAliases($projectConfig),
                'joinColumnAliases' => $this->getColumnAliases($projectConfig, K::TABLE__JOIN_COLUMN_ALIASES),
                'hiddenColumns' => $this->getHiddenColumns($projectConfig),
                'listHiddenColumns' => $this->getListHiddenColumns($projectConfig),
                'listColumnsByColumn' => $this->getListColumnsByColumn($projectConfig),
                'columnsTransformerChains' => $this->getColumnsTransformerChains(
                    $projectName,
                    $projectConfig,
                    $transformerChainParser,
                ),
                'columnsPossibleValues' => $this->getColumnsPossibleValues($projectConfig),
            ];
        }

        return [
            K::TABLE_DEFAULTS => $this->config[K::ROOT][K::TABLE_DEFAULTS],
            K::PROJECTS => $configProviderProjectConfigs,
        ];
    }

    /**
     * Creates an array with:
     *  - Table alias as key,
     *  - Real table name as value.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, string>
     *
     * @throws MultipleTableAliasDefinitionException
     */
    protected function getTableNamesByAlias(array $projectConfig): array
    {
        $allTablesAliases = [];

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            foreach ($tableDef[K::TABLE__ALIASES] ?? [] as $tableAlias) {
                if (key_exists($tableAlias, $allTablesAliases)) {
                    throw new MultipleTableAliasDefinitionException(
                        $tableAlias,
                        [
                            $tableName,
                            $allTablesAliases[$tableAlias],
                        ],
                    );
                }
                $allTablesAliases[$tableAlias] = $tableName;
            }
        }

        return $allTablesAliases;
    }

    /**
     * Creates an array with:
     *  - Table aliases as key,
     *  - The "best" alias as value, i.e. the first defined alias or the table name if there is no
     *    table alias.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, string>
     */
    protected function getTableBestAliases(array $projectConfig): array
    {
        $allBestAliases = [];

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            $bestAlias = $tableDef[K::TABLE__ALIASES][0] ?? null;
            if ($bestAlias !== null) {
                $allBestAliases[$tableName] = $bestAlias;
            }
        }

        return $allBestAliases;
    }

    /**
     * Creates a 2-level array with:
     *  - Table name as primary key,
     *  - Column alias as secondary key,
     *  - Real column name as value.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     * @phpstan-param 'column_aliases'|'join_column_aliases' $tableConfigKey
     *
     * @return array<string, array<string, string>>
     *
     * @throws MultipleColumnAliasDefinitionException
     */
    protected function getColumnAliases(array $projectConfig, string $tableConfigKey): array
    {
        $allColumnAliases = [];

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            $columnAliasesDefs = $tableDef[$tableConfigKey];
            $tableColumnAliases = &$allColumnAliases[$tableName];

            foreach ($columnAliasesDefs as $columnName => $aliases) {
                foreach ($aliases as $alias) {
                    if (is_array($tableColumnAliases) && key_exists($alias, $tableColumnAliases)) {
                        // Error when the alias is already defined for one column in the table:
                        $columnKind = match ($tableConfigKey) {
                            K::TABLE__COLUMN_ALIASES => 'column',
                            K::TABLE__JOIN_COLUMN_ALIASES => 'join-column',
                        };

                        throw new MultipleColumnAliasDefinitionException(
                            $columnKind,
                            $alias,
                            $tableName,
                            [
                                $columnName,
                                $tableColumnAliases[$alias],
                            ],
                        );
                    }
                    $tableColumnAliases[$alias] = $columnName;
                }
            }
        }

        return $allColumnAliases;
    }

    /**
     * Returns a 2-level array with:
     *  - Table name as primary key,
     *  - Column name as secondary key,
     *  - The best column alias as value.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, array<string, string>>
     */
    protected function getColumnBestAliases(array $projectConfig): array
    {
        $allBestAliases = [];

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            foreach ($tableDef[K::TABLE__COLUMN_ALIASES] as $columnName => $aliases) {
                $bestAlias = $aliases[0] ?? null;
                if ($bestAlias !== null) {
                    $allBestAliases[$tableName][$columnName] = $bestAlias;
                }
            }
        }

        return $allBestAliases;
    }

    /**
     * Returns an array with:
     *   - Table name as primary key,
     *   - An array of hidden columns as value.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, array<string, true>>
     *
     * @throws MultipleColumnAliasDefinitionException
     */
    protected function getHiddenColumns(array $projectConfig): array
    {
        return $this->getColumnsInverseArray($projectConfig, K::TABLE__HIDDEN_COLUMNS);
    }

    /**
     * Returns an array with:
     *   - Table name as primary key,
     *   - An array of list hidden columns as value.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, array<string, true>>
     *
     * @throws MultipleColumnAliasDefinitionException
     */
    protected function getListHiddenColumns(array $projectConfig): array
    {
        $hiddenColumns = $this->getHiddenColumns($projectConfig);
        $columnAliases = $this->getColumnAliases($projectConfig, K::TABLE__COLUMN_ALIASES);
        $listHiddenColumns = [];

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            $tableHiddenColumns = $hiddenColumns[$tableName] ?? [];
            $tableColumnAliases = $columnAliases[$tableName] ?? [];
            $tableListHiddenColumns = $tableDef[K::TABLE__LIST_COLUMNS][K::TABLE__LIST_COLUMNS__HIDE] ?? [];
            // Resolve aliases:
            $tableListHiddenColumns = iter($tableListHiddenColumns)
                ->map(fn(string $columnAlias) => $tableColumnAliases[$columnAlias] ?? $columnAlias)
                ->toValues()
            ;
            $tableListHiddenColumns = array_fill_keys($tableListHiddenColumns, true);
            // Merge with hidden columns of the table root config:
            $tableListHiddenColumns = array_merge($tableListHiddenColumns, $tableHiddenColumns);
            $listHiddenColumns[$tableName] = $tableListHiddenColumns;
        }

        return $listHiddenColumns;
    }

    /**
     * Returns an array with:
     *   - Table name as primary key,
     *   - A column name as secondary key,
     *   - A table column definition as value.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, array<string, array{
     *             column: ?string,
     *             format: ?string,
     *             title: ?string,
     *         }>>
     *
     * @throws MultipleColumnAliasDefinitionException
     */
    protected function getListColumnsByColumn(array $projectConfig): array
    {
        $listColumnsByColumn = [];
        $columnAliases = $this->getColumnAliases($projectConfig, K::TABLE__COLUMN_ALIASES);

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            $tableListColumnsDefs = $tableDef[K::TABLE__LIST_COLUMNS][K::TABLE__LIST_COLUMNS__COLUMNS] ?? [];
            $tableColumnAliases = $columnAliases[$tableName] ?? [];

            foreach ($tableListColumnsDefs as $tableListColumnDef) {
                $columnAlias = $tableListColumnDef[K::TABLE__LIST_COLUMNS__COLUMNS__COLUMN] ?? null;
                if ($columnAlias === null) {
                    continue;
                }
                $columnName = $tableColumnAliases[$columnAlias] ?? $columnAlias;
                $listColumnsByColumn[$tableName][$columnName] = $tableListColumnDef;
            }
        }

        return $listColumnsByColumn;
    }

    /**
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, array<string, true>>
     *
     * @throws MultipleColumnAliasDefinitionException
     */
    protected function getColumnsInverseArray(array $projectConfig, string $tableConfigKey): array
    {
        $hiddenColumns = [];
        $columnAliases = $this->getColumnAliases($projectConfig, K::TABLE__COLUMN_ALIASES);

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            $tableColumnAliases = $columnAliases[$tableName] ?? [];
            /** @var mixed[] $columnNames */
            $columnNames = $tableDef[$tableConfigKey];
            $columnNames = iter($columnNames)
                ->map(fn(string $columnAlias) => $tableColumnAliases[$columnAlias] ?? $columnAlias)
                ->toValues()
            ;
            $hiddenColumns[$tableName] = array_fill_keys($columnNames, true);
        }

        return $hiddenColumns;
    }

    /**
     * Returns an array with:
     *   - Table name as primary key,
     *   - A column name as secondary key,
     *   - A serialized version of {@see TransformerChain}.
     *
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, array<string, string>>
     *
     * @throws TransformerChainParseException
     */
    protected function getColumnsTransformerChains(
        string $projectName,
        array $projectConfig,
        TransformerChainParser $transformerChainParser,
    ): array
    {
        $transformers = [];

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            $tableColumnsDefs = $tableDef[K::TABLE__COLUMNS];
            foreach ($tableColumnsDefs as $columnName => $columnDef) {
                $transformExpression = $columnDef[K::TABLE__COLUMNS__TRANSFORM] ?? null;
                if ($transformExpression === null) {
                    continue;
                }

                try {
                    $parseResult = $transformerChainParser->parse($transformExpression);
                } catch (Throwable $e) {
                    throw new TransformerChainParseException(
                        $projectName,
                        $tableName,
                        $columnName,
                        0,
                        $e,
                    );
                }
                /** @var TransformerChain $transformerChain */
                $transformerChain = $parseResult->output;

                $transformers[$tableName][$columnName] = serialize($transformerChain);
            }
        }

        return $transformers;
    }

    /**
     * @phpstan-param Type_UserConfig_Project $projectConfig
     *
     * @return array<string, array<string, mixed[]>>
     */
    protected function getColumnsPossibleValues(array $projectConfig): array
    {
        $allPossibleValues = [];

        foreach ($projectConfig[K::PROJECT__TABLES] as $tableName => $tableDef) {
            $tableColumnsDefs = $tableDef[K::TABLE__COLUMNS];
            foreach ($tableColumnsDefs as $columnName => $columnDef) {
                $possibleValues = $columnDef[K::TABLE__COLUMNS__POSSIBLE_VALUES] ?? null;
                if ($possibleValues === null) {
                    continue;
                }

                $allPossibleValues[$tableName][$columnName] = $possibleValues;
            }
        }

        return $allPossibleValues;
    }
}
