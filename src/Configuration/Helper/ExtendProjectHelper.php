<?php

declare(strict_types=1);

namespace App\Configuration\Helper;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Domain\Merger\Exception\MergerException;
use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\ArrayRecursiveMerger;
use App\Domain\Merger\Mergers\DynamicMerger\DynamicMerger;
use App\Domain\Merger\Mergers\ReplaceMerger;
use Symfony\Component\Config\Definition\Exception\Exception as ConfigurationException;

/**
 * @phpstan-import-type Type_UserConfig_Project from ConfigurationSchema
 */
readonly class ExtendProjectHelper
{
    public function __construct(
        /** @var array<string, mixed> */
        protected array $config,
    ) {
    }

    /**
     * Merge possible extends with the final project config, for a given project.
     *
     * @return Type_UserConfig_Project
     *
     * @throws MergerException
     */
    public function getExtendedProjectConfig(string $baseProjectName): array
    {
        return $this->_getExtendedProjectConfig($baseProjectName, []);
    }

    /**
     * @param string[] $parentExtends Used to prevent infinite recursion: stores all extend project
     *                                names already encountered
     *
     * @return Type_UserConfig_Project
     *
     * @throws MergerException
     */
    protected function _getExtendedProjectConfig(string $baseProjectName, array $parentExtends): array
    {
        // Prevent infinite recursion:
        if (in_array($baseProjectName, $parentExtends)) {
            $message =
                "Infinite recursion detected: project ".
                iter($parentExtends)->map(fn($projectName) => "\"$projectName\" extends ")->join().
                "\"$baseProjectName\""
            ;
            throw new ConfigurationException($message);
        }
        $parentExtends[] = $baseProjectName;

        $baseProjectConfig = $this->getProjectConfig($baseProjectName);
        $merger = self::getMerger();

        // Merge all extend configs together, recursively:
        $extendConfig = iter($baseProjectConfig[K::PROJECT__EXTENDS])
            ->map(fn(string $projectName) => $this->_getExtendedProjectConfig($projectName, $parentExtends))
            ->reduce($merger->merge(...), [])
        ;

        // Finally merge the base project config:
        /** @var Type_UserConfig_Project */
        return $merger->merge($extendConfig, $baseProjectConfig);
    }

    /**
     * Get a project configuration, or throw an exception if not found.
     *
     * @return Type_UserConfig_Project
     */
    protected function getProjectConfig(string $projectName): array
    {
        $projectConfig = $this->config[K::ROOT][K::PROJECTS][$projectName] ?? null;

        if ($projectConfig === null) {
            throw new ConfigurationException("Project not found: \"$projectName\"");
        }

        return $projectConfig;
    }

    protected static function getMerger(): Merger
    {
        static $merger = null;

        $merger ??= new Merger([
            new DynamicMerger(),
            new ArrayRecursiveMerger(),
            new ReplaceMerger(),
        ]);

        return $merger;
    }
}
