<?php

declare(strict_types=1);

namespace App\Configuration\Helper;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Configuration\Definition\ConfigurationSchema;
use App\Configuration\Model\FilesTimestampsBag;
use App\Domain\Merger\Exception\MergerException;
use App\Exception\InternalException;
use App\Helper\CircularReference\CircularReferenceBlocker;
use App\Helper\CircularReference\CircularReferenceException;
use App\Util\ArrayUtil;
use Dajoha\Schema\Exception\ValidationException;
use RuntimeException;
use Symfony\Component\Yaml\Yaml;

/**
 * Pre-processes the main configuration file, see {@see self::preProcessConfig()} for details.
 *
 * @phpstan-import-type Type_UserConfig_Root from ConfigurationSchema
 */
class ConfigurationPreProcessor
{
    public function __construct(protected string $mainConfigFile)
    {
    }

    /**
     * Performs all the passes needed to transform a user config file (with possible imports) into
     * a fully processed, validated configuration.
     *
     * Major passes are (in order of execution):
     *  - Retrieving the main config file data;
     *  - Importing (aka. merging) the files provided in the root key "import", recursively;
     *  - For each project, merging extension configs provided by the "extends" key;
     *  - Normalizing the resulting config (e.g., transform a string into an single-element array
     *    when a list is expected);
     *  - Validating the config (e.g., will throw an error if an array is provided when a string
     *    was expected);
     *
     * @return Type_UserConfig_Root
     *
     * @throws ValidationException
     * @throws CircularReferenceException
     * @throws MergerException
     * @throws InternalException
     *
     * @noinspection PhpUnnecessaryLocalVariableInspection
     */
    public function preProcessConfig(?FilesTimestampsBag $filesTimestampsBag = null): array
    {
        // Get data from the main config file:
        $config = $this->getUnprocessedConfig($filesTimestampsBag);

        // Merge configs from imported files:
        $config = $this->resolveImports($config, $filesTimestampsBag);

        // Make a first basic validation/process pass, because $this->resolveExtends() needs to
        // access the "extends" key and "abstract" key of each project:
        $config = $this->processFirstPassSchema($config);

        // Merge configs for projects extending other projects:
        $config = $this->resolveExtends($config);

        // Process the the whole config (normalization, validation and transformation):
        $config = $this->processMainSchema($config);

        return $config;
    }

    /**
     * Retrieves the data from the main configuration file, without any processing.
     *
     * @return array<mixed, mixed>
     * @throws InternalException
     */
    protected function getUnprocessedConfig(?FilesTimestampsBag $filesTimestampsBag = null): array
    {
        $filesTimestampsBag?->addFile($this->mainConfigFile);

        return Yaml::parseFile($this->mainConfigFile);
    }

    /**
     * Merges the configs found in the "import" section.
     *
     * @param array<mixed, mixed> $config
     *
     * @return Type_UserConfig_Root
     *
     * @throws CircularReferenceException
     * @throws InternalException
     */
    protected function resolveImports(
        array $config,
        ?FilesTimestampsBag $filesTimestampsBag = null,
        ?string $sourceFile = null,
        ?CircularReferenceBlocker $circularReferenceBlocker = null,
    ): array
    {
        $sourceFile ??= $this->mainConfigFile;
        $circularReferenceBlocker ??= new CircularReferenceBlocker('Importing configuration files');

        $imports = (array) ($config['import'] ?? []);
        $sourceDirectory = dirname($sourceFile);

        foreach ($imports as $importFile) {
            if (!is_string($importFile)) {
                throw new RuntimeException("Bad import: must be string or array of strings");
            }

            if (!str_starts_with($importFile, '/')) {
                $importFile = "$sourceDirectory/$importFile";
            }

            $sourceFileRealpath = realpath($importFile);
            if ($sourceFileRealpath === false || !file_exists($sourceFile)) {
                throw new RuntimeException("Bad import: imported file does not exist: $sourceFile");
            }

            // Prevent circular references:
            $circularReferenceBlocker->addReferenceOrThrow($sourceFileRealpath);

            $importedConfig = Yaml::parseFile($importFile);
            if (!is_array($importedConfig)) {
                throw new RuntimeException("Bad import: yaml content must be an array in \"$importFile\"");
            }

            // Make the config reprocess automatically when the import file has changed:
            $filesTimestampsBag?->addFile($sourceFileRealpath);

            // Recursion:
            $importedConfig = $this->resolveImports(
                $importedConfig,
                $filesTimestampsBag,
                $importFile,
                $circularReferenceBlocker,
            );

            $config = ArrayUtil::recursiveMerge($config, $importedConfig);
        }

        unset($config['import']);

        /** @var Type_UserConfig_Root */
        return $config;
    }

    /**
     * @param Type_UserConfig_Root $config
     *
     * @return Type_UserConfig_Root
     *
     * @throws ValidationException
     */
    protected function processFirstPassSchema(array $config): array
    {
        return ConfigurationSchema::getFirstPassSchema()->process($config);
    }

    /**
     * For each project, merges the config of each (potentially abstract) project provided in the
     * "extends" key of the project config, recursively. This is the pass where abstract projects
     * are removed from the list of projects, after being potentially used as extensions by
     * non-abstract projects.
     *
     * @param Type_UserConfig_Root $config
     *
     * @return Type_UserConfig_Root
     *
     * @throws MergerException
     */
    protected function resolveExtends(array $config): array
    {
        $extendProjectHelper = new ExtendProjectHelper($config);

        $extendedProjectConfigs = [];
        foreach ($config[K::ROOT][K::PROJECTS] as $projectName => $projectConfig) {
            // Don't register abstract projects (only used when extending concrete projects):
            if ($projectConfig[K::PROJECT__ABSTRACT]) {
                continue;
            }

            // Merge possible extends with the final project config:
            $projectConfig = $extendProjectHelper->getExtendedProjectConfig($projectName);

            $extendedProjectConfigs[$projectName] = $projectConfig;
        }

        $config[K::ROOT][K::PROJECTS] = $extendedProjectConfigs;

        return $config;
    }

    /**
     * @param Type_UserConfig_Root $config
     *
     * @return Type_UserConfig_Root
     *
     * @throws ValidationException
     */
    protected function processMainSchema(array $config): array
    {
        return ConfigurationSchema::getSchema()->process($config);
    }
}
