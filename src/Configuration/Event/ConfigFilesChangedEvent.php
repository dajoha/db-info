<?php

declare(strict_types=1);

namespace App\Configuration\Event;

/**
 * Dispatched when user configuration files have been changed. Used to invalidate config cache.
 */
class ConfigFilesChangedEvent
{
}
