<?php

declare(strict_types=1);

namespace App\Configuration\Exception;

use App\Exception\AppException;
use Throwable;

class MultipleTableAliasDefinitionException extends AppException
{
    /**
     * @param iterable<string> $tableNames
     */
    public function __construct(
        public readonly string $alias,
        public readonly iterable $tableNames,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $tableNames = iter($tableNames)
            ->map(fn($name) => "\"$name\"")
            ->join(', ');
        $message = "The same table alias \"$alias\" has been defined for tables $tableNames";
        parent::__construct($message, $code, $previous);
    }
}
