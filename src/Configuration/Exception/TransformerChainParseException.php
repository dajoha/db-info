<?php

declare(strict_types=1);

namespace App\Configuration\Exception;

use App\Configuration\Definition\ConfigurationKeys as K;
use App\Exception\AppException;
use Throwable;

class TransformerChainParseException extends AppException
{
    public function __construct(
        public readonly string $projectName,
        public readonly string $tableName,
        public readonly string $columnName,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $message =
            "Project '$projectName', table '$tableName', column '$columnName': ".
            "Error while parsing the '".K::TABLE__COLUMNS__TRANSFORM."' property"
        ;
        parent::__construct($message, $code, $previous);
    }
}
