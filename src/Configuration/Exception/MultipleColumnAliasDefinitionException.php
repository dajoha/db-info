<?php

declare(strict_types=1);

namespace App\Configuration\Exception;

use App\Exception\AppException;
use Throwable;

class MultipleColumnAliasDefinitionException extends AppException
{
    /**
     * @param iterable<string> $columnNames
     */
    public function __construct(
        public readonly string $columnKind,
        public readonly string $alias,
        public readonly string $tableName,
        public readonly iterable $columnNames,
        int $code = 0,
        ?Throwable $previous = null,
    ) {
        $columnNames = iter($columnNames)
            ->map(fn($name) => "\"$name\"")
            ->join(', ');
        $message = "The same $this->columnKind alias \"$alias\" has been defined in table \"$tableName\" for columns $columnNames";
        parent::__construct($message, $code, $previous);
    }
}
