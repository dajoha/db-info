<?php

declare(strict_types=1);

namespace App\Configuration\Model;

use App\Exception\InternalException;

/**
 * Used to store the last modification dates of a group of files, then check later if one of the
 * files have been changed.
 */
class FilesTimestampsBag
{
    /**
     * @var array<string, int>
     */
    protected array $files = [];

    public function __construct()
    {
    }

    /**
     * @throws InternalException
     */
    public function addFile(string $file): static
    {
        $mtime = filemtime($file);
        if ($mtime === false) {
            throw new InternalException("Cannot stat file \"$file\"");
        }

        $this->files[$file] = $mtime;

        return $this;
    }

    public function anyFileHasChanged(): bool
    {
        foreach ($this->files as $file => $oldMtime) {
            $currentMtime = filemtime($file);
            if ($currentMtime === false || $currentMtime > $oldMtime) {
                return true;
            }
        }

        return false;
    }
}
