<?php

declare(strict_types=1);

namespace App\Configuration\Model;

use App\Repository\QueryPart\Sort\OptSortDirection;

enum SortDirection: string
{
    case Asc = 'asc';
    case Desc = 'desc';

    public function toOptSortDirection(): OptSortDirection
    {
        return match ($this) {
            self::Asc => OptSortDirection::Asc,
            self::Desc => OptSortDirection::Desc,
        };
    }
}
