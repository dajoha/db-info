<?php

declare(strict_types=1);

namespace App\Doctrine;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use SensitiveParameter;

/**
 * Used to create a DBAL connection:
 *  - In prod environment, will use {@see ConnectionFactory};
 *  - In dev environment, will use {@see DevConnectionFactory} in order to have debug information in
 *    the Symfony profiler.
 *
 * @psalm-import-type Params from DriverManager
 */
interface ConnectionFactoryInterface
{
    /**
     * @param Params $params
     */
    public function getConnection(
        #[SensitiveParameter]
        array $params,
        ?Configuration $configuration = null,
    ): Connection;
}
