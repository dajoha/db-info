<?php

declare(strict_types=1);

namespace App\Doctrine;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Configuration as DoctrineConfiguration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use SensitiveParameter;
use Symfony\Bridge\Doctrine\Middleware\Debug\DebugDataHolder;
use Symfony\Bridge\Doctrine\Middleware\Debug\Middleware;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\When;
use Symfony\Component\Stopwatch\Stopwatch;

#[When('dev')]
readonly class DevConnectionFactory implements ConnectionFactoryInterface
{
    public function __construct(
        #[Autowire(service: 'doctrine.debug_data_holder')]
        protected DebugDataHolder $debugDataHolder,
        protected Stopwatch $stopwatch,
    ) {
    }

    public function getConnection(
        #[SensitiveParameter]
        array $params,
        ?Configuration $configuration = null,
    ): Connection
    {
        $configuration ??= new DoctrineConfiguration();
        $configuration->setMiddlewares([new Middleware($this->debugDataHolder, $this->stopwatch)]);

        return DriverManager::getConnection($params, $configuration);
    }
}
