<?php

declare(strict_types=1);

namespace App\Doctrine;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use SensitiveParameter;
use Symfony\Component\DependencyInjection\Attribute\WhenNot;

/**
 * See {@see ConnectionFactoryInterface} for information.
 *
 * @psalm-import-type Params from DriverManager
 */
#[WhenNot('dev')]
class ConnectionFactory implements ConnectionFactoryInterface
{
    /**
     * @param Params $params
     */
    public function getConnection(
        #[SensitiveParameter]
        array $params,
        ?Configuration $configuration = null,
    ): Connection
    {
        return DriverManager::getConnection($params, $configuration);
    }
}
