# Db info

A web interface to browse, view and search a database quickly, mainly intended for developers.

## Installation

```
$ cd .docker
$ docker compose up -d
$ docker compose exec php composer install
$ docker compose exec php yarn
$ docker compose exec php yarn dev
```

Adapt the database URL to your needs:

```
$ echo 'DATABASE_URL=mysql://root:root@127.0.0.1:3306/my_db' > .env.local
```

The website should be available on this URL: http://127.0.0.1:9000

## Run php tests

```
$ cd .docker
$ docker compose exec php vendor/bin/phpunit
```
