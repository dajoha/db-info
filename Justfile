set quiet
set positional-arguments
set ignore-comments

alias t := test

# Warning: The following directory will be deleted when running coverage:
coverage_html_dir := "var/coverage-html"

docker_env_file := ".docker/.env"

# Run the test suite
[no-exit-message]
test *phpunit_args:
    ./run/docker-run vendor/bin/phpunit "$@"

phpstan:
    cp --update=none phpstan.neon.dist phpstan.neon
    ./run/docker-run vendor/bin/phpstan --memory-limit=1024M analyse -v

# Generate HTML code coverage page
[no-exit-message]
coverage *phpunit_args:
    rm -rf {{coverage_html_dir}}
    ./run/docker-run-with-compose-args \
        -e XDEBUG_MODE=coverage \
        -- \
        vendor/bin/phpunit --coverage-html={{coverage_html_dir}} "$@"; \
        true
    command -v miniserve >/dev/null && miniserve --index=index.html {{coverage_html_dir}}

coverage-serve:
    command -v miniserve >/dev/null && miniserve --index=index.html {{coverage_html_dir}}

dev-server *ARGS:
    ./run/yarn dev-server "$@"

docker-build:
    #!/bin/bash
    set -e
    docker build \
        -t db-info-build \
        -f .docker/build/Dockerfile \
        --build-arg PHP_SYNTECT_VERSION="$(grep ^PHP_SYNTECT_VERSION= {{docker_env_file}} | cut -d= -f2)" \
        .
    docker images db-info-build:latest --format "{{{{.Repository}}:{{{{.Tag}} -> {{{{.Size}}"
