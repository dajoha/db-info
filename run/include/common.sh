project_dir=$(dirname "$script_dir")
docker_dir="$project_dir/.docker"

docker_compose() {
    local f
    local config_files=(
        "$docker_dir/docker-compose.yml"
        "$docker_dir/docker-compose.override.yml"
    )
    local com=( docker compose )
    for f in "${config_files[@]}"; do
        [[ -f "$f" ]] && com+=( -f "$f" )
    done
    "${com[@]}" "$@"
}

docker_run() {
    docker_compose exec db-info-php "$@"
}

docker_run_with_compose_args() {
    local arg is_com= compose_args=() com=()

    for arg in "$@"; do
        if [[ -z $is_com && $arg == '--' ]]; then
            is_com=1
            continue
        fi
        if [[ -n $is_com ]]; then
            com+=( "$arg" )
        else
            compose_args+=( "$arg" )
        fi
    done

    docker_compose exec "${compose_args[@]}" db-info-php "${com[@]}"
}
