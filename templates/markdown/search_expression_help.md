# Search expression help

## Basic example

```
user.address.city = 'Paris' & user.age > 30 
```

## Operators

### Arithmetic

- `*` : multiplication
- `/` : division
- `+` : addition
- `-` : subtraction
- `()` : override precedence with parentheses

### Comparison

- `=` or `==` : equality
- `!=` : inequality
- `>=` : greater or equal
- `<=` : less or equal
- `>` : less
- `<` : greater

### Logical

- `&`, `&&` or `AND` : boolean AND
- `|`, `||` or `OR` : boolean OR

### Tests

- `... IN [val1, val2, ...]` : SQL native `IN` statement (*NOTE*: square brackets are used instead of parentheses);
- `... LIKE '...'` : SQL native `LIKE` statement
- `... ILIKE '...'` : SQL native `ILIKE` statement
- `... ~ ...` : smart shortcut for `... ILIKE '%...%'` with the following differences:  
  * escape the potential wild-card characters `%` and `_`
  * as rhs, this operator accepts strings, numbers and even unquoted identifiers, e.g.
    `name~albert` means `name ILIKE '%albert%'`

## Literal values

### Numbers

Numbers can have a decimal part.  
**Examples**: `32`, `0`, `5.5`.

### Strings

For now, strings are delimited by single quotes. Escaping a single quote can be done with `\'`.  
**Examples**: `'hi'`, `'Escaped \'single\' quotes'`  
*TODO*: add support for special characters like `\n`, `\r`, `\t`, eventually utf-8 chars with
something like `\u{2764}\u{FE0E}`

### Collections

Actually only used with the operator `IN`; a list of values separated by optional commas, delimited
by square brackets.  
**Examples**: `[12, 45]`, `[12 45]`, `['foo', 'bar']`, `['foo' 'bar']`

## Targeting fields

Unlike classical SQL syntax, fields contained in a foreign table can be accessed directly, by
using a dot-separated syntax. Internally, the needed `JOIN` statements are added automatically.

For example, if:
* the table `blog_table` has a FK field `user_id`, which points to the table `user_table`;
* the table `user_table` has a FK field `address_id`, which points to the table `address_table`;
* and the table `address_table` table has a string field `city`;

, then blogs can be filtered by user's city in the following way:

```
user_id.address_id.city = 'New Delhi'
```

#### Using join-column aliases instead of real FK column names

Additionally, with the same example, if the following join-column aliases have been defined:
* `blog_table.user_id` => alias `user`;
* `user_table.address_id` => alias `address`,

Then the expression can be made even simpler:

```
user.address.city = 'New Delhi'
```
