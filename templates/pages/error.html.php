<?php
// A very basic HTML error page, to display configuration errors for example.
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex,nofollow,noarchive" />
    <title>Db info - Error</title>
    <style>
        body {
            margin: 1em;
            font-size: 20px;
            color: ivory;
            background-color: #111;
            font-family: monospace;
        }

        h1 {
            font-size: 1.5em;
        }

        .error {
            color: #dd3a3a;
        }
    </style>
</head>
<body>
<div class="container">
    <h1 class="error">Db info - Error</h1>
    <p><?= htmlspecialchars($errorMessage) ?></p>
</div>
</body>
</html>
