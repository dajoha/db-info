Db-info changelog
=================

## master

#### Features

* Add value transformer `bin_to_uuid`.
* Improve highlight transformer: use Rust Syntect by default (much more quicker).
* Search expression: Add operator `IS`, `IS NOT`.
* Search expression: Add values `NULL`, `TRUE`, `FALSE`, `UNKNOWN`.

#### Internal

* Upgrade to Symfony 7.2.2.
* Add command `db-info:value-transformer:list`.
* Config: Use APCu instead of Symfony container for caching.

## v0.1.5 - 2024-12-14

#### Features

* Frontend: Add a new page which shows the structure of a table (fields, foreign keys, indexes...).
* Frontend: Add a button in navbar to clear the application's cache (3 levels: Table, Project, All).
* Frontend: Display null values with a dimmed "null" string.
* Configuration: Can define a list of possible values for a column (useful for text completion).
* Configuration: When extending a project from a base project, can use special keywords in yaml
  files to interfer with the merging strategy (`+prepend`, `+append` for lists, `+replace` for
  dicts...).
* Search expressions: Add function calls (forwarded to DB engine).

#### Bugfixes

* Prevent circular configuration file import.
* Handle binary types (convert to hexadecimal representation for display).

#### Internal

* Add phpstan (level = 8).
* Dev: Add XDebug + Code coverage.
* Use APCu cache.


## v0.1.4 - 2024-10-18

Minor version:

* Improve error handling.
* Create a decent bundled docker image.
* Internal code improvements.


## v0.1.3 - 2024-10-01

* Add the command `db-info:config:example` which dumps an auto-generated configuration schema
  example.
* Can define a default column order configuration for each table.
* Improve navigation bar.
* Frontend: Add global keyboard shortcuts ("/", "#", "@" and "r").
* Can view table listings in a transposed way (each row is displayed as a block, with each column
  disposed vertically).
* Improve error handling (display errors in a modal window when fetching data).
* Config: Can set default pagination size for table, project or globally.
* Add "first row" page: view the 1st result accordingly to filter expression and sort.


## v0.1.2 - 2024-02-16

* Frontend: Sort buttons:
    - Replace old Shift-click by Control-click (multiple sorts).
    - Shift-click (and Control-Shift-click) now reverse toggle direction.
* Frontend: Table listings: Add "Reload" button.
* Feature: Add "quick find" zone in page headers.


## v0.1.1 - 2024-02-06

* Configuration: move db-info config from Symfony config system to custom system: the only
                 remaining Symfony config is one scalar which defines the main db-info config file
                 (defined in `/config/db_info.yaml`).
* Configuration: can import other config files (relative paths allowed).
* Configuration: can extend a project's config from another one, by using config key `extends`.

#### Internal

* Configuration: custom schema validation system (don't use Symfony's `TreeBuilder` anymore).


## v0.1.0

* Add value transformers.
* Start CHANGELOG.md.
