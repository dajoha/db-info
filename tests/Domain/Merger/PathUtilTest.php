<?php

declare(strict_types=1);

namespace App\Tests\Domain\Merger;

use App\Domain\Merger\Util\PathUtil;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class PathUtilTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testPathUtil(array $path, string $expectedOutput): void
    {
        $output = PathUtil::toString($path);

        $this->assertSame($expectedOutput, $output);
    }

    public static function dataProvider(): array
    {
        return [
            [
                [],
                '(root)'
            ],
            [
                ['foo'],
                '(root).foo'
            ],
            [
                ['one', 'two', 'three'],
                '(root).one.two.three'
            ],
            [
                ['one', 4, 'two', 'three', 7],
                '(root).one[4].two.three[7]'
            ],
        ];
    }
}
