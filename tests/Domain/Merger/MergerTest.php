<?php

declare(strict_types=1);

namespace App\Tests\Domain\Merger;

use App\Domain\Merger\Exception\MergerException;
use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\ArrayRecursiveMerger;
use App\Domain\Merger\Mergers\ListAppendMerger;
use App\Domain\Merger\Mergers\ListPrependMerger;
use App\Domain\Merger\Mergers\ListUniqueMerger;
use App\Domain\Merger\Mergers\ReplaceMerger;
use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class MergerTest extends TestCase
{
    /**
     * @throws MergerException
     */
    #[DataProvider('dataProvider')]
    public function testMerger(Merger $merger, mixed $a, mixed $b, mixed $expectedOutput): void
    {
        if ($expectedOutput instanceof Exception) {
            $this->expectException(get_class($expectedOutput));
            $this->expectExceptionMessage($expectedOutput->getMessage());
        }

        $output = $merger->merge($a, $b);

        $this->assertSame($expectedOutput, $output);
    }

    public static function dataProvider(): array
    {
        return [
            [
                new Merger(),
                0,
                0,
                new MergerException('Cannot merge data: no suitable merger was found'),
            ],
            [
                new Merger([new ListAppendMerger()]),
                0,
                0,
                new MergerException('Cannot merge data: no suitable merger was found'),
            ],
            [
                self::getBasicMerger(),
                0,
                0,
                0,
            ],
            [
                self::getBasicMerger(),
                0,
                [45],
                [45],
            ],
            [
                self::getBasicMerger(),
                [45],
                0,
                0,
            ],
            [
                self::getBasicMerger(),
                [13, 14, 15],
                [45],
                [45],
            ],
            [
                self::getBasicMerger(),
                null,
                null,
                null,
            ],
            [
                self::getBasicMerger(),
                null,
                0,
                0,
            ],
            [
                self::getBasicMerger(),
                0,
                null,
                null,
            ],
            [
                self::getBasicMerger(),
                ['foo' => 13],
                ['foo' => 45],
                ['foo' => 45],
            ],
            [
                self::getBasicMerger(),
                ['foo' => 13],
                ['bar' => 45],
                ['foo' => 13, 'bar' => 45],
            ],
            [
                self::getBasicMerger(),
                [
                    'foo' => 13,
                    'bar' => ['one' => 1, 'two' => 2],
                ],
                [
                    'hello' => 45,
                    'bar' => ['two' => 22, 'three' => 3],
                ],
                [
                    'foo' => 13,
                    'bar' => ['one' => 1, 'two' => 22, 'three' => 3],
                    'hello' => 45,
                ],
            ],
            [
                new Merger([new ListAppendMerger()]),
                [13],
                [14],
                [13, 14],
            ],
            [
                new Merger([new ArrayRecursiveMerger(), new ReplaceMerger()]),
                ['foo' => [13]],
                ['foo' => [14]],
                ['foo' => [14]],
            ],
            [
                new Merger([new ArrayRecursiveMerger(), new ListAppendMerger()]),
                [13],
                [14],
                [13, 14],
            ],
            [
                new Merger([new ArrayRecursiveMerger(), new ListAppendMerger()]),
                ['foo' => [13]],
                ['foo' => [14]],
                ['foo' => [13, 14]],
            ],
            [
                new Merger([new ArrayRecursiveMerger(), new ListPrependMerger()]),
                ['foo' => [13]],
                ['foo' => [14]],
                ['foo' => [14, 13]],
            ],
            [
                new Merger([new ArrayRecursiveMerger(), new ListUniqueMerger()]),
                ['foo' => [13, 14, 15]],
                ['foo' => [14, 16]],
                ['foo' => [13, 14, 15, 16]],
            ],
        ];
    }

    public static function getBasicMerger(): Merger
    {
        return new Merger([
           new ArrayRecursiveMerger(),
           new ReplaceMerger(),
        ]);
    }
}
