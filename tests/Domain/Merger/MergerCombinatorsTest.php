<?php

declare(strict_types=1);

namespace App\Tests\Domain\Merger;

use App\Domain\Merger\Combinators\MergeCondition;
use App\Domain\Merger\Combinators\MergePath;
use App\Domain\Merger\Combinators\MergePaths;
use App\Domain\Merger\Exception\MergerException;
use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\ArrayRecursiveMerger;
use App\Domain\Merger\Mergers\ListAppendMerger;
use App\Domain\Merger\Mergers\ListPrependMerger;
use App\Domain\Merger\Mergers\ReplaceMerger;
use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class MergerCombinatorsTest extends TestCase
{
    /**
     * @throws MergerException
     */
    #[DataProvider('dataProvider')]
    public function testMergerCombinators(Merger $merger, mixed $a, mixed $b, mixed $expectedOutput): void
    {
        if ($expectedOutput instanceof Exception) {
            $this->expectException(get_class($expectedOutput));
            $this->expectExceptionMessage($expectedOutput->getMessage());
        }

        $output = $merger->merge($a, $b);

        $this->assertSame($expectedOutput, $output);
    }

    public static function dataProvider(): array
    {
        return [
            [
                new Merger([
                    new ArrayRecursiveMerger(),
                    new MergePath(['foo'], new ListPrependMerger()),
                    new ReplaceMerger(),
                ]),
                [
                    'foo' => [13, 14],
                    'bar' => [13, 14],
                ],
                [
                    'foo' => [15],
                    'bar' => [15],
                ],
                [
                    'foo' => [15, 13, 14],
                    'bar' => [15],
                ],
            ],
            [
                new Merger([
                    new ArrayRecursiveMerger(),
                    new MergePaths([
                        'foo' => new ListPrependMerger(),
                        'a.b' => new ListAppendMerger(),
                    ]),
                    new ReplaceMerger(),
                ]),
                [
                    'foo' => [13, 14],
                    'a' => [
                        'b' => [13, 14],
                    ],
                    'bar' => [13, 14],
                ],
                [
                    'foo' => [15],
                    'a' => [
                        'b' => [15],
                    ],
                    'bar' => [15],
                ],
                [
                    'foo' => [15, 13, 14],
                    'a' => [
                        'b' => [13, 14, 15],
                    ],
                    'bar' => [15],
                ],
            ],
            [
                new Merger([
                    new ArrayRecursiveMerger(),
                    new MergeCondition(
                        fn($a, $b, $path) => count($path) === 3,
                        new ListPrependMerger(),
                    ),
                    new ListAppendMerger(),
                    new ReplaceMerger(),
                ]),
                [
                    'a' => [11],
                    'b' => ['c' => [11]],
                    'd' => ['e' => ['f' => [11]]],
                ],
                [
                    'a' => [222],
                    'b' => ['c' => [222]],
                    'd' => ['e' => ['f' => [222]]],
                ],
                [
                    'a' => [11, 222],
                    'b' => ['c' => [11, 222]],
                    'd' => ['e' => ['f' => [222, 11]]],
                ],
            ],
        ];
    }
}
