<?php

declare(strict_types=1);

namespace App\Tests\Domain\Merger;

use App\Domain\Merger\Exception\MergerException;
use App\Domain\Merger\Merger;
use App\Domain\Merger\Mergers\ArrayRecursiveMerger;
use App\Domain\Merger\Mergers\DynamicMerger\DynamicMerger;
use App\Domain\Merger\Mergers\ReplaceMerger;
use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class DynamicMergerTest extends TestCase
{
    /**
     * @throws MergerException
     */
    #[DataProvider('dataProvider')]
    public function testMerger(Merger $merger, mixed $a, mixed $b, mixed $expectedOutput): void
    {
        if ($expectedOutput instanceof Exception) {
            $this->expectException(get_class($expectedOutput));
            $this->expectExceptionMessage($expectedOutput->getMessage());
        }

        $output = $merger->merge($a, $b);

        $this->assertSame($expectedOutput, $output);
    }

    public static function dataProvider(): array
    {
        return [
            [
                self::getMerger(),
                3,
                7,
                7,
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['bar' => [14, 16]],
                ['foo' => [13, 14, 15], 'bar' => [14, 16]],
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['foo' => [16]],
                ['foo' => [16]],
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['foo' => ['+append' => [16]]],
                ['foo' => [13, 14, 15, 16]],
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['foo' => ['+prepend' => [16]]],
                ['foo' => [16, 13, 14, 15]],
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['+replace' => ['bar' => [17]]],
                ['bar' => [17]],
            ],
            [
                self::getMerger(),
                [
                    'foo' => [
                        'one' => 1,
                    ],
                ],
                [
                    '+merge-all' => [
                        'foo' => [
                            'two' => 2,
                        ],
                        'bar' => true,
                    ],
                ],
                [
                    'foo' => [
                        'one' => 1,
                        'two' => 2,
                    ],
                    'bar' => true,
                ],
            ],
            [
                self::getMerger(),
                [
                    'foo' => [
                        'one' => 1,
                    ],
                ],
                [
                    '+merge-first-level' => [
                        'foo' => [
                            'two' => 2,
                        ],
                        'bar' => true,
                    ],
                ],
                [
                    'foo' => [
                        'two' => 2,
                    ],
                    'bar' => true,
                ],
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['foo' => ['+merge-unique-values' => [14, 16]]],
                ['foo' => [13, 14, 15, 16]],
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['foo' => ['+merge-unique-values' => 3]],
                new Exception("Dynamic merger: At (root).foo: Expect list array value"),
            ],
            [
                self::getMerger(),
                ['foo' => [13, 14, 15]],
                ['foo' => ['+merge-unique-values' => ['foo' => 3]]],
                new Exception("Dynamic merger: At (root).foo: Expect list array value"),
            ],
        ];
    }

    public static function getMerger(): Merger
    {
        return new Merger([
           new DynamicMerger(),
           new ArrayRecursiveMerger(),
           new ReplaceMerger(),
        ]);
    }
}
