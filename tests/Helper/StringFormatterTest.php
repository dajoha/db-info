<?php

declare(strict_types=1);

namespace App\Tests\Helper;

use App\Helper\StringFormatter;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class StringFormatterTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testFormatString(
        string $input,
        array $expectedStrings,
        array $expectedPlaceholders,
    ) {
        $stringFormatter = new StringFormatter($input);
        $this->assertEquals($expectedStrings, $stringFormatter->strings);
        $this->assertEquals($expectedPlaceholders, $stringFormatter->placeholders);
    }

    public static function dataProvider(): array
    {
        return [
            [
                '', [''], [],
            ], [
                'hi', ['hi'], [],
            ], [
                '{', ['{'], [],
            ], [
                '{foo', ['{foo'], [],
            ], [
                '{}', ['', ''], [''],
            ], [
                '{}{', ['', '{'], [''],
            ], [
                '{}{}', ['', '', ''], ['', ''],
            ], [
                '{foo}', ['', ''], ['foo'],
            ], [
                'hi{foo}', ['hi', ''], ['foo'],
            ], [
                'hi{foo}ho', ['hi', 'ho'], ['foo'],
            ], [
                'hi{foo}ho{bar}', ['hi', 'ho', ''], ['foo', 'bar'],
            ], [
                '{foo}{bar}', ['', '', ''], ['foo', 'bar'],
            ],
        ];
    }
}
