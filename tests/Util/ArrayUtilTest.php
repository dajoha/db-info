<?php

declare(strict_types=1);

namespace App\Tests\Util;

use App\Util\ArrayUtil;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class ArrayUtilTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function testRecursiveMerge(
        array $array1,
        array $array2,
        array $expectedResult,
    ) {
        $this->assertEquals(ArrayUtil::recursiveMerge($array1, $array2), $expectedResult);
    }

    public static function dataProvider(): array
    {
        return [
            [
                [], [], [],
            ], [
                [1], [2], [2],
            ], [
                [ ['foo' => 1] ],
                [ ['bar' => 2] ],
                [ ['bar' => 2] ],
            ], [
                [ null ],
                [ ['bar' => 2] ],
                [ ['bar' => 2] ],
            ], [
                [ ['foo' => 1] ],
                [ null ],
                [ null ],
            ], [
                [ 'foo' => 1 ],
                [ 'bar' => 2 ],
                [ 'foo' => 1, 'bar' => 2 ],
            ], [
                [ 'foo' => 1 ],
                [],
                [ 'foo' => 1 ],
            ], [
                [],
                [ 'foo' => 1 ],
                [ 'foo' => 1 ],
            ], [
                [ 'foo' => 1 ],
                [ 'bar' => 2, 'foo' => 3 ],
                [ 'foo' => 3, 'bar' => 2 ],
            ], [
                [ 'foo' => [ 1, 2 ] ],
                [ 'foo' => [ 8, 2 ] ],
                [ 'foo' => [ 8, 2 ] ],
            ], [
                [
                    'foo' => [ 'one' => 1 ],
                ],
                [
                    'foo' => [ 'two' => 2 ],
                    'three' => 3,
                ],
                [
                    'foo' => [
                        'one' => 1,
                        'two' => 2,
                    ],
                    'three' => 3,
                ],
            ],
        ];
    }
}
