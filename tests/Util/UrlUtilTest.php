<?php

declare(strict_types=1);

namespace App\Tests\Util;

use App\Exception\NotImplementedException;
use App\Router\Util\UrlUtil;
use Doctrine\DBAL\Types;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class UrlUtilTest extends TestCase
{
    /**
     * @throws NotImplementedException
     */
    #[DataProvider('dataProvider')]
    public function testUrlUtil(string $typeClass, mixed $value, string $expectedEncodedValue)
    {
        $type = new $typeClass;

        $encodedValue = UrlUtil::encodeDatabaseValue($value, $type);
        $this->assertSame($expectedEncodedValue, $encodedValue);

        $decodedValue = UrlUtil::decodeDatabaseValue($encodedValue, $type);

        $this->assertEquals($decodedValue, $value);
    }

    #[DataProvider('dataProviderFail')]
    public function testUrlUtilException(string $typeClass)
    {
        $this->expectException(NotImplementedException::class);
        UrlUtil::encodeDatabaseValue('hi', new $typeClass);
    }

    public static function dataProvider(): array
    {
        return [
            [Types\IntegerType::class, 12, '12'],
            [Types\FloatType::class, 12.3, '12.3'],
            [Types\BooleanType::class, true, 'true'],
            [Types\BooleanType::class, false, 'false'],
            [Types\BinaryType::class, 'abc', '616263'],
            [Types\TextType::class, 'abc', 'abc'],
            [Types\DateType::class, '1980-01-01', '1980-01-01'],
            [Types\TimeType::class, '19:04', '19:04'],
            [Types\DateTimeType::class, '1980-01-01 19:04', '1980-01-01 19:04'],
        ];
    }

    public static function dataProviderFail(): array
    {
        return [
            [Types\BlobType::class],
            [Types\DateIntervalType::class],
            [Types\EnumType::class],
            [Types\JsonType::class],
            [Types\SimpleArrayType::class],
        ];
    }
}
