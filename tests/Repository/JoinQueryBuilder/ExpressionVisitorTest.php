<?php

declare(strict_types=1);

namespace App\Tests\Repository\JoinQueryBuilder;

use App\Project\Project;
use App\Repository\JoinQueryBuilder\ExpressionVisitor;
use App\Repository\JoinQueryBuilder\JoinQueryBuilder;
use App\Repository\SearchExpression\Ast\DatabasePathExpr;
use App\Repository\SearchExpression\Ast\NumberExpr;
use App\Repository\SearchExpression\Ast\StringExpr;
use App\Repository\SearchExpression\ExpressionInterface;
use Dajoha\ParserCombinator\Stream\Base\Span\SpanInterface;
use Doctrine\DBAL\Exception as DbalException;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;

class ExpressionVisitorTest extends TestCase
{
    /**
     * @throws Exception
     * @throws DbalException
     * @throws ReflectionException
     */
    #[DataProvider('SimpleLikeOperatorRhsProvider')]
    public function testSimpleLikeOperatorRhs(ExpressionInterface $input, string $expectedOutput)
    {
        $expressionVisitor = $this->createMySqlExpressionVisitor();

        $rc = new ReflectionClass(ExpressionVisitor::class);
        $getRhsForSimpleLikeOperator = $rc->getMethod('getRhsForSimpleLikeOperator');

        $input->setSpan($this->createMock(SpanInterface::class));

        $output = $getRhsForSimpleLikeOperator->invoke($expressionVisitor, $input);

        $this->assertEquals($expectedOutput, $output);
    }

    public static function SimpleLikeOperatorRhsProvider(): array
    {
        return [
            [new StringExpr(''), "'%%'"],
            [new StringExpr('foo'), "'%foo%'"],
            [new StringExpr('foo%bar'), "'%foo\\\\%bar%'"],
            [new StringExpr('foo_bar'), "'%foo\\\\_bar%'"],
            [new StringExpr('foo\\bar'), "'%foo\\\\\\\\bar%'"],
            [new NumberExpr(12), "'%12%'"],
            [new NumberExpr(12.3), "'%12.3%'"],
            [new DatabasePathExpr(['foo', 'bar']), "'%foo.bar%'"],
            [new DatabasePathExpr(['foo_bar', 'qux']), "'%foo\\\\_bar.qux%'"],
        ];
    }

    /**
     * @throws Exception
     * @throws DbalException
     */
    protected function createMySqlExpressionVisitor(): ExpressionVisitor
    {
        $project = $this->createMock(Project::class);
        $project->method('getPlatform')
            ->willReturn(new MySQLPlatform());

        $joinQueryBuilder = $this->createMock(JoinQueryBuilder::class);

        return new ExpressionVisitor($joinQueryBuilder, $project);
    }
}
