<?php

namespace App\Tests\Repository\Sort;

use App\Repository\QueryPart\Sort\OptSortDirection;
use App\Repository\QueryPart\Sort\Sorts;
use PHPUnit\Framework\TestCase;

class SortTest extends TestCase
{
    /**
     * @dataProvider parsePositionDirectionProvider
     */
    public function testParsePositionDirection(string $input, array|null $expectedOutput)
    {
        $parser = Sorts::getPositionDirectionParser();
        $parseResult = $parser->parse($input);

        if ($expectedOutput === null) {
            $this->assertFalse($parseResult->isSuccess());
        } else {
            $this->assertEquals($expectedOutput, $parseResult->output);
        }
    }

    public static function parsePositionDirectionProvider(): array
    {
        return [
            ['', null],
            ['hello', null],
            ['0', [0, OptSortDirection::Asc]],
            ['1', [1, OptSortDirection::Asc]],
            ['1-asc', [1, OptSortDirection::Asc]],
            ['1-asc-', null],
            ['-asc', null],
            ['1-foo', null],
            ['1-desc', [1, OptSortDirection::Desc]],
            ['123-desc', [123, OptSortDirection::Desc]],
        ];
    }
}
