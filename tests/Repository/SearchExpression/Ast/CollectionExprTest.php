<?php

namespace App\Tests\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\BinaryOperationExpr;
use App\Repository\SearchExpression\Ast\CollectionExpr;
use App\Repository\SearchExpression\Ast\DatabasePathExpr;
use App\Repository\SearchExpression\Ast\Model\Operator;
use App\Repository\SearchExpression\Ast\NumberExpr;
use App\Repository\SearchExpression\Ast\StringExpr;
use App\Repository\SearchExpression\Exception\SearchExpressionException;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Tests\Repository\SearchExpression\SearchExprTestHelper;
use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class CollectionExprTest extends TestCase
{
    /**
     * @param ExpressionInterface[]|ExpressionInterface|Exception|null $expectedOutput
     *
     * @throws ReflectionException
     * @throws SearchExpressionException
     */
    #[DataProvider('collectionExprProvider')]
    public function testCollectionExprParser(
        string $input,
        array|ExpressionInterface|Exception|null $expectedOutput,
    ) {
        $parser = CollectionExpr::getParser();

        if ($expectedOutput instanceof Exception) {
            $this->expectException(get_class($expectedOutput));
            $this->expectExceptionMessage($expectedOutput->getMessage());
        }

        $parseResult = $parser->parse($input);

        if ($expectedOutput === null) {
            $this->assertTrue($parseResult->isError());
        } else {
            if (is_array($expectedOutput)) {
                $expectedOutput = new CollectionExpr($expectedOutput);
            }
            SearchExprTestHelper::unsetSpan($parseResult->output);
            $this->assertEquals($expectedOutput, $parseResult->output);
            $this->assertTrue($parseResult->isSuccess());
        }
    }

    /**
     * @throws SearchExpressionException
     */
    public static function collectionExprProvider(): array
    {
        return [
            ['', null],
            ['12', null],
            ['ab', null],
            ['[', null],
            ['[]', null],
            ['[  ]', null],
            ['[ , ]', null],
            ['[ abc ]', [new DatabasePathExpr(['abc'])]],
            ['[12]', [new NumberExpr(12)]],
            ['[  12    ]', [new NumberExpr(12)]],
            ['[  12  ,  ]', [new NumberExpr(12)]],
            ['[12,78]', [new NumberExpr(12), new NumberExpr(78)]],
            ['[12,78,]', [new NumberExpr(12), new NumberExpr(78)]],
            ['[ 12  , 78,  ]', [new NumberExpr(12), new NumberExpr(78)]],
            ['[ 12  , 78.2,  ]', [new NumberExpr(12), new NumberExpr(78.2)]],
            ["[ 12  , 'foo', 'bar', ]", [new NumberExpr(12), new StringExpr('foo'), new StringExpr('bar')]],
            ['[12 78]', [new NumberExpr(12), new NumberExpr(78)]],
            ["[ 12 'foo' 'bar']", [new NumberExpr(12), new StringExpr('foo'), new StringExpr('bar')]],
            ["[ 12 'foo' ,'bar']", [new NumberExpr(12), new StringExpr('foo'), new StringExpr('bar')]],
            ['[ 3 + 5 ]', [new BinaryOperationExpr(Operator::Add, new NumberExpr(3), new NumberExpr(5))]],
            ['[ 3 + 5 7+13 ]', [
                new BinaryOperationExpr(Operator::Add, new NumberExpr(3), new NumberExpr(5)),
                new BinaryOperationExpr(Operator::Add, new NumberExpr(7), new NumberExpr(13)),
            ]],
            ['[ 3 + 5 7+13 - 78, 100 ]', [
                new BinaryOperationExpr(Operator::Add, new NumberExpr(3), new NumberExpr(5)),
                new BinaryOperationExpr(
                    Operator::Sub,
                    new BinaryOperationExpr(Operator::Add, new NumberExpr(7), new NumberExpr(13)),
                    new NumberExpr(78),
                ),
                new NumberExpr(100),
            ]],
            [
                '[ (3 + 5) 7+13 - 78, 100 ]',
                [
                    (new BinaryOperationExpr(Operator::Add, new NumberExpr(3), new NumberExpr(5)))
                        ->withParentheses(),
                    new BinaryOperationExpr(
                        Operator::Sub,
                        new BinaryOperationExpr(Operator::Add, new NumberExpr(7), new NumberExpr(13)),
                        new NumberExpr(78),
                    ),
                    new NumberExpr(100),
                ],
            ],
            ['[ 3 + 5 [1,2] ]', new SearchExpressionException(
                'Collections cannot be nested',
                new CollectionExpr([new NumberExpr(1), new NumberExpr(2)]),
            )],
        ];
    }
}
