<?php

namespace App\Tests\Repository\SearchExpression\Ast;

use App\Repository\SearchExpression\Ast\StringExpr;
use App\Tests\Repository\SearchExpression\SearchExprTestHelper;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class StringExprTest extends TestCase
{
    /**
     * @throws ReflectionException
     */
    #[DataProvider('getStringExprProvider')]
    public function testStringExpr(string $input, string|null $expectedOutput)
    {
        $parser = StringExpr::getParser();

        $parseResult = $parser->parse($input);
        if ($expectedOutput === null) {
            $this->assertTrue($parseResult->isError());
        } else {
            SearchExprTestHelper::unsetSpan($parseResult->output);
            $this->assertEquals(new StringExpr($expectedOutput), $parseResult->output);
            $this->assertTrue($parseResult->isSuccess());
        }
    }

    public static function getStringExprProvider(): array
    {
        return [
            ['', null],
            ['a', null],
            ["'", null],
            ["a'", null],
            ["''", ''],
            ["'a'", 'a'],
            ["'123'", '123'],
            ["'123\\4'", '1234'],
            ["'123\\4 \\'hello\\''", "1234 'hello'"],
        ];
    }
}
