<?php

declare(strict_types=1);

namespace App\Tests\Repository\SearchExpression;

use App\Repository\SearchExpression\Ast\BinaryOperationExpr;
use App\Repository\SearchExpression\Ast\CollectionExpr;
use App\Repository\SearchExpression\Ast\FunctionCallExpr;
use Dajoha\ParserCombinator\Stream\Base\Span\SpanAwareInterface;
use ReflectionClass;
use ReflectionException;

class SearchExprTestHelper
{
    /**
     * @throws ReflectionException
     */
    public static function unsetSpan(SpanAwareInterface $object): void
    {
        $rc = new ReflectionClass($object);
        $property = $rc->getProperty('span');
        $property->setValue($object, null);

        if ($object instanceof CollectionExpr) {
            foreach ($object->values as $value) {
                self::unsetSpan($value);
            }
        } else if ($object instanceof FunctionCallExpr) {
            foreach ($object->arguments as $argument) {
                self::unsetSpan($argument);
            }
        } else if ($object instanceof BinaryOperationExpr) {
            self::unsetSpan($object->operand1);
            self::unsetSpan($object->operand2);
        }
    }
}
