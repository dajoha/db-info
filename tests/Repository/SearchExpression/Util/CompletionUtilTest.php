<?php

declare(strict_types=1);

namespace App\Tests\Repository\SearchExpression\Util;

use App\Repository\SearchExpression\Util\CompletionUtil;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class CompletionUtilTest extends TestCase
{
    #[DataProvider('getInsideStringProvider')]
    public function testInsideString(
        string $input,
        bool $expectedOutput,
        ?int $expectedStringPosition,
    ) {
        $this->assertEquals($expectedOutput, CompletionUtil::insideString($input, $stringPosition));
        $this->assertEquals($expectedStringPosition, $stringPosition);
    }

    public static function getInsideStringProvider(): array
    {
        return [
            ['', false, null],
            ['\\', false, null],
            ['\\', false, null],
            ['foo', false, null],
            ["'", true, 1],
            ["'foo", true, 1],
            ["  '", true, 3],
            ["  'foo", true, 3],
            ["  'foo'", false, null],
            ["  'foo'  bar", false, null],
            ["  'foo'  bar  '", true, 15],
            ["  'fo\\ao'  bar  '", true, 17],
            ["  'fo\\'o'  bar  '", true, 17],
        ];
    }
}
