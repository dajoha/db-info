<?php

declare(strict_types=1);

namespace App\Tests\Repository\SearchExpression;

use App\Repository\SearchExpression\Ast\BinaryOperationExpr;
use App\Repository\SearchExpression\Ast\CollectionExpr;
use App\Repository\SearchExpression\Ast\DatabasePathExpr;
use App\Repository\SearchExpression\Ast\FunctionCallExpr;
use App\Repository\SearchExpression\Ast\LogicalValueExpr;
use App\Repository\SearchExpression\Ast\Model\LogicalValue;
use App\Repository\SearchExpression\Ast\Model\Operator;
use App\Repository\SearchExpression\Ast\NumberExpr;
use App\Repository\SearchExpression\Ast\StringExpr;
use App\Repository\SearchExpression\Exception\SearchExpressionException;
use App\Repository\SearchExpression\ExpressionInterface;
use App\Repository\SearchExpression\SearchExpressionParser;
use Exception;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class SearchExpressionParserTest extends TestCase
{
    /**
     * @throws Exception
     */
    #[DataProvider('searchExpressionParserProvider')]
    public function testSearchExpressionParser(string|array $input, ?ExpressionInterface $expectedOutput)
    {
        $parser = SearchExpressionParser::getParser();

        foreach ((array) $input as $input) {
            $parseResult = $parser->parse($input);
            if ($expectedOutput === null) {
                $this->assertTrue($parseResult->isError());
            } else {
                SearchExprTestHelper::unsetSpan($parseResult->output);
                $this->assertEquals($expectedOutput, $parseResult->output);
                $this->assertTrue($parseResult->isSuccess());
            }
        }
    }

    /**
     * @throws Exception
     */
    #[DataProvider('searchExpressionParserExceptionProvider')]
    public function testSearchExpressionParserException(string|array $input, ?string $exceptionMessage)
    {
        $this->expectException(SearchExpressionException::class);
        if ($exceptionMessage !== null) {
            $this->expectExceptionMessage($exceptionMessage);
        }

        foreach ((array) $input as $input) {
            SearchExpressionParser::parse($input);
        }
    }

    /**
     * @throws SearchExpressionException
     */
    public static function searchExpressionParserProvider(): array
    {
        return [
            ['', null],
            ['1', new NumberExpr(1)],
            ["'hi'", new StringExpr('hi')],
            ["foo", new DatabasePathExpr(['foo'])],
            ["foo.bar", new DatabasePathExpr(['foo', 'bar'])],
            [' 1 ', new NumberExpr(1)],
            [' 1 2 ', null],
            [[' null ', 'NULL', 'nUlL'], new LogicalValueExpr(LogicalValue::Null)],
            [['false', 'False'], new LogicalValueExpr(LogicalValue::False)],
            [['true', 'tRUe'], new LogicalValueExpr(LogicalValue::True)],
            [['unknown', 'UnKnoWn'], new LogicalValueExpr(LogicalValue::Unknown)],
            [
                '1 + 2',
                new BinaryOperationExpr(Operator::Add, new NumberExpr(1), new NumberExpr(2)),
            ], [
                '1 + 2 + 3',
                new BinaryOperationExpr(
                    Operator::Add,
                    new BinaryOperationExpr(Operator::Add, new NumberExpr(1), new NumberExpr(2)),
                    new NumberExpr(3),
                ),
            ], [
                '1 + 2 * 3 + 4 + 5',
                new BinaryOperationExpr(
                    Operator::Add,
                    new BinaryOperationExpr(
                        Operator::Add,
                        new BinaryOperationExpr(
                            Operator::Add,
                            new NumberExpr(1),
                            new BinaryOperationExpr(Operator::Mul, new NumberExpr(2), new NumberExpr(3)),
                        ),
                        new NumberExpr(4),
                    ),
                    new NumberExpr(5),
                ),
            ], [
                '1 + 2 * (3 + 4) + 5',
                new BinaryOperationExpr(
                    Operator::Add,
                    new BinaryOperationExpr(
                        Operator::Add,
                        new NumberExpr(1),
                        new BinaryOperationExpr(
                            Operator::Mul,
                            new NumberExpr(2),
                            (new BinaryOperationExpr(Operator::Add, new NumberExpr(3), New NumberExpr(4)))->withParentheses(),
                        ),
                    ),
                    new NumberExpr(5),
                ),
            ], [
                '1 is null',
                new BinaryOperationExpr(Operator::Is, new NumberExpr(1), new LogicalValueExpr(LogicalValue::Null)),
            ], [
                '1 is  not  null',
                new BinaryOperationExpr(Operator::IsNot, new NumberExpr(1), new LogicalValueExpr(LogicalValue::Null)),
            ], [
                'NULL IS UNKNOWN',
                new BinaryOperationExpr(
                    Operator::Is,
                    new LogicalValueExpr(LogicalValue::Null),
                    new LogicalValueExpr(LogicalValue::Unknown),
                ),
            ], [
                "foo = 'hi' & bar.seven = 'bye' + 7",
                new BinaryOperationExpr(
                    Operator::And,
                    new BinaryOperationExpr(
                        Operator::Eq,
                        new DatabasePathExpr(['foo']),
                        new StringExpr('hi'),
                    ),
                    new BinaryOperationExpr(
                        Operator::Eq,
                        new DatabasePathExpr(['bar', 'seven']),
                        new BinaryOperationExpr(
                            Operator::Add,
                            new StringExpr('bye'),
                            new NumberExpr(7),
                        ),
                    ),
                ),
            ], [
                "foo='hi'&bar.seven='bye'+7",
                new BinaryOperationExpr(
                    Operator::And,
                    new BinaryOperationExpr(
                        Operator::Eq,
                        new DatabasePathExpr(['foo']),
                        new StringExpr('hi'),
                    ),
                    new BinaryOperationExpr(
                        Operator::Eq,
                        new DatabasePathExpr(['bar', 'seven']),
                        new BinaryOperationExpr(
                            Operator::Add,
                            new StringExpr('bye'),
                            new NumberExpr(7),
                        ),
                    ),
                ),
            ], [
                "one | two = 2 & three",
                new BinaryOperationExpr(
                    Operator::Or,
                    new DatabasePathExpr(['one']),
                    new BinaryOperationExpr(
                        Operator::And,
                        new BinaryOperationExpr(
                            Operator::Eq,
                            new DatabasePathExpr(['two']),
                            new NumberExpr(2),
                        ),
                        new DatabasePathExpr(['three']),
                    ),
                ),
            ], [
                ['1 | 2', '1 || 2', '1 or 2', '1 OR 2', '1 oR 2'],
                new BinaryOperationExpr(Operator::Or, new NumberExpr(1), new NumberExpr(2)),
            ], [
                ['abc.def IN [1, 2]'],
                new BinaryOperationExpr(
                    Operator::In,
                    new DatabasePathExpr(['abc', 'def']),
                    new CollectionExpr([new NumberExpr(1), new NumberExpr(2)]),
                ),
            ], [
                ["abc LIKE '%b%'", "abc Like'%b%'"],
                new BinaryOperationExpr(
                    Operator::Like,
                    new DatabasePathExpr(['abc']),
                    new StringExpr('%b%'),
                ),
            ], [
                ["abc ILIKE '%b%'", "abc iLike'%b%'"],
                new BinaryOperationExpr(
                    Operator::ILike,
                    new DatabasePathExpr(['abc']),
                    new StringExpr('%b%'),
                ),
            ], [
                "MY_FUNC(1, 'abc')",
                new FunctionCallExpr(
                    'MY_FUNC',
                    [
                        new NumberExpr(1),
                        new StringExpr('abc'),
                    ],
                )
            ],
        ];
    }

    public static function searchExpressionParserExceptionProvider(): array
    {
        return [
            [
                "abc like 32",
                "At line 1, column 10: Operator \"LIKE\": right operand must be a string",
            ], [
                "abc ilike 32",
                "At line 1, column 11: Operator \"ILIKE\": right operand must be a string",
            ], [
                "abc + [1,2,3]",
                "At line 1, column 7: List is not allowed at this place",
            ],
        ];
    }
}
